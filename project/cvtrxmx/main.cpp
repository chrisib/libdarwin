//--------------------------------------------------
// PROJECT: RX and MX converter
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//--------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "darwin/framework/MX28.h"
#include "darwin/framework/RX28.h"
#include "darwin/framework/AX12.h"
using namespace Robot;
using namespace std;

//------------------------------------------------------------------------------
// FUNCTIONS AND METHODS
//------------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
    int value;

    cout << "Enter a value to see converted value" << endl;
    cout << "(use -1 to exit)" << endl;
    do
    {
		cout << "?> ";
		cin >> value;
		
		if(value != -1)
		{
			cout << "MX --> RX :: " << value << " --> " << RX28::Virtual2RealValue(value) << endl;
			cout << "RX --> MX :: " << value << " --> " << RX28::Real2VirtualValue(value) << endl;
			cout << "MX --> AX :: " << value << " --> " << AX12::Virtual2RealValue(value) << endl;
			cout << "AX --> MX :: " << value << " --> " << AX12::Real2VirtualValue(value) << endl;
		}
    } while(value != -1);
    return EXIT_SUCCESS;
}

