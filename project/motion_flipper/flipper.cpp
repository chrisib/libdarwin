/*
 * main.cpp
 *
 * JDJ
 * Reverse a motion (or series of motions) from left to right in a motion.bin file
 * Eg. Flipping a motion that spans pages 230-239:
 * ./flipper --from 230 --to 239 --motors 20 ~/robocup/player/motion.bin
 * OR making use of defaults:
 * ./flipper --from 230 --to 239
 */

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <cstring>
#include <iostream>
#include <sstream>
#include "LinuxDARwIn.h"
#include "Action.h"
#include "AX12.h"
#include "RX28.h"
#include "MX28.h"

#define DEFAULT_MOTION_FILE "/home/darwin/robocup/player/motion.bin"
#define DEFAULT_NUM_MOTORS 20
#define HEAD_PAN_MOTOR 19
#define HEAD_TILT_MOTOR 20

using namespace std;
using namespace Robot;

int from = -1;
int to = -1;
int numMotors = DEFAULT_NUM_MOTORS;
char *motionFile = DEFAULT_MOTION_FILE;

int reversePosition(int posn) {
	int newPosn;

	if (posn > 2048)
		newPosn = 2048 - (posn - 2048);
	else
		newPosn = 2048 + (2048 - posn);

	return newPosn;
}

int main(int argc, char** argv)
{
    // parse command-line argument
    for(int i=1; i<argc; i++)
    {
		if(!strcmp("--from",argv[i]))
		{
			i++;
			stringstream convert(argv[i]);
			if (!(convert >> from)) {
			    cout << "Invalid input: " << argv[i] << endl;
			    exit(-1);
			}
		}
		else if(!strcmp("--to",argv[i]))
		{
			i++;
			stringstream convert(argv[i]);
			if (!(convert >> to)) {
				cout << "Invalid input: " << argv[i] << endl;
				exit(-1);
			}
		}
		else if(!strcmp("--motors",argv[i]))
		{
			i++;
			stringstream convert(argv[i]);
			if (!(convert >> numMotors)) {
				cout << "Invalid input: " << argv[i] << endl;
				exit(-1);
			}
		}
		else if(!strcmp("--help",argv[i]))
		{
			cout << endl <<
			"Usage" << endl <<
			"\t" << argv[0] << " [--from START_LINE_NUM] [--to END_LINE_NUM] [--motors NUMBER_OF_MOTORS] [--help] [MOTION_FILE]" << endl;
			exit(0);
		}
		else
		{
			motionFile = argv[i];
		}
	}

	if(from<0 || to<0 || from>=Action::MAXNUM_PAGE || to>=Action::MAXNUM_PAGE || from > to) {
		cout << "Invalid arguments, program closing" << endl;
		exit(-1);
	} else
		cout << "Flipping " << numMotors << " motors in " << motionFile << " from " << from << " to " << to << endl;

	//begin doing useful stuff
	Action::GetInstance()->LoadFile(motionFile);

	int j,k,p;
	int position1, position2, newPosition1, newPosition2;
	Action::PAGE Page;
	for(k=from;k<=to;k++)
	{
		Action::GetInstance()->LoadPage(k, &Page);
		for(j=0;j<Action::MAXNUM_STEP;j++)
		{
			for(p=1;p<=numMotors;p+=2)
			{
				position1 = Page.step[j].position[p];
				position2 = Page.step[j].position[p+1];
				if(p!=HEAD_PAN_MOTOR && p!=HEAD_TILT_MOTOR && position1!=16896 && position2!=16896) {
					newPosition1 = reversePosition(position2);
					newPosition2 = reversePosition(position1);

					Page.step[j].position[p] = newPosition1;
					Page.step[j].position[p+1] = newPosition2;

					cout << position1 << " --> " << newPosition1 << endl;
					cout << position2 << " --> " << newPosition2 << endl;
				}
			}
		}
		Action::GetInstance()->SavePage(k, &Page);
	}

	cout << "Finished successfully" << endl;
	exit(0);
}
