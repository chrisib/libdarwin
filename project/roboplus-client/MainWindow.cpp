#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <iostream>
#include <QMessageBox>
#include <QHostAddress>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/LeftLeg.h>
#include <darwin/framework/RightLeg.h>
#include <QProcess>

#define PAGE2TIME(x) (double)((x)/TIME_SCALE)
#define TIME2PAGE(x) (int)((x)*TIME_SCALE)
#define PAGE2SPEED(x) (double)((x)/SPEED_SCALE)
#define SPEED2PAGE(x) (int)((x)*SPEED_SCALE)
#define PAGE2SLOPE(x) (double)((x)/SLOPE_SCALE)
#define SLOPE2PAGE(x) (int)((x)*SLOPE_SCALE)

using namespace std;
using namespace Robot;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    suppressAngleChangeEvents = true;
    suppressPageChangeEvents = true;

    ui->setupUi(this);

    ui->tablePages->setColumnCount(3);
    ui->tablePages->setHorizontalHeaderLabels(QStringList() << "Page Name" << "Next" << "Exit");
    ui->tablePages->setRowCount(MAX_NUM_PAGES);
    ui->tablePages->setColumnWidth(0,150);
    ui->tablePages->setColumnWidth(1,36);
    ui->tablePages->setColumnWidth(2,36);

    for(int i=0; i<MAX_NUM_PAGES; i++)
    {
        ui->tablePages->setItem(i,COLUMN_PAGE_NAME,new QTableWidgetItem(""));
        ui->tablePages->setItem(i,COLUMN_PAGE_NEXT,new QTableWidgetItem(QString::number(0)));
        ui->tablePages->setItem(i,COLUMN_PAGE_EXIT,new QTableWidgetItem(QString::number(0)));
    }

    ui->tableSteps->setColumnCount(2);
    ui->tableSteps->setHorizontalHeaderLabels(QStringList() << "Pause" << "Time");
    ui->tableSteps->setColumnWidth(0,85);
    ui->tableSteps->setColumnWidth(1,85);

    ui->tablePagePose->setColumnCount(2);
    ui->tablePagePose->setHorizontalHeaderLabels(QStringList() << "Joint" << "Angle");
    ui->tablePagePose->setRowCount(JointData::NUMBER_OF_JOINTS-1);
    ui->tablePagePose->setColumnWidth(0,110);
    ui->tablePagePose->setColumnWidth(1,65);

    ui->tableRobotPose->setColumnCount(2);
    ui->tableRobotPose->setHorizontalHeaderLabels(QStringList() << "Joint" << "Angle");
    ui->tableRobotPose->setRowCount(JointData::NUMBER_OF_JOINTS-1);
    ui->tableRobotPose->setColumnWidth(0,110);
    ui->tableRobotPose->setColumnWidth(1,65);

    ui->tableJointSoftness->setRowCount(JointData::NUMBER_OF_JOINTS-1);
    ui->tableJointSoftness->setColumnCount(1);
    ui->tableJointSoftness->setHorizontalHeaderLabels(QStringList() << "Lvl[1-7]");

    for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(JointID2QString(i));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        ui->tablePagePose->setItem(i-1,0,item);
        item = new QTableWidgetItem(JointID2QString(i));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        ui->tableRobotPose->setItem(i-1,0,item);

        ui->tablePagePose->setItem(i-1,1,new QTableWidgetItem(QString::number(0.0)));
        ui->tableRobotPose->setItem(i-1,1,new QTableWidgetItem(QString::number(0.0)));

        ui->tableJointSoftness->setItem(i-1,0,new QTableWidgetItem(QString::number(5)));

        //jointIDs << ("ID[" + QString::number(i) + "]");
    }

    connect(&socket,SIGNAL(readyRead()),this,SLOT(onReadyRead()));
    connect(&socket,SIGNAL(connected()),this,SLOT(onSocketConnected()));
    connect(&socket,SIGNAL(disconnected()),this,SLOT(onSocketDisconnected()));
    connect(&positionPingTimer,SIGNAL(timeout()),this,SLOT(onPositionTimerInterrupt()));
    connect(&renderProcess,SIGNAL(finished(int)),this,SLOT(onPyRenderComplete(int)));

    this->setFixedSize(this->size());

    for(int i=0; i<MAX_NUM_PAGES; i++)
        pageLoaded[i] = false;

    selectedPageID = 0;
    selectedPageStep = -1;
    sendPositionChangeToRobot = true;
    pageCutFromIndex = -1;
    pageClipboard = NULL;

    suppressAngleChangeEvents = false;
    suppressPageChangeEvents = false;
    currentViewMode = VIEW_DEFAULT;
}

MainWindow::~MainWindow()
{
    if(renderProcess.isOpen())
    {
        renderProcess.kill();
    }
    delete ui;
}

void MainWindow::onConnectClicked()
{
    const int PORT = 6501;

    socket.connectToHost(QHostAddress(ui->txtNetworkAddress->currentText()),PORT);

    if(socket.waitForConnected(3000))
    {
        //onSocketConnected();

        ui->statusBar->setStatusTip("Connected to " + ui->txtNetworkAddress->currentText());
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Connection Failed");
        msgBox.setText("Failed to connect to Robo+ Server");
        msgBox.setInformativeText("Please ensure the server is running on the selected host");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        ui->statusBar->setStatusTip("Failed to connect to " + ui->txtNetworkAddress->currentText());
    }
}

void MainWindow::onDisconnectClicked()
{
    //onSocketDisconnected();

    socket.close();

    ui->statusBar->setStatusTip("Disconnected from " + ui->txtNetworkAddress->currentText());
}

void MainWindow::onSocketConnected()
{
    ui->txtNetworkAddress->setEnabled(false);
    ui->btnConnect->setEnabled(false);
    ui->btnDisconnect->setEnabled(true);

    ui->statusBar->setStatusTip("Connected to " + ui->txtNetworkAddress->currentText());
    ui->txtNetworkAddress->setEnabled(false);
    ui->btnConnect->setEnabled(false);
    ui->btnDisconnect->setEnabled(true);

    ui->tableJointSoftness->setEnabled(true);
    ui->tablePagePose->setEnabled(true);
    ui->tablePages->setEnabled(true);
    ui->tableRobotPose->setEnabled(true);
    ui->tableSteps->setEnabled(true);
    ui->btnAddStep->setEnabled(true);
    ui->btnCopyToMotion->setEnabled(true);
    ui->btnCopyToRobot->setEnabled(true);
    ui->btnInsertStep->setEnabled(true);
    ui->btnMoveDown->setEnabled(true);
    ui->btnMoveUp->setEnabled(true);
    ui->btnPause->setEnabled(true);
    ui->btnPlay->setEnabled(true);
    ui->btnReload->setEnabled(true);
    ui->btnRemoveStep->setEnabled(true);
    ui->btnSave->setEnabled(true);
    ui->btnStop->setEnabled(true);
    ui->numAngle->setEnabled(true);
    ui->numInertialForce->setEnabled(true);
    ui->numRepeat->setEnabled(true);
    ui->numSpeedRate->setEnabled(true);

    // read the version and get the initial page listing from the server
    sentCommands.clear();
    sendCommand(CMD_VERSION,false);
    sendCommand(CMD_LIST,true);

    //positionPingTimer.setSingleShot(false);
    //positionPingTimer.start(1000);
}

void MainWindow::onSocketDisconnected()
{
    ui->statusBar->setStatusTip("Disconnected from " + ui->txtNetworkAddress->currentText());

    ui->tableJointSoftness->setEnabled(false);
    ui->tablePagePose->setEnabled(false);
    ui->tablePages->setEnabled(false);
    ui->tableRobotPose->setEnabled(false);
    ui->tableSteps->setEnabled(false);
    ui->btnAddStep->setEnabled(false);
    ui->btnCopyToMotion->setEnabled(false);
    ui->btnCopyToRobot->setEnabled(false);
    ui->btnInsertStep->setEnabled(false);
    ui->btnMoveDown->setEnabled(false);
    ui->btnMoveUp->setEnabled(false);
    ui->btnPause->setEnabled(false);
    ui->btnPlay->setEnabled(false);
    ui->btnReload->setEnabled(false);
    ui->btnRemoveStep->setEnabled(false);
    ui->btnSave->setEnabled(false);
    ui->btnStop->setEnabled(false);
    ui->numAngle->setEnabled(false);
    ui->numInertialForce->setEnabled(false);
    ui->numRepeat->setEnabled(false);
    ui->numSpeedRate->setEnabled(false);

    ui->btnCopyPage->setEnabled(false);
    ui->btnCutPage->setEnabled(false);
    ui->btnDeletePage->setEnabled(false);
    ui->btnPastePage->setEnabled(false);

    ui->btnDisconnect->setEnabled(false);
    ui->btnConnect->setEnabled(true);
    ui->txtNetworkAddress->setEnabled(true);

    positionPingTimer.stop();
}

void MainWindow::onReadyRead()
{
    while(socket.canReadLine())
    {
        QByteArray data = socket.readLine();

        for(int i=0; i<data.size(); i++)
        {
            //cout << endl << data.at(i);

            if(data.at(i) != '\n')
                toParse.append(data.at(i));

            if(data.at(i) == '}')
            {
#ifdef DEBUG
                cout << "Parsing data:" << endl;
                for(int i=0; i<toParse.size(); i++)
                    cout << toParse.at(i);
                cout << endl;
#endif
                // deal with the command
                if(sentCommands.size() > 0)
                {
                    if(sentCommands.front() == CMD_LIST)
                    {
                        parseList(toParse);
                    }
                    else if(sentCommands.front() == CMD_VERSION)
                    {
                        int version = parseVersion(toParse);

                        if(version < 2)
                        {
                            QMessageBox msgBox;
                            msgBox.setWindowTitle("Invalid Server Version");
                            msgBox.setText("Invalid RoboPlus Server Version; requires version 2+");
                            msgBox.setStandardButtons(QMessageBox::Ok);
                            msgBox.setIcon(QMessageBox::Warning);

                            msgBox.exec();
                        }
                    }
                    else if(sentCommands.front() == CMD_GET)
                    {
                        parseGet(toParse);
                    }
                    else if(sentCommands.front() == CMD_PAGEINFO)
                    {
                        parsePageInfo(toParse);
                    }
                    else if(sentCommands.front() == CMD_INFO)
                    {
                        finishedPlaying = parseInfo(toParse);
                    }

                    sentCommands.pop_front();
                    toParse.clear();
                }
            }
        }
    }
}

void MainWindow::onPositionTimerInterrupt()
{
    sendCommand(CMD_GET,true);
}

void MainWindow::sendCommand(QString command, bool waitForReply)
{
    QStringList payload;
    sendCommand(command,payload,waitForReply);
}

void MainWindow::sendCommand(QString command, QStringList &payload, bool waitForReply)
{
#ifdef DEBUG
    cout << "Sending command " << command.toStdString() << endl;
#endif
    if(!socket.isOpen())
        return;

    sentCommands.push_back(command);

    socket.write(command.toStdString().c_str());

    if(payload.size() > 0)
    {
        socket.write(" ");
        for(int i=0; i<payload.size(); i++)
        {
            socket.write(payload.at(i).toStdString().c_str());
        }
    }
    socket.write("\n");
    socket.flush();

    if(waitForReply)
        usleep(1000);
}

int MainWindow::parseVersion(QByteArray &data)
{
    char buffer[data.size()];
    for(int i=0; i<data.size(); i++)
        buffer[i] = data.at(i);

    return atoi(buffer+1);
}

bool MainWindow::parseInfo(QByteArray &data)
{
    char d[data.size()];
    for(int i=0; i<data.size(); i++)
        d[i] = data[i];

    char *token = strtok(d+1,":}");

    if(!strcmp(token,"END"))
        return true;
    else
    {
        int page = atoi(token);
        token = strtok(NULL,"}");
        int step = atoi(token);

        ui->tablePages->selectRow(page-1);
        ui->tableSteps->selectRow(step);

        cout << page << " " << step << endl;

        return false;
    }
}

void MainWindow::parseList(QByteArray &byteData)
{
    // ignore the data if it's not the minimum allowable length
    if(byteData.size() < (JointData::NUMBER_OF_JOINTS-1) * 17 )
        return;

    // clear all loaded page data
    for(int i=0; i<MAX_NUM_PAGES; i++)
        pageLoaded[i] = false;

    int ignoreFirst = 0; // characters at the beginning of the data that we just don't care about (up to and including the first { )

    for(int i=0; i<byteData.size() && byteData.at(i)!='{'; i++)
        ignoreFirst++;
    ignoreFirst++;

    // copy the data that we actually care about into a temporary buffer
    char data[byteData.size()];
    for(int i=0; i<byteData.size()-ignoreFirst; i++)
    {
        data[i] = byteData.at(i+ignoreFirst);
    }
    //cout << endl;

    int id;
    char name[14];
    int next, exit, numSteps;
    char *token;
    char *pageData = data;
    for(int page=0; page<=255; page++)   // 256 pages, but we ignore the 0 page
    {
        // page data is [id:name:next:exit:steps]
        // pageStart is our opening [ character, so increment it
        pageData++;
        strtok(pageData,"]");
        //cout << "New page: " << pageData << endl;

        // check to see if the name field is empty before going any further
        bool hasName = true;
        if(strstr(pageData,"::")!=NULL)
        {
            //cout << "No name" << endl;
            hasName = false;
        }



        token = strtok(pageData,":");
        //cout << "id " << token << endl;
        id = atoi(token);

        // grab the name if necessary
        if(hasName)
        {
            token = strtok(NULL,":");
            for(int i=0; i<13; i++)
            {
                name[i] = token[i];
                name[i+1] = 0;
            }
        }
        else
            name[0] = 0;
        //cout << "name " << name << endl;

        // the next page to play
        token = strtok(NULL,":");
        //cout << "next " << token << endl;
        next = atoi(token);

        // the exit motion
        token = strtok(NULL,":");
        //cout << "exit " << token << endl;
        exit = atoi(token);

        // the number of steps in the motion
        token = strtok(NULL,"]");
        //cout << "steps " << token << endl;
        numSteps = atoi(token);

        while(*pageData != '[')
            pageData++;

        strcpy((char*)motionPages[id].header.name,name);
        motionPages[id].header.next = next;
        motionPages[id].header.exit = exit;
        motionPages[id].header.stepnum = numSteps;

        if(id>0)
        {
            ui->tablePages->item(id-1,0)->setText(name);
            ui->tablePages->item(id-1,1)->setText(QString::number(next));
            ui->tablePages->item(id-1,2)->setText(QString::number(exit));
        }
    }

    //ui->tablePages->selectRow(selectedPageID-1);
    //populatePageUI();
}

void MainWindow::parseGet(QByteArray &byteData)
{
    JointData jointData;

    // ignore the packet if it's not long enough
    if(byteData.size() < (JointData::NUMBER_OF_JOINTS-1) * 3)
        return;

    char data[byteData.size()];
    for(int i=0; i<byteData.size(); i++)
        data[i] = byteData.at(i);

    // find the opening bracket of the first motor
    char *motorData = data;
    while(*motorData != '[')
        motorData++;

    sendPositionChangeToRobot = false;

    char *token;
    for(int i=0; i<JointData::NUMBER_OF_JOINTS; i++)
    {
        // motorData points to the opening bracket
        // move it ahead, grab & parse the token
        motorData++;
        token = strtok(motorData,"]");

        //cout << i << ": >" << token << "<" << endl;

        if(i==0)
        {
            // do nothing
        }
        else if(!strcmp(token,"----"))
        {
            // no position to read, or failed serial I/O
            //cout << i << ":---- ";
        }
        else if(!strcmp(token,"????"))
        {
            // torque turned off
            //cout << i << ":???? ";
        }
        else
        {
            int position = atoi(token);
            jointData.SetValue(i,position);
            ui->tableRobotPose->item(i-1,1)->setText(QString::number(jointData.GetBodyAngle(i)));
        }

        motorData += strlen(token)+1;
    }
    //cout << endl;

    sendPositionChangeToRobot = true;
}

void MainWindow::parsePageInfo(QByteArray &data)
{
    if(data.size() < JointData::NUMBER_OF_JOINTS * 2) // somewhat arbitrary to ensure that the packet isn't horribly malformed
        return;

    // make a local copy of the data
    char d[data.size()];
    for(int i=0; i<data.size(); i++)
        d[i] = data.at(i);

    Action::PAGE *page = (motionPages+selectedPageID);

    char *token;

    token = strtok(d+1,":");
    page->header.accel = atoi(token);

    token = strtok(NULL,":");
    page->header.repeat = atoi(token);

    token = strtok(NULL,":");
    page->header.schedule = atof(token);

    token = strtok(NULL,":");
    page->header.speed = atoi(token);

    char *position = token;
    while(*position!='[')
        position++;

    for(int i=0; i<MAX_NUM_STEPS; i++)
    {
        position++;

        token = strtok(position,":");
        page->step[i].time = atoi(token);

        token = strtok(NULL,":");
        page->step[i].pause = atoi(token);

        for(int j=0; j<JointData::NUMBER_OF_JOINTS; j++)
        {
            token = strtok(NULL,":");
            page->step[i].position[j] = atoi(token);
        }

        // move to the next step's position
        position = token;
        while(*position!='[')
            position++;
    }

    position++;
    token = strtok(position,":");
    page->header.slope[0] = atoi(token);

    for(int i=1; i<32; i++)
    {
        token = strtok(NULL,":]");
        page->header.slope[i] = atoi(token);
    }

    pageLoaded[selectedPageID] = true;
    populatePageUI();
}

void MainWindow::onPageSelected()
{
    if(ui->tablePages->selectedItems().size() == 0)
    {
        selectedPageID = 0;
    }
    else
    {
        int row = ui->tablePages->selectedItems().at(0)->row()+1;

        selectedPageID = row;
        ui->tableRobotPose->clearSelection();

        if(!pageLoaded[row])
        {
            QStringList params;
            params << QString::number(row);

            sendCommand(CMD_PAGEINFO, params,true);
        }
        else
        {
            populatePageUI();
        }
    }
}

void MainWindow::onStepSelectionChanged()
{
    if(ui->tableSteps->selectedItems().size() == 0)
    {
        selectedPageStep = -1;
        return;
    }
    else
    {
        selectedPageStep = ui->tableSteps->selectedItems().at(0)->row();

        JointData jointData;

        for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
        {
            jointData.SetValue(i,(int)(motionPages[selectedPageID].step[selectedPageStep].position[i]));
            ui->tablePagePose->item(i-1,1)->setText(QString::number(jointData.GetBodyAngle(i)));
        }

        drawRobotModel(selectedPageID, selectedPageStep, currentViewMode);
    }
}

void MainWindow::populatePageUI()
{
    suppressPageChangeEvents = true;

    Action::PAGE *selectedPage = motionPages + selectedPageID;

    while(ui->tableSteps->rowCount() > 0)
        ui->tableSteps->removeRow(0);

    ui->tableSteps->setRowCount(selectedPage->header.stepnum);

    double playTime = 0;
    for(int i=0; i<selectedPage->header.stepnum; i++)
    {
        ui->tableSteps->setItem(i,COLUMN_PAUSE,new QTableWidgetItem(QString::number(PAGE2TIME(selectedPage->step[i].pause))));
        ui->tableSteps->setItem(i,COLUMN_TIME,new QTableWidgetItem(QString::number(PAGE2TIME(selectedPage->step[i].time))));

        playTime += selectedPage->step[i].time;
    }

    ui->numRepeat->setValue(selectedPage->header.repeat);
    ui->numSpeedRate->setValue(PAGE2SPEED(selectedPage->header.speed));
    ui->numInertialForce->setValue(selectedPage->header.accel);
    for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
    {
        ui->tableJointSoftness->item(i-1,0)->setText(QString::number(PAGE2SLOPE(selectedPage->header.slope[i])));
    }

    playTime /= TIME_SCALE;

    ui->txtPlayTime->setText(QString::number(playTime / ui->numSpeedRate->value() * ui->numRepeat->value()) + "sec = (" +
                             QString::number(playTime) + "/" + QString::number(ui->numSpeedRate->value()) + ") x" + QString::number(ui->numRepeat->value()));


    ui->tableSteps->selectRow(0);

    ui->tablePages->item(selectedPageID-1,COLUMN_PAGE_NAME)->setText(QString((const char*)selectedPage->header.name));
    ui->tablePages->item(selectedPageID-1,COLUMN_PAGE_NEXT)->setText(QString::number(selectedPage->header.next));
    ui->tablePages->item(selectedPageID-1,COLUMN_PAGE_EXIT)->setText(QString::number(selectedPage->header.exit));

    ui->btnCopyPage->setEnabled(true);
    ui->btnDeletePage->setEnabled(true);
    ui->btnCutPage->setEnabled(true);

    suppressPageChangeEvents = false;
}

void MainWindow::onPageItemChanged(QTableWidgetItem* item)
{
    if(suppressPageChangeEvents)
        return;

    switch(item->column())
    {
    case COLUMN_PAGE_NAME:
        motionPages[item->row()+1].header.name[0] = '\0';
        for(int i=0; i<MAX_LEN_NAME && i<item->text().size(); i++)
        {
            motionPages[item->row()+1].header.name[i] = item->text().at(i).toLatin1();
            motionPages[item->row()+1].header.name[i+1] = '\0';
        }
        break;

    case COLUMN_PAGE_NEXT:
        motionPages[item->row()+1].header.next = item->text().toInt();
        break;

    case COLUMN_PAGE_EXIT:
        motionPages[item->row()+1].header.exit = item->text().toInt();
        break;

    default:
        break;
    }
}

void MainWindow::onStepItemChanged(QTableWidgetItem *item)
{
    if(suppressPageChangeEvents)
        return;

    switch(item->column())
    {
    case COLUMN_PAUSE:
        motionPages[selectedPageID].step[item->row()].pause = TIME2PAGE(item->text().toInt());
        break;

    case COLUMN_TIME:
        motionPages[selectedPageID].step[item->row()].time = TIME2PAGE(item->text().toInt());
        break;

    default:
        break;
    }
}

void MainWindow::onSaveClicked()
{
    QStringList packetPayload;

    // transmit and save each page
    for(int i=0; i<MAX_NUM_PAGES; i++)
    {
        if(pageLoaded[i]) // only save pages we've actually loaded locally (since we couldn't make changes if they're not loaded)
        {

            packetPayload.clear();
            packetPayload << QString::number(i) << " {";

            packetPayload << QString((const char*)motionPages[i].header.name) << ":" <<
                             QString::number(motionPages[i].header.next) << ":" <<
                             QString::number(motionPages[i].header.exit) << ":" <<
                             QString::number(motionPages[i].header.stepnum) << ":" <<
                             QString::number(motionPages[i].header.accel) << ":" <<
                             QString::number(motionPages[i].header.repeat) << ":" <<
                             QString::number(motionPages[i].header.schedule) << ":" <<
                             QString::number(motionPages[i].header.speed) << ":";

            for(int j=0; j<motionPages[i].header.stepnum; j++)
            {
                packetPayload << "[" <<
                                 QString::number(motionPages[i].step[j].time) << ":" <<
                                 QString::number(motionPages[i].step[j].pause) << ":";

                for(int k=0; k<32; k++)
                {
                    packetPayload << QString::number(motionPages[i].step[j].position[k]);

                    if(k < 31)
                        packetPayload << ":";
                }

                packetPayload << "]";
            }

            packetPayload << ":[";
            for(int j=0; j<32; j++)
            {
                packetPayload << QString::number(motionPages[i].header.slope[j]);

                if(j < 31)
                    packetPayload << ":";
            }
            packetPayload << "]";

            packetPayload << "}";

            cout << "Packet Payload: " << endl;
            for(int i=0; i<packetPayload.size(); i++)
                cout << packetPayload.at(i).toStdString();
            cout << endl;

            // transmit the data
            sendCommand(CMD_SAVE,packetPayload,true);
        }
    }
}

void MainWindow::onReloadClicked()
{
    if(!socket.isOpen())
        return;

    sendCommand(CMD_LIST,true);
}

void MainWindow::onPlayClicked()
{
    if(!socket.isOpen())
        return;

    finishedPlaying = false;
    sendCommand(CMD_PLAY,QStringList() << QString::number(selectedPageID),true);

    //while(!finishedPlaying)
    //{
    //    sendCommand(CMD_INFO,true);
    //    sendCommand(CMD_GET,true);
    //}
}

void MainWindow::onPauseClicked()
{
    if(!socket.isOpen())
        return;

    sendCommand(CMD_PAUSE,false);
    sendCommand(CMD_GET,true);
}

void MainWindow::onStopClicked()
{
    if(!socket.isOpen())
        return;

    sendCommand(CMD_STOP, false);
    sendCommand(CMD_GET,true);
}

void MainWindow::onAddStepClicked()
{
    if(motionPages[selectedPageID].header.stepnum == MAX_NUM_STEPS)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Cannot Add Step");
        msgBox.setText("Motion pages are limited to 7 steps");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
    else
    {
        motionPages[selectedPageID].header.stepnum++;

        if(motionPages[selectedPageID].header.stepnum > 1)
        {
            memcpy(&(motionPages[selectedPageID].step[motionPages[selectedPageID].header.stepnum-1]),
                   &(motionPages[selectedPageID].step[motionPages[selectedPageID].header.stepnum-2]),
                   sizeof(Action::STEP));
        }
        else
        {
            for(int i=0; i<32; i++)
                motionPages[selectedPageID].step[0].position[i] = 2048;
            motionPages[selectedPageID].step[0].time = 0;
            motionPages[selectedPageID].step[0].pause = 0;
        }
    }

    populatePageUI();
    ui->tableSteps->selectRow(motionPages[selectedPageID].header.stepnum-1);
}

void MainWindow::onInsertStepClicked()
{
    if(ui->tableSteps->selectedItems().size() == 0)
        return;

    int selectedStep = ui->tableSteps->selectedItems().at(0)->row();

    if(motionPages[selectedPageID].header.stepnum == MAX_NUM_STEPS)
    {
        QMessageBox msgBox;
        msgBox.setText("Cannot Add Step");
        msgBox.setInformativeText("Motion pages are limited to 7 steps");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
    else
    {
        motionPages[selectedPageID].header.stepnum++;

        // move all the steps from the selected one +1 to the end ahead one space
        for(int i=MAX_NUM_STEPS-2; i>=selectedStep; i--)
        {
            memcpy(&(motionPages[selectedPageID].step[i+1]),
                   &(motionPages[selectedPageID].step[i]),
                   sizeof(Action::STEP));
        }
        populatePageUI();
        ui->tableSteps->selectRow(selectedStep+1);
    }
}

void MainWindow::onDeleteStepClicked()
{
    if(ui->tableSteps->selectedItems().size() == 0)
        return;

    int selectedStep = ui->tableSteps->selectedItems().at(0)->row();
    motionPages[selectedPageID].header.stepnum--;

    // move all the steps from the selected one +1 to the end back one space
    for(int i=selectedStep; i<MAX_NUM_STEPS-1; i++)
    {
        memcpy(&(motionPages[selectedPageID].step[i]),
               &(motionPages[selectedPageID].step[i+1]),
               sizeof(Action::STEP));
    }

    populatePageUI();
}

void MainWindow::onMoveStepUpClicked()
{
    if(ui->tableSteps->selectedItems().size() == 0)
        return;

    int selectedStep = ui->tableSteps->selectedItems().at(0)->row();

    // swap the selected step with the one above it
    if(selectedStep > 0)
    {
        Action::STEP tmp;
        memcpy(&tmp,&(motionPages[selectedPageID].step[selectedStep]),sizeof(Action::STEP));
        memcpy(&(motionPages[selectedPageID].step[selectedStep]),&(motionPages[selectedPageID].step[selectedStep-1]),sizeof(Action::STEP));
        memcpy(&(motionPages[selectedPageID].step[selectedStep-1]),&tmp,sizeof(Action::STEP));

        populatePageUI();
        ui->tableSteps->selectRow(selectedStep-1);
    }
}

void MainWindow::onMoveStepDownClicked()
{
    if(ui->tableSteps->selectedItems().size() == 0)
        return;

    int selectedStep = ui->tableSteps->selectedItems().at(0)->row();

    // swap the selected step with the one below it
    if(selectedStep < MAX_NUM_STEPS-1)
    {
        Action::STEP tmp;
        memcpy(&tmp,&(motionPages[selectedPageID].step[selectedStep]),sizeof(Action::STEP));
        memcpy(&(motionPages[selectedPageID].step[selectedStep]),&(motionPages[selectedPageID].step[selectedStep+1]),sizeof(Action::STEP));
        memcpy(&(motionPages[selectedPageID].step[selectedStep+1]),&tmp,sizeof(Action::STEP));

        populatePageUI();
        ui->tableSteps->selectRow(selectedStep+1);
    }
}

QString MainWindow::JointID2QString(int id)
{
    using namespace Robot;
    switch(id)
    {
    case JointData::ID_L_SHOULDER_PITCH:
        return "L Shoulder Pitch";
    case JointData::ID_L_SHOULDER_ROLL:
        return "L Shoulder Roll";
    case JointData::ID_L_ELBOW:
        return "L Elbow";
    case JointData::ID_L_HAND:
        return "L Hand";
    case JointData::ID_L_HIP_PITCH:
        return "L Hip Pitch";
    case JointData::ID_L_HIP_ROLL:
        return "L Hip Roll";
    case JointData::ID_L_HIP_YAW:
        return "L Hip Yaw";
    case JointData::ID_L_KNEE:
        return "L Knee";
    case JointData::ID_L_ANKLE_PITCH:
        return "L Ankle Pitch";
    case JointData::ID_L_ANKLE_ROLL:
        return "L Ankle Roll";

    case JointData::ID_R_SHOULDER_PITCH:
        return "R Shoulder Pitch";
    case JointData::ID_R_SHOULDER_ROLL:
        return "R Shoulder Roll";
    case JointData::ID_R_ELBOW:
        return "R Elbow";
    case JointData::ID_R_HAND:
        return "R Hand";
    case JointData::ID_R_HIP_PITCH:
        return "R Hip Pitch";
    case JointData::ID_R_HIP_ROLL:
        return "R Hip Roll";
    case JointData::ID_R_HIP_YAW:
        return "R Hip Yaw";
    case JointData::ID_R_KNEE:
        return "R Knee";
    case JointData::ID_R_ANKLE_PITCH:
        return "R Ankle Pitch";
    case JointData::ID_R_ANKLE_ROLL:
        return "R Ankle Roll";

    case JointData::ID_HEAD_PAN:
        return "Head Pan";
    case JointData::ID_HEAD_TILT:
        return "Head Tilt";

    default:
        return "Unknown";
    }
}

void MainWindow::getSelectedRobotIDs(QStringList &packetPayload)
{
    packetPayload.clear();
    QList<QTableWidgetItem*> items = ui->tableRobotPose->selectedItems();

    for(int i=0; i<items.size(); i++)
    {
        if(items.at(i)->column()==0)
        {
            packetPayload << QString::number(items.at(i)->row()+1) << " ";
        }
    }
}

void MainWindow::onTorqueEnableClicked()
{
    QStringList payload;
    getSelectedRobotIDs(payload);

    sendCommand(CMD_TORQUE_ON,payload,true);
    sendCommand(CMD_GET,true);
}

void MainWindow::onTorqueDisableClicked()
{
    QStringList payload;
    getSelectedRobotIDs(payload);

    sendCommand(CMD_TORQUE_OFF,payload,true);
    sendCommand(CMD_GET,true);
}

void MainWindow::onToPoseClicked()
{
    if(ui->tableRobotPose->selectedItems().size() == 0)
    {
        // no items selected; confirm we want to overwrite everything
        QMessageBox msgBox;
        msgBox.setText("Set angles for all motors?");
        msgBox.setInformativeText("Doing this will replace all angles in the current pose with the robot's current position.\nPress YES to contine or NO to cancel.");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        msgBox.setIcon(QMessageBox::Question);
        int ret = msgBox.exec();

        if(ret == QMessageBox::Yes)
        {
            ui->tableRobotPose->selectAll();
            copySelectionsToPose();
        }
    }
    else
    {
        copySelectionsToPose();
    }
}

void MainWindow::onToRobotClicked()
{
    if(ui->tablePagePose->selectedItems().size() == 0)
    {
        // no items selected; confirm we want to overwrite everything
        QMessageBox msgBox;
        msgBox.setText("Set angles for all joints?");
        msgBox.setInformativeText("Doing this will set all of the robot's motors to those in the selected pose.  If the pose is wrong damage may occur to the robot.\nPress YES to contine or NO to cancel.");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        msgBox.setIcon(QMessageBox::Question);
        int ret = msgBox.exec();

        if(ret == QMessageBox::Yes)
        {
            ui->tablePagePose->selectAll();
            copySelectionsToRobot();
        }
    }
    else
    {
        copySelectionsToRobot();
    }
}

void MainWindow::copySelectionsToPose()
{
    QList<QTableWidgetItem*> items = ui->tableRobotPose->selectedItems();
    suppressAngleChangeEvents = true;

    for(int i=0; i<items.size(); i++)
    {
        if(items.at(i)->column() == 1)
        {
            ui->tablePagePose->item(items.at(i)->row(),items.at(i)->column())->setText(items.at(i)->text());
        }
    }
    suppressAngleChangeEvents = false;
    saveCurrentStep();
}

void MainWindow::copySelectionsToRobot()
{
    QList<QTableWidgetItem*> items = ui->tablePagePose->selectedItems();
    suppressAngleChangeEvents = true;
    JointData joint;
    int jointID;
    double angle;

    for(int i=0; i<items.size(); i++)
    {
        if(items.at(i)->column() == 1)
        {
            jointID = items.at(i)->row()+1;
            angle = items.at(i)->text().toDouble();

            ui->tableRobotPose->item(items.at(i)->row(),items.at(i)->column())->setText(items.at(i)->text());

            // send the position to the robot too
            joint.SetBodyAngle(jointID,angle);
            QStringList params;
            params << " " << QString::number(jointID) << " " << QString::number(joint.GetValue(jointID));
            sendCommand(CMD_SET,params,true);
        }
    }
    suppressAngleChangeEvents = false;
}

void MainWindow::setSelectedMotorAngle(int id, double angle)
{
    JointData jointData;

    jointData.SetBodyAngle(id,angle);
    int position = jointData.GetValue(id);

    suppressAngleChangeEvents = true;

    ui->lblMotorPosition->setText(QString::number(position));
    ui->sliderAngle->setValue(position);
    ui->numAngle->setValue(angle);
    selectedMotorTableWidget->setText(QString::number(angle));

    suppressAngleChangeEvents = false;
}

void MainWindow::onAngleSliderChanged(int value)
{
    if(suppressAngleChangeEvents)
        return;

    int motorId = getSelectedMotorID();
    if(motorId < 0)
        return;

    JointData jointData;

    jointData.SetValue(motorId,value);
    double angle = jointData.GetBodyAngle(motorId);
    setSelectedMotorAngle(motorId, angle);
}

void MainWindow::onAngleFieldChanged(double value)
{
    if(suppressAngleChangeEvents)
        return;

    int motorId = getSelectedMotorID();
    if(motorId < 0)
        return;

    setSelectedMotorAngle(motorId,value);
}

void MainWindow::onPageMotorAngleChanged(QTableWidgetItem* item)
{
    int motorId = getSelectedMotorID();
    if(motorId < 0)
        return;

    // save the edited motor angle to the current pose
    JointData jointData;
    jointData.SetBodyAngle(motorId,item->text().toDouble());
    motionPages[selectedPageID].step[selectedPageStep].position[motorId] = jointData.GetValue(motorId);

    if(suppressAngleChangeEvents)
        return;

    double value = item->text().toDouble();
    setSelectedMotorAngle(motorId,value);

    drawRobotModel(selectedPageID,selectedPageStep,this->currentViewMode);
}

void MainWindow::onRobotMotorAngleChanged(QTableWidgetItem* item)
{
    int motorId = getSelectedMotorID();
    if(motorId < 0)
        return;

    double value = item->text().toDouble();

    if(sendPositionChangeToRobot)
    {
        JointData jointData;
        jointData.SetBodyAngle(motorId,value);

        sendCommand(CMD_SET,QStringList() << QString::number(motorId) << " " << QString::number(jointData.GetValue(motorId)), true);
    }

    if(suppressAngleChangeEvents)
        return;

    setSelectedMotorAngle(motorId,value);
}

void MainWindow::onPageMotorSelectionChanged()
{
    int numSelectedRows = ui->tablePagePose->selectedItems().size() / ui->tablePagePose->columnCount();

    if(numSelectedRows == 1)
    {
        // only one row selected
        ui->tableRobotPose->clearSelection();

        selectedMotorTableWidget = ui->tablePagePose->selectedItems().at(1);
        ui->sliderAngle->setEnabled(true);
        ui->numAngle->setEnabled(true);

        setSelectedMotorAngle(getSelectedMotorID(), selectedMotorTableWidget->text().toDouble());
    }
    else
    {
        // 0 or 2+ items selected
        selectedMotorTableWidget = NULL;
        ui->sliderAngle->setEnabled(false);
        ui->numAngle->setEnabled(false);
    }
}

void MainWindow::onRobotMotorSelectionChanged()
{
    int numSelectedRows = ui->tableRobotPose->selectedItems().size() / ui->tableRobotPose->columnCount();

    if(numSelectedRows == 1)
    {
        // only one row selected
        ui->tablePagePose->clearSelection();

        selectedMotorTableWidget = ui->tableRobotPose->selectedItems().at(1);
        ui->sliderAngle->setEnabled(true);
        ui->numAngle->setEnabled(true);

        setSelectedMotorAngle(getSelectedMotorID(), selectedMotorTableWidget->text().toDouble());
    }
    else
    {
        // 0 or 2+ items selected
        selectedMotorTableWidget = NULL;
        ui->sliderAngle->setEnabled(false);
        ui->numAngle->setEnabled(false);
    }

    if(ui->tableRobotPose->selectedItems().size() == 0)
    {
        ui->btnTorqueOff->setEnabled(false);
        ui->btnTorqueOn->setEnabled(false);
    }
    else
    {
        ui->btnTorqueOff->setEnabled(true);
        ui->btnTorqueOn->setEnabled(true);
    }
}

int MainWindow::getSelectedMotorID()
{
    if(selectedMotorTableWidget == NULL)
        return -1;
    else
        return selectedMotorTableWidget->row()+1;
}

void MainWindow::onCopyPageClicked()
{
    if(pageClipboard != NULL)
    {
        free(pageClipboard);
    }

    pageClipboard = (Action::PAGE*)malloc(sizeof(Action::PAGE));
    memcpy(pageClipboard,&(motionPages[selectedPageID]),sizeof(Action::PAGE));
    pageCutFromIndex = -1;

    ui->btnPastePage->setEnabled(true);
}

void MainWindow::onCutPageClicked()
{
    if(pageClipboard != NULL)
    {
        free(pageClipboard);
    }

    pageClipboard = (Action::PAGE*)malloc(sizeof(Action::PAGE));
    memcpy(pageClipboard,&(motionPages[selectedPageID]),sizeof(Action::PAGE));
    pageCutFromIndex = selectedPageID;

    ui->btnPastePage->setEnabled(true);
}

void MainWindow::onDeletePageClicked()
{
    deletePage(selectedPageID);
    ui->tablePagePose->selectRow(selectedPageID-1);
    populatePageUI();
}

void MainWindow::deletePage(int index)
{
    motionPages[index].header.stepnum=0;
    motionPages[index].header.name[0] = '\0';
    motionPages[index].header.exit = 0;
    motionPages[index].header.next = 0;

    ui->tablePages->item(index-1,COLUMN_PAGE_NAME)->setText("");
    ui->tablePages->item(index-1,COLUMN_PAGE_NEXT)->setText("0");
    ui->tablePages->item(index-1,COLUMN_PAGE_EXIT)->setText("0");
}

void MainWindow::onPastePageClicked()
{
    memcpy(motionPages+selectedPageID,pageClipboard,sizeof(Action::PAGE));

    if(pageCutFromIndex != -1)
        deletePage(pageCutFromIndex);

    populatePageUI();
}

void MainWindow::onJointSoftnessChanged(QTableWidgetItem* item)
{
    motionPages[selectedPageID].header.slope[item->row()+1] = SLOPE2PAGE(item->text().toInt());
}

void MainWindow::onRepeatChanged(int value)
{
    motionPages[selectedPageID].header.repeat = value;
}

void MainWindow::onInertialForceChanged(int value)
{
    motionPages[selectedPageID].header.accel = value;
}

void MainWindow::onSpeedRateChanged(double value)
{
    motionPages[selectedPageID].header.speed = SPEED2PAGE(value);
}

void MainWindow::saveCurrentStep()
{
    if(selectedPageStep == -1 || selectedPageID == 0 || ui->tableSteps->rowCount() == 0)
        return;

    cout << "Saving page " << selectedPageID << " step " << selectedPageStep << endl;

    // copy the name of the motion
    motionPages[selectedPageID].header.name[0] = '\0';
    for(int i=0; i<MAX_LEN_NAME && i<ui->tablePages->item(selectedPageID-1,COLUMN_PAGE_NAME)->text().size(); i++)
    {
        motionPages[selectedPageID].header.name[i] = ui->tablePages->item(selectedPageID-1,COLUMN_PAGE_NAME)->text().at(i).toLatin1();
        motionPages[selectedPageID].header.name[i+1] = '\0';
    }

    // copy the rest of the header data
    motionPages[selectedPageID].header.accel = ui->numInertialForce->value();
    motionPages[selectedPageID].header.exit = ui->tablePages->item(selectedPageID-1,COLUMN_PAGE_EXIT)->text().toInt();
    motionPages[selectedPageID].header.repeat = ui->numRepeat->value();
    for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
        motionPages[selectedPageID].header.slope[i] = SLOPE2PAGE(ui->tableJointSoftness->item(i-1,0)->text().toInt());
    motionPages[selectedPageID].header.speed = SPEED2PAGE(ui->numSpeedRate->value());
    motionPages[selectedPageID].header.stepnum = ui->tableSteps->rowCount();

    // copy the current pose of the robot
    JointData jointData;
    for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
    {
        jointData.SetBodyAngle(i,ui->tablePagePose->item(i-1,COLUMN_MOTOR_ANGLE)->text().toDouble());
        //cout << i << ": " << jointData.GetValue(i) << endl;
        motionPages[selectedPageID].step[selectedPageStep].position[i] = jointData.GetValue(i);
    }

    // copy the rest of the step-specific items
    motionPages[selectedPageID].step[selectedPageStep].pause = TIME2PAGE(ui->tableSteps->item(selectedPageStep,COLUMN_PAUSE)->text().toDouble());
    motionPages[selectedPageID].step[selectedPageStep].time = TIME2PAGE(ui->tableSteps->item(selectedPageStep,COLUMN_TIME)->text().toDouble());
}

void MainWindow::drawRobotModel(int pageIndex, int stepIndex, int viewMode)
{
    if(renderProcess.state() != QProcess::NotRunning)
        renderProcess.kill();

    currentViewMode = viewMode;
    JointData jointData;
    QStringList angles;
    for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
    {
        jointData.SetValue(i,motionPages[pageIndex].step[stepIndex].position[i]);
        angles << QString::number(jointData.GetBodyAngle(i));
    }

    QStringList args;
    switch(viewMode)
    {
    case VIEW_SIDE:
        args << "-e" << "0" << "-a" << "90";
        break;
    case VIEW_TOP:
        args << "-e" << "90" << "-a" << "0";
        break;
    case VIEW_FRONT:
        args << "-e" << "0" << "-a" << "0";
        break;
    default:
        break;
    }

    args << "-d" << angles;
    renderProcess.start("draw_robot.py",args);
}

void MainWindow::onPyRenderComplete(int exitCode)
{
    if(exitCode != 0)
        return;
    QByteArray imgBuffer = renderProcess.readAllStandardOutput();
    if(imgBuffer.size() == 0)
        return;
    QImage img = QImage::fromData((const uchar*)imgBuffer.data(),imgBuffer.size(),0);
    img = img.scaled(ui->imgRobotPose->width(), ui->imgRobotPose->height());
    ui->imgRobotPose->setImage(img);
}

void MainWindow::onDefaultViewClicked()
{
    drawRobotModel(selectedPageID, selectedPageStep, VIEW_DEFAULT);
}

void MainWindow::onTopViewClicked()
{
    drawRobotModel(selectedPageID, selectedPageStep, VIEW_TOP);
}

void MainWindow::onSideViewClicked()
{
    drawRobotModel(selectedPageID, selectedPageStep, VIEW_SIDE);
}

void MainWindow::onFrontViewClicked()
{
    drawRobotModel(selectedPageID, selectedPageStep, VIEW_FRONT);
}
