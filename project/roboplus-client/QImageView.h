//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef QIMAGEVIEW_H
#define QIMAGEVIEW_H


//------------------------------------------------------------------------------
// INCLUDE HEADERS AND FORWARD DECLARATION
//------------------------------------------------------------------------------
#include <QGraphicsView>
class QGraphicsPixmapItem;
class QGraphicsScene;
class QImage;
class QPixmap;


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class QImageView : public QGraphicsView
{
    Q_OBJECT

private:
    QGraphicsScene* _scene; // graphics scene
    QGraphicsPixmapItem* _gpiImage; // pixmap image

public:
    explicit QImageView( QWidget* parent = 0 );
    virtual ~QImageView( void );
    virtual void setImage( const QImage& image );
    virtual void setPixmap( const QPixmap& pixmap );

signals:
    void mouseMoved( Qt::MouseButtons buttons, int x, int y );
    void mousePressed( Qt::MouseButtons buttons, int x, int y );

private:
    virtual void mouseMoveEvent( QMouseEvent* event );
    virtual void mousePressEvent( QMouseEvent* event );

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
