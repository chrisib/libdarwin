#-------------------------------------------------
#
# Project created by QtCreator 2014-09-10T14:30:32
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = roboplus-client
TEMPLATE = app

LIBS += -ldarwin

SOURCES += main.cpp\
        MainWindow.cpp \
    QImageView.cpp

HEADERS  += MainWindow.h \
    QImageView.h

FORMS    += MainWindow.ui

RESOURCES += \
    icons.qrc

##################################################
# Build Directories                              #
##################################################
OBJECTS_DIR = .objects
MOC_DIR     = .moc
UI_DIR      = .ui

##################################################
# Installation Directories                       #
##################################################
INSTALLBASE = /usr/local
bin.path    = $$INSTALLBASE/bin/
bin.files   = $$TARGET
INSTALLS   += bin

post_install.path = $$INSTALLBASE
post_install.commands = ldconfig
INSTALLS += post_install

py.path = $$INSTALLBASE/bin
py.commands = cp draw_robot.py $$INSTALLBASE/bin
INSTALLS   += py
