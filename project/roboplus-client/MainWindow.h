#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <stddef.h>
#include <QMainWindow>
#include <QTableWidgetItem>
#include <darwin/framework/Action.h>
#include <QTcpSocket>
#include <QTimer>
#include <QProcess>

// reverse-engineered from roboplus executable
// not sure what the VERSION2 command is actually good for
#define CMD_VERSION     "version"
#define CMD_LIST        "list"
#define CMD_PAGEINFO    "pageinfo"
#define CMD_TORQUE_ON   "on"
#define CMD_TORQUE_OFF  "off"
#define CMD_GET         "getpositions"
#define CMD_SET         "set"
#define CMD_PLAY        "play"
#define CMD_PAUSE       "pause"
#define CMD_STOP        "stop"
#define CMD_INFO        "info"
#define CMD_SAVE        "save"
#define CMD_EXIT        "exit"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onAngleSliderChanged(int value);
    void onAngleFieldChanged(double value);

    void onConnectClicked();
    void onDisconnectClicked();

    void onSaveClicked();
    void onReloadClicked();

    void onPlayClicked();
    void onPauseClicked();
    void onStopClicked();

    void onAddStepClicked();
    void onInsertStepClicked();
    void onDeleteStepClicked();
    void onMoveStepUpClicked();
    void onMoveStepDownClicked();

    void onTorqueEnableClicked();
    void onTorqueDisableClicked();

    void onPageSelected();
    void onPageItemChanged(QTableWidgetItem* item);
    void onStepSelectionChanged();
    void onStepItemChanged(QTableWidgetItem* item);

    void onPageMotorSelectionChanged();
    void onPageMotorAngleChanged(QTableWidgetItem* item);

    void onRobotMotorSelectionChanged();
    void onRobotMotorAngleChanged(QTableWidgetItem* item);

    void onToPoseClicked();
    void onToRobotClicked();

    void onReadyRead();
    void onSocketConnected();
    void onSocketDisconnected();

    void onCopyPageClicked();
    void onDeletePageClicked();
    void onCutPageClicked();
    void onPastePageClicked();

    void onJointSoftnessChanged(QTableWidgetItem*);
    void onRepeatChanged(int value);
    void onInertialForceChanged(int value);
    void onSpeedRateChanged(double value);

    void onPositionTimerInterrupt();

    void onPyRenderComplete(int exitCode);
    void onDefaultViewClicked();
    void onTopViewClicked();
    void onSideViewClicked();
    void onFrontViewClicked();

private:
    enum{
        COLUMN_PAGE_NAME = 0,
        COLUMN_PAGE_NEXT,
        COLUMN_PAGE_EXIT
    };

    enum{
        COLUMN_PAUSE=0,
        COLUMN_TIME
    };

    enum{
        COLUMN_MOTOR_NAME = 0,
        COLUMN_MOTOR_ANGLE
    };

    Ui::MainWindow *ui;

    QTcpSocket socket;

    QTableWidgetItem *selectedMotorTableWidget;

    static const double SPEED_SCALE = 32.0;
    static const double TIME_SCALE = 125.0;
    static const double SLOPE_SCALE = 17.0;

    static const int MAX_NUM_PAGES = 255;
    static const int MAX_NUM_STEPS = 7;
    static const int MAX_LEN_NAME = 13;
    bool pageLoaded[MAX_NUM_PAGES];
    int selectedPageID;
    int selectedPageStep;
    Robot::Action::PAGE motionPages[MAX_NUM_PAGES+1];
    Robot::Action::PAGE *pageClipboard;
    int pageCutFromIndex;

    bool sendPositionChangeToRobot;
    bool finishedPlaying;

    bool suppressAngleChangeEvents;
    bool suppressPageChangeEvents;
    std::list<QString> sentCommands;

    QTimer positionPingTimer;

    QProcess renderProcess;

    QByteArray toParse;
    void sendCommand(QString command, bool waitForReply);
    void sendCommand(QString command, QStringList &payload, bool waitForReply);

    int parseVersion(QByteArray &data);
    void parseList(QByteArray &data);
    void parseGet(QByteArray &data);
    void parsePageInfo(QByteArray &data);
    bool parseInfo(QByteArray &data);

    void populatePageUI();

    int getSelectedMotorID();
    void setSelectedMotorAngle(int id, double angle);

    void copySelectionsToPose();
    void copySelectionsToRobot();

    QString JointID2QString(int id);
    void getSelectedRobotIDs(QStringList &packetPayload);

    void deletePage(int index);
    void saveCurrentStep();

    enum{
        VIEW_DEFAULT,
        VIEW_TOP,
        VIEW_SIDE,
        VIEW_FRONT
    };
    int currentViewMode;
    void drawRobotModel(int pageIndex, int stepIndex, int viewMode=VIEW_DEFAULT);
};

#endif // MAINWINDOW_H
