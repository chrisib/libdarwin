#include "RoboPlusV2.h"

#include <LinuxDARwIn.h>
#include "LinuxDARwIn.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <term.h>
#include <signal.h>
#include <ncurses.h>
#include <libgen.h>
#include "darwin/framework/LeftArm.h"
#include "darwin/framework/RightArm.h"

using namespace Robot;
using namespace std;

#define RPLUS_VERSION			2.000
#define TCPIP_PORT				6501
#define ROBOPLUS_JOINT_MAXNUM	26

#define CMD_VERSION     "version"
#define CMD_LIST        "list"
#define CMD_PAGEINFO    "pageinfo"
#define CMD_TORQUE_ON   "on"
#define CMD_TORQUE_OFF  "off"
#define CMD_GET         "getpositions"
#define CMD_SET         "set"
#define CMD_PLAY        "play"
#define CMD_PAUSE       "pause"
#define CMD_STOP        "stop"
#define CMD_INFO        "info"
#define CMD_SAVE        "save"
#define CMD_EXIT        "exit"

RoboPlusV2::RoboPlusV2(QObject *parent = NULL) : QObject(parent)
{
    if(!server.listen(QHostAddress::Any,TCPIP_PORT))
    {
        cerr << "Failed to listen on port " << TCPIP_PORT;
        ::exit(-1);
    }

    socket = NULL;
}

void RoboPlusV2::onConnectionAccepted()
{
    cout << "[Connected]" << endl;
    socket = server.nextPendingConnection();
    connect(socket,SIGNAL(readyRead()),this,SLOT(onDataReceived()));
}

void RoboPlusV2::onDataReceived()
{
    if(!Action::GetInstance()->IsRunning())
        LinuxMotionTimer::Stop();
        
    while(socket->canReadLine())
    {
        QByteArray data = socket->readLine();
        parseData(data);
    }
}

// determine what the command is, apply its arguments as needed and send a response back
void RoboPlusV2::parseData(QByteArray datagram)
{
    char data[datagram.size()];
    for(int i=0; i<datagram.size(); i++)
        data[i] = datagram.at(i);

    char *command = strtok(data," \n");

    cout << "Received command \"" << command << "\"" << endl;

    if(!strcmp(CMD_VERSION,command))
    {
        socket->write("{");
        socket->write(QString::number(RPLUS_VERSION).toStdString().c_str());
        socket->write("}\n");
    }
    else if(!strcmp(CMD_LIST,command))
    {
        list();
    }
    else if(!strcmp(CMD_PAGEINFO,command))
    {
        char *token = strtok(NULL," \n");
        int index = atoi(token);

        pageInfo(index);
    }
    else if(!strcmp(CMD_TORQUE_ON,command) || !strcmp(CMD_TORQUE_OFF,command))
    {
        QByteArray motorIDs;
        char *token = strtok(NULL," \n");
        while(token!=NULL)
        {
            cout << ">" << token << "<" << endl;
            motorIDs.push_back(atoi(token));
            token = strtok(NULL," \n");
        }

        setTorque(strcmp(CMD_TORQUE_OFF,command),motorIDs);
    }
    else if(!strcmp(CMD_GET,command))
    {
        getPositions();
    }
    else if(!strcmp(CMD_SET,command))
    {
        char *token = strtok(NULL," ");
        int id = atoi(token);
        token = strtok(NULL," \n");
        int position = atoi(token);
        setPosition(id,position);
    }
    else if(!strcmp(CMD_PLAY,command))
    {
        char *token = strtok(NULL," \n");
        int page = atoi(token);
        playMotion(page);
    }
    else if(!strcmp(CMD_PAUSE,command))
    {
        pauseMotion();
    }
    else if(!strcmp(CMD_STOP,command))
    {
        stopMotion();
    }
    else if(!strcmp(CMD_INFO,command))
    {
        playingInfo();
    }
    else if(!strcmp(CMD_SAVE,command))
    {
        char *token = strtok(NULL," ");
        int page = atoi(token);
        token += strlen(token)+1;

        save(page, token);
    }
    else if(!strcmp(CMD_EXIT,command))
    {
        disconnect();
    }
    else
    {
        cout << "Unknown command " << command << endl;
        socket->write("{Unknown Command}\n");
    }
    
    socket->flush();
}

void RoboPlusV2::list()
{
    // meta-data about all pages (name,#steps, etc...)
    socket->write("{");
    for(int i = 0; i < 256; i++)
    {
        Action::PAGE page;
        Action::GetInstance()->LoadPage(i, &page);
        socket->write("[");
        socket->write(QString::number(i).toStdString().c_str());
        socket->write(":");
        socket->write((const char*)page.header.name);
        socket->write(":");
        socket->write(QString::number(page.header.next).toStdString().c_str());
        socket->write(":");
        socket->write(QString::number(page.header.exit).toStdString().c_str());
        socket->write(":");
        socket->write(QString::number(page.header.stepnum).toStdString().c_str());
        socket->write("]");

        cout << i << ":" << page.header.name << ":" << (int)page.header.next << ":" << (int)page.header.exit << ":" << (int)page.header.stepnum << endl;
    }
    socket->write("}\n");
}

void RoboPlusV2::pageInfo(int index)
{
    cout << "Pageinfo for page " << index << endl;

    Action::PAGE page;
    Action::GetInstance()->LoadPage(index, &page);

    socket->write("{");
    socket->write(QString::number(page.header.accel).toStdString().c_str());
    socket->write(":");
    socket->write(QString::number(page.header.repeat).toStdString().c_str());
    socket->write(":");
    socket->write(QString::number(page.header.schedule).toStdString().c_str());
    socket->write(":");
    socket->write(QString::number(page.header.speed).toStdString().c_str());
    socket->write(":");

    for(int i=0; i<7; i++)
    {
        socket->write("[");
        socket->write(QString::number(page.step[i].time).toStdString().c_str());
        socket->write(":");
        socket->write(QString::number(page.step[i].pause).toStdString().c_str());
        socket->write(":");

        for(int k=0; k<32; k++)
        {
            socket->write(QString::number(page.step[i].position[k]).toStdString().c_str());

            if(k<31)
                socket->write(":");
        }

        socket->write("]");
    }

    socket->write(":[");
    for(int i=0; i<32; i++)
    {
        socket->write(QString::number(page.header.slope[i]).toStdString().c_str());

        if(i < 31)
            socket->write(":");
    }
    socket->write("]");

    socket->write("}\n");
}

void RoboPlusV2::playingInfo()
{
    int ipage, istep;
    if(Action::GetInstance()->IsRunning(&ipage, &istep) == 1)
    {
        socket->write("{");
        socket->write(QString::number(ipage).toStdString().c_str());
        socket->write(":");
        socket->write(QString::number(istep).toStdString().c_str());
        socket->write("}\n");
        cout << "[" << ipage << ":" << istep << "]" << endl;
    }
    else
    {
        socket->write("{END}");
        cout << "[END]" << endl;
        MotionManager::GetInstance()->SetEnable(false);
    }
}

void RoboPlusV2::setTorque(bool enable, QByteArray ids)
{
    int torque = enable ? 1:0;
    
    if(enable)
        cout << "Enabling torque on IDs ";
    else
        cout << "Disabling torque on IDs ";
    
    if(ids.size() == 0)
    {
        for(int i=1; i<JointData::NUMBER_OF_JOINTS; i++)
        {
            cout << i << " ";
            cm730->WriteByte(i,MX28::P_TORQUE_ENABLE, torque,0);
            //MotionManager::GetInstance()->SetTorqueEnable(i,enable);
        }
    }
    else
    {
        for(int i=0; i<ids.size(); i++)
        {
            cout << (int)ids.at(i) << " ";
            cm730->WriteByte(ids.at(i),MX28::P_TORQUE_ENABLE,torque,0);
            //MotionManager::GetInstance()->SetTorqueEnable(ids.at(i),enable);
        }
    }
    cout << endl;

    socket->write("{Torque:");
    socket->write(QString::number(torque).toStdString().c_str());
    socket->write("}\n)");
}

void RoboPlusV2::getPositions()
{
    int torque;
    int position;

    socket->write("{");
    for(int id=0; id<ROBOPLUS_JOINT_MAXNUM; id++)
    {
        socket->write("[");
        cout << "[";
        if(id >= 1 && id < JointData::NUMBER_OF_JOINTS)
        {
            if(cm730->ReadByte(id, MX28::P_TORQUE_ENABLE, &torque, 0) == CM730::SUCCESS)
            {
                if(torque == 1)
                {
                    if(cm730->ReadWord(id, MX28::P_GOAL_POSITION_L, &position, 0) == CM730::SUCCESS)
                    {
                        socket->write(QString::number(position).toStdString().c_str());
                        cout << position;
                    }
                    else
                    {
                        socket->write("----");
                        cout << "Fail to read present position ID(" << id << ")";
                    }
                }
                else
                {
                    socket->write("????");
                    cout << "????";
                }
            }
            else
            {
                socket->write("----");
                cout << "Fail to read torque ID(" << id << ")";
            }
        }
        else
        {
            socket->write("----");
            cout << "----";
        }
        socket->write("]");
        cout << "]";
        cout << endl;
    }
    cout << endl;
    socket->write("}\n");
}

void RoboPlusV2::setPosition(int id, int position)
{
    if(cm730->WriteWord(id, MX28::P_GOAL_POSITION_L, position, 0) == CM730::SUCCESS)
    {
        if(cm730->ReadWord(id, MX28::P_GOAL_POSITION_L, &position, 0) == CM730::SUCCESS)
        {
            cout << "{[" << position << "]}";
            socket->write("{[");
            socket->write(QString::number(position).toStdString().c_str());
            socket->write("]}\n");
        }
        else
        {
            cout << "[Fail to read goal position ID(" << id << ")]";
            socket->write("{[----]}\n");
        }
    }
    else
    {
        cout << "[Fail to write goal position ID(" << id << ")]";
        socket->write("{[----]}\n");
    }
    cout << endl;
}

void  RoboPlusV2::playMotion(int page)
{
    cout << "Playing motion " << page << endl;

    if(Action::GetInstance()->IsRunning())
    {
        Action::GetInstance()->Stop();
        Action::GetInstance()->Finish();
    }
    else
    {
        MotionManager::GetInstance()->SetEnable(true);
        Action::GetInstance()->m_Joint.SetEnableBody(true,true);
        LinuxMotionTimer::Start();
    }
    Action::GetInstance()->Start(page);

    socket->write("{Play ");
    socket->write(QString::number(page).toStdString().c_str());
    socket->write("}\n");
}

void  RoboPlusV2::pauseMotion()
{
    cout << "Pausing motion" << endl;

    if(Action::GetInstance()->IsRunning())
    {
        Action::GetInstance()->Brake();
        socket->write("{Pause}\n");
    }
    else
    {
        socket->write("{Already stopped}\n");
    }
}

void  RoboPlusV2::stopMotion()
{
    cout << "Stopping motion" << endl;

    if(Action::GetInstance()->IsRunning())
    {
        Action::GetInstance()->Stop();
        Action::GetInstance()->Finish();
        
        socket->write("{Stop}\n");
    }
    else
    {
        socket->write("{Already stopped}\n");
    }
    LinuxMotionTimer::Stop();
}

void  RoboPlusV2::save(int index, char *pageData)
{
    /* pageData format is:
     * {name:next:exit:#steps:accel:repeat:schedule:speed:[time:pause:position1:position2:...:position32][time:pause....]...}
     *                                                    |<----- #steps times ---------------------------------------->|
     */

    Action::PAGE page;
    char *token;

    cout << "Saving page " << index << endl;
    cout << pageData << endl;

    if(strstr(pageData,"::")==NULL)
    {
        token = strtok(pageData+1,":");
        if(strlen(token) > 13)
            token[13] = 0;
        strcpy((char*)page.header.name,token);
        cout << "\tName: \"" << token << "\"" << endl;
    }
    else
    {
        page.header.name[0] = 0;
        cout << "\tName: \"\"" << endl;
    }

    token = strtok(NULL,":");
    page.header.next = atoi(token);
    cout << "\tNext: " << token << endl;

    token = strtok(NULL,":");
    page.header.exit = atoi(token);
    cout << "\tExit: " << token << endl;

    token = strtok(NULL,":");
    page.header.stepnum = atoi(token);
    cout << "\tSteps: " << token << endl;

    token = strtok(NULL,":");
    page.header.accel = atoi(token);
    cout << "\tAccel: " << token << endl;

    token = strtok(NULL,":");
    page.header.repeat = atoi(token);
    cout << "\tRepeat: " << token << endl;

    token = strtok(NULL,":");
    page.header.schedule = atoi(token);
    cout << "\tSchedule: " << token << endl;

    token = strtok(NULL,":");
    page.header.speed = atoi(token);
    cout << "\tSpeed: " << token << endl;

    // read the motion steps
    for(int i=0; i<page.header.stepnum; i++)
    {
        // skip to the opening [ for the next step
        token += strlen(token);
        while(*token!='[')
            token++;
        token++;

        cout << "\tStep " << (i+1) << ":" << endl;

        token = strtok(token,":");
        page.step[i].time = atoi(token);
        cout << "\t\tTime: " << token << endl;

        token = strtok(NULL,":");
        page.step[i].pause = atoi(token);
        cout << "\t\tPause: " << token << endl;

        cout << "\t\tPositions: ";
        for(int j=0; j<32; j++)
        {
            token = strtok(NULL,":]");
            page.step[i].position[j] = atoi(token);
            cout << "[" << j << ":" << token << "]";
        }
        cout << endl;
    }

    token = strtok(NULL,"[");
    for(int i=0; i<32; i++)
    {
        token = strtok(NULL,":]");
        page.header.slope[i] = atoi(token);

        cout << i << ": " << token << endl;
    }

    Action::GetInstance()->SavePage(index,&page);

}

void  RoboPlusV2::disconnect()
{
    cout << "Disconnecting" << endl;

    socket->write("{Exit}\n");
    if(Action::GetInstance()->IsRunning())
    {
        Action::GetInstance()->Stop();
        Action::GetInstance()->Finish();
    }

    socket->close();
}

int RoboPlusV2::_getch()
{
    struct termios oldt, newt;
    int ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    return ch;
}

int RoboPlusV2::execute(const char *filename)
{
    fprintf(stderr, "\n***********************************************************************\n");
    fprintf(stderr,   "*                      RoboPlus Server Program 2.0                    *\n");
    fprintf(stderr,   "***********************************************************************\n\n");

    initialize(filename);

    connect(&server, SIGNAL(newConnection()),this, SLOT(onConnectionAccepted()));

    cout << "[Running....]" << endl;

    return 0;
}

void RoboPlusV2::initialize(const char *filename)
{
    //signal(SIGABRT, &sighandler);
    //signal(SIGTERM, &sighandler);
    //signal(SIGQUIT, &sighandler);
    //signal(SIGINT, &sighandler);

    /////////////// Load/Create Action File //////////////////
    if(Action::GetInstance()->LoadFile(filename) == false)
    {
        printf("Can not open %s\n", filename);
        printf("Do you want to make a new action file? (y/n) ");
        ch = _getch();
        if(ch != 'y')
        {
            printf("\n");
            exit(0);
        }

        if(Action::GetInstance()->CreateFile(filename) == false)
        {
            printf("Fail to create %s\n", filename);
            exit(1);
        }
    }
    ////////////////////////////////////////////////////////////


    //////////////////// Framework Initialize ////////////////////////////
    cm730 = new CM730(new LinuxCM730("/dev/ttyUSB0"));
    if(MotionManager::GetInstance()->Initialize(cm730) == false)
    {
        printf("Fail to initialize Motion Manager!\n");
        //return 0;
    }
    MotionManager::GetInstance()->AddModule((MotionModule*)Action::GetInstance());
    
    LinuxMotionTimer::Initialize(MotionManager::GetInstance());
    LinuxMotionTimer::Stop();
    /////////////////////////////////////////////////////////////////////

}

void RoboPlusV2::sighandler(int sig)
{
    cout << "Received signal " << sig << endl;

    if(Action::GetInstance()->IsRunning())
    {
        Action::GetInstance()->Stop();
        Action::GetInstance()->Finish();
    }

    exit(sig);
}
