QT       += core network

QT       -= gui

TARGET = roboplus2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    RoboPlusV1.cpp \
    RoboPlusV2.cpp

LIBS += -ldarwin

HEADERS += \
    RoboPlusV1.h \
    RoboPlusV2.h

##################################################
# Output Directories                             #
##################################################
OBJECTS_DIR = .objects
MOC_DIR     = .moc
UI_DIR      = .ui

##################################################
# Installation Directories                       #
##################################################
INSTALLBASE = /usr/local
bin.path    = $$INSTALLBASE/bin/
bin.files   = $$TARGET
INSTALLS   += bin
