#ifndef ROBOPLUSV1_H
#define ROBOPLUSV1_H
#include <string>

class RoboPlusV1
{
public:
    RoboPlusV1();

    int execute(const char *filename);

private:
    int ch;

    std::string *string_split(std::string str_org, std::string str_tok);
    void change_current_dir();
    int _getch();

    static void sighandler(int sig);
};

#endif // ROBOPLUSV1_H
