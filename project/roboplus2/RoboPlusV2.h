#ifndef ROBOPLUSV2_H
#define ROBOPLUSV2_H

#include <QTcpSocket>
#include <QTcpServer>

#include <darwin/framework/CM730.h>
#include <darwin/linux/LinuxCM730.h>

class RoboPlusV2 : QObject
{
    Q_OBJECT
public:
    RoboPlusV2(QObject *parent);

    int execute(const char *filename);

public slots:
    void onDataReceived();
    void onConnectionAccepted();

private:
    int ch;

    QTcpSocket *socket;
    QTcpServer server;

    Robot::CM730 *cm730;

    QByteArray toParse;

    int _getch();
    void initialize(const char *filename);
    void parseData(QByteArray datagram);

    void list();
    void pageInfo(int index);
    void getPositions();
    void setPosition(int motorID, int value);
    void setTorque(bool enable, QByteArray ids);
    void playingInfo();
    void playMotion(int page);
    void pauseMotion();
    void stopMotion();
    void save(int page, char *pageData);
    void disconnect();

    static void sighandler(int sig);
};

#endif // ROBOPLUSV2_H
