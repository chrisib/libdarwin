/****************************************************************************
** Meta object code from reading C++ file 'RoboPlusV2.h'
**
** Created: Mon Sep 22 15:50:17 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "RoboPlusV2.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'RoboPlusV2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RoboPlusV2[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      29,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RoboPlusV2[] = {
    "RoboPlusV2\0\0onDataReceived()\0"
    "onConnectionAccepted()\0"
};

void RoboPlusV2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RoboPlusV2 *_t = static_cast<RoboPlusV2 *>(_o);
        switch (_id) {
        case 0: _t->onDataReceived(); break;
        case 1: _t->onConnectionAccepted(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData RoboPlusV2::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RoboPlusV2::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_RoboPlusV2,
      qt_meta_data_RoboPlusV2, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RoboPlusV2::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RoboPlusV2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RoboPlusV2::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RoboPlusV2))
        return static_cast<void*>(const_cast< RoboPlusV2*>(this));
    return QObject::qt_metacast(_clname);
}

int RoboPlusV2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
