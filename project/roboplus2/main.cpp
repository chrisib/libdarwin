﻿#include "LinuxDARwIn.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <term.h>
#include <signal.h>
#include <ncurses.h>
#include <libgen.h>
#include "darwin/framework/LeftArm.h"
#include "darwin/framework/RightArm.h"
#include <RoboPlusV1.h>
#include <RoboPlusV2.h>
#include <QCoreApplication>

using namespace Robot;
using namespace std;


#if 0

// horrible copy and paste job from above, with the new commands inserted
int newmain(int argc, char** argv)
{

//	change_current_dir();

    cout << "[Running....]\n";
    try
    {
        // Create the socket
        LinuxServer new_sock;
        LinuxServer server ( TCPIP_PORT );

        while ( true )
        {
            cout << "[Waiting..]" << endl;
            server.accept ( new_sock );
            cout << "[Accepted..]" << endl;

            try
            {
#define CMD_VERSION     "version"
#define CMD_LIST        "list"
#define CMD_PAGEINFO    "pageinfo"
#define CMD_TORQUE_ON   "on"
#define CMD_TORQUE_OFF  "off"
#define CMD_GET         "getpositions"
#define CMD_SET         "set"
#define CMD_PLAY        "play"
#define CMD_PAUSE       "pause"
#define CMD_STOP        "stop"
#define CMD_INFO        "info"
#define CMD_SAVE        "save"
#define CMD_EXIT        "exit"

                while ( true )
                {
                    string data;
                    Action::PAGE page;

                    new_sock >> data;
                    cout << data << endl;

                    string* p_str_tok = string_split(data, " ");

                    if(p_str_tok[0] == CMD_VERSION)
                    {
                        // just the version number
                        new_sock << "{" << VERSION << "}\n";
                        cout << "Version " << VERSION << endl;
                    }
                    else if(p_str_tok[0] == CMD_LIST)
                    {
                        // meta-data about all pages (name,#steps, etc...)
                        new_sock << "{";
                        for(int i = 0; i < 256; i++)
                        {
                            Action::GetInstance()->LoadPage(i, &page);
                            new_sock << "[" << i << ":";
                            new_sock.send(page.header.name, 14);
                            new_sock << ":" << (int)page.header.next << ":" << (int)page.header.exit << ":" << (int)page.header.stepnum << "]";
                            
                            cout << i << ":" << page.header.name << ":" << (int)page.header.next << ":" << (int)page.header.exit << ":" << (int)page.header.stepnum << endl;
                        }
                        new_sock << "}\n";
                    }
                    else if(p_str_tok[0] == CMD_PAGEINFO)
                    {
                        int index = (int)(atoi(p_str_tok[1].c_str()));

                        Action::GetInstance()->LoadPage(index, &page);

                        new_sock << "{";
                        new_sock << page.header.accel << ":" <<
                                    page.header.repeat << ":" <<
                                    page.header.schedule << ":" <<
                                    page.header.speed << ":";

                        for(int i=0; i<7; i++)
                        {
                            new_sock << "[";
                            new_sock << page.step[i].time << ":" <<
                                        page.step[i].pause << ":";

                            for(int k=0; k<32; k++)
                            {
                                new_sock << page.step[i].position[k];

                                if(k < 31)
                                    new_sock << ":";
                            }

                            new_sock << "]";
                        }

                        new_sock << "}\n";
                    }
                    else if(p_str_tok[0] == CMD_TORQUE_ON || p_str_tok[0] == CMD_TORQUE_OFF)
                    {
                        // turn torque on/off
                        int torque;
                        if(p_str_tok[0] == "on")
                        {
                            torque = 1;

                        }
                        else
                        {
                            torque = 0;
                        }

                        int i;
                        for(i=1; i<ARGUMENT_NAXNUM; i++)
                        {
                            if(p_str_tok[i].length() > 0)
                            {
                                cm730.WriteByte(atoi(p_str_tok[i].c_str()), MX28::P_TORQUE_ENABLE, torque, 0);
                            }
                            else
                                break;
                        }

                        if(i == 1)
                        {
                            for(int id=1; id<JointData::NUMBER_OF_JOINTS; id++)
                                cm730.WriteByte(id, MX28::P_TORQUE_ENABLE, torque, 0);
                        }

                        new_sock << "{Torque:" << torque << "}\n";
                    }
                    else if(p_str_tok[0] == CMD_GET)
                    {
                        int torque;
                        int position;

                        new_sock << "{";
                        for(int id=0; id<ROBOPLUS_JOINT_MAXNUM; id++)
                        {
                            new_sock << "[";
                            cout << "[";
                            if(id >= 1 && id < JointData::NUMBER_OF_JOINTS)
                            {
                                if(cm730.ReadByte(id, MX28::P_TORQUE_ENABLE, &torque, 0) == CM730::SUCCESS)
                                {
                                    if(torque == 1)
                                    {
                                        if(cm730.ReadWord(id, MX28::P_GOAL_POSITION_L, &position, 0) == CM730::SUCCESS)
                                        {
                                            new_sock << position;
                                            cout << position;
                                        }
                                        else
                                        {
                                            new_sock << "----";
                                            cout << "Fail to read present position ID(" << id << ")";
                                        }
                                    }
                                    else
                                    {
                                        new_sock << "????";
                                        cout << "????";
                                    }
                                }
                                else
                                {
                                    new_sock << "----";
                                    cout << "Fail to read torque ID(" << id << ")";
                                }
                            }
                            else
                            {
                                new_sock << "----";
                                cout << "----";
                            }
                            new_sock << "]";
                            cout << "]";
                        }
                        cout << endl;
                        new_sock << "}\n";
                    }
                    else if(p_str_tok[0] == CMD_SET)
                    {
                        int id = (int)atoi(p_str_tok[1].c_str());
                        int position = (int)atoi(p_str_tok[2].c_str());

                        if(cm730.WriteWord(id, MX28::P_GOAL_POSITION_L, position, 0) == CM730::SUCCESS)
                        {
                            if(cm730.ReadWord(id, MX28::P_GOAL_POSITION_L, &position, 0) == CM730::SUCCESS)
                            {
                                cout << "{[" << position << "]}";
                                new_sock << "{[" << position << "]}\n";
                            }
                            else
                            {
                                cout << "[Fail to read goal position ID(" << id << ")]";
                                new_sock << "{[----]}\n";
                            }
                        }
                        else
                        {
                            cout << "[Fail to write goal position ID(" << id << ")]";
                            new_sock << "{[----]}\n";
                        }
                        cout << endl;
                    }
                    else if(p_str_tok[0] == CMD_INFO)
                    {
                        int ipage, istep;
                        if(Action::GetInstance()->IsRunning(&ipage, &istep) == 1)
                        {
                            new_sock << "{" << ipage << ":" << istep << "}\n";
                            cout << "[" << ipage << ":" << istep << "]" << endl;
                        }
                        else
                        {
                            new_sock << "{END}\n";
                            cout << "[END]" << endl;
                            MotionManager::GetInstance()->SetEnable(false);
                        }
                    }
                    else if(p_str_tok[0] == CMD_PLAY)
                    {
                        int page = (int)atoi(p_str_tok[1].c_str());
                        if(Action::GetInstance()->IsRunning())
                        {
                            Action::GetInstance()->Stop();
                            Action::GetInstance()->Finish();
                        }
                        Action::GetInstance()->Start(page);

                        new_sock << "{Play " << page << "}\n";
                    }
                    else if(p_str_tok[0] == CMD_PAUSE)
                    {
                        Action::GetInstance()->Brake();
                        new_sock << "{Pause}\n";
                    }
                    else if(p_str_tok[0] == CMD_STOP)
                    {
                        Action::GetInstance()->Stop();
                        new_sock << "{Stop}\n";
                    }
                    else if(p_str_tok[0] == CMD_EXIT)
                    {
                        Action::GetInstance()->Stop();
                        new_sock << "{Exit}\n";
                    }
                    else if(p_str_tok[0] == CMD_SAVE)
                    {
                        int index = (int)(atoi(p_str_tok[1].c_str()));  // index of this page
                        /* format is:
                         * {name:next:exit:#steps:accel:repeat:schedule:speed:[time:pause:position1:position2:...:position32][time:pause....]...}
                         */

                        string data = p_str_tok[1];

                        cout << "New data for page " << index << endl;
                        cout << data << endl;

                        cerr << "SAVE NOT IMPLEMENTED YET" << endl;

                        new_sock << "{Saved " << index << "}\n";

                    }
                    else
                    {
                        cout << "Unknown command: " << p_str_tok[0] << endl;
                    }
                }
            }
            catch ( LinuxSocketException& )
            {
                cout << "[Disconnected]" << endl;

                if(Action::GetInstance()->IsRunning() == 1)
                {
                    Action::GetInstance()->Stop();
                    while(Action::GetInstance()->IsRunning() == 1)
                        usleep(1);
                    MotionManager::GetInstance()->SetEnable(false);
                    LinuxMotionTimer::Stop();
                }
            }
        }
    }
    catch ( LinuxSocketException& e )
    {
        cout << "Exception was caught:" << e.description() << "\nExiting.\n";
    }

    return 0;
}

#endif

#define MOTION_FILE_PATH    "motion.bin"

int main(int argc, char** argv)
{
    bool useOld = false;

    char filename[512];

    strcpy(filename, MOTION_FILE_PATH); // Set default motion file path

    for(int i=1; i<argc; i++)
    {
        cout << argv[i] << endl;

        if(!strcmp(argv[i],"--help") || !strcmp(argv[1],"-h"))
        {
            fprintf(stdout,"\nUsage:\n\t%s [/path/to/motion/file.bin | --old | --help]\n\n",argv[0]);
            fprintf(stdout,"\t--old: use official Robotis RoboPlus communication protocol\n");
            return 0;
        }
        else if(!strcmp(argv[i],"--old"))
        {
            useOld = true;
        }
        else
        {
            strcpy(filename, argv[i]);
        }
    }

    if(useOld)
    {
        RoboPlusV1 handler;
        return handler.execute(filename);
    }
    else
    {
        QCoreApplication a(argc, argv);
        RoboPlusV2 handler(NULL);
        handler.execute(filename);
        return a.exec();
    }
}
