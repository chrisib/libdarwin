#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>

#include <termios.h>
#include <term.h>
#include <pthread.h>

#include "LinuxDARwIn.h"
#include "FSR.h"
#include "mjpg_streamer.h"

using namespace Robot;

#define INI_FILE_PATH       "config.ini"
#define U2D_DEV_NAME        "/dev/ttyUSB0"

void draw_target(Image* img, int x, int y, int r, int g, int b);

void change_current_dir()
{
    char exepath[1024] = {0};
    if(readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
        chdir(dirname(exepath));
}

int _getch()
{
    struct termios oldt, newt;
    int ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    return ch;
}

void* walk_thread(void* ptr)
{
    while(1) {
        int ch = _getch();
        if(ch == 0x20) {
            if(Walking::GetInstance()->IsRunning() == true) {
                MotionManager::GetInstance()->StopLogging();
                Walking::GetInstance()->Stop();
            }
            else {
                MotionManager::GetInstance()->StartLogging();
                Walking::GetInstance()->Start();
            }
        }
    }
    return NULL;
}

int main()
{
	printf( "\n===== FSR Tutorial for DARwIn =====\n\n");

    change_current_dir();
    minIni* ini = new minIni(INI_FILE_PATH);

    mjpg_streamer* streamer = new mjpg_streamer(Camera::WIDTH, Camera::HEIGHT);

    Image* img_position = new Image(Camera::WIDTH, Camera::HEIGHT, Image::RGB_PIXEL_SIZE);
    Image* img_send = new Image(Camera::WIDTH, Camera::HEIGHT, Image::RGB_PIXEL_SIZE);

    FILE *src;
    src = fopen("foot.raw", "rb");
    if(src != NULL)
    {
        fread(img_position->m_ImageData, 1, img_position->m_ImageSize, src);
        fclose(src);
    }

    //////////////////// Framework Initialize ////////////////////////////
    LinuxDARwIn::Initialize("../../data/motion_4096.bin",Action::DEFAULT_MOTION_STAND_UP);
    MotionManager::GetInstance()->AddModule((MotionModule*)Head::GetInstance());
    MotionManager::GetInstance()->AddModule((MotionModule*)Walking::GetInstance());
    //LinuxMotionTimer *motion_timer = new LinuxMotionTimer();//
    //motion_timer->Start();
    /////////////////////////////////////////////////////////////////////

    printf("Press the ENTER key to begin!\n");
    getchar();
    //printf("Press the SPACE key to start/stop walking.. \n\n");

	Action::GetInstance()->m_Joint.SetEnableBody(false);
    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
    MotionManager::GetInstance()->SetEnable(true);

    static const int MAX_FSR_VALUE = 254;
    int left_fsr_x, left_fsr_y, right_fsr_x, right_fsr_y;

    Walking::GetInstance()->LoadINISettings(ini);
    pthread_t thread_t;
    pthread_create(&thread_t, NULL, walk_thread, NULL);

    while(1)
    {
        printf("\r");

        /* Read & print FSR value */
//        printf(" L1:%5d", cm730.m_BulkReadData[FSR::ID_L_FSR].ReadWord(FSR::P_FSR1_L));
//        printf(" L2:%5d", cm730.m_BulkReadData[FSR::ID_L_FSR].ReadWord(FSR::P_FSR2_L));
//        printf(" L3:%5d", cm730.m_BulkReadData[FSR::ID_L_FSR].ReadWord(FSR::P_FSR3_L));
//        printf(" L4:%5d", cm730.m_BulkReadData[FSR::ID_L_FSR].ReadWord(FSR::P_FSR4_L));

        left_fsr_x = MotionStatus::L_FSR_X;
        left_fsr_y = MotionStatus::L_FSR_Y;
        printf(" LX:%3d", left_fsr_x);
        printf(" LY:%3d", left_fsr_y);

//        printf(" R1:%5d", cm730.m_BulkReadData[FSR::ID_R_FSR].ReadWord(FSR::P_FSR1_L));
//        printf(" R2:%5d", cm730.m_BulkReadData[FSR::ID_R_FSR].ReadWord(FSR::P_FSR2_L));
//        printf(" R3:%5d", cm730.m_BulkReadData[FSR::ID_R_FSR].ReadWord(FSR::P_FSR3_L));
//        printf(" R4:%5d", cm730.m_BulkReadData[FSR::ID_R_FSR].ReadWord(FSR::P_FSR4_L));

        right_fsr_x = MotionStatus::R_FSR_X;
        right_fsr_y = MotionStatus::R_FSR_Y;
        printf(" RX:%3d", right_fsr_x);
        printf(" RY:%3d", right_fsr_y);


        /* draw a position of ZMP */
        int r_position_x = (98*(MAX_FSR_VALUE-right_fsr_y)/MAX_FSR_VALUE) + 24;
        int r_position_y = (160*(MAX_FSR_VALUE-right_fsr_x)/MAX_FSR_VALUE) + 40;
        int l_position_x = (98*left_fsr_y/MAX_FSR_VALUE) + 198;
        int l_position_y = (160*(MAX_FSR_VALUE-left_fsr_x)/MAX_FSR_VALUE) + 40;

        memcpy(img_send->m_ImageData, img_position->m_ImageData, LinuxCamera::GetInstance()->fbuffer->m_RGBFrame->m_ImageSize);
        if(left_fsr_x != -1 && left_fsr_y != -1)
            draw_target(img_send, l_position_x, l_position_y, 255, 0, 0);
        if(right_fsr_x != -1 && right_fsr_y != -1)
            draw_target(img_send, r_position_x, r_position_y, 0, 255, 0);

        if(left_fsr_x != -1 && left_fsr_y != -1 && right_fsr_x != -1 && right_fsr_y != -1)
            draw_target(img_send, (l_position_x+r_position_x)/2, (l_position_y+r_position_y)/2, 0, 0, 0);

        streamer->send_image(img_send);
    }

    return 0;
}

void draw_target(Image* img, int x, int y, int r, int g, int b)
{
    img->m_ImageData[(y-1)*img->m_WidthStep + x*img->m_PixelSize + 0] = r;
    img->m_ImageData[(y-1)*img->m_WidthStep + x*img->m_PixelSize + 1] = g;
    img->m_ImageData[(y-1)*img->m_WidthStep + x*img->m_PixelSize + 2] = b;

    img->m_ImageData[(y-2)*img->m_WidthStep + x*img->m_PixelSize + 0] = r;
    img->m_ImageData[(y-2)*img->m_WidthStep + x*img->m_PixelSize + 1] = g;
    img->m_ImageData[(y-2)*img->m_WidthStep + x*img->m_PixelSize + 2] = b;

    img->m_ImageData[(y-3)*img->m_WidthStep + x*img->m_PixelSize + 0] = r;
    img->m_ImageData[(y-3)*img->m_WidthStep + x*img->m_PixelSize + 1] = g;
    img->m_ImageData[(y-3)*img->m_WidthStep + x*img->m_PixelSize + 2] = b;

    img->m_ImageData[(y+1)*img->m_WidthStep + x*img->m_PixelSize + 0] = r;
    img->m_ImageData[(y+1)*img->m_WidthStep + x*img->m_PixelSize + 1] = g;
    img->m_ImageData[(y+1)*img->m_WidthStep + x*img->m_PixelSize + 2] = b;

    img->m_ImageData[(y+2)*img->m_WidthStep + x*img->m_PixelSize + 0] = r;
    img->m_ImageData[(y+2)*img->m_WidthStep + x*img->m_PixelSize + 1] = g;
    img->m_ImageData[(y+2)*img->m_WidthStep + x*img->m_PixelSize + 2] = b;

    img->m_ImageData[(y+3)*img->m_WidthStep + x*img->m_PixelSize + 0] = r;
    img->m_ImageData[(y+3)*img->m_WidthStep + x*img->m_PixelSize + 1] = g;
    img->m_ImageData[(y+3)*img->m_WidthStep + x*img->m_PixelSize + 2] = b;


    img->m_ImageData[y*img->m_WidthStep + (x-1)*img->m_PixelSize + 0] = r;
    img->m_ImageData[y*img->m_WidthStep + (x-1)*img->m_PixelSize + 1] = g;
    img->m_ImageData[y*img->m_WidthStep + (x-1)*img->m_PixelSize + 2] = b;

    img->m_ImageData[y*img->m_WidthStep + (x-2)*img->m_PixelSize + 0] = r;
    img->m_ImageData[y*img->m_WidthStep + (x-2)*img->m_PixelSize + 1] = g;
    img->m_ImageData[y*img->m_WidthStep + (x-2)*img->m_PixelSize + 2] = b;

    img->m_ImageData[y*img->m_WidthStep + (x-3)*img->m_PixelSize + 0] = r;
    img->m_ImageData[y*img->m_WidthStep + (x-3)*img->m_PixelSize + 1] = g;
    img->m_ImageData[y*img->m_WidthStep + (x-3)*img->m_PixelSize + 2] = b;

    img->m_ImageData[y*img->m_WidthStep + (x+1)*img->m_PixelSize + 0] = r;
    img->m_ImageData[y*img->m_WidthStep + (x+1)*img->m_PixelSize + 1] = g;
    img->m_ImageData[y*img->m_WidthStep + (x+1)*img->m_PixelSize + 2] = b;

    img->m_ImageData[y*img->m_WidthStep + (x+2)*img->m_PixelSize + 0] = r;
    img->m_ImageData[y*img->m_WidthStep + (x+2)*img->m_PixelSize + 1] = g;
    img->m_ImageData[y*img->m_WidthStep + (x+2)*img->m_PixelSize + 2] = b;

    img->m_ImageData[y*img->m_WidthStep + (x+3)*img->m_PixelSize + 0] = r;
    img->m_ImageData[y*img->m_WidthStep + (x+3)*img->m_PixelSize + 1] = g;
    img->m_ImageData[y*img->m_WidthStep + (x+3)*img->m_PixelSize + 2] = b;
}

