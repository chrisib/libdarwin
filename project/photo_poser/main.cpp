#include <iostream>

#include "PhotoPoser.h"

using namespace std;

int main(int argc, char** argv)
{
    PhotoPoser p;

    p.ParseArguments(argc,argv);
    p.Initialize();

    p.Execute();
    return 0;
}

