#ifndef PHOTOPOSER_H
#define PHOTOPOSER_H

#include <darwin/framework/Application.h>

class PhotoPoser : public Robot::Application
{
public:
    PhotoPoser();

    virtual void Process();

private:
    virtual void HandleVideo();

    enum Pose {
        POSE_SITTING,
        POSE_STANDING,
        POSE_STANDING_STRAIGHT
    };
    int pose;
    int lastButton;
};

#endif // PHOTOPOSER_H
