TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    PhotoPoser.cpp

HEADERS += \
    PhotoPoser.h

LIBS += -ldarwin \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lespeak \
