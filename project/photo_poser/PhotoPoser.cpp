#include "PhotoPoser.h"

#include <iostream>
#include <darwin/framework/CM730.h>
#include <darwin/framework/MotionStatus.h>
#include <LinuxDARwIn.h>

using namespace Robot;
using namespace std;

PhotoPoser::PhotoPoser()
{
    UnregisterArgument(Application::SHOW_VIDEO_FLAG);
    UnregisterArgument(Application::VISION_TEST_FLAG);

    lastButton = 0;
    pose = POSE_SITTING;
}

void PhotoPoser::Process()
{
    int newButton = MotionManager::GetButton();

    if(newButton != lastButton && newButton != 0)
    {
        switch(pose)
        {
        case POSE_SITTING:
            pose = POSE_STANDING;
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
            Action::GetInstance()->Finish();
            newButton = 0;

            break;

        case POSE_STANDING:
            pose = POSE_STANDING_STRAIGHT;
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_INIT);
            Action::GetInstance()->Finish();
            newButton = 0;

            break;

        case POSE_STANDING_STRAIGHT:
        default:
            pose = POSE_SITTING;
            Action::GetInstance()->Start(Action::DEFAULT_MOTION_SIT_DOWN);
            Action::GetInstance()->Finish();
            newButton = 0;

            break;
        }
    }

    lastButton = newButton;
    MotionManager::usleep(100);
}

void PhotoPoser::HandleVideo()
{
    cout << "Exiting video thread" << endl;
    pthread_exit(0);
}
