import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# MKS Radians System

# Joints

LeftUpperArmLength = 0.2

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d',aspect='equal')
ax.set_title("DARwIn-OP Kinematics")

robot_torso_shoulder_distance_y = 0.1315
robot_torso_shoulder_distance_z = 0.1950

def DrawZAxis( A, ax):
    zaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.00, 0.00, 0.02, 1.0] ] ).T
    zaxis_d = A.dot( zaxis )
    ax.plot( zaxis_d[0,:], zaxis_d[1,:], zaxis_d[2,:],"b-")

def DrawCoordinateAxis( A, ax ):
    xaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.05, 0.00, 0.00, 1.0] ] ).T
    yaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.00, 0.05, 0.00, 1.0] ] ).T
    zaxis=np.array([ [ 0.0, 0.0, 0.0, 1.0], [ 0.00, 0.00, 0.05, 1.0] ] ).T

    xaxis_d = A.dot( xaxis )
    yaxis_d = A.dot( yaxis )
    zaxis_d = A.dot( zaxis )
    
    ax.plot( xaxis_d[0,:], xaxis_d[1,:], xaxis_d[2,:],"r-")
    ax.plot( yaxis_d[0,:], yaxis_d[1,:], yaxis_d[2,:],"g-")
    ax.plot( zaxis_d[0,:], zaxis_d[1,:], zaxis_d[2,:],"b-")

def Translation( dx, dy, dz ):
    m = np.eye(4)
    m[0,3] = dx
    m[1,3] = dy
    m[2,3] = dz
    m[3,3] = 1.0
    return m

def RotationX( theta ):
    m = np.array([ [ 1.0, 0.0, 0.0, 0.0 ],
                   [ 0.0, math.cos(theta), - math.sin( theta ), 0.0 ],
                   [ 0.0, math.sin(theta), math.cos(theta), 0.0 ],
                   [ 0.0, 0.0, 0.0, 1.0 ] ] )
    return m

def RotationY( theta ):
    m = np.array([ [ math.cos(theta), 0.0, math.sin(theta), 0.0 ],
                   [ 0.0, 1.0, 0.0, 0.0 ],
                   [ - math.sin(theta), 0.0, math.cos(theta), 0.0 ],
                   [ 0.0, 0.0, 0.0, 1.0 ] ] )
    return m

def RotationZ( theta ):
    m = np.array([ [ math.cos(theta), -math.sin( theta ), 0.0, 0.0 ],
                   [ math.sin(theta), math.cos( theta ), 0.0, 0.0 ],
                  [ 0.0, 0.0, 1.0, 0.0 ],
                   [ 0.0, 0.0, 0.0, 1.0 ] ] )
    return m
    
def deg2rad(x):
    return x/180.0*math.pi
    
def rad2deg(x):
    return x/math.pi*180.0
    
class Kinematics:
    "Dimensions for all frames and segments on the DARwIn-OP robot"
    CAMERA_DISTANCE = 33.2/1000.0 #mm
    EYE_TILT_OFFSET_ANGLE = 40.0 #degree
    LEG_SIDE_OFFSET = 37.0/1000.0 #mm
    THIGH_LENGTH = 93.0/1000.0 #mm
    CALF_LENGTH = 93.0/1000.0 #mm
    ANKLE_LENGTH = 33.5/1000.0 #mm
    LEG_LENGTH = 219.5/1000.0 #mm (THIGH_LENGTH + CALF_LENGTH + ANKLE_LENGTH)

    FOOT_LENGTH = 104.0/1000.0 #mm
    SHOULDER_SIDE_OFFSET = (82.0 - 57.5)/1000.0 #mm
    SHOULDER_BOTTOM_OFFSET = 16.0/1000.0 #mm
    ELBOW_OFFSET = 15.0/1000.0 #mm
    UPPER_ARM_LENGTH = 60.0/1000.0 #mm
    LOWER_ARM_LENGTH = 129.0/1000.0 #mm
    DISTANCE_TO_ELBOW = 89.262/1000.0 #mm
    ARM_SERVO_OFFSET = 16.0/1000.0 #mm
    HEAD_TO_SHOULDER = 82.0/1000.0 #mm (y-axis)
    EYE_TO_SHOULDER = 85.0/1000.0 #mm (z-axis)
    TORSO_LENGTH = 122.2/1000.0 #mm (hip to shoulder)
    NECK_LENGTH = 50.5/1000.0 #mm (shoulder to neck)
    NECK_TO_FACE = 34.5/1000.0 #mm (neck to camera)
    HIP_DROP_LENGTH = 30.0/1000.0 #mm (torso to hip pitch motor)
    HIP_WIDTH = 74.0/1000.0 #mm (separation between hip motors)
    ANKLE_TO_TOE = 52.0/1000.0 #mm (ankle bracket ctr to toe)
    
class Arm:
    def __init__(self,flipYAxis=False):
        self.flipYAxis = flipYAxis
    
    def Render(self,shoulderPitch,shoulderRoll,elbow):
        "Draw the arm in 3D.  All parameters are in radians"
        #shoulderPitchHome = RotationX(90) # pitch motor is the origin
        shoulderPitchHome = RotationX(deg2rad(90)).dot(Translation(-0.005,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH,-57.5/1000.0))
        shoulderPitchJoint = shoulderPitchHome.dot(RotationZ(shoulderPitch))

        shoulderRollHome = shoulderPitchJoint.dot(Translation(0.0,-Kinematics.SHOULDER_BOTTOM_OFFSET,-Kinematics.SHOULDER_SIDE_OFFSET).dot(RotationY(deg2rad(90)).dot(RotationZ(deg2rad(90)))))
        shoulderRollJoint = shoulderRollHome.dot(RotationZ(shoulderRoll))

        elbowHome = shoulderRollJoint.dot(Translation(-Kinematics.UPPER_ARM_LENGTH,0,0).dot(RotationX(deg2rad(-90))))
        elbowJoint = elbowHome.dot(RotationZ(elbow))

        fingerHome = elbowJoint.dot(Translation(-Kinematics.LOWER_ARM_LENGTH,0,0))
        fingerJoint = fingerHome
        
        if(self.flipYAxis):
            shoulderPitchHome[1,3] = -shoulderPitchHome[1,3]
            shoulderPitchJoint[1,3] = -shoulderPitchJoint[1,3]
            
            shoulderRollHome[1,3] = -shoulderRollHome[1,3]
            shoulderRollJoint[1,3] = -shoulderRollJoint[1,3]
            
            elbowHome[1,3] = -elbowHome[1,3]
            elbowJoint[1,3] = -elbowJoint[1,3]
            
            fingerHome[1,3] = -fingerHome[1,3]
            #fingerJoint[1,3] = -fingerJoint[1,3]

        psn = [fingerJoint[0,3],fingerJoint[1,3],fingerJoint[2,3]]

        DrawZAxis(shoulderPitchJoint,ax)
        DrawZAxis(shoulderRollJoint,ax)
        DrawZAxis(elbowJoint,ax)
        DrawZAxis(fingerJoint,ax)

        ax.plot( [ shoulderPitchHome[0,3], shoulderRollHome[0,3] ],
                 [ shoulderPitchHome[1,3], shoulderRollHome[1,3] ],
                 [ shoulderPitchHome[2,3], shoulderRollHome[2,3] ], 
                 "y-", linewidth=5.0 )

        ax.plot( [ shoulderRollHome[0,3], elbowHome[0,3] ],
                 [ shoulderRollHome[1,3], elbowHome[1,3] ],
                 [ shoulderRollHome[2,3], elbowHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        ax.plot( [ elbowHome[0,3], fingerHome[0,3] ],
                 [ elbowHome[1,3], fingerHome[1,3] ],
                 [ elbowHome[2,3], fingerHome[2,3] ], 
                 "c-", linewidth=5.0 )
                 
        return psn
    
class Leg:
    def __init__(self,flipYAxis=False):
        self.flipYAxis=flipYAxis
        
    def Render(self,yaw, hipPitch, hipRoll, knee, anklePitch, ankleRoll):
        "Draw the leg in 3D.  All parameters are in radians"
        #hipYawHome = np.eye(4)   # the z-axis goes through the yaw motor already, so nothing to change
        hipYawHome = Translation(0,Kinematics.HIP_WIDTH/2,0)
        hipYawJoint = hipYawHome.dot(RotationZ(yaw))
        
        hipRollHome = hipYawJoint.dot(Translation(0,0,-Kinematics.HIP_DROP_LENGTH).dot(RotationX(deg2rad(90)).dot(RotationY(deg2rad(90)))))
        hipRollJoint = hipRollHome.dot(RotationZ(hipRoll))
        
        hipPitchHome = hipRollJoint.dot(RotationY(deg2rad(-90)))
        hipPitchJoint = hipPitchHome.dot(RotationZ(hipPitch))
        
        kneeHome = hipPitchJoint.dot(Translation(0,-Kinematics.THIGH_LENGTH,0))
        kneeJoint = kneeHome.dot(RotationZ(-knee))
        
        anklePitchHome = kneeJoint.dot(Translation(0,-Kinematics.CALF_LENGTH,0))
        anklePitchJoint = anklePitchHome.dot(RotationZ(anklePitch))
        
        ankleRollHome = anklePitchJoint.dot(RotationY(deg2rad(90)))
        ankleRollJoint = ankleRollHome.dot(RotationZ(-ankleRoll))
        
        footHome = ankleRollJoint.dot(Translation(0,-Kinematics.ANKLE_LENGTH,0))
        footJoint = footHome
        
        if(self.flipYAxis):
            hipYawHome[1,3] = -hipYawHome[1,3]
            hipYawJoint[1,3] = -hipYawJoint[1,3]
            
            hipRollHome[1,3] = -hipRollHome[1,3]
            hipRollJoint[1,3] = -hipRollJoint[1,3]
            
            hipPitchHome[1,3] = -hipPitchHome[1,3]
            hipPitchJoint[1,3] = -hipPitchJoint[1,3]
            
            kneeHome[1,3] = -kneeHome[1,3]
            kneeJoint[1,3] = -kneeJoint[1,3]
            
            anklePitchHome[1,3] = -anklePitchHome[1,3]
            anklePitchJoint[1,3] = -anklePitchJoint[1,3]
            
            ankleRollHome[1,3] = -ankleRollHome[1,3]
            ankleRollJoint[1,3] = -ankleRollJoint[1,3]
            
            footHome[1,3] = -footHome[1,3]
            #footJoint[1,3] = -footJoint[1,3]
        
        psn = [footJoint[0,3],footJoint[1,3],footJoint[2,3]]
        
        DrawZAxis(hipYawJoint,ax)
        DrawZAxis(hipRollJoint,ax)
        DrawZAxis(hipPitchJoint,ax)
        DrawZAxis(kneeJoint,ax)
        DrawZAxis(anklePitchJoint,ax)
        DrawZAxis(ankleRollJoint,ax)
        DrawZAxis(footJoint,ax)

        ax.plot( [ hipYawHome[0,3], hipRollHome[0,3] ],
                 [ hipYawHome[1,3], hipRollHome[1,3] ],
                 [ hipYawHome[2,3], hipRollHome[2,3] ], 
                 "y-", linewidth=5.0 )

        ax.plot( [ hipRollHome[0,3], hipPitchHome[0,3] ],
                 [ hipRollHome[1,3], hipPitchHome[1,3] ],
                 [ hipRollHome[2,3], hipPitchHome[2,3] ], 
                 "c-", linewidth=5.0 )
                 
        ax.plot( [ hipPitchHome[0,3], kneeHome[0,3] ],
                 [ hipPitchHome[1,3], kneeHome[1,3] ],
                 [ hipPitchHome[2,3], kneeHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        ax.plot( [ kneeHome[0,3], anklePitchHome[0,3] ],
                 [ kneeHome[1,3], anklePitchHome[1,3] ],
                 [ kneeHome[2,3], anklePitchHome[2,3] ], 
                 "c-", linewidth=5.0 )

        ax.plot( [ anklePitchHome[0,3], ankleRollHome[0,3] ],
                 [ anklePitchHome[1,3], ankleRollHome[1,3] ],
                 [ anklePitchHome[2,3], ankleRollHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        ax.plot( [ ankleRollHome[0,3], footHome[0,3] ],
                 [ ankleRollHome[1,3], footHome[1,3] ],
                 [ ankleRollHome[2,3], footHome[2,3] ], 
                 "y-", linewidth=5.0 )
    
        return psn
    
class Head:
    def __init__(self):
        pass
        
    def Render(self,pan,tilt):
        "Draw the head in 3D.  Parameters are in radians"
        
        panHome = Translation(0,0,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH)
        panJoint = panHome.dot(RotationZ(pan))
        
        tiltHome = panJoint.dot(Translation(0,0,Kinematics.NECK_LENGTH).dot(RotationX(deg2rad(90))))
        tiltJoint = tiltHome.dot(RotationZ(tilt))
        
        cameraHome = tiltJoint.dot(Translation(Kinematics.NECK_TO_FACE,0,0))
        
        psn = [cameraHome[0][3],cameraHome[1][3],cameraHome[2][3]]
        
        DrawZAxis(panHome,ax)
        DrawZAxis(tiltHome,ax)
        DrawZAxis(cameraHome,ax)
        
        ax.plot( [ panHome[0,3], tiltHome[0,3] ],
                 [ panHome[1,3], tiltHome[1,3] ],
                 [ panHome[2,3], tiltHome[2,3] ], 
                 "y-", linewidth=5.0 )
        
        ax.plot( [ tiltHome[0,3], cameraHome[0,3] ],
                 [ tiltHome[1,3], cameraHome[1,3] ],
                 [ tiltHome[2,3], cameraHome[2,3] ], 
                 "m-", linewidth=5.0 )
                 
        return psn
    
A = np.eye(4)
DrawCoordinateAxis( A, ax ) # origin

# draw a reference box because it looks pretty?
#BOX_SIZE = 0.3
#ax.plot([-BOX_SIZE,  BOX_SIZE],
#        [-BOX_SIZE, -BOX_SIZE],
#        [-BOX_SIZE, -BOX_SIZE],
#        'r-')
#ax.plot([-BOX_SIZE, -BOX_SIZE],
#        [-BOX_SIZE,  BOX_SIZE],
#        [-BOX_SIZE, -BOX_SIZE],
#        'g-')
#ax.plot([-BOX_SIZE, -BOX_SIZE],
#        [-BOX_SIZE, -BOX_SIZE],
#        [-BOX_SIZE,  BOX_SIZE],
#        'b-')

leftArm = Arm(False)
rightArm = Arm(True)    
leftLeg = Leg(False)
rightLeg = Leg(True)
head = Head()

# angles for all joints
# these are in degrees (but are converted in the parameters below)
leftShoulderPitch = -45
leftShoulderRoll = 10
leftElbow = 90

rightShoulderPitch = -45
rightShoulderRoll = 10
rightElbow = 90

leftHipYaw = 10
leftHipPitch = 22.5
leftHipRoll = 5
leftKnee = 45
leftAnklePitch = 22.5
leftAnkleRoll = 0

rightHipYaw = 10
rightHipPitch = 22.5
rightHipRoll = 5
rightKnee = 45
rightAnklePitch = 22.5
rightAnkleRoll = 0

pan = 0
tilt = 0

# render the limbs
print "Left Arm :",leftArm.Render(deg2rad(leftShoulderPitch),deg2rad(leftShoulderRoll),deg2rad(leftElbow))
print "Right Arm:",rightArm.Render(deg2rad(rightShoulderPitch),deg2rad(rightShoulderRoll),deg2rad(rightElbow))
print "Left Leg :",leftLeg.Render(deg2rad(leftHipYaw),deg2rad(leftHipPitch),deg2rad(leftHipRoll),deg2rad(leftKnee),deg2rad(leftAnklePitch),deg2rad(leftAnkleRoll))
print "Right Leg:",rightLeg.Render(deg2rad(rightHipYaw),deg2rad(rightHipPitch),deg2rad(rightHipRoll),deg2rad(rightKnee),deg2rad(rightAnklePitch),deg2rad(rightAnkleRoll))
print "Head:",head.Render(deg2rad(pan),deg2rad(tilt))

plt.show()
