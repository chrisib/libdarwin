#include <iostream>
#include <LinuxDARwIn.h>
#include "darwin/framework/LeftArm.h"
#include "darwin/framework/RightArm.h"
#include "darwin/framework/LeftLeg.h"
#include "darwin/framework/RightLeg.h"
#include <cstring>
#include <iostream>
#include <algorithm>

#define MOTION_FILE "motion.bin"

using namespace std;
using namespace Robot;

bool testMode = false;

void printHelp()
{
    cout << endl << "Perform interactive forward and inverse kinematics tests with the Darwin" << endl <<
            "\t--test\tDo not initialize the CM730" << endl;
}

int main(int argc, char** argv)
{
    for(int i=1; i<argc; i++)
    {
        if(!strcmp("--help", argv[i]))
        {
            printHelp();
            return 0;
        }
        else if(!strcmp("--test", argv[i]))
			testMode = true;
    }

    // initialize the robot
    if(!testMode)
    {
		LinuxDARwIn::Initialize(MOTION_FILE, Action::DEFAULT_MOTION_STAND_UP);
	}
	// initialize the remaining modules
	MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
	MotionManager::GetInstance()->AddModule(RightArm::GetInstance());
	MotionManager::GetInstance()->AddModule(LeftLeg::GetInstance());
	MotionManager::GetInstance()->AddModule(RightLeg::GetInstance());

	LeftArm::GetInstance()->SetEnable(true,true);
	RightArm::GetInstance()->SetEnable(true,true);
	LeftLeg::GetInstance()->SetEnable(true,true);
	RightLeg::GetInstance()->SetEnable(true,true);

    cout << "Usage:" << endl <<
            ">> {L|R}{L|A} X Y Z" << endl <<
            "   e.g. LA 10 15 -5 (to move left arm to (10,15,-5)" << endl <<
            "   X Y Z are in mm" << endl <<
            ">> e {L|R}{L|A}" << endl <<
            "   e.g. e LL (to extend the left leg until the foot hits something" << endl <<
            "              or the leg is straight)" << endl;

    string limbCode;
    double x,y,z;
    Point3D pt;
    Limb *limb;
    for(;;)
    {
        cout << ">> ";
        cin >> limbCode;

        std::transform(limbCode.begin(), limbCode.end(), limbCode.begin(), ::tolower);

        if(limbCode != "e")
        {
            cin >> x >> y >> z;
			pt = Point3D(x,y,z);

            if(limbCode=="la")
            {
                limb = LeftArm::GetInstance();
                cout << "Moving left arm to (" << x << "," << y << "," << z << ")" << endl;

                LeftArm::GetInstance()->SetGoalPosition(pt);
            }
            else if(limbCode=="ra")
            {
                limb = RightArm::GetInstance();
                cout << "Moving right arm to (" << x << "," << y << "," << z << ")" << endl;

                RightArm::GetInstance()->SetGoalPosition(pt);
            }
            else if(limbCode=="ll")
            {
                limb = LeftLeg::GetInstance();
                cout << "Moving left leg to (" << x << "," << y << "," << z << ")" << endl;

                LeftLeg::GetInstance()->SetGoalPosition(pt,0);
            }
            else if(limbCode=="rl")
            {
                limb = RightLeg::GetInstance();
                cout << "Moving right leg to (" << x << "," << y << "," << z << ")" << endl;

                RightLeg::GetInstance()->SetGoalPosition(pt,0);
            }
            else
            {
                limb = NULL;
                cout << "Unknown limb code: " << limbCode << endl;
            }

            do
            {
                Point3D p = limb->GetPosition();
                cout << p.X << " " << p.Y << " " << p.Z << endl;
                MotionManager::msleep(100);
            }while(limb!=NULL && limb->IsRunning());
        }
        else
        {
            cin >> limbCode;

            if(limbCode == "ll")
            {
                limb = LeftLeg::GetInstance();
                cout << "Extending left leg" << endl;

                LeftLeg::GetInstance()->Extend();
            }
            else if(limbCode == "rl")
            {
                limb = RightLeg::GetInstance();
                cout << "Extending right leg" << endl;

                RightLeg::GetInstance()->Extend();
            }
            else if(limbCode == "la")
            {
                limb = LeftArm::GetInstance();
                cout << "Extending left arm" << endl;

                LeftArm::GetInstance()->Extend();
            }
            else if(limbCode == "ra")
            {
                limb = RightArm::GetInstance();
                cout << "Extending right arm" << endl;

                RightArm::GetInstance()->Extend();
            }
            else
            {
                limb = NULL;
                cout << "Unknown limb code: " << limbCode << endl;
            }

            do
            {
                Point3D p = limb->GetPosition();
                cout << p.X << " " << p.Y << " " << p.Z << endl;
                MotionManager::msleep(100);
            }while(limb!=NULL && limb->IsRunning());
        }
    }

    return 0;
}

