#include <LinuxDARwIn.h>
#include <iostream>
#include <darwin/framework/Walking.h>
#include <darwin/framework/Action.h>
#include <darwin/framework/MotionStatus.h>

using namespace Robot;
using namespace std;

int main()
{
    minIni* ini=NULL;
    
	cerr << "Initializing..." << endl;
	LinuxDARwIn::ChangeCurrentDir();
	LinuxDARwIn::Initialize("../../data/motion_4096.bin",Action::DEFAULT_MOTION_SIT_DOWN);
    //LinuxDARwIn::InitializeSignals();
    //Voice::Initialize();
	cerr << "done" << endl << endl;
    
    cerr << "Standing up..." << endl;
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
	Action::GetInstance()->Finish();
    cerr << "done" << endl << endl;
    
    for(;;)
    {
        cout << "Gyro: " << MotionStatus::FB_GYRO << " " << MotionStatus::RL_GYRO << " " << MotionStatus::Z_GYRO << "   Accel: " << MotionStatus::FB_ACCEL << " " << MotionStatus::RL_ACCEL << " " << MotionStatus::Z_ACCEL << endl;
    }
        
    return 0;
}
