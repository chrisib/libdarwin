#include <LinuxDARwIn.h>
#include <iostream>
#include <darwin/framework/Walking.h>
#include <darwin/framework/Action.h>
#include <darwin/framework/MotionStatus.h>

using namespace Robot;
using namespace std;

int main()
{
    minIni* ini=NULL;
    
	cerr << "Initializing..." << endl;
	LinuxDARwIn::ChangeCurrentDir();
	LinuxDARwIn::Initialize("../../data/motion_4096.bin",Action::DEFAULT_MOTION_SIT_DOWN);
    //LinuxDARwIn::InitializeSignals();
    //Voice::Initialize();
	cerr << "done" << endl << endl;
    
    
    
    cerr << "Setting up Walking module..." << endl;
    MotionManager::GetInstance()->AddModule((MotionModule*)Walking::GetInstance());
    //cerr << Walking::GetInstance()->PUSH_HIP_ROLL_GAIN << endl <<
    //        Walking::GetInstance()->PUSH_HIP_PITCH_GAIN << endl <<
    //        Walking::GetInstance()->PUSH_SHOULDER_ROLL_GAIN << endl;
    cerr << "done" << endl << endl;
    
    cerr << "Standing up..." << endl;
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
	Action::GetInstance()->Finish();
    cerr << "done" << endl << endl;
	
	Walking::GetInstance()->m_Joint.SetEnableBody(true,true);
    
    for(;;)
    {
        if(ini!=NULL)
            delete ini;
        
        cerr << "Loading ini..." << endl;
        ini = new minIni("config.ini");
        Walking::GetInstance()->LoadINISettings(ini);
        Walking::GetInstance()->X_MOVE_AMPLITUDE = ini->getd("Options","x_amplitude",20);
        Walking::GetInstance()->Y_MOVE_AMPLITUDE = ini->getd("Options","y_amplitude",0);
        Walking::GetInstance()->A_MOVE_AMPLITUDE = ini->getd("Options","a_amplitude",0);
        Walking::GetInstance()->DATA_RECORD = ini->geti("Options","data_record",0);
        
        cerr << "\tParameters:" << endl
             << "\t\tX: " << Walking::GetInstance()->X_MOVE_AMPLITUDE << endl
             << "\t\tY: " << Walking::GetInstance()->Y_MOVE_AMPLITUDE << endl
             << "\t\tA: " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl;
        
        cerr << "done" << endl << endl;
        cerr << "Press the middle button" << endl;
        
        MotionManager::WaitButton(CM730::MIDDLE_BUTTON);
        Walking::GetInstance()->Start();
        
        MotionManager::WaitButton(CM730::LEFT_BUTTON);
        Walking::GetInstance()->Stop();
    }
        
    return 0;
}
