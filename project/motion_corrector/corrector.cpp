/*
 * main.cpp
 *
 * JDJ
 * Reverse a motion (or series of motions) from left to right in a motion.bin file
 * Eg. Flipping a motion that spans pages 230-239:
 * ./flipper --from 230 --to 239 --motors 20 ~/robocup/player/motion.bin
 * OR making use of defaults:
 * ./flipper --from 230 --to 239
 */

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <cstring>
#include <iostream>
#include <sstream>
#include "LinuxDARwIn.h"
#include "Action.h"
#include "AX12.h"
#include "RX28.h"
#include "MX28.h"

using namespace std;
using namespace Robot;

int from = 1;
int to = Action::MAXNUM_PAGE-1;
int motorNum = -1;
int amount = 0;
char *motionFile = NULL;

int main(int argc, char** argv)
{
    // parse command-line argument
    for(int i=1; i<argc; i++)
    {
		if(!strcmp("--from",argv[i]))
		{
			i++;
			stringstream convert(argv[i]);
			if (!(convert >> from)) {
			    cout << "Invalid input: " << argv[i] << endl;
			    exit(-1);
			}
		}
		else if(!strcmp("--to",argv[i]))
		{
			i++;
			stringstream convert(argv[i]);
			if (!(convert >> to)) {
				cout << "Invalid input: " << argv[i] << endl;
				exit(-1);
			}
		}
		else if(!strcmp("--motor",argv[i]))
		{
			i++;
			stringstream convert(argv[i]);
			if (!(convert >> motorNum)) {
				cout << "Invalid input: " << argv[i] << endl;
				exit(-1);
			}
		}
		else if(!strcmp("--amount",argv[i]))
		{
			i++;
			stringstream convert(argv[i]);
			if (!(convert >> amount)) {
				cout << "Invalid input: " << argv[i] << endl;
				exit(-1);
			}
		}
		else if(!strcmp("--help",argv[i]))
		{
			cout << endl <<
			"Usage" << endl <<
			"\t" << argv[0] << " [--from START_LINE_NUM] [--to END_LINE_NUM] [--motor NUMBER_OF_MOTOR] [--amount CORRECTION_AMOUNT] [--help] [MOTION_FILE]" << endl;
			exit(0);
		}
		else
		{
			motionFile = argv[i];
		}
	}

	if(from<0 || to<0 || from>=Action::MAXNUM_PAGE || to>=Action::MAXNUM_PAGE || from>to || motorNum<=0 || amount==0 || motionFile==NULL) {
		cout << "Invalid arguments, program closing" << endl;
		exit(-1);
	} else
		cout << "Correcting motor " << motorNum << " in " << motionFile << " from " << from << " to " << to << " by an amount of " << amount << endl;

	//begin doing useful stuff
	Action::GetInstance()->LoadFile(motionFile);

	int j,k;
	int position;
	Action::PAGE Page;
	for(k=from;k<=to;k++)
	{
		Action::GetInstance()->LoadPage(k, &Page);
		for(j=0;j<Action::MAXNUM_STEP;j++)
		{
			Page.step[j].position[motorNum] += amount;
		}
		Action::GetInstance()->SavePage(k, &Page);
	}

	cout << "Finished successfully" << endl;
	exit(0);
}
