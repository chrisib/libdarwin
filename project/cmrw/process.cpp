//--------------------------------------------------
// PROJECT: FIRA2011 CM730 Read/Write
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//          Chris Iverach-Brereton <ve4cib@gmail.com>
//--------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
// Project and C/C++ libraries
#include "process.h"
#include "parser.h"
#include <stdlib.h>
#include <stdio.h>
// Robot
#include "LinuxDARwIn.h"
using namespace Robot;


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define DEVICE_NAME "/dev/ttyUSB0"
#define SLEEP_INTERVAL 25

#define COMMAND_EXIT        "exit"
#define COMMAND_HELP        "help"
#define COMMAND_CLEAR       "clear"
#define COMMAND_ADDRESS     "address"
#define COMMAND_PING        "ping"
#define COMMAND_READ        "read"
#define COMMAND_WRITE       "write"
#define COMMAND_RESET       "reset"
#define COMMAND_SYNCWRITE   "syncwrite"

#define ARGUMENT_ADDRESS    "[ADDR]"
#define ARGUMENT_COLLECTION "[...]"
#define ARGUMENT_ID         "[ID]"
#define ARGUMENT_NBYTES     "[NBYTES]"
#define ARGUMENT_PRODUCT    "[PRODUCT]"

#define ARGUMENT_VALUE_CM           "CM"
#define ARGUMENT_VALUE_MX           "MX"
#define ARGUMENT_VALUE_RX           "RX"
#define ARGUMENT_VALUE_AX			"AX"
#define ARGUMENT_VALUE_BROADCAST    "BROADCAST"

#define ADDRESS_PRINT_HEADER "Addresses of %s\n"
#define ADDRESS_PRINT_COLUMN "| Name                   | Address |\n"
#define ADDRESS_PRINT_FORMAT "| %-22s |   %-3d   |\n"
#define ADDRESS_PRINT_SEPARATOR "+------------------------+---------+\n"


//------------------------------------------------------------------------------
// FUNCTIONS AND METHODS
//------------------------------------------------------------------------------
int stricmp( const char* str1, const char* str2 )
{
    int result;

    while( (result = tolower( *str1 ) - tolower( *str2 )) == 0 && *(++str1) != '\0' )
    {
        ++str2;
    }
    return result;
}


Process::Process( bool debug )
{
    _status = STATUS_ERROR;
    _serial = new LinuxCM730( DEVICE_NAME); //new LinuxSerialPort( DEVICE_NAME );
    _cm730 = new CM730( _serial );
    _debug = debug;
    _cmdcount = 100;
    if( _debug || _cm730->Connect( ) )
    {
        _status = STATUS_INITIALIZED;
    }
    else
    {
        fprintf( stdout, "Fail to initialize\n" );
    }
}


Process::~Process( void )
{
    delete( _cm730 );
    delete( _serial );
}


void Process::update( void )
{
    switch( _status )
    {
    case STATUS_INITIALIZED:
        _argc = 0;
        processHelp( _argc, _argv );
        _status = STATUS_PROCESSING;
        break;
    case STATUS_PROCESSING:
        fprintf( stdout, "Command %%%d> ", ++_cmdcount );
        if( fgets( _line, MAX_LINECHARS, stdin ) )
        {
            _argc = parseLine( _line, MAX_ARGUMENTS, _argv );
            process( _argc, _argv );
        }
        break;
    case STATUS_FINISHED:
        _status = STATUS_ERROR;
        break;
    case STATUS_ERROR:
    default:
        break;
    }
}


void Process::process( int argc, char* argv[] )
{
    if( argc <= 0 )
    {
    }
    else if( stricmp( argv[0], COMMAND_EXIT ) == 0 )
    {
        _status = STATUS_FINISHED;
    }
    else if( stricmp( argv[0], COMMAND_HELP ) == 0 )
    {
        processHelp( argc - 1, argv + 1 );
    }
    else if( stricmp( argv[0], COMMAND_CLEAR ) == 0 )
    {
        processClear( argc - 1, argv + 1 );
    }
    else if( stricmp( argv[0], COMMAND_ADDRESS ) == 0 )
    {
        processAddress( argc - 1, argv + 1 );
    }
    else if( stricmp( argv[0], COMMAND_PING ) == 0 )
    {
        processPing( argc - 1, argv + 1 );
    }
    else if( stricmp( argv[0], COMMAND_READ ) == 0 )
    {
        processRead( argc - 1, argv + 1 );
    }
    else if( stricmp( argv[0], COMMAND_WRITE ) == 0 )
    {
        processWrite( argc - 1, argv + 1 );
    }
    else if( stricmp( argv[0], COMMAND_RESET ) == 0 )
    {
        processReset( argc - 1, argv + 1 );
    }
    else if( stricmp( argv[0], COMMAND_SYNCWRITE ) == 0 )
    {
        processSyncWrite( argc - 1, argv + 1 );
    }
    else
    {
        fprintf( stdout, "Unknown command: %s\n", argv[0] );
    }
}


void Process::processHelp( int argc, char* argv[] )
{
    fprintf( stdout, "Command Line Tool to control CM730\n" );
    fprintf( stdout, "  %-10s %-19s Show address\n", COMMAND_ADDRESS, ARGUMENT_PRODUCT );
    fprintf( stdout, "  %-10s %-19s Ping a servo\n", COMMAND_PING, ARGUMENT_ID );
    fprintf( stdout, "  %-10s %s%s%-9s Read data\n", COMMAND_READ, ARGUMENT_ID, ARGUMENT_ADDRESS, ARGUMENT_NBYTES );
    fprintf( stdout, "  %-10s %s%s%-9s Write data\n", COMMAND_WRITE, ARGUMENT_ID, ARGUMENT_ADDRESS, ARGUMENT_COLLECTION );
    fprintf( stdout, "  %-10s %-19s Reset to factory default (CAUTION!)\n", COMMAND_RESET, ARGUMENT_ID );
    fprintf( stdout, "  %-10s %s%s%-5s Execute sync-write command on board\n", COMMAND_SYNCWRITE, ARGUMENT_ADDRESS, ARGUMENT_NBYTES, ARGUMENT_COLLECTION );
    fprintf( stdout, "  %-30s Clear the screen\n", COMMAND_CLEAR );
    fprintf( stdout, "  %-30s Print this page\n", COMMAND_HELP );
    fprintf( stdout, "  %-30s Finish this program\n", COMMAND_EXIT );
    fprintf( stdout, "\n" );
    fprintf( stdout, "Arguments\n" );
    fprintf( stdout, "  %-10s Product prefix (%s | %s | %s | %s)\n", ARGUMENT_PRODUCT, ARGUMENT_VALUE_CM, ARGUMENT_VALUE_MX, ARGUMENT_VALUE_RX, ARGUMENT_VALUE_AX );
    fprintf( stdout, "  %-10s ID to control (%s | %s | Byte)\n", ARGUMENT_ID, ARGUMENT_VALUE_CM, ARGUMENT_VALUE_BROADCAST );
    fprintf( stdout, "  %-10s Address value (Byte)\n", ARGUMENT_ADDRESS );
    fprintf( stdout, "  %-10s Number of byte (Byte)\n", ARGUMENT_NBYTES );
    fprintf( stdout, "  %-10s Collection of Bytes\n", ARGUMENT_COLLECTION );
    fprintf( stdout, "\n" );
    fprintf( stdout, "Integers can be entered as digits (eg, 50) or hex (eg, 0x32)\n" );
    fprintf( stdout, "Commands and arguments are case insensitive\n" );
    fprintf( stdout, "\n" );
}


void Process::processClear( int argc, char* argv[] )
{
    for( int i = 0; i < 0xFF; ++i )
    {
        fprintf( stdout, "\n" );
    }
}


void Process::processAddress( int argc, char* argv[] )
{
    if( argc <= 0 )
    {
        fprintf( stdout, "Please enter %s to execute '%s' command\n", ARGUMENT_PRODUCT, COMMAND_ADDRESS );
    }
    else if( stricmp( argv[0], ARGUMENT_VALUE_CM ) == 0 )
    {
        printAddrCM( );
    }
    else if( stricmp( argv[0], ARGUMENT_VALUE_MX ) == 0 )
    {
        printAddrMX( );
    }
    else if( stricmp( argv[0], ARGUMENT_VALUE_RX ) == 0 )
    {
        printAddrRX( );
    }
    else if( stricmp( argv[0], ARGUMENT_VALUE_AX ) == 0 )
    {
		printAddrAX( );
	}
    else
    {
        fprintf( stdout, "Please enter valid byte value for %s\n", ARGUMENT_PRODUCT );
    }
}


void Process::processPing( int argc, char* argv[] )
{
    unsigned char id = CM730::ID_CM;
    int index = -1;
    int error = 0;
    int result;

    if( argId( COMMAND_PING, ++index, argc, argv, &id ) )
    {
        fprintf( stdout, "Execute '%s' command. . . ", COMMAND_PING );
        result = _cm730->Ping( id, &error );
        printResult( result, error );
        fprintf( stdout, "\n" );
    }
}


void Process::processRead( int argc, char* argv[] )
{
    unsigned char buffer[BUFFER_SIZE] = {0};
    unsigned char id = CM730::ID_CM;
    unsigned char addr = 0;
    unsigned char nbytes = 0;
    int index = 0;
    int error = 0;
    int result;

    if( argId( COMMAND_READ, index, argc, argv, &id ) &&
        argByte( COMMAND_READ, ARGUMENT_ADDRESS, ++index, argc, argv, &addr ) &&
        argByte( COMMAND_READ, ARGUMENT_NBYTES, ++index, argc, argv, &nbytes, false ) )
    {
        fprintf( stdout, "Execute '%s' command. . . ", COMMAND_READ );
        result = _cm730->ReadData( id, addr, addr + (nbytes - 1), buffer, &error );
        printResult( result, error );
        if( result == CM730::SUCCESS )
        {
            fprintf( stdout, "\n0x%02X", buffer[0] );
            for( index = 1; index < nbytes; ++index )
            {
                fprintf( stdout, " 0x%02X", buffer[index] );
            }
        }
        fprintf( stdout, "\n" );
    }
}


void Process::processWrite( int argc, char* argv[] )
{
    unsigned char buffer[BUFFER_SIZE] = {0};
    unsigned char id = CM730::ID_CM;
    unsigned char addr = 0;
    unsigned char nbytes = 0;
    int index = 0;
    int error = 0;
    int result;

    if( argId( COMMAND_WRITE, index, argc, argv, &id ) &&
        argByte( COMMAND_WRITE, ARGUMENT_ADDRESS, ++index, argc, argv, &addr ) )
    {
        fprintf( stdout, "Execute '%s' command. . .\n", COMMAND_WRITE );
        fprintf( stdout, "Writing. . ." );
        for( index = index + 1; index < argc; ++index, ++nbytes )
        {
            argByte( 0, 0, index, argc, argv, &(buffer[nbytes]), true, false );
            fprintf( stdout, " 0x%02X", buffer[nbytes] );
        }
        fprintf( stdout, "\n" );
        if( nbytes <= 0 )
        {
            fprintf( stdout, "No data to write\n" );
        }
        else
        {
            result = _cm730->WriteData( id, addr, buffer, nbytes, &error );
            printResult( result, error );
            fprintf( stdout, "\n" );
        }
    }
}


void Process::processReset( int argc, char* argv[] )
{
    char buffer[BUFFER_SIZE];
    unsigned char id = CM730::ID_CM;
    int index = -1;
    int error = 0;
    int result;

    if( argByte( COMMAND_RESET, ARGUMENT_ID, ++index, argc, argv, &id ) )
    {
        fprintf( stdout, "Are you sure to reset servo ID=%d? (y or n) ", id );
        if( fgets( buffer, BUFFER_SIZE, stdin ) && (buffer[0] == 'y' || buffer[0] == 'Y') )
        {
            fprintf( stdout, "Execute '%s' command. . . ", COMMAND_RESET );
            result = _cm730->Reset( id, &error );
            printResult( result, error );
            fprintf( stdout, "\n" );
        }
    }
}


void Process::processSyncWrite( int argc, char* argv[] )
{
    unsigned char addr = 0;
    unsigned char nbytes = 0;
    int index = -1;
    int length = 0;
    int buffer[BUFFER_SIZE] = {0};
    int result;

    if( argByte( COMMAND_SYNCWRITE, ARGUMENT_ADDRESS, ++index, argc, argv, &addr ) &&
        argByte( COMMAND_SYNCWRITE, ARGUMENT_NBYTES, ++index, argc, argv, &nbytes, false ) )
    {
        fprintf( stdout, "Execute '%s' command. . .\n", COMMAND_SYNCWRITE );
        fprintf( stdout, "Writing. . ." );
        for( index = index + 1; index < argc; ++index, ++length )
        {
            argInt( 0, 0, index, argc, argv, &(buffer[length]), false );
            fprintf( stdout, " 0x%02X", buffer[length] );
        }
        fprintf( stdout, "\n" );
        if( length <= 0 )
        {
            fprintf( stdout, "No data to write\n" );
        }
        else
        {
            result = _cm730->SyncWrite( addr, nbytes, length, buffer );
            printResult( result, 0 );
            fprintf( stdout, "\n" );
        }
    }
}


bool Process::argId( const char* command, int index, int argc, char* argv[], unsigned char* value, bool prtErrMsg )
{
    bool result = false;

    if( argc <= index )
    {
        *value = 0;
        if( prtErrMsg )
        {
            fprintf( stdout, "Please enter %s to execute '%s' command\n", ARGUMENT_ID, command );
        }
    }
    else if( stricmp( argv[index], ARGUMENT_VALUE_CM ) == 0 )
    {
        *value = CM730::ID_CM;
        result = true;
    }
    else if( stricmp( argv[index], ARGUMENT_VALUE_BROADCAST ) == 0 )
    {
        *value = CM730::ID_BROADCAST;
        result = true;
    }
    else
    {
        result = argByte( command, ARGUMENT_ID, index, argc, argv, value, true, prtErrMsg );
    }
    return result;
}


bool Process::argByte( const char* command, const char* argname, int index, int argc, char* argv[], unsigned char* value, bool allowZero, bool prtErrMsg )
{
    int okay = 0;

    if( argc <= index )
    {
        *value = 0;
        if( prtErrMsg )
        {
            fprintf( stdout, "Please enter %s to execute '%s' command\n", argname, command );
        }
    }
    else
    {
        *value = parseByte( argv[index], &okay );
        if( okay && !allowZero && *value <= 0 )
        {
            okay = false;
        }
        if( !okay )
        {
            if( prtErrMsg )
            {
                fprintf( stdout, "Please enter valid byte value for %s\n", argname );
            }
        }
    }
    return okay != 0;
}


bool Process::argInt( const char* command, const char* argname, int index, int argc, char* argv[], int* value, bool prtErrMsg )
{
    int okay = 0;

    if( argc <= index )
    {
        *value = 0;
        if( prtErrMsg )
        {
            fprintf( stdout, "Please enter %s to execute '%s' command\n", argname, command );
        }
    }
    else
    {
        *value = parseInt( argv[index], &okay );
        if( !okay )
        {
            if( prtErrMsg )
            {
                fprintf( stdout, "Please enter valid byte value for %s\n", argname );
            }
        }
    }
    return okay != 0;
}


void Process::printAddrCM( void )
{
    fprintf( stdout, ADDRESS_PRINT_HEADER, "CM730" );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_COLUMN );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_LOW", 0 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_HIGH", 1 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "VERSION", 2 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ID", 3 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "BAUD_RATE", 4 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_DELAY_TIME", 5 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_LEVEL", 16 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "DXL_POWER", 24 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED_PANNEL", 25 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED_HEAD_LOW", 26 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED_HEAD_HIGH", 27 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED_EYE_LOW", 28 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED_EYE_HIGH", 29 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "BUTTON", 30 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GYROSCOPE_Z_LOW", 38 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GYROSCOPE_Z_HIGH", 39 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GYROSCOPE_Y_LOW", 40 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GYROSCOPE_Y_HIGH", 41 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GYROSCOPE_X_LOW", 42 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GYROSCOPE_X_HIGH", 43 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ACCELEROMETER_X_LOW", 44 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ACCELEROMETER_X_HIGH", 45 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ACCELEROMETER_Y_LOW", 46 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ACCELEROMETER_Y_HIGH", 47 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ACCELEROMETER_Z_LOW", 48 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ACCELEROMETER_Z_HIGH", 49 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "VOLTAGE", 50 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LEFT_MIC_LOW", 51 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LEFT_MIC_HIGH", 52 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC2_LOW", 53 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC2_HIGH", 54 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC3_LOW", 55 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC3_HIGH", 56 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC4_LOW", 57 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC4_HIGH", 58 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC5_LOW", 59 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC5_HIGH", 60 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC6_LOW", 61 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC6_HIGH", 62 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC7_LOW", 63 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC7_HIGH", 64 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC8_LOW", 65 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC8_HIGH", 66 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RIGHT_MIC_LOW", 67 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RIGHT_MIC_HIGH", 68 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC10_LOW", 69 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC10_HIGH", 70 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC11_LOW", 71 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC11_HIGH", 72 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC12_LOW", 73 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC12_HIGH", 74 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC13_LOW", 75 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC13_HIGH", 76 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC14_LOW", 77 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC14_HIGH", 78 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC15_LOW", 79 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ADC15_HIGH", 80 );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
}


void Process::printAddrMX( void )
{
    fprintf( stdout, ADDRESS_PRINT_HEADER, "MX-28M" );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_COLUMN );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_LOW", 0 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_HIGH", 1 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "VERSION", 2 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ID", 3 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "BAUD_RATE", 4 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_DELAY_TIME", 5 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_ANGLE_LIMIT_LOW", 6 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_ANGLE_LIMIT_HIGH", 7 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_ANGLE_LIMIT_LOW", 8 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_ANGLE_LIMIT_HIGH", 9 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "HIGH_LIMIT_TEMPERATURE", 11 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LOW_LIMIT_VOLTAGE", 12 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "HIGH_LIMIT_VOLTAGE", 13 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MAX_TORQUE_LOW", 14 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MAX_TORQUE_HIGH", 15 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_LEVEL", 16 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ALARM_LED", 17 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ALARM_SHUTDOWN", 18 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_ENABLE", 24 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED", 25 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "P_GAIN", 26 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "I_GAIN", 27 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "D_GAIN", 28 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GOAL_POSITION_LOW", 30 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GOAL_POSITION_HIGH", 31 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING_SPEED_LOW", 32 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING_SPEED_HIGH", 33 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_LIMIT_LOW", 34 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_LIMIT_HIGH", 35 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_POSITION_LOW", 36 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_POSITION_HIGH", 37 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_SPEED_LOW", 38 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_SPEED_HIGH", 39 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_LOAD_LOW", 40 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_LOAD_HIGH", 41 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_VOLTAGE", 42 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_TEMPERATURE", 43 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "REGISTERED_INSTRUCTION", 44 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING", 46 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LOCK", 47 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PUNCH_LOW", 48 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PUNCH_HIGH", 49 );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
}


void Process::printAddrRX( void )
{
    fprintf( stdout, ADDRESS_PRINT_HEADER, "RX-28M" );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_COLUMN );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_LOW", 0 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_HIGH", 1 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "VERSION", 2 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ID", 3 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "BAUD_RATE", 4 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_DELAY_TIME", 5 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_ANGLE_LIMIT_LOW", 6 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_ANGLE_LIMIT_HIGH", 7 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_ANGLE_LIMIT_LOW", 8 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_ANGLE_LIMIT_HIGH", 9 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "HIGH_LIMIT_TEMPERATURE", 11 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LOW_LIMIT_VOLTAGE", 12 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "HIGH_LIMIT_VOLTAGE", 13 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MAX_TORQUE_LOW", 14 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MAX_TORQUE_HIGH", 15 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_LEVEL", 16 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ALARM_LED", 17 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ALARM_SHUTDOWN", 18 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_ENABLE", 24 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED", 25 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_COMPLIANCE_MARGIN", 26 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_COMPLIANCE_MARGIN", 27 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_COMPLIANCE_SLOPE", 28 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_COMPLIANCE_SLOPE", 29 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GOAL_POSITION_LOW", 30 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GOAL_POSITION_HIGH", 31 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING_SPEED_LOW", 32 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING_SPEED_HIGH", 33 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_LIMIT_LOW", 34 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_LIMIT_HIGH", 35 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_POSITION_LOW", 36 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_POSITION_HIGH", 37 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_SPEED_LOW", 38 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_SPEED_HIGH", 39 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_LOAD_LOW", 40 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_LOAD_HIGH", 41 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_VOLTAGE", 42 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_TEMPERATURE", 43 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "REGISTERED_INSTRUCTION", 44 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING", 46 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LOCK", 47 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PUNCH_LOW", 48 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PUNCH_HIGH", 49 );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
}

void Process::printAddrAX( void )
{
    fprintf( stdout, ADDRESS_PRINT_HEADER, "AX-12" );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_COLUMN );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_LOW", 0 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MODEL_NUMBER_HIGH", 1 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "VERSION", 2 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ID", 3 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "BAUD_RATE", 4 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_DELAY_TIME", 5 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_ANGLE_LIMIT_LOW", 6 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_ANGLE_LIMIT_HIGH", 7 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_ANGLE_LIMIT_LOW", 8 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_ANGLE_LIMIT_HIGH", 9 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "HIGH_LIMIT_TEMPERATURE", 11 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LOW_LIMIT_VOLTAGE", 12 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "HIGH_LIMIT_VOLTAGE", 13 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MAX_TORQUE_LOW", 14 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MAX_TORQUE_HIGH", 15 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "RETURN_LEVEL", 16 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ALARM_LED", 17 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "ALARM_SHUTDOWN", 18 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "DOWN_CALIBRATION_LOW", 20 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "DOWN_CALIBRATION_HIGH", 21 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "UP_CALIBRATION_LOW", 22 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "UP_CALIBRATION_HIGH", 23 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_ENABLE", 24 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LED", 25 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_COMPLIANCE_MARGIN", 26 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_COMPLIANCE_MARGIN", 27 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CW_COMPLIANCE_SLOPE", 28 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "CCW_COMPLIANCE_SLOPE", 29 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GOAL_POSITION_LOW", 30 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "GOAL_POSITION_HIGH", 31 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING_SPEED_LOW", 32 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING_SPEED_HIGH", 33 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_LIMIT_LOW", 34 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "TORQUE_LIMIT_HIGH", 35 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_POSITION_LOW", 36 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_POSITION_HIGH", 37 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_SPEED_LOW", 38 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_SPEED_HIGH", 39 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_LOAD_LOW", 40 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_LOAD_HIGH", 41 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_VOLTAGE", 42 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PRESENT_TEMPERATURE", 43 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "REGISTERED_INSTRUCTION", 44 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "MOVING", 46 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "LOCK", 47 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PUNCH_LOW", 48 );
    fprintf( stdout, ADDRESS_PRINT_FORMAT, "PUNCH_HIGH", 49 );
    fprintf( stdout, ADDRESS_PRINT_SEPARATOR );
}


void Process::printResult( int result, int errorbit )
{
    switch( result )
    {
    case CM730::SUCCESS:
        fprintf( stdout, "SUCCESS" );
        break;
    case CM730::TX_CORRUPT:
        fprintf( stdout, "TX_CORRUPT" );
        break;
    case CM730::TX_FAIL:
        fprintf( stdout, "TX_FAIL" );
        break;
    case CM730::RX_FAIL:
        fprintf( stdout, "RX_FAIL" );
        break;
    case CM730::RX_TIMEOUT:
        fprintf( stdout, "RX_TIMEOUT" );
        break;
    case CM730::RX_CORRUPT:
        fprintf( stdout, "RX_CORRUPT" );
        break;
    default:
        fprintf( stdout, "UNKNOWN_RESULT" );
        break;
    }
    fprintf( stdout, " (ERRBIT=%02X", errorbit );
    
    if(errorbit & (1 << 0))
		fprintf(stdout, " Voltage");
	if(errorbit & (1 << 1))
		fprintf(stdout, " Angle-Limit");
	if(errorbit & (1 << 2))
		fprintf(stdout, " Overheating");
	if(errorbit & (1 << 3))
		fprintf(stdout, " Range");
	if(errorbit & (1 << 4))
		fprintf(stdout, " Checksum");
	if(errorbit & (1 << 5))
		fprintf(stdout, " Over-Torque");
	if(errorbit & (1 << 6))
		fprintf(stdout, " Instruction");
	if(errorbit & (1 << 7))
		fprintf(stdout, " Bit 7 should never, ever be 1");
	
	fprintf(stdout, ")");
}

