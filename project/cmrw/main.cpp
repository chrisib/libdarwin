//--------------------------------------------------
// PROJECT: FIRA2011 CM730 Read/Write
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//          Chris Iverach-Brereton <ve4cib@gmail.com>
//--------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include "process.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <LinuxDARwIn.h>

//------------------------------------------------------------------------------
// FUNCTIONS AND METHODS
//------------------------------------------------------------------------------
bool change_current_dir( void )
{
    char exepath[1024] = {0};

    return readlink( "/proc/self/exe", exepath, sizeof( exepath ) ) != -1
        && chdir( dirname( exepath ) ) == 0;
}


int main( int argc, char* argv[] )
{
    int index = 0;
    bool debug = false;

    while( ++index < argc )
    {
        if( stricmp( argv[index], "debug" ) == 0 )
        {
            debug = true;
        }
    }

    fprintf( stdout, "FIRA2011 CM730 Read/Write\n" );
    fprintf( stdout, "Stela H. Seo<shb8775@hotmail.com>\n" );
    fprintf( stdout, "--------------------------------------------------\n" );
    
    printf("Checking Servo Types...\n");
    for(index = Robot::JointData::ID_R_SHOULDER_PITCH; index < Robot::JointData::NUMBER_OF_JOINTS; index++)
	{
		if(Robot::RX28::IsRX28(index))
			printf("   Servo %i is RX28\n",index);
		else if(Robot::AX12::IsAX12(index))
			printf("   Servo %i is AX12\n",index);
		else
			printf("   Servo %i is MX28\n",index);
	}
	printf("\n--------------------------------------------------\n");
    
    change_current_dir( );
    Process proc( debug );
    if( proc.isInitialized( ) )
    {
        do
        {
            proc.update( );
        }while( proc.isRunning( ) );
    }
    fprintf( stdout, "\n*** END OF PROCESS ***\n" );
    return EXIT_SUCCESS;
}

