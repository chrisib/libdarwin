//--------------------------------------------------
// PROJECT: Safe parser functions
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//--------------------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef PARSER_H
#define PARSER_H


//------------------------------------------------------------------------------
// EXTERN C
//------------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
unsigned char parseByte( const char* str, int* okay );
short parseShort( const char* str, int* okay );
int parseInt( const char* str, int* okay );
int parseLine( char line[], int maxArgs, char* argv[] );


//------------------------------------------------------------------------------
// END OF EXTERN C
//------------------------------------------------------------------------------
#ifdef __cplusplus
} // end extern "C"
#endif


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
