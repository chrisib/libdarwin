//--------------------------------------------------
// PROJECT: FIRA2011 CM730 Read/Write
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//          Chris Iverach-Brereton <ve4cib@gmail.com>
//--------------------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef PROCESS_H
#define PROCESS_H


//------------------------------------------------------------------------------
// INCLUDES AND FORWARD DECLARATION
//------------------------------------------------------------------------------
namespace Robot
{
    class CM730;
    //class LinuxSerialPort;
    class LinuxCM730;
}


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
enum _PROCESS_STATUS
{
    STATUS_ERROR,
    STATUS_INITIALIZED,
    STATUS_PROCESSING,
    STATUS_FINISHED,
};
typedef enum _PROCESS_STATUS PROCESS_STATUS;

#define BUFFER_SIZE 250
#define MAX_ARGUMENTS 256
#define MAX_LINECHARS 2048


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
int stricmp( const char* str1, const char* str2 );

class Process
{
private:
    PROCESS_STATUS _status; // process status
    Robot::CM730* _cm730; // CM730
    //Robot::LinuxSerialPort* _serial; // serial port
    Robot::LinuxCM730* _serial;
    bool _debug; // true, debug processing; otherwise, false
    int _cmdcount; // command count
    int _argc; // argument count
    char* _argv[MAX_ARGUMENTS]; // argument variables
    char _line[MAX_LINECHARS]; // input line buffer

public:
    Process( bool debug = false );
    virtual ~Process( void );
    virtual void update( void );
    virtual bool isInitialized( void ) const { return _status >= STATUS_INITIALIZED; }
    virtual bool isRunning( void ) const { return STATUS_INITIALIZED <= _status && _status <= STATUS_FINISHED; }

private:
    Process( const Process& other ) { }
    virtual Process& operator=( const Process& other ) { return *this; }
    virtual void process( int argc, char* argv[] );
    virtual void processHelp( int argc, char* argv[] );
    virtual void processClear( int argc, char* argv[] );
    virtual void processAddress( int argc, char* argv[] );
    virtual void processPing( int argc, char* argv[] );
    virtual void processRead( int argc, char* argv[] );
    virtual void processWrite( int argc, char* argv[] );
    virtual void processReset( int argc, char* argv[] );
    virtual void processSyncWrite( int argc, char* argv[] );
    static bool argId( const char* command, int index, int argc, char* argv[], unsigned char* value, bool prtErrMsg = true );
    static bool argByte( const char* command, const char* argname, int index, int argc, char* argv[], unsigned char* value, bool allowZero = true, bool prtErrMsg = true );
    static bool argInt( const char* command, const char* argname, int index, int argc, char* argv[], int* value, bool prtErrMsg = true );
    static void printAddrCM( void );
    static void printAddrMX( void );
    static void printAddrRX( void );
    static void printAddrAX( void );
    static void printResult( int result, int errorbit );

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
