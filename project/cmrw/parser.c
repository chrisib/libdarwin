//--------------------------------------------------
// PROJECT: Safe parser functions
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//          Chris Iverach-Brereton <ve4cib@gmail.com>
//--------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include "parser.h"
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>


//------------------------------------------------------------------------------
// FUNCTIONS AND METHODS
//------------------------------------------------------------------------------
unsigned char parseByte( const char* str, int* okay )
{
    long int result;

    errno = 0;
    while( *str == ' ' )
    {
        ++str;
    }
    result = strtol( str, 0, 0 );
    if( okay != 0 )
    {
        *okay = (errno == 0 && 0 <= result && result <= UCHAR_MAX );
    }
    return (unsigned char)result;
}


short parseShort( const char* str, int* okay )
{
    long int result;

    errno = 0;
    while( *str == ' ' )
    {
        ++str;
    }
    result = strtol( str, 0, 0 );
    if( okay != 0 )
    {
        *okay = (errno == 0 && SHRT_MIN <= result && result <= SHRT_MAX );
    }
    return (short)result;
}


int parseInt( const char* str, int* okay )
{
    long int result;

    errno = 0;
    while( *str == ' ' )
    {
        ++str;
    }
    result = strtol( str, 0, 0 );
    if( okay != 0 )
    {
        *okay = (errno == 0 && INT_MIN <= result && result <= INT_MAX );
    }
    return (int)result;
}


int parseLine( char line[], int maxArgs, char* argv[] )
{
    int finished = 0; // true, if parsing is finished; otherwise, false
    char* begin = line; // beginning of each argument
    char* ptr = line; // pointer to iterate line
    int argc = 0;

    memset( argv, 0, sizeof( char* ) * maxArgs );
    while( !finished && argc < maxArgs )
    {
        switch( *ptr )
        {
        case '\0': // string finished
        case '#': // comment begin
            if( ptr != begin )
            {
                argv[argc++] = begin;
            }
            // no more arguments
            *ptr = '\0';
            finished = 1;
            break;
        case '\r': // line finished
        case '\n':
            if( ptr != begin )
            {   // if argument is not empty
                argv[argc++] = begin;
            }
            *ptr = '\0';
            ++ptr;
            finished = 1;
            break;
        case ' ': // argument
            if( ptr != begin )
            {   // if argument is not empty
                argv[argc++] = begin;
            }
            *ptr = '\0';
            begin = ++ptr;
            break;
        default: // anything else
            ++ptr;
            break;
        }
    }
    return argc;
}

