#ifndef IMAGEPROCESSING_H
#define IMAGEPROCESSING_H

#include <QImage>
#include <opencv2/opencv.hpp>

class ImageProcessing
{
public:
    static QImage* cvMat2QImage(cv::Mat &src);
    static void overlayImages(cv::Mat &bg, cv::Mat &fg, cv::Scalar fgColour, cv::Mat &dst);

private:
    ImageProcessing();
};

#endif // IMAGEPROCESSING_H
