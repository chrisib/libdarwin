#ifndef COLOURPROFILE_H
#define COLOURPROFILE_H

#include <QString>
#include <darwin/framework/ThresholdRange.h>
#include <opencv2/core/core.hpp>
#include <darwin/framework/minIni.h>
#include <darwin/framework/MultiBlob.h>
#include <darwin/framework/MultiLine.h>
#include <string>

class ScanlineProfile
{
public:
    ScanlineProfile();
    virtual ~ScanlineProfile();

    int subsample;
    int scanLength;
    int scanThreshold;
    int fillThreshold;
    int minHeight;
    int minWidth;
    int realHeight;
    int realWidth;

    bool disableSize;
    bool disableAspectRatio;
    bool disableCompactness;
    bool disableYChannel;
    bool disableUChannel;
    bool disableVChannel;
    bool disableKeyChannel;

    int keyChannel;

    void loadConfig(minIni &ini, std::string section);
    void saveConfig(minIni &ini, std::string section);
    void applyTo(Robot::MultiBlob &target);
};

class HoughProfile {
public:
    HoughProfile();
    virtual ~HoughProfile();

    int minVotes;
    double maxGap;
    double minLength;
    double maxLength;
    double minAngle;
    double maxAngle;
    bool zeroIsVertical;
    int minVotesPerMerge;
    double endpointMergeTolerance;
    double slopeMergeTolerance;
    int endpointRadius;

    void loadConfig(minIni &ini, std::string section);
    void saveConfig(minIni &ini, std::string section);
    void applyTo(Robot::MultiLine &target);
};

class ColourProfile
{
public:
    ColourProfile();
    virtual ~ColourProfile();

    QString name;
    Robot::ThresholdRange yRange;
    Robot::ThresholdRange uRange;
    Robot::ThresholdRange vRange;

    cv::Scalar markupColour;

    ScanlineProfile scanlineConfig;
    HoughProfile houghConfig;

    void loadConfig(minIni &ini, std::string section);
    void saveConfig(minIni &ini, std::string section);
    void applyTo(Robot::MultiBlob &target);
    void applyTo(Robot::MultiLine &target);
};

#endif // COLOURPROFILE_H
