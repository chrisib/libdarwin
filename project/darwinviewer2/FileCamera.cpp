#include "FileCamera.h"

using namespace cv;
using namespace Robot;
using namespace std;

FileCamera::FileCamera()
{
}

FileCamera::~FileCamera()
{
    Release();
}

int FileCamera::Initialize(std::string filePath)

{
    bool ok = videoFile.open(filePath);

    return (int)ok;
}

void FileCamera::Release()
{
    videoFile.release();
}


bool FileCamera::CaptureFrame()
{
    bool frameRead = videoFile.read(yuvFrame);
    if(frameRead)
        ApplyPreprocessors();

    return frameRead;
}
