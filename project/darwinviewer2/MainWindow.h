#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <darwin/framework/minIni.h>
#include <darwin/linux/CvCamera.h>
#include <darwin/framework/MultiBlob.h>
#include <darwin/framework/Target.h>
#include <opencv2/core/core.hpp>
#include <ColourProfile.h>
#include <vector>
#include <QTimer>
#include "QImageView.h"

#include "PreprocessorDialog.h"

#define DEFAULT_CONFIG_EXTENSION ".ini"
#define MAX_PROFILES 10

#define YUV_WINDOW "YUV Image"
#define RGB_WINDOW "RGB Image"
#define THRESHOLD_WINDOW "Threshold"
#define SCANLINE_WINDOW "Scanline"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

// handlers for UI events
public slots:
    void onConfigLoad();
    void onConfigSave();

    void onCameraSettingsChanged();
    void onCameraOpen();
    void onCameraClose();
    void onPreprocessorsClicked();

    void onColourReset();
    void onColourChanged();
    void onProfileSelected();

    void onScanlineSettingsChanged();
    void onLineDetectSettingsChanged();

    void onResetExposureClicked();
    void onResetBrightnessClicked();
    void onResetContrastClicked();
    void onResetSaturationClicked();
    void onResetHueClicked();
    void onResetGammaClicked();
    void onResetBacklightClicked();
    void onResetGainClicked();

    void onDeviceMenuSelectionChanged(int newSelection);

    void onVisionTimerInterval();

    void onImageMouseMoved( Qt::MouseButtons buttons, int x, int y );
    void onImageMousePressed( Qt::MouseButtons buttons, int x, int y );

    void onLineMinLengthChanged(double newLength);

    // callback function for OpenCV mouse events
    // *param should be a pointer to the MainWindow instance that owns the windows
    static void onMouseEvent(int evt, int x, int y, int flags, void *param);

    
private:
    Ui::MainWindow *ui;

    // ini handler
    minIni *ini;

    // camera interface
    Robot::CvCamera *camera;
    Robot::CameraSettings camSettings;

    ColourProfile profiles[MAX_PROFILES];
    ColourProfile *currentProfile;
    Robot::MultiBlob blobTarget;
    Robot::MultiLine lineTarget;
    std::vector<Robot::BlobTarget*> scanlineVec;
    std::vector<Robot::MultiLine*> lineVec;

    // the images we can edit/display
    cv::Mat yuvFrame;
    cv::Mat rgbFrame;
    cv::Mat binaryFrame;
    cv::Mat thresholdFrame;
    cv::Mat scanlineFrame;
    cv::Mat lineFrame;

    enum VIDEO_SOURCE {
        MENU_ITEM_USB = 0,
        MENU_ITEM_FILE,
        MENU_ITEM_NETWORK
    };

    bool loadConfig(QString filename);
    bool saveConfig(QString filename);

    void selectPixel(int row, int col);

    void readCameraSettings();

    bool visionRunning;
    QTimer visionTimer;
    void processFrame();
    void showImages();

    PreprocessorDialog preprocessorDialog;

    bool blockSignals;

    void showColourSwatch(cv::Scalar colour, QImageView *swatch, bool convertFromYUV=false);
};

#endif // MAINWINDOW_H
