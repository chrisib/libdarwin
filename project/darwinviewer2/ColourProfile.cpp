#include <ColourProfile.h>

using namespace Robot;
using namespace std;
using namespace cv;

ColourProfile::ColourProfile()
{
}

ColourProfile::~ColourProfile()
{
}

void ColourProfile::loadConfig(minIni &ini, string section)
{
    cout << "Loading colour config from section " << section << endl;

    yRange.min = ini.geti(section,"MinY",255);
    yRange.max = ini.geti(section,"MaxY",0);
    uRange.min = ini.geti(section,"MinU",255);
    uRange.max = ini.geti(section,"MaxU",0);
    vRange.min = ini.geti(section,"MinV",255);
    vRange.max = ini.geti(section,"MaxV",0);
#ifdef DEBUG
    cout << yRange.min << " " <<
            yRange.max << " " <<
            uRange.min << " " <<
            uRange.max << " " <<
            vRange.min << " " <<
            vRange.max << " " <<
            endl;
#endif

    int defaultR, defaultG, defaultB;

    // assign default markup colours for simplicity's sake
    if(name=="Yellow") {
        defaultR = 255;
        defaultG = 255;
        defaultB = 0;
    }
    else if(name=="Red")
    {
        defaultR = 255;
        defaultG = 0;
        defaultB = 0;
    }
    else if(name=="Blue")
    {
        defaultR = 0;
        defaultG = 255;
        defaultB = 0;
    }
    else if(name=="Green")
    {
        defaultR = 0;
        defaultG = 255;
        defaultB = 0;
    }
    else if(name=="Orange")
    {
        defaultR = 255;
        defaultG = 128;
        defaultB = 0;
    }
    else if(name=="Pink")
    {
        defaultR = 255;
        defaultG = 0;
        defaultB = 255;
    }
    else if(name=="Aqua")
    {
        defaultR = 0;
        defaultG = 255;
        defaultB = 255;
    }
    else if(name=="Black")
    {
        defaultR = 0;
        defaultG = 0;
        defaultB = 0;
    }
    else if(name=="White")
    {
        defaultR = 255;
        defaultG = 255;
        defaultB = 255;
    }
    else if(name=="Grey")
    {
        defaultR = 128;
        defaultG = 128;
        defaultB = 128;
    }
    else
    {
        defaultR = 255;
        defaultG = 255;
        defaultB = 255;
    }

    int r = ini.geti(section,"MarkR",defaultR);
    int g = ini.geti(section,"MarkG",defaultG);
    int b = ini.geti(section,"MarkB",defaultB);

    markupColour = Scalar(b,g,r);

    scanlineConfig.loadConfig(ini,section);
    houghConfig.loadConfig(ini,section);
}

void ColourProfile::saveConfig(minIni &ini, string section)
{
    ini.put(section,"MinY",(int)yRange.min);
    ini.put(section,"MaxY",(int)yRange.max);
    ini.put(section,"MinU",(int)uRange.min);
    ini.put(section,"MaxU",(int)uRange.max);
    ini.put(section,"MinV",(int)vRange.min);
    ini.put(section,"MaxV",(int)vRange.max);

    ini.put(section,"MarkR",(int)markupColour[2]);
    ini.put(section,"MarkG",(int)markupColour[1]);
    ini.put(section,"MarkB",(int)markupColour[0]);

    scanlineConfig.saveConfig(ini,section);
    houghConfig.saveConfig(ini,section);
}

void ColourProfile::applyTo(MultiBlob &target)
{
    target.SetYRange(yRange.min,yRange.max);
    target.SetURange(uRange.min,uRange.max);
    target.SetVRange(vRange.min,vRange.max);

    target.SetMarkColour(markupColour[2],markupColour[1],markupColour[0]);

    scanlineConfig.applyTo(target);
}

void ColourProfile::applyTo(MultiLine &target)
{
    target.SetYRange(yRange.min,yRange.max);
    target.SetURange(uRange.min,uRange.max);
    target.SetVRange(vRange.min,vRange.max);

    target.SetMarkColour(markupColour[2],markupColour[1],markupColour[0]);

    houghConfig.applyTo(target);
}


/**********************************************************************************************************/


ScanlineProfile::ScanlineProfile()
{
    // hard-coded default values that work most of the time

    subsample = 2;
    scanLength = 8;
    scanThreshold = 24;
    fillThreshold = 32;
    minHeight = 5;
    minWidth = 5;
    realHeight = 0;
    realWidth = 0;

    disableSize = false;
    disableAspectRatio = false;
    disableCompactness = false;
    disableYChannel = false;
    disableUChannel = false;
    disableVChannel = false;
    disableKeyChannel = false;

    keyChannel = 1;
}

ScanlineProfile::~ScanlineProfile()
{
}

void ScanlineProfile::loadConfig(minIni &ini, string section)
{
    subsample = ini.geti(section,"Subsample",2);
    scanLength = ini.geti(section,"MinScanLength",8);
    scanThreshold = ini.geti(section,"ScanThreshold",24);
    fillThreshold = ini.geti(section,"FillThreshold",32);
    minHeight = ini.geti(section,"MinPixelHeight",5);
    minWidth = ini.geti(section,"MinPixelWidth",5);
    realHeight = ini.geti(section,"RealHeight",0);
    realWidth = ini.geti(section,"RealWidth",0);

    disableSize = !!ini.geti(section,"SizeDisabled",0);
    disableAspectRatio = !!ini.geti(section,"ProportionsDisabled",0);
    disableCompactness = !!ini.geti(section,"FillDisabled",0);
    disableYChannel = !!ini.geti(section,"YRangeDisabled",0);
    disableUChannel = !!ini.geti(section,"URangeDisabled",0);
    disableVChannel = !!ini.geti(section,"VRangeDisabled",0);
    disableKeyChannel = !!ini.geti(section,"KeyRangeDisabled",0);

    keyChannel = ini.geti(section,"KeyChannel",1);
}

void ScanlineProfile::saveConfig(minIni &ini, string section)
{
    ini.put(section,"Subsample",subsample);
    ini.put(section,"MinScanLength",scanLength);
    ini.put(section,"ScanThreshold",scanThreshold);
    ini.put(section,"FillThreshold",fillThreshold);
    ini.put(section,"MinPixelHeight",minHeight);
    ini.put(section,"MinPixelWidth",minWidth);
    ini.put(section,"RealHeight",realHeight);
    ini.put(section,"RealWidth",realWidth);

    ini.put(section,"SizeDisabled",disableSize);
    ini.put(section,"ProportionsDisabled",disableAspectRatio);
    ini.put(section,"FillDisabled",disableCompactness);
    ini.put(section,"YRangeDisabled",disableYChannel);
    ini.put(section,"URangeDisabled",disableUChannel);
    ini.put(section,"VRangeDisabled",disableVChannel);
    ini.put(section,"KeyRangeDisabled",disableKeyChannel);

    ini.put(section,"KeyChannel",keyChannel);
}

void ScanlineProfile::applyTo(MultiBlob &target)
{
    target.SetDimensions(realWidth,realHeight);
    target.SetFillDisabled(disableCompactness);
    target.SetFillThreshold(fillThreshold);
    target.SetKeyChannel(keyChannel);
    target.SetKeyRangeDisabled(disableKeyChannel);
    target.SetMinPixelHeight(minHeight);
    target.SetMinPixelWidth(minWidth);
    target.SetMinScanLength(scanLength);
    target.SetSubsample(subsample);
    target.SetYRangeDisabled(disableYChannel);
    target.SetURangeDisabled(disableUChannel);
    target.SetVRangeDisabled(disableVChannel);
}

/**********************************************************************************************************/

HoughProfile::HoughProfile()
{
    minAngle = 0.0;
    maxAngle = 180.0;

    minVotes = 1;

    minLength = 1;
    maxLength = sqrt(pow(Camera::HEIGHT,2)+pow(Camera::WIDTH,2));

    maxGap = 1;

    zeroIsVertical = false;
}

HoughProfile::~HoughProfile()
{

}

void HoughProfile::loadConfig(minIni &ini, string section)
{
    minAngle = ini.getd(section,"MinAngle",0);
    maxAngle = ini.getd(section,"MaxAngle",180);
    minVotes = ini.getd(section,"MinVotes",0);
    minLength = ini.getd(section,"MinLength",0);
    maxLength = ini.getd(section,"MaxLength",sqrt(pow(Camera::HEIGHT,2)+pow(Camera::WIDTH,2)));
    maxGap = ini.getd(section,"MaxGap",0);
    slopeMergeTolerance = ini.getd(section,"SlopeMergeTolerance",0.12);
    endpointMergeTolerance = ini.getd(section,"EndpointMergeTolerance",35);
    minVotesPerMerge = ini.geti(section,"MinVotesPerMerge",3);
    endpointRadius = ini.geti(section,"EndpointRadius",15);

    zeroIsVertical = !!ini.geti(section,"ZeroIsVertical",0);
}

void HoughProfile::saveConfig(minIni &ini, string section)
{
    ini.put(section,"MinAngle",minAngle);
    ini.put(section,"MaxAngle",maxAngle);
    ini.put(section,"MinVotes",minVotes);
    ini.put(section,"MinLength",minLength);
    ini.put(section,"MaxLength",maxLength);
    ini.put(section,"MaxGap",maxGap);
    ini.put(section,"SlopeMergeTolerance",slopeMergeTolerance);
    ini.put(section,"EndpointMergeTolerance",endpointMergeTolerance);
    ini.put(section,"MinVotesPerMerge",minVotesPerMerge);
    ini.put(section,"EndpointRadius",endpointRadius);

    ini.put(section,"ZeroIsVertical",zeroIsVertical);
}

void HoughProfile::applyTo(MultiLine &target)
{
    target.SetMinAngle(minAngle);
    target.SetMaxAngle(maxAngle);
    target.SetMinVotes(minVotes);
    target.SetMinLength(minLength);
    target.SetMaxLength(maxLength);
    target.SetMaxGap(maxGap);
    target.SetSlopeMergeTolerance(slopeMergeTolerance);
    target.SetEndpointMergeTolerance(endpointMergeTolerance);
    target.SetMergeMinVotes(minVotesPerMerge);
    target.SetEndpointRadius(endpointRadius);

    target.SetZeroIsVertical(zeroIsVertical);
}
