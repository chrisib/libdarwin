#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include "ImageProcessing.h"
#include "FileCamera.h"

#include <darwin/linux/CvCamera.h>
#include <darwin/framework/MultiBlob.h>
#include <iostream>

#include <darwin/framework/Math.h>

using namespace std;
using namespace cv;
using namespace Robot;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ini=NULL;

    //camera = CvCamera::GetInstance();
    camera = NULL;

    blockSignals = false;

    yuvFrame = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    rgbFrame = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    thresholdFrame = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    scanlineFrame = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    binaryFrame = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC1);
    lineFrame = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);

#ifdef USE_CV_WINDOWS
    // create the rest of the UI
    cv::namedWindow(YUV_WINDOW);
    cv::namedWindow(RGB_WINDOW);
    cv::namedWindow(THRESHOLD_WINDOW);
    cv::namedWindow(SCANLINE_WINDOW);
    cv::moveWindow(YUV_WINDOW,0,0);
    cv::moveWindow(RGB_WINDOW,Camera::WIDTH+12,0);
    cv::moveWindow(THRESHOLD_WINDOW,2*(Camera::WIDTH+12),0);
    cv::moveWindow(SCANLINE_WINDOW,3*(Camera::WIDTH+12),0);
    cv::waitKey(1);
    this->move(0,Camera::HEIGHT+64);

    // connect OpenCV mouse events from the window to our handler so that we can draw on the image to select things
    cvSetMouseCallback(YUV_WINDOW, onMouseEvent, this);
    cvSetMouseCallback(RGB_WINDOW, onMouseEvent, this);
    cvSetMouseCallback(THRESHOLD_WINDOW, onMouseEvent, this);
    cvSetMouseCallback(SCANLINE_WINDOW, onMouseEvent, this);

#endif

    // connect the vision timer's timeout signal to our vision-capture function
    connect(&visionTimer,SIGNAL(timeout()),this,SLOT(onVisionTimerInterval()));
    visionTimer.setSingleShot(true);
    visionTimer.setInterval(1);
    visionRunning = false;

    // set some initial properties for the colour profiles
    profiles[0].name = "Yellow";
    profiles[1].name = "Red";
    profiles[2].name = "Blue";
    profiles[3].name = "Green";
    profiles[4].name = "Orange";
    profiles[5].name = "Pink";
    profiles[6].name = "Aqua";
    profiles[7].name = "Black";
    profiles[8].name = "White";
    profiles[9].name = "Grey";

    currentProfile = profiles+0;
    currentProfile->applyTo(blobTarget);
    currentProfile->applyTo(lineTarget);

    scanlineVec.push_back(&blobTarget);
    lineVec.push_back(&lineTarget);


    ui->setupUi(this);

    // try loading the default config
    loadConfig("config.ini");

    // enable mouse tracking on the images so we get the proper hover colour indicator
    ui->imgSource->setMouseTracking(true);
    ui->imgScanline->setMouseTracking(true);
    ui->imgThreshold->setMouseTracking(true);
    ui->imgLineDetect->setMouseTracking(true);
}

MainWindow::~MainWindow()
{
    if(ini!=NULL)
        delete ini;
    delete ui;
}

// event handlers
void MainWindow::onConfigLoad()
{
    bool visionWasRunning = visionRunning;
    if(visionWasRunning)
    {
        visionRunning = false;
        visionTimer.stop();
    }

    QString filename = QFileDialog::getOpenFileName(this,"Load...","./","*.ini");

    if( !filename.isNull( ) && !filename.isEmpty( ) )
    {
        if( filename.indexOf( '.' ) == -1 )
        {   // no extension, add default extension
            filename.append( DEFAULT_CONFIG_EXTENSION );
        }
        if( loadConfig( filename ) )
        {
            cout << "Loaded config from " << filename.toStdString() << endl;
            ui->statusBar->showMessage("Loaded config file");
        }
        else
        {
            QMessageBox::warning(this,"Load Failed","Failed to load config file");
            cout << "Loading config file " << filename.toStdString() << " failed" << endl;
        }
    }

    if(visionWasRunning)
    {
        visionRunning = true;
        visionTimer.start(1);
    }
}

void MainWindow::onConfigSave()
{
    bool visionWasRunning = visionRunning;
    if(visionWasRunning)
    {
        visionRunning = false;
        visionTimer.stop();
    }

    QString filename = QFileDialog::getSaveFileName(this,"Save...","./","*.ini");

    if( !filename.isNull( ) && !filename.isEmpty( ) )
    {
        if( filename.indexOf( '.' ) == -1 )
        {   // no extension, add default extension
            filename.append( DEFAULT_CONFIG_EXTENSION );
        }
        if( saveConfig( filename ) )
        {
            cout << "Saved config to " << filename.toStdString() << endl;
            ui->statusBar->showMessage("Saved config file");
        }
        else
        {
            QMessageBox::warning(this,"Save Failed","Failed to save config file");
            cout << "Saving config file " << filename.toStdString() << " failed" << endl;
        }
    }

    if(visionWasRunning)
    {
        visionRunning = true;
        visionTimer.start();
    }
}

void MainWindow::onCameraSettingsChanged()
{
    if(blockSignals)
        return;

    // pause video capture during this operation as we may clobber the camera settings at a bad time
    bool visionWasRunning = visionRunning;
    if(visionWasRunning)
    {
        visionRunning = false;
        visionTimer.stop();
    }

    camSettings.autoExposure = ui->chkAutoExposure->isChecked();
    camSettings.exposure = ui->numExposure->value();

    camSettings.brightness = ui->numBrightness->value();
    camSettings.contrast = ui->numContrast->value();
    camSettings.hue = ui->numHue->value();
    camSettings.gamma = ui->numGamma->value();
    camSettings.backlight = ui->numBacklight->value();
    camSettings.gain = ui->numGain->value();
    camSettings.saturation = ui->numSaturation->value();

    camSettings.hFlip = ui->chkFlipH->isChecked();
    camSettings.vFlip = ui->chkFlipV->isChecked();

    if(camera!=NULL)
    {
        camera->SetCameraSettings(camSettings);
        readCameraSettings();

        ui->statusBar->showMessage("Camera settings changed");

        /*
        camera->ClearPreprocessors();
        cout << "Processor Dialog has " << preprocessorDialog.getPreprocessors()->size() << " processors" << endl;
        for(unsigned int i=0; i<preprocessorDialog.getPreprocessors()->size(); i++)
        {
            camera->AddPreprocessor((*preprocessorDialog.getPreprocessors())[i]);
        }
        */
    }

    // restart the vision processing if necessary
    if(visionWasRunning)
    {
        visionRunning = true;
        visionTimer.start(1);
    }
}

void MainWindow::onResetExposureClicked()
{
    ui->numExposure->setValue(-1);
}

void MainWindow::onResetBrightnessClicked()
{
    ui->numBrightness->setValue(-1);
}

void MainWindow::onResetContrastClicked()
{
    ui->numContrast->setValue(-1);
}

void MainWindow::onResetSaturationClicked()
{
    ui->numSaturation->setValue(-1);
}

void MainWindow::onResetHueClicked()
{
    ui->numHue->setValue(-1);
}

void MainWindow::onResetGammaClicked()
{
    ui->numGamma->setValue(-1);
}

void MainWindow::onResetBacklightClicked()
{
    ui->numBacklight->setValue(-1);
}

void MainWindow::onResetGainClicked()
{
    ui->numGain->setValue(-1);
}

void MainWindow::readCameraSettings()
{
    if(camera!=NULL)
    {
        blockSignals = true;

        preprocessorDialog.setPreprocessors(*(camera->GetPreprocessors()));
        camSettings = camera->GetCameraSettings();


        ui->chkAutoExposure->setChecked(camSettings.autoExposure);
        ui->numExposure->setValue(camSettings.exposure);

        ui->numBrightness->setValue(camSettings.brightness);
        ui->numContrast->setValue(camSettings.contrast);
        ui->numHue->setValue(camSettings.hue);
        ui->numGamma->setValue(camSettings.gamma);
        ui->numBacklight->setValue(camSettings.backlight);
        ui->numGain->setValue(camSettings.gain);
        ui->numSaturation->setValue(camSettings.saturation);

        ui->chkFlipH->setChecked(!!camSettings.hFlip);
        ui->chkFlipV->setChecked(!!camSettings.vFlip);

        // disable manual exposure if auto-exposure is enabled
        ui->numExposure->setEnabled(!ui->chkAutoExposure->isChecked());

        blockSignals = false;
    }
}

void MainWindow::onCameraOpen()
{
    if(ui->cboSource->currentIndex() == MENU_ITEM_USB)
    {
        bool ok;
        int deviceID = ui->txtDeviceID->text().toInt(&ok);

        if(!ok)
        {
            QMessageBox::warning(this,"Invalid Device ID","Unable to open camera: invalid device ID");
            camera = NULL;
        }
        else
        {
            camera = CvCamera::GetInstance();
            if(!camera->Initialize(deviceID))
            {
                QMessageBox::warning(this,"USB Camera Failed","Unable to open V4L2 camera");
                camera = NULL;
            }
        }
    }
    else if(ui->cboSource->currentIndex() == MENU_ITEM_FILE)
    {
        QString filename = QFileDialog::getOpenFileName(this,"Open Video...","./","Video Files (*.mp4 *.avi *.ogv *.mkv *.flv);;All Files *.*");

        if( !filename.isNull( ) && !filename.isEmpty( ) )
        {
            FileCamera *f = new FileCamera();
            if(f->Initialize(filename.toStdString()))
            {
                camera = f;
            }
            else
            {
                QMessageBox::warning(this,"Failed to open file","Unable to video file; unsupported format?");
                camera = NULL;
                delete f;
            }
        }
    }
    else
    {
        // TODO: implement network camera devices
        QMessageBox::warning(this,"Unsupported Device","Network Cameras not implemented yet");

        camera = NULL;
    }

    // initialize the camera's settings and start the framerate timer
    if(camera != NULL)
    {
        camera->RGB_ENABLE = true;

        if(ini!=NULL)
            camera->LoadINISettings(ini);
        preprocessorDialog.setCamera(camera);
        readCameraSettings();
        ui->btnOpen->setEnabled(false);
        ui->btnClose->setEnabled(true);
        ui->cboSource->setEnabled(false);
        ui->txtDeviceID->setEnabled(false);
        ui->btnPreprocessors->setEnabled(true);
        ui->chkFlipH->setEnabled(true);
        ui->chkFlipV->setEnabled(true);

        visionRunning = true;
        visionTimer.setInterval(1);
        visionTimer.start();

        ui->statusBar->showMessage("Opened camera");
    }
}

void MainWindow::onCameraClose()
{
    visionRunning = false;
    visionTimer.stop();

    if(ui->cboSource->currentIndex() == MENU_ITEM_USB)
    {
        camera->Release();
    }
    else if(ui->cboSource->currentIndex() == MENU_ITEM_FILE)
    {
        FileCamera *fc = (FileCamera*)camera;
        delete fc;
    }
    else
    {
        // TODO: implement network camera devices
        QMessageBox::warning(this,"Unsupported Device","Network Cameras not implemented yet");
    }
    camera = NULL;

    ui->btnOpen->setEnabled(true);
    ui->btnClose->setEnabled(false);
    ui->cboSource->setEnabled(true);
    ui->txtDeviceID->setEnabled(true);
    ui->btnPreprocessors->setEnabled(false);
    ui->chkFlipH->setEnabled(false);
    ui->chkFlipV->setEnabled(false);

    ui->statusBar->showMessage("Closed camera");
}

void MainWindow::onPreprocessorsClicked()
{
    int result = preprocessorDialog.exec();

    // if they clicked OK then the preprocessors may have changed
    // re-apply the settings
    if(result == QDialog::Accepted)
    {
        onCameraSettingsChanged();
    }
}

void MainWindow::onColourChanged()
{
    if(blockSignals)
        return;

    currentProfile->yRange.min = ui->numYMin->value();
    currentProfile->yRange.max = ui->numYMax->value();
    currentProfile->uRange.min = ui->numUMin->value();
    currentProfile->uRange.max = ui->numUMax->value();
    currentProfile->vRange.min = ui->numVMin->value();
    currentProfile->vRange.max = ui->numVMax->value();

    currentProfile->markupColour = Scalar(ui->numMarkB->value(),ui->numMarkG->value(),ui->numMarkR->value());

    Scalar minColour = Scalar(currentProfile->yRange.min, currentProfile->uRange.min, currentProfile->vRange.min);
    Scalar maxColour = Scalar(currentProfile->yRange.max, currentProfile->uRange.max, currentProfile->vRange.max);
    showColourSwatch(minColour,ui->imgMinColourRGB,true);
    showColourSwatch(maxColour,ui->imgMaxColourRGB,true);
    showColourSwatch(minColour,ui->imgMinColourYUV);
    showColourSwatch(maxColour,ui->imgMaxColourYUV);

    showColourSwatch(currentProfile->markupColour,ui->imgMarkColour);

    currentProfile->applyTo(blobTarget);
    currentProfile->applyTo(lineTarget);

    ui->statusBar->showMessage("Colour changed");
}

void MainWindow::showColourSwatch(Scalar colour, QImageView *swatch, bool convertFromYUV)
{
    Mat m = Mat::zeros(swatch->geometry().height(),swatch->geometry().width(),CV_8UC3);
    cv::rectangle(m,Point(0,0),Point(swatch->geometry().height(),swatch->geometry().width()),colour,-1);

    if(convertFromYUV)
    {
        //cout << "Converting colours" << endl;
        Mat tmp(m);
        cv::cvtColor(tmp,m,CV_YCrCb2RGB);
    }

    QImage *img = ImageProcessing::cvMat2QImage(m);

    swatch->setImage(*img);
    delete img;
}

void MainWindow::onColourReset()
{
    blockSignals = true;

    ui->numYMin->setValue(255);
    ui->numYMax->setValue(0);
    ui->numUMin->setValue(255);
    ui->numUMax->setValue(0);
    ui->numVMin->setValue(255);
    ui->numVMax->setValue(0);

    blockSignals = false;

    onColourChanged();
}

void MainWindow::onProfileSelected()
{
    currentProfile = profiles + ui->cboProfile->currentIndex();
    blockSignals = true;

    // colour threshold settings
    ui->numYMin->setValue(currentProfile->yRange.min);
    ui->numYMax->setValue(currentProfile->yRange.max);
    ui->numUMin->setValue(currentProfile->uRange.min);
    ui->numUMax->setValue(currentProfile->uRange.max);
    ui->numVMin->setValue(currentProfile->vRange.min);
    ui->numVMax->setValue(currentProfile->vRange.max);
    ui->numMarkR->setValue(currentProfile->markupColour[2]);
    ui->numMarkG->setValue(currentProfile->markupColour[1]);
    ui->numMarkB->setValue(currentProfile->markupColour[0]);

    // scanline settings
    ui->numSubsample->setValue(currentProfile->scanlineConfig.subsample);
    ui->numScanLength->setValue(currentProfile->scanlineConfig.scanLength);
    ui->numScanThreshold->setValue(currentProfile->scanlineConfig.scanThreshold);
    ui->numFillThreshold->setValue(currentProfile->scanlineConfig.fillThreshold);
    ui->numMinHeight->setValue(currentProfile->scanlineConfig.minHeight);
    ui->numMinWidth->setValue(currentProfile->scanlineConfig.minWidth);
    ui->numRealHeight->setValue(currentProfile->scanlineConfig.realHeight);
    ui->numRealWidth->setValue(currentProfile->scanlineConfig.realWidth);

    // scanline settings part 2
    ui->chkDisableSize->setChecked(currentProfile->scanlineConfig.disableSize);
    ui->chkDisableAspect->setChecked(currentProfile->scanlineConfig.disableAspectRatio);
    ui->chkDisableCompactness->setChecked(currentProfile->scanlineConfig.disableCompactness);
    ui->chkDisableY->setChecked(currentProfile->scanlineConfig.disableYChannel);
    ui->chkDisableU->setChecked(currentProfile->scanlineConfig.disableUChannel);
    ui->chkDisableV->setChecked(currentProfile->scanlineConfig.disableVChannel);
    ui->chkDisableKey->setChecked(currentProfile->scanlineConfig.disableKeyChannel);
    ui->cboKeyChannel->setCurrentIndex(currentProfile->scanlineConfig.keyChannel);

    // hough detection settings
    ui->numHoughMaxGap->setValue(currentProfile->houghConfig.maxGap);
    ui->numHoughMinVotes->setValue(currentProfile->houghConfig.minVotes);
    ui->numMinLength->setValue(currentProfile->houghConfig.minLength);
    ui->numMaxLength->setValue(currentProfile->houghConfig.maxLength);
    ui->numMinAngle->setValue(currentProfile->houghConfig.minAngle);
    ui->numMaxAngle->setValue(currentProfile->houghConfig.maxAngle);
    ui->chkZeroVertical->setChecked(currentProfile->houghConfig.zeroIsVertical);
    //ui->numMergeDistance->setValue(currentProfile->houghConfig.mergeDistance);

    blockSignals = false;

    onColourChanged();
    onScanlineSettingsChanged();
    onLineDetectSettingsChanged();

    ui->statusBar->showMessage("New colour profile selected");
}

void MainWindow::onScanlineSettingsChanged()
{
    if(blockSignals)
        return;

    currentProfile->scanlineConfig.subsample = ui->numSubsample->value();
    currentProfile->scanlineConfig.scanLength = ui->numScanLength->value();
    currentProfile->scanlineConfig.scanThreshold = ui->numScanThreshold->value();
    currentProfile->scanlineConfig.fillThreshold = ui->numFillThreshold->value();
    currentProfile->scanlineConfig.minHeight = ui->numMinHeight->value();
    currentProfile->scanlineConfig.minWidth = ui->numMinWidth->value();
    currentProfile->scanlineConfig.realHeight = ui->numRealHeight->value();
    currentProfile->scanlineConfig.realWidth = ui->numRealWidth->value();

    currentProfile->scanlineConfig.disableSize = ui->chkDisableSize->isChecked();
    currentProfile->scanlineConfig.disableAspectRatio = ui->chkDisableAspect->isChecked();
    currentProfile->scanlineConfig.disableCompactness = ui->chkDisableCompactness->isChecked();
    currentProfile->scanlineConfig.disableYChannel = ui->chkDisableY->isChecked();
    currentProfile->scanlineConfig.disableUChannel = ui->chkDisableU->isChecked();
    currentProfile->scanlineConfig.disableVChannel = ui->chkDisableV->isChecked();
    currentProfile->scanlineConfig.disableKeyChannel = ui->chkDisableKey->isChecked();
    currentProfile->scanlineConfig.keyChannel = ui->cboKeyChannel->currentIndex();

    ui->cboKeyChannel->setEnabled(!ui->chkDisableKey->isChecked());
    currentProfile->applyTo(blobTarget);

    ui->statusBar->showMessage("Scanline settings changed");
}

void MainWindow::onLineDetectSettingsChanged()
{
    if(blockSignals)
        return;

    /* minvotes
     * maxgap
     * slope merge tol
     * endpoint merge tol
     * min votes per merge
     * endpoint radius
     * min/max length
     * min/max angle
     * zero vertical?
     */

    HoughProfile &hp = currentProfile->houghConfig;

    hp.minVotes = ui->numHoughMinVotes->value();
    hp.maxGap = ui->numHoughMaxGap->value();
    hp.slopeMergeTolerance = ui->numMergeTolerance->value();
    hp.endpointMergeTolerance = ui->numEndpointTolerance->value();
    hp.minVotesPerMerge = ui->numMinMergeVote->value();

    hp.minAngle = ui->numMinAngle->value();
    hp.maxAngle = ui->numMaxAngle->value();
    hp.minLength = ui->numMinLength->value();
    hp.maxLength = ui->numMaxLength->value();

    hp.zeroIsVertical = ui->chkZeroVertical->isChecked();

    currentProfile->applyTo(lineTarget);

    ui->statusBar->showMessage("Line detection settings changed");
}

// callback function for OpenCV mouse events
// *param should be a pointer to the MainWindow instance that owns the windows
void MainWindow::onMouseEvent(int evt, int x, int y, int flags, void *param)
{
    // if the mouse event was a left button press/click
    // or a mouse move with the left button held down then do stuff
    // otherwise just ignore it
    if(evt == CV_EVENT_LBUTTONDOWN || evt == CV_EVENT_LBUTTONDOWN || (evt == CV_EVENT_MOUSEMOVE && flags == CV_EVENT_FLAG_LBUTTON))
    {
        MainWindow *window = (MainWindow*)param;
        ColourProfile *profile = window->currentProfile;

        // get the pixel at the position clicked
        // note that the at function uses row/column parameters, so x and y are reversed
        Vec3b pixel = window->yuvFrame.at<Vec3b>(y,x);
        int y = pixel[0];
        int u = pixel[1];
        int v = pixel[2];

        // update the colour profile
        if(y < profile->yRange.min)
            profile->yRange.min = y;
        else if(y > profile->yRange.max)
            profile->yRange.max = y;

        if(u < profile->uRange.min)
            profile->uRange.min = u;
        else if(u > profile->uRange.max)
            profile->uRange.max = u;

        if(v < profile->vRange.min)
            profile->vRange.min = v;
        else if(v > profile->vRange.max)
            profile->vRange.max = v;

        // update the UI
        window->ui->numYMin->setValue(profile->yRange.min);
        window->ui->numYMax->setValue(profile->yRange.max);
        window->ui->numUMin->setValue(profile->uRange.min);
        window->ui->numUMax->setValue(profile->uRange.max);
        window->ui->numVMin->setValue(profile->vRange.min);
        window->ui->numVMax->setValue(profile->vRange.max);
    }
}

// open a configuration file
bool MainWindow::loadConfig(QString filename)
{
    try
    {
        FILE *f;
        if(!ini_openread(filename.toStdString().c_str(),&f))
            return false;

        if(ini!=NULL)
            delete ini;

        ini = new minIni(filename.toStdString());

        for(int i=0; i<MAX_PROFILES; i++)
            profiles[i].loadConfig(*ini,profiles[i].name.toStdString());

        if(camera != NULL)
        {
            camera->LoadINISettings(ini);
            readCameraSettings();

            preprocessorDialog.setPreprocessors(*(camera->GetPreprocessors()));
        }

        // auto-select the current profile to populate everything as needed
        onProfileSelected();

        return true;
    }
    catch(...)
    {
        return false;
    }
}

// save a configuration file
bool MainWindow::saveConfig(QString filename)
{
    try
    {
        minIni iniOut(filename.toStdString());

        for(int i=0; i<MAX_PROFILES; i++)
            profiles[i].saveConfig(iniOut,profiles[i].name.toStdString());

        if(camera!=NULL)
            camera->SaveINISettings(&iniOut);

        return true;
    }
    catch(...)
    {
        return false;
    }
}

void MainWindow::onImageMouseMoved(Qt::MouseButtons buttons, int x, int y)
{
    //cout << "Mouse Moved " << x << " " << y << endl;

    if( (buttons & Qt::LeftButton) == Qt::LeftButton && camera != NULL)
    {
        selectPixel(y,x);
    }

    if(camera!=NULL)
    {
        Vec3b pixel = camera->yuvFrame.at<Vec3b>(y,x);
        int y2 = pixel[0];
        int u = pixel[1];
        int v = pixel[2];
        Scalar s(y2,u,v);
        showColourSwatch(s,ui->imgHoverColour,true);
    }
}

void MainWindow::onImageMousePressed(Qt::MouseButtons buttons, int x, int y)
{
    //cout << "Mouse Pressed " << x << " " << y << endl;

    if( (buttons & Qt::LeftButton) == Qt::LeftButton && camera != NULL)
    {
        selectPixel(y,x);
    }
}

void MainWindow::selectPixel(int row, int col)
{
    Vec3b pixel = camera->yuvFrame.at<Vec3b>(row,col);
    int y = pixel[0];
    int u = pixel[1];
    int v = pixel[2];

    //cout << y << " " << u << " " << v << endl;

    blockSignals = true;

    // update the UI
    if(y < ui->numYMin->value())
        ui->numYMin->setValue(y);
    else if(y > ui->numYMax->value())
        ui->numYMax->setValue(y);

    if(u < ui->numUMin->value())
        ui->numUMin->setValue(u);
    else if(u > ui->numUMax->value())
        ui->numUMax->setValue(u);

    if(v < ui->numVMin->value())
        ui->numVMin->setValue(v);
    else if(v > ui->numVMax->value())
        ui->numVMax->setValue(v);

    blockSignals = false;

    onColourChanged();
}

void MainWindow::onVisionTimerInterval()
{
    if(!ui->btnPlayPause->isChecked())
    {
        // if we fail to capture a frame then automatically pause the video
        if(!this->camera->CaptureFrame())
            ui->btnPlayPause->setChecked(true);
    }

    // do the vision processing and display the results
    //cout << "Processing image" << endl;
    this->camera->rgbFrame.copyTo(this->rgbFrame);
    this->camera->yuvFrame.copyTo(this->yuvFrame);
    this->processFrame();
    this->showImages();
    //cout << "done" << endl;

    if(visionRunning)
        visionTimer.start(1);
}

void MainWindow::processFrame()
{
    // do the simple thresholding for the image
    Vec3i lowerBound, upperBound;
    lowerBound[0] = currentProfile->yRange.min;
    lowerBound[1] = currentProfile->uRange.min;
    lowerBound[2] = currentProfile->vRange.min;
    upperBound[0] = currentProfile->yRange.max;
    upperBound[1] = currentProfile->uRange.max;
    upperBound[2] = currentProfile->vRange.max;
    cv::inRange(yuvFrame,lowerBound,upperBound,binaryFrame);

    // do the scanline
    Robot::BlobTarget::FindTargets(scanlineVec,yuvFrame,&scanlineFrame,currentProfile->scanlineConfig.subsample);

    // do the hough line detection
    lineTarget.FindInFrame(yuvFrame,NULL);
}

void MainWindow::showImages()
{
    QImage *img;

    // overlay the original image with the threshold & line detection images if necessary
    if(ui->chkOverlaySource->isChecked())
    {
        Scalar colour = *(blobTarget.GetMarkColour());

        // overlay the binary image with the source if necessary
        Mat colourSource;
        if(ui->chkShowRGB->isChecked())
            colourSource = rgbFrame;
        else
            colourSource = yuvFrame;

        ImageProcessing::overlayImages(colourSource,binaryFrame,colour,thresholdFrame);
        //ImageProcessing::overlayImages(colourSource,binaryFrame,Scalar(0xff,0xff,0xff),lineFrame);
        colourSource.copyTo(lineFrame);
    }
    else
    {
        Mat black = Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);

        ImageProcessing::overlayImages(black,binaryFrame,Scalar(0xff,0xff,0xff),thresholdFrame);
        //ImageProcessing::overlayImages(black,binaryFrame,Scalar(0xff,0xff,0xff),lineFrame);
        lineFrame=Mat::zeros(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    }

    lineTarget.DRAW_BUCKETS = ui->chkDrawBuckets->isChecked();
    lineTarget.Draw(lineFrame); // always draw the lines
    blobTarget.Draw(scanlineFrame);

    if(ui->chkShowAngles->isChecked())
    {
        char buffer[32];
        vector<Line2D*> *lines = lineTarget.GetAllLines();

        for(vector<Line2D*>::iterator i=lines->begin(); i!=lines->end(); i++)
        {
            double angle = 0.0;
            Line2D *l = *i;

            if(!lineTarget.GetZeroIsVertical())
                angle = rad2deg(atan2(l->start.Y-l->end.Y,l->start.X-l->end.X));
            else
                angle = rad2deg(atan2(l->start.X-l->end.X,l->start.Y-l->end.Y));

            while(angle < 180)
                angle += 180;
            while(angle > 180)
                angle -=180;

            sprintf(buffer,"%0.2f",angle);

            cv::putText(lineFrame,buffer,Point(l->start.X,l->start.Y),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,255));
        }
    }

    // draw the images onto the widgets
    if(ui->chkShowRGB->isChecked())
        img = ImageProcessing::cvMat2QImage(this->rgbFrame);
    else
        img = ImageProcessing::cvMat2QImage(this->yuvFrame);
    ui->imgSource->setImage(*img);
    delete img;

    img = ImageProcessing::cvMat2QImage(this->thresholdFrame);
    ui->imgThreshold->setImage(*img);
    delete img;

    img = ImageProcessing::cvMat2QImage(this->scanlineFrame);
    ui->imgScanline->setImage(*img);
    delete img;

    img = ImageProcessing::cvMat2QImage(this->lineFrame);
    ui->imgLineDetect->setImage(*img);
    delete img;


#ifdef USE_CV_WINDOWS
    //cout << "displaying images" << endl;
    cv::imshow(YUV_WINDOW,this->yuvFrame);
    cv::imshow(RGB_WINDOW,this->rgbFrame);
    cv::imshow(THRESHOLD_WINDOW,this->thresholdFrame);
    cv::imshow(SCANLINE_WINDOW,this->scanlineFrame);
    cv::waitKey(1);
    //cout << "done" << endl;
#endif
}

void MainWindow::onDeviceMenuSelectionChanged(int newSelection)
{
    if(newSelection == MENU_ITEM_FILE)
    {
        ui->txtDeviceID->setEnabled(false);
    }
    else
    {
        ui->txtDeviceID->setEnabled(true);
    }
}

void MainWindow::onLineMinLengthChanged(double newLength)
{
    // keep the read-only hough min length equal to our filter's min length
    ui->numHoughMinLength->setValue((int)newLength);
}
