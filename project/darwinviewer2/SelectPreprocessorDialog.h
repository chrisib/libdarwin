#ifndef SELECTPREPROCESSORDIALOG_H
#define SELECTPREPROCESSORDIALOG_H

#include <QDialog>
#include <darwin/framework/CvPreprocessor.h>

namespace Ui {
class SelectPreprocessorDialog;
}

class SelectPreprocessorDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SelectPreprocessorDialog(QWidget *parent = 0);
    ~SelectPreprocessorDialog();

    // creates a new instance of a CvPreprocessor and returns it
    Robot::CvPreprocessor *getPreprocessor();

public slots:
    void onArgumentChanged(double newArg);
    void onTwoArgFilterSelected(bool checked);
    void onOneArgFilterSelected(bool checked);

    void onIntFilterSelected(bool checked);
    void onDoubleFilterSelected(bool checked);

private:
    Ui::SelectPreprocessorDialog *ui;
};

#endif // SELECTPREPROCESSORDIALOG_H
