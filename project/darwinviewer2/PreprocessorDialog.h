#ifndef PREPROCESSORDIALOG_H
#define PREPROCESSORDIALOG_H

#include <QDialog>
#include "SelectPreprocessorDialog.h"

#include <darwin/framework/CvPreprocessor.h>
#include <vector>
#include <darwin/linux/CvCamera.h>

namespace Ui {
class PreprocessorDialog;
}

class PreprocessorDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit PreprocessorDialog(QWidget *parent = 0);
    ~PreprocessorDialog();

    //std::vector<Robot::CvPreprocessor*> *getPreprocessors(){return &preprocessors;}
    void setPreprocessors(std::vector<Robot::CvPreprocessor*> &preprocessors);

    void setCamera(Robot::CvCamera *camera);

public slots:
    void onAddClicked();
    void onRemoveClicked();
    void onMoveUpClicked();
    void onMoveDownClicked();
    virtual void accept();
    virtual void accepted();
    virtual void reject();
    virtual void rejected();


    
private:
    Ui::PreprocessorDialog *ui;

    SelectPreprocessorDialog selectDialog;

    Robot::CvCamera *camera;

    std::vector<Robot::CvPreprocessor*> preprocessors;
    std::vector<Robot::CvPreprocessor*> wipPreprocessors;

    void acceptWipPreprocessors();
    void rejectWipPreprocessors();
    void updateListWidget();
};

#endif // PREPROCESSORDIALOG_H
