#include "ImageProcessing.h"
#include <QColor>
#include <iostream>
#include <darwin/framework/Camera.h>

using namespace cv;
using namespace std;
using namespace Robot;

ImageProcessing::ImageProcessing()
{
}

QImage *ImageProcessing::cvMat2QImage(Mat &src)
{
    QImage* image = NULL;

    //cout << "Frame has " << src.channels() << " channels" << endl;

    switch( src.channels() )
    {
    case 3:
        image = new QImage( src.cols, src.rows, QImage::Format_RGB32 );
        for( int row = 0; row < src.rows; row++ )
        {
            for( int col = 0; col < src.cols; col++ )
            {
                cv::Vec3b cvPixel = src.at<Vec3b>(row,col);
                int r = (int)cvPixel[2];
                int g = (int)cvPixel[1];
                int b = (int)cvPixel[0];

                image->setPixel(col,row,qRgb(r,g,b));
            }
        }
        break;

    case 1:
        image = new QImage( src.cols, src.rows, QImage::Format_RGB32 );
        for( int row = 0; row < src.rows; row++ )
        {
            for( int col = 0; col < src.cols; col++ )
            {
                cv::Vec3b cvPixel = src.at<Vec3b>(row,col);
                int r = (int)cvPixel[0];
                int g = (int)cvPixel[0];
                int b = (int)cvPixel[0];

                image->setPixel(col,row,qRgb(r,g,b));
            }
        }
        break;

    default:
        cerr << "Unsupported openCV image format: " << src.type() << endl;
        break;
    }
    return image;

}

void ImageProcessing::overlayImages(Mat &bg, Mat &fg, Scalar fgColour, Mat &dst)
{
    IplImage binImg(fg);
    IplImage colourImg(bg);
    IplImage outImg(dst);

    unsigned char *binData = (unsigned char*)binImg.imageData;
    unsigned char *colourData = (unsigned char*)colourImg.imageData;
    unsigned char *outData = (unsigned char*)outImg.imageData;

    for(int i=0; i<Camera::HEIGHT * Camera::WIDTH; i++)
    {
        if(*binData)
        {
            outData[0] = fgColour[0];
            outData[1] = fgColour[1];
            outData[2] = fgColour[2];
        }
        else
        {
            outData[0] = colourData[0];
            outData[1] = colourData[1];
            outData[2] = colourData[2];
        }

        binData++;
        colourData += bg.channels();
        outData += dst.channels();
    }
}
