//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "QImageView.h"
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QImage>
#include <QMouseEvent>
#include <QPixmap>
#include <QTransform>


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
QImageView::QImageView( QWidget* parent )
    : QGraphicsView( parent )
{
    _scene = new QGraphicsScene( this );
    _gpiImage = _scene->addPixmap( QPixmap( ) );
    setScene( _scene );
}


QImageView::~QImageView( void )
{
}


void QImageView::setImage( const QImage& image )
{
    setPixmap( QPixmap::fromImage( image ) );
}


void QImageView::setPixmap( const QPixmap& pixmap )
{
    _gpiImage->setPixmap( pixmap );
    _scene->setSceneRect( 0.0, 0.0, pixmap.width( ), pixmap.height( ) );
}


void QImageView::mouseMoveEvent( QMouseEvent* event )
{
    QSize pxsize = _gpiImage->pixmap( ).size( );
    int x = event->x( );
    int y = event->y( );

    // if image is bigger than widget, the image origin (0,0) is left top
    // if image is smaller than widget, the image will be on centre
    if( pxsize.width( ) < width( ) )
    {
        x = x - (width( ) / 2 - pxsize.width( ) / 2);
    }
    if( pxsize.height( ) < height( ) )
    {
        y = y - (height( ) / 2 - pxsize.height( ) / 2);
    }
    mouseMoved( event->buttons( ), x, y );
    event->accept( );
}


void QImageView::mousePressEvent( QMouseEvent* event )
{
    QSize pxsize = _gpiImage->pixmap( ).size( );
    int x = event->x( );
    int y = event->y( );

    if( pxsize.width( ) < width( ) )
    {
        x = x - (width( ) / 2 - pxsize.width( ) / 2);
    }
    if( pxsize.height( ) < height( ) )
    {
        y = y - (height( ) / 2 - pxsize.height( ) / 2);
    }
    mousePressed( event->buttons( ), x, y );
    event->accept( );
}

