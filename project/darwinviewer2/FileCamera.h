#ifndef FILECAMERA_H
#define FILECAMERA_H

#include <darwin/linux/CvCamera.h>
#include <opencv2/opencv.hpp>

class FileCamera : public Robot::CvCamera
{
public:
    FileCamera();
    virtual ~FileCamera();

    virtual int Initialize(std::string filePath);
    virtual void Release();
    virtual bool CaptureFrame();

    // virtual functions we just don't care about and shouldn't use with this class because they make no sense
    virtual int Initialize(int){return 0;}
    virtual int v4l2GetControl(int control) { (void)control; return -1;}
    virtual int v4l2SetControl(int control, int value) { (void)control; (void)value; return -1;}
    virtual int v4l2ResetControl(int control) { (void)control; return -1; }

    virtual void SetCameraSettings(const Robot::CameraSettings& newset) {settings = newset;}
    virtual const Robot::CameraSettings& GetCameraSettings(){ return settings;}

    virtual void SetAutoWhiteBalance(int isAuto) { (void)isAuto; }
    virtual unsigned char GetAutoWhiteBalance() { return 0; }



private:
    cv::VideoCapture videoFile;
};

#endif // FILECAMERA_H
