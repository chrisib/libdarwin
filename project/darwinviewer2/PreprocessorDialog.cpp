#include "PreprocessorDialog.h"
#include "ui_PreprocessorDialog.h"

#include <iostream>

using namespace Robot;
using namespace std;

PreprocessorDialog::PreprocessorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreprocessorDialog)
{
    ui->setupUi(this);
}

PreprocessorDialog::~PreprocessorDialog()
{
    delete ui;
}

void PreprocessorDialog::setPreprocessors(std::vector<CvPreprocessor *> &preprocessors)
{
    wipPreprocessors.clear();
    this->preprocessors.clear();

    //cout << "Initializing " << preprocessors.size() << " preprocessors" << endl;

    for(unsigned int i=0; i<preprocessors.size(); i++)
    {
        wipPreprocessors.push_back(preprocessors[i]);
        this->preprocessors.push_back(preprocessors[i]);
    }

    updateListWidget();
}

void PreprocessorDialog::updateListWidget()
{
    ui->listPreprocessors->clear();

    if(camera!=NULL)
        camera->ClearPreprocessors();

    QString name;

    for(unsigned int i=0; i<wipPreprocessors.size(); i++)
    {
        name = QString("%1 - %2").arg(i+1).arg(wipPreprocessors[i]->Name());
        ui->listPreprocessors->addItem(new QListWidgetItem(name));

        if(camera != NULL)
            camera->AddPreprocessor(wipPreprocessors[i]);
    }
}

void PreprocessorDialog::onAddClicked()
{
    int result = selectDialog.exec();

    if(result == QDialog::Accepted)
    {
        CvPreprocessor *newProcessor = selectDialog.getPreprocessor();

        if(newProcessor != NULL)
        {
            wipPreprocessors.push_back(newProcessor);
            cout << "Adding new " << newProcessor->Name() << " preprocessor" << endl;
        }

        updateListWidget();
    }
}

void PreprocessorDialog::onRemoveClicked()
{
    QList<QListWidgetItem *> selectedItems = ui->listPreprocessors->selectedItems();

    if(selectedItems.count() > 0)
    {
        int index = ui->listPreprocessors->currentIndex().row();

        wipPreprocessors.erase(wipPreprocessors.begin()+index);

        updateListWidget();
    }


}

// TODO: this doesn't work yet
void PreprocessorDialog::onMoveDownClicked()
{
    QList<QListWidgetItem *> selectedItems = ui->listPreprocessors->selectedItems();

    if(selectedItems.count() > 0 && wipPreprocessors.size() > 1)
    {
        int index = ui->listPreprocessors->currentIndex().row();

        if(index < wipPreprocessors.size()-1)
        {
            CvPreprocessor *item = wipPreprocessors[index];
            wipPreprocessors.erase(wipPreprocessors.begin()+index);
            wipPreprocessors.insert(wipPreprocessors.begin()+index+1,item);

            updateListWidget();

            ui->listPreprocessors->setCurrentRow(index+1);
        }
    }
}

// TODO: this doesn't work yet
void PreprocessorDialog::onMoveUpClicked()
{
    QList<QListWidgetItem *> selectedItems = ui->listPreprocessors->selectedItems();

    if(selectedItems.count() > 0 && wipPreprocessors.size() > 1)
    {
        int index = ui->listPreprocessors->currentIndex().row();

        if(index > 0)
        {
            CvPreprocessor *item = wipPreprocessors[index];
            wipPreprocessors.erase(wipPreprocessors.begin()+index);
            wipPreprocessors.insert(wipPreprocessors.begin()+index-1,item);

            updateListWidget();

            ui->listPreprocessors->setCurrentRow(index-1);
        }
    }
}

void PreprocessorDialog::acceptWipPreprocessors()
{
    // clear the existing preprocessors
    preprocessors.clear();

    for(unsigned int i=0;i<wipPreprocessors.size(); i++)
    {
        preprocessors.push_back(wipPreprocessors[i]);
    }

    /*
    if(camera != NULL)
    {
        camera->ClearPreprocessors();
        for(unsigned int i=0; i<preprocessors.size(); i++)
            camera->AddPreprocessor(preprocessors[i]);
    }
    */
}

void PreprocessorDialog::rejectWipPreprocessors()
{
    wipPreprocessors.clear();

    for(unsigned int i=0; i<preprocessors.size(); i++)
        wipPreprocessors.push_back(preprocessors[i]);

    updateListWidget();
}

void PreprocessorDialog::accept()
{
    acceptWipPreprocessors();
    QDialog::accept();
}

void PreprocessorDialog::accepted()
{
    acceptWipPreprocessors();
    QDialog::accepted();
}

void PreprocessorDialog::reject()
{
    rejectWipPreprocessors();
    QDialog::reject();
}

void PreprocessorDialog::rejected()
{
    rejectWipPreprocessors();
    QDialog::rejected();
}

void PreprocessorDialog::setCamera(CvCamera *camera)
{
    this->camera = camera;

    setPreprocessors(*camera->GetPreprocessors());
}
