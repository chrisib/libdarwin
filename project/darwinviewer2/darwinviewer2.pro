#-------------------------------------------------
#
# Project created by QtCreator 2014-03-07T10:23:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = darwinviewer2
TEMPLATE = app

LIBS += -ldarwin \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lopencv_videoio \
        -lopencv_video


SOURCES += main.cpp\
        MainWindow.cpp \
    ColourProfile.cpp \
    PreprocessorDialog.cpp \
    SelectPreprocessorDialog.cpp \
    QImageView.cpp \
    ImageProcessing.cpp \
    FileCamera.cpp

HEADERS  += MainWindow.h \
    ColourProfile.h \
    PreprocessorDialog.h \
    SelectPreprocessorDialog.h \
    QImageView.h \
    ImageProcessing.h \
    FileCamera.h

FORMS    += MainWindow.ui \
    PreprocessorDialog.ui \
    SelectPreprocessorDialog.ui

OTHER_FILES += \
    config.ini

RESOURCES += \
    icons.qrc

##################################################
# Output Directories                             #
##################################################
OBJECTS_DIR = .objects
MOC_DIR     = .moc
UI_DIR      = .ui

##################################################
# Installation Directories                       #
##################################################
INSTALLBASE = /usr/local
bin.path    = $$INSTALLBASE/bin/
bin.files   = $$TARGET
INSTALLS   += bin

icon.path = /usr/share/pixmaps
icon.files = images/darwinviewer2.png
INSTALLS  += icon

launcher.path = /usr/share/applications
launcher.files = darwinviewer2.desktop
INSTALLS += launcher
