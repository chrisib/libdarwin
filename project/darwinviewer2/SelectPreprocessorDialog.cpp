#include "SelectPreprocessorDialog.h"
#include "ui_SelectPreprocessorDialog.h"
#include <QPushButton>

using namespace Robot;

SelectPreprocessorDialog::SelectPreprocessorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectPreprocessorDialog)
{
    ui->setupUi(this);

    ui->numArgument2->setVisible(false);
    onArgumentChanged(ui->numArgument->value());
}

SelectPreprocessorDialog::~SelectPreprocessorDialog()
{
    delete ui;
}

CvPreprocessor *SelectPreprocessorDialog::getPreprocessor()
{
    double darg1 = ui->numArgument->value();
    double darg2 = ui->numArgument2->value();

    int iarg1 = (int)darg1;

    if(ui->rdoBackground->isChecked() && iarg1 > 0)
        return new BackgroundRemover(darg1);

    else if(ui->rdoContrast->isChecked())
        return new ContrastPreprocessor(darg1, darg2);

    else if(ui->rdoDilate->isChecked() && iarg1 %2 == 1 && iarg1 > 0)
        return new DilatePreprocessor(iarg1);

    else if(ui->rdoErode->isChecked() && iarg1 %2 == 1 && iarg1 > 0)
        return new ErodePreprocessor(iarg1);

    else if(ui->rdoGaussian->isChecked() && iarg1 %2 == 1 && iarg1 > 0)
        return new GaussianPreprocessor(iarg1);

    else if(ui->rdoMedian->isChecked() && iarg1 %2 == 1 && iarg1 > 0)
        return new MedianPreprocessor(iarg1);

    else
        return NULL;
}

void SelectPreprocessorDialog::onArgumentChanged(double newArg)
{
    int arg = (int)newArg;
    bool argValid =
            (ui->rdoBackground->isChecked() && arg > 0) ||
            (ui->rdoContrast->isChecked()) ||
            (ui->rdoDilate->isChecked() && arg %2 == 1 && arg > 0) ||
            (ui->rdoErode->isChecked() && arg %2 == 1 && arg > 0) ||
            (ui->rdoGaussian->isChecked() && arg %2 == 1 && arg > 0) ||
            (ui->rdoMedian->isChecked() && arg %2 == 1 && arg > 0);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(argValid);
}

void SelectPreprocessorDialog::onOneArgFilterSelected(bool checked)
{
    if(!checked)
        return;

    ui->lblArgument2->setVisible(false);
    ui->numArgument2->setVisible(false);
    }

void SelectPreprocessorDialog::onTwoArgFilterSelected(bool checked)
{
    if(!checked)
        return;
    ui->lblArgument2->setVisible(true);
    ui->numArgument2->setVisible(true);
}

void SelectPreprocessorDialog::onIntFilterSelected(bool checked)
{
    if(!checked)
        return;

    ui->numArgument->setDecimals(0);
    ui->numArgument2->setDecimals(0);

    onArgumentChanged(ui->numArgument->value());
}

void SelectPreprocessorDialog::onDoubleFilterSelected(bool checked)
{
    if(!checked)
        return;

    ui->numArgument->setDecimals(2);
    ui->numArgument2->setDecimals(2);

    onArgumentChanged(ui->numArgument->value());
}
