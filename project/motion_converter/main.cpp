/*
 * main.cpp
 *
 *  Convert motion files between 1024 and 4096 resolution
 */

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <cstring>
#include <iostream>
#include "LinuxDARwIn.h"
#include "Action.h"
#include "AX12.h"
#include "RX28.h"
#include "MX28.h"
#include "Arm.h"
#include "LeftArm.h"
#include "RightArm.h"

#define DEFAULT_MOTION_FILE "motion.bin"

using namespace std;
using namespace Robot;

enum
{
	MOTOR_MX28,
	MOTOR_RX28,
	MOTOR_AX12
};

int src = MOTOR_MX28;
int dst = MOTOR_MX28;
char *motionFile = DEFAULT_MOTION_FILE;
bool setHandPositions = false;

int main(int argc, char** argv)
{
    // parse command-line argument
    for(int i=1; i<argc; i++)
    {
		if(!strcmp("--from",argv[i]))
		{
			i++;
			if(!strcmp("mx28",argv[i]))
			{
				src = MOTOR_MX28;
			}
			else if(!strcmp("rx28",argv[i]))
			{
				src = MOTOR_RX28;
			}
			else if(!strcmp("ax12",argv[i]))
			{
				src = MOTOR_AX12;
			}
			else
			{
				cout << "Unknown source motor type: " << argv[i] << endl;
				exit(-1);
			}
		}
		else if(!strcmp("--to",argv[i]))
		{
			i++;
			if(!strcmp("mx28",argv[i]))
			{
				dst = MOTOR_MX28;
			}
			else if(!strcmp("rx28",argv[i]))
			{
				dst = MOTOR_RX28;
			}
			else if(!strcmp("ax12",argv[i]))
			{
				dst = MOTOR_AX12;
			}
			else
			{
				cout << "Unknown destination motor type: " << argv[i] << endl;
				exit(-1);
			}
		}
        else if(!strcmp("--set-hands",argv[i]))
        {
            setHandPositions = true;
        }
		else if(!strcmp("--help",argv[i]))
		{
			cout << endl <<
					"Usage" << endl <<
					"\t" << argv[0] << " [--from MOTOR_TYPE] [--to MOTOR_TYPE] [--set-hands] [--help] [MOTION_FILE]" << endl <<
					endl <<
					"\twhere MOTOR_TYPE is one of ax12, rx28, or mx28" << endl <<
                    "\t  (default: mx28 --> mx28)" << endl <<
                    "\t--set-hands will overwite the gripper positions if the motor is set to an invalid angle" << endl;
			exit(0);
		}
		else
		{
			motionFile = argv[i];
		}
	}
	
	cout << "Converting " << motionFile << " from ";
	switch(src)
	{
		case MOTOR_MX28:
			cout << "MX-28";
			break;
		case MOTOR_RX28:
			cout << "RX-28";
			break;
		case MOTOR_AX12:
			cout << "AX-12";
			break;
		default:
			break;
	}
	cout << " to ";
	switch(dst)
	{
		case MOTOR_MX28:
			cout << "MX-28";
			break;
		case MOTOR_RX28:
			cout << "RX-28";
			break;
		case MOTOR_AX12:
			cout << "AX-12";
			break;
		default:
			break;
	}
	cout << endl;
	
	Action::GetInstance()->LoadFile(motionFile);
	
	int j,k,p;
	int position, newPosition;
	double angle;
	Action::PAGE Page;
	for(k=0;k<Action::MAXNUM_PAGE;k++)
	{
		Action::GetInstance()->LoadPage(k, &Page);
		for(j=0;j<Action::MAXNUM_STEP;j++)
		{
			for(p=0;p<31;p++)
			{
				position = Page.step[j].position[p];
				switch(src)
				{
					case MOTOR_AX12:
						angle = AX12::Value2Angle(position);
						break;
					case MOTOR_RX28:
						angle = RX28::Value2Angle(position);
						break;
					case MOTOR_MX28:
						angle = MX28::Value2Angle(position);
						break;
					default:
						angle = 0.0;
						break;
				}
                
                // write the hand position data
                if(setHandPositions && (p == JointData::ID_R_HAND || p == JointData::ID_L_HAND))
                {
                    // convert to body angle (from raw angle)
                    if(p==JointData::ID_L_HAND)
                    {
                        angle = -angle;
                    }
                    
                    // set the hand to the neutral position if necessary
                    if(p==JointData::ID_L_HAND && (angle < LeftArm::GetInstance()->CLOSE_ANGLE - 5 || angle > LeftArm::GetInstance()->OPEN_ANGLE + 5)) 
                    {
                        cout << "[info] set hand angle for motion " << j << " on page " << k << endl;
                        angle = LeftArm::GetInstance()->NEUTRAL_ANGLE;
                    }
                    else if(p==JointData::ID_R_HAND && (angle < RightArm::GetInstance()->CLOSE_ANGLE - 5 || angle > RightArm::GetInstance()->OPEN_ANGLE + 5))
                    {
                        cout <<  "[info] set hand angle for motion " << j << " on page " << k << endl;
                        angle = RightArm::GetInstance()->NEUTRAL_ANGLE;
                    }
                    
                    // convert back to raw angle
                    if(p==JointData::ID_L_HAND)
                    {
                        angle = -angle;
                    }
                }
				
				switch(dst)
				{
					case MOTOR_AX12:
						newPosition = AX12::Angle2Value(angle);
						break;
					case MOTOR_RX28:
						newPosition = RX28::Angle2Value(angle);
						break;
					case MOTOR_MX28:
						newPosition = MX28::Angle2Value(angle);
						break;
					default:
						newPosition = 0;
						break;
				}
				
				Page.step[j].position[p] = newPosition;
				
				cout << position << " --> " << newPosition << endl;
			}
		}
		Action::GetInstance()->SavePage(k, &Page);
	}
	
	exit(0);
}
