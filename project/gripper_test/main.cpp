#include <LinuxDARwIn.h>
#include <iostream>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>

#define OPEN_HAND 1
#define CLOSE_HAND 2
#define RELAX_HAND 3
#define TORQUE_ON  4
#define TORQUE_OFF 5
#define QUIT 0

#include <list>

using namespace std;
using namespace Robot;

int main()
{
    cout << "Initializing..." << endl;
	LinuxDARwIn::Initialize("../../data/motion_4096.bin",Action::DEFAULT_MOTION_SIT_DOWN);
	cout << "done" << endl;
	
	cout << "Enabling arm modules" << endl;

	LeftArm::GetInstance()->m_Joint.SetEnableLowerBody(false);
	LeftArm::GetInstance()->m_Joint.SetEnableLeftArmOnly(true,true);
    MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());

	RightArm::GetInstance()->m_Joint.SetEnableLowerBody(false);
	RightArm::GetInstance()->m_Joint.SetEnableRightArmOnly(true,true);
    MotionManager::GetInstance()->AddModule(RightArm::GetInstance());


	LeftArm::GetInstance()->m_Joint.SetEnableLeftArmOnly(true,true);
	RightArm::GetInstance()->m_Joint.SetEnableRightArmOnly(true,true);
    
    // disable torque on the arms
    /*
#ifdef HAS_R_HAND
    LinuxDARwIn::cm730.WriteByte(JointData::ID_R_SHOULDER_PITCH, MX28::P_TORQUE_ENABLE, 0, 0);
    LinuxDARwIn::cm730.WriteByte(JointData::ID_R_SHOULDER_ROLL, MX28::P_TORQUE_ENABLE, 0, 0);
    LinuxDARwIn::cm730.WriteByte(JointData::ID_R_ELBOW, MX28::P_TORQUE_ENABLE, 0, 0);
#endif
#ifdef HAS_L_HAND
    LinuxDARwIn::cm730.WriteByte(JointData::ID_L_SHOULDER_PITCH, MX28::P_TORQUE_ENABLE, 0, 0);
    LinuxDARwIn::cm730.WriteByte(JointData::ID_L_SHOULDER_ROLL, MX28::P_TORQUE_ENABLE, 0, 0);
    LinuxDARwIn::cm730.WriteByte(JointData::ID_L_ELBOW, MX28::P_TORQUE_ENABLE, 0, 0);
#endif
	*/
    cout << "done" << endl;
    
    list<int> armMotors;
    list<bool> torqueOn;
    list<bool> torqueOff;
    
    armMotors.push_back(JointData::ID_R_SHOULDER_PITCH);
    armMotors.push_back(JointData::ID_L_SHOULDER_PITCH);
    armMotors.push_back(JointData::ID_R_SHOULDER_ROLL);
    armMotors.push_back(JointData::ID_L_SHOULDER_ROLL);
    armMotors.push_back(JointData::ID_R_ELBOW);
    armMotors.push_back(JointData::ID_L_ELBOW);
    armMotors.push_back(JointData::ID_R_HAND);
    armMotors.push_back(JointData::ID_L_HAND);
    for(list<int>::iterator it = armMotors.begin(); it!=armMotors.end(); it++)
    {
        torqueOn.push_back(true);
        torqueOff.push_back(false);
    }
    
    int command;
    for(;;)
    {
        cout << "\nOpen/Close/Relax/Torque-On/Torque-Off/Quit [1/2/3/4/5/0]:" << endl;
        cin >> command;
        
        
		if(command==OPEN_HAND)
		{

			cout << "Opening right hand" << endl;
			RightArm::GetInstance()->OpenHand(1);

			cout << "Opening left hand" << endl;
			LeftArm::GetInstance()->OpenHand(1);

		}
		else if(command==CLOSE_HAND)
		{

			cout << "Closing right hand" << endl;
			RightArm::GetInstance()->CloseHand(2);

			cout << "Closing left hand" << endl;
			LeftArm::GetInstance()->CloseHand(2);

		}
		else if(command==RELAX_HAND)
		{

			cout << "Relaxing right hand (" << RightArm::GetInstance()->NEUTRAL_ANGLE << ")" << endl;
			RightArm::GetInstance()->RelaxHand(1);

			cout << "Relaxing left hand (" << LeftArm::GetInstance()->NEUTRAL_ANGLE << ")" << endl;
			LeftArm::GetInstance()->RelaxHand(1);

		}
        else if(command==TORQUE_ON)
        {
			cout << "Torque on" << endl;
            cout << MotionManager::GetInstance()->SetTorqueEnable(armMotors, torqueOn) << endl;
        }
        else if(command==TORQUE_OFF)
        {
			cout << "Torque off" << endl;
            cout << MotionManager::GetInstance()->SetTorqueEnable(armMotors, torqueOff) << endl;
        }
		else if(command==QUIT)
			return 0;
		else
			cout << "Invalid option.  Try again" << endl;
    }

	return 0;
}
