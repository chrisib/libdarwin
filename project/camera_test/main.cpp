#include <darwin/linux/CvCamera.h>
#include <darwin/framework/Target.h>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <darwin/framework/minIni.h>
using namespace std;
int main()
{
	minIni ini("config.ini");
	Robot::CvCamera::GetInstance()->Initialize(0);
	Robot::CvCamera::GetInstance()->LoadINISettings(&ini);

    Robot::CvCamera::GetInstance()->RGB_ENABLE = true;
    Robot::CvCamera::GetInstance()->HSV_ENABLE = true;

	cv::namedWindow("YUV");
	cv::namedWindow("RGB");
	cv::namedWindow("YUV - Y");
	cv::namedWindow("YUV - U");
	cv::namedWindow("YUV - V");
	cv::namedWindow("RGB - R");
	cv::namedWindow("RGB - G");
	cv::namedWindow("RGB - B");
	cv::namedWindow("HSV");
	cv::namedWindow("HSV - H");	
	cv::namedWindow("HSV - S");
	cv::namedWindow("HSV - V");

	cvMoveWindow("YUV",0,0);
	cvMoveWindow( "YUV - Y", 310, 0 );
	cvMoveWindow( "YUV - U", 620, 0 );
	cvMoveWindow( "YUV - V", 930, 0 );
	
	cvMoveWindow("RGB",0,300);
	cvMoveWindow( "RGB - R", 310, 300 );
	cvMoveWindow( "RGB - G", 620, 300 );
	cvMoveWindow( "RGB - B", 930, 300 );
	
	cvMoveWindow("HSV",0,600);
    cvMoveWindow( "HSV - H", 310, 600 );
	cvMoveWindow( "HSV - S", 620, 600 );
	cvMoveWindow( "HSV - V", 930, 600 );    
	
	cv::Mat *y,*u,*v, *h, *s, *vv, *r, *g, *b;
	
	for(;;)
	{
		Robot::CvCamera::GetInstance()->CaptureFrame();
		
		y = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->yuvFrame,0);
		u = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->yuvFrame,1);
		v = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->yuvFrame,2);

		r = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->rgbFrame,2);
		g = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->rgbFrame,1);
		b = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->rgbFrame,0);
		
		h = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->hsvFrame,0);
		s = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->hsvFrame,1);
		vv = Robot::Target::ExtractChannel(Robot::CvCamera::GetInstance()->hsvFrame,2);
		
		
		cv::imshow("YUV", Robot::CvCamera::GetInstance()->yuvFrame);
		cv::imshow("YUV - Y", *y);
		cv::imshow("YUV - U", *u);
		cv::imshow("YUV - V", *v);
		
		cv::imshow("RGB", Robot::CvCamera::GetInstance()->rgbFrame);
		cv::imshow("RGB - R", *r);
		cv::imshow("RGB - G", *g);
		cv::imshow("RGB - B", *b);
		
		cv::imshow("HSV", Robot::CvCamera::GetInstance()->hsvFrame);
		cv::imshow("HSV - H", *h);
		cv::imshow("HSV - S", *s);
		cv::imshow("HSV - V", *vv);
		cv::waitKey(1);
		
		delete y;
		delete u;
		delete v;
		delete r;
		delete g;
		delete b;
		delete h;
		delete s;
		delete vv;
	}

	return 0;
}
