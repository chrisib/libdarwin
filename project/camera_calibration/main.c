#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <LinuxDARwIn.h>
#include <darwin/linux/YuvCamera.h>
#include <darwin/framework/Head.h>

using namespace cv;
using namespace Robot;

// FOR USE WITH THIS CALIBRATION GRID: http://dasl.mem.drexel.edu/~noahKuntz/Calibration_Grid.png

int main(int argc, char* argv[])
{
    minIni *ini = new minIni("config.ini");
    
    int n_boards = 0;
    const int board_dt = 20;
    int board_w;
    int board_h;

    int alwaysFindCorners = ini->geti("Config","always_find_corners");
    
    char baseName[255];
    char filename[255];
    
    // get board dimensions
	board_w = ini->geti("Board","columns")-1;
	board_h = ini->geti("Board","rows")-1;
    
    double pan = ini->getd("Head","pan");
    double tilt = ini->getd("Head","tilt");
    
    sprintf(baseName,"pan%0.2f_tilt%0.2f",pan,tilt);
    
	n_boards = ini->geti("Config","num_boards");
	
    int board_n = board_w * board_h;
	CvSize board_sz = cvSize( board_w, board_h );
	
    YuvCamera::GetInstance()->Initialize(0);
    if(!ini->geti("Config","disable_motions"))
    {
        LinuxDARwIn::Initialize("motion.bin",Action::DEFAULT_MOTION_STAND_UP);
        Head::GetInstance()->m_Joint.SetEnableBody(false);
        Head::GetInstance()->m_Joint.SetEnableHeadOnly(true,true);
        MotionManager::GetInstance()->AddModule(Head::GetInstance());
        Head::GetInstance()->MoveByAngle(pan, tilt);
    }
    else
    {
        printf("Robot Motions Disabled");
    }
	

	cvNamedWindow( "Calibration" );
    cvMoveWindow("Calibration",0,0);
	// Allocate Sotrage
	CvMat* image_points		= cvCreateMat( n_boards*board_n, 2, CV_32FC1 );
	CvMat* object_points		= cvCreateMat( n_boards*board_n, 3, CV_32FC1 );
	CvMat* point_counts			= cvCreateMat( n_boards, 1, CV_32SC1 );
	CvMat* intrinsic_matrix		= cvCreateMat( 3, 3, CV_32FC1 );
	CvMat* distortion_coeffs	= cvCreateMat( 5, 1, CV_32FC1 );

	CvPoint2D32f* corners = new CvPoint2D32f[ board_n ];
	int corner_count;
	int successes = 0;
	int step, frame = 0;

	YuvCamera::GetInstance()->CaptureFrame();
	Mat img = YuvCamera::GetInstance()->img;

	IplImage iplImage = IplImage(img);
	IplImage *image = &iplImage;
	IplImage *gray_image = cvCreateImage( cvGetSize( image ), 8, 1 );

	// Capture Corner views loop until we've got n_boards
	// succesful captures (all corners on the board are found)

	printf("Press space to record the chess board corners for calibration purposes\n");
	printf("Press T to toggle always show corners\n");

	int found;
	while( successes < n_boards )
	{
		if(alwaysFindCorners)
		{
			// Find chessboard corners:
			found = cvFindChessboardCorners( image, board_sz, corners,
				&corner_count, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS );

			// Get subpixel accuracy on those corners
			cvCvtColor( image, gray_image, CV_BGR2GRAY );
			cvFindCornerSubPix( gray_image, corners, corner_count, cvSize( 11, 11 ), 
				cvSize( -1, -1 ), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

			// Draw it
			cvDrawChessboardCorners( image, board_sz, corners, corner_count, found );
		}
		
		cvShowImage( "Calibration", image );
		
		int c = cvWaitKey(10);
		if(c=='t' || c=='T')
		{
			// toggle always capture corners
			alwaysFindCorners = !alwaysFindCorners;
            if(alwaysFindCorners)
                printf("Always find corners enabled\n");
            else
                printf("Always find corners disabled\n");
                
            fflush(stdout);
		}
		else if(c==' ')
		{
			// space bar to capture
			if(!alwaysFindCorners)
			{
				// Find chessboard corners:
				found = cvFindChessboardCorners( image, board_sz, corners,
					&corner_count, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS );

				// Get subpixel accuracy on those corners
				cvCvtColor( image, gray_image, CV_BGR2GRAY );
				cvFindCornerSubPix( gray_image, corners, corner_count, cvSize( 11, 11 ), 
					cvSize( -1, -1 ), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

				// Draw it
				cvDrawChessboardCorners( image, board_sz, corners, corner_count, found );
			}
			
			cvShowImage( "Calibration", image );
			cvWaitKey(250);
			
			// If we got a good board, add it to our data
			if( corner_count == board_n ){
				step = successes*board_n;
				for( int i=step, j=0; j < board_n; ++i, ++j ){
					CV_MAT_ELEM( *image_points, float, i, 0 ) = corners[j].x;
					CV_MAT_ELEM( *image_points, float, i, 1 ) = corners[j].y;
					CV_MAT_ELEM( *object_points, float, i, 0 ) = j/board_w;
					CV_MAT_ELEM( *object_points, float, i, 1 ) = j%board_w;
					CV_MAT_ELEM( *object_points, float, i, 2 ) = 0.0f;
				}
				CV_MAT_ELEM( *point_counts, int, successes, 0 ) = board_n;
				successes++;
				
				printf("Captured %d of %d calibration images\n",successes, n_boards);
			}
		}
		else
		
		// continually capture frames and show them
		YuvCamera::GetInstance()->CaptureFrame();
		img = YuvCamera::GetInstance()->img;
		iplImage = IplImage(img);
		
	} // End collection while loop

	// Allocate matrices according to how many chessboards found
	CvMat* object_points2 = cvCreateMat( successes*board_n, 3, CV_32FC1 );
	CvMat* image_points2 = cvCreateMat( successes*board_n, 2, CV_32FC1 );
	CvMat* point_counts2 = cvCreateMat( successes, 1, CV_32SC1 );
	
	// Transfer the points into the correct size matrices
	for( int i = 0; i < successes*board_n; ++i ){
		CV_MAT_ELEM( *image_points2, float, i, 0) = CV_MAT_ELEM( *image_points, float, i, 0 );
		CV_MAT_ELEM( *image_points2, float, i, 1) = CV_MAT_ELEM( *image_points, float, i, 1 );
		CV_MAT_ELEM( *object_points2, float, i, 0) = CV_MAT_ELEM( *object_points, float, i, 0 );
		CV_MAT_ELEM( *object_points2, float, i, 1) = CV_MAT_ELEM( *object_points, float, i, 1 );
		CV_MAT_ELEM( *object_points2, float, i, 2) = CV_MAT_ELEM( *object_points, float, i, 2 );
	}

	for( int i=0; i < successes; ++i ){
		CV_MAT_ELEM( *point_counts2, int, i, 0 ) = CV_MAT_ELEM( *point_counts, int, i, 0 );
	}
	cvReleaseMat( &object_points );
	cvReleaseMat( &image_points );
	cvReleaseMat( &point_counts );

	// At this point we have all the chessboard corners we need
	// Initiliazie the intrinsic matrix such that the two focal lengths
	// have a ratio of 1.0

	CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0;
	CV_MAT_ELEM( *intrinsic_matrix, float, 1, 1 ) = 1.0;

	// Calibrate the camera
	cvCalibrateCamera2( object_points2, image_points2, point_counts2, cvGetSize( image ), 
		intrinsic_matrix, distortion_coeffs, NULL, NULL, CV_CALIB_FIX_ASPECT_RATIO ); 

	// Save the intrinsics and distortions
    sprintf(filename,"%s_intrinsic.xml",baseName);
	cvSave( filename, intrinsic_matrix );
    sprintf(filename,"%s_distortion.xml",baseName);
	cvSave( filename, distortion_coeffs );

	// Example of loading these matrices back in
    sprintf(filename,"%s_intrinsic.xml",baseName);
	CvMat *intrinsic = (CvMat*)cvLoad( filename );
    sprintf(filename,"%s_distortion.xml",baseName);
	CvMat *distortion = (CvMat*)cvLoad( filename);

	// Build the undistort map that we will use for all subsequent frames
	IplImage* mapx = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
	IplImage* mapy = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
	cvInitUndistortMap( intrinsic, distortion, mapx, mapy );

	// Run the camera to the screen, now showing the raw and undistorted image
	cvNamedWindow( "Undistort" );
    cvMoveWindow("Undistort",320,0);

	while( image ){
		IplImage *t = cvCloneImage( image );
		cvShowImage( "Calibration", image ); // Show raw image
		cvRemap( t, image, mapx, mapy ); // undistort image
		cvReleaseImage( &t );
		cvShowImage( "Undistort", image ); // Show corrected image

		// Handle pause/unpause and esc
		int c = cvWaitKey( 15 );
		if( c == 'p' ){
			c = 0;
			while( c != 'p' && c != 27 ){
				c = cvWaitKey( 250 );
			}
		}
		if( c == 27 )
			break;
		
		YuvCamera::GetInstance()->CaptureFrame();
		img = YuvCamera::GetInstance()->img;
		iplImage = IplImage(img);
	}

	return 0;
}
