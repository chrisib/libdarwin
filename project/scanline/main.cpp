#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <vector>
#include "minIni.h"
#include "SingleBlob.h"
#include "MultiBlob.h"
#include <cstring>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

#define VIDEO_FILE "../../data/video/yuv_obstacle_scene.avi"
#define INI_FILE "config.ini"

#define ROW_HEIGHT  300
#define COL_WIDTH   330

// comment to disable scanning for each target type individually and use the
// Target::FindTargets function instead
//
// with individual scanning:
// Max FPS: 90.9091
// Min FPS: 12.6582
// Avg FPS: 65.4085
//
// without individual scanning:
// Max FPS: 125
// Min FPS: 23.8095
// Avg FPS: 70.8363
//
// without individual scanning & with erode/dilate:
// Max FPS: 76.9231
// Min FPS: 17.8571
// Avg FPS: 46.6918

using namespace cv;
using namespace Robot;
using namespace std;

CvCapture *camera;
cv::VideoWriter  *videoStream;
const char* videoFile = "foo.avi";
const char* imageFile = "foo.png";

bool interactive = true;
bool scanIndividually = false;
bool twoPass = false;
int subsample = 1;

void setupWindows()
{
    namedWindow("YUV", CV_WINDOW_AUTOSIZE);
    namedWindow("RGB", CV_WINDOW_AUTOSIZE);
    namedWindow("RGB*", CV_WINDOW_AUTOSIZE);
    if(scanIndividually)
    {
        namedWindow("Wall", CV_WINDOW_AUTOSIZE);
        namedWindow("Gate", CV_WINDOW_AUTOSIZE);
        namedWindow("Pit", CV_WINDOW_AUTOSIZE);
    }
    else
        namedWindow("Scanline", CV_WINDOW_AUTOSIZE);
    if(twoPass)
    {
        namedWindow("Filter", CV_WINDOW_AUTOSIZE);
        namedWindow("Second Pass", CV_WINDOW_AUTOSIZE);
    }
	namedWindow("Y", CV_WINDOW_AUTOSIZE);
	namedWindow("U", CV_WINDOW_AUTOSIZE);
	namedWindow("V", CV_WINDOW_AUTOSIZE);
	namedWindow("R", CV_WINDOW_AUTOSIZE);
	namedWindow("G", CV_WINDOW_AUTOSIZE);
	namedWindow("B", CV_WINDOW_AUTOSIZE);
	namedWindow("R-G", CV_WINDOW_AUTOSIZE);
	namedWindow("R-B", CV_WINDOW_AUTOSIZE);
	namedWindow("G-B", CV_WINDOW_AUTOSIZE);
	
	cvMoveWindow( "YUV", 0, 0 );
    cvMoveWindow( "RGB", COL_WIDTH, 0 );
    cvMoveWindow( "RGB*", COL_WIDTH*2, 0 );
    if(scanIndividually)
    {
        cvMoveWindow( "Wall", COL_WIDTH*3, 0 );
        cvMoveWindow( "Gate", COL_WIDTH*3, ROW_HEIGHT );
        cvMoveWindow( "Pit", COL_WIDTH*3, ROW_HEIGHT*2 );
    }
    else
        cvMoveWindow( "Scanline", COL_WIDTH*3, 0 );
    if(twoPass)
    {
        cvMoveWindow( "Filter",  COL_WIDTH*2, ROW_HEIGHT );
        cvMoveWindow( "Second Pass",  COL_WIDTH*3, ROW_HEIGHT );
    }
	cvMoveWindow( "Y", 0, ROW_HEIGHT );
	cvMoveWindow( "U", COL_WIDTH/2, ROW_HEIGHT );
	cvMoveWindow( "V", COL_WIDTH*2/2, ROW_HEIGHT );
	cvMoveWindow( "R", 0, ROW_HEIGHT*3/2 );
	cvMoveWindow( "G", COL_WIDTH/2, ROW_HEIGHT*3/2 );
	cvMoveWindow( "B", COL_WIDTH*2/2, ROW_HEIGHT*3/2 );
    cvMoveWindow( "R-G", 0, ROW_HEIGHT*4/2 );
	cvMoveWindow( "R-B", COL_WIDTH/2, ROW_HEIGHT*4/2 );
	cvMoveWindow( "G-B", COL_WIDTH*2/2, ROW_HEIGHT*4/2 );
}

void parseArgs(int argc, char** argv)
{
    if(argc>1)
    {
        for(int i=0; i<argc; i++)
        {
            if(!strcmp(argv[i],"--play"))
                interactive = false;
            else if(!strcmp(argv[i],"--separate"))
                scanIndividually = true;
            else if(!strcmp(argv[i],"--twopass"))
            {
                twoPass = true;
                scanIndividually = false;
            }
            else if(!strcmp(argv[i],"--subsample"))
            {
				i++;
				subsample = atoi(argv[i]);
				cout << "Subsample: " << subsample << endl;
			}
            else if(!strcmp(argv[i],"--help"))
            {
                cout << argv[0] << " [--play] [--separate] [--twopass]" << endl <<
                        "    --play         non-interactive mode" << endl <<
                        "    --separate     perform a separate scanline for each target" << endl <<
                        "    --twopass      perform a two-pass scanline (overrides --separate)" << endl << 
                        "    --subsample X  use a subsample of X when performing scanline" << endl;
                exit(0);
            }
        }
    }
}

int main(int argc, char** argv)
{
    string iniFile = string(INI_FILE);
	minIni ini = minIni(iniFile);
	
	Mat mat;
	Mat blur;
    Mat rgb;
    Mat rgbc;
	Mat wallImg;
	Mat pitImg;
	Mat gateImg;
    Mat *processed = NULL;
    Mat *eroded = NULL;
    Mat *dilated = NULL;
	Mat y, u, v, r, g, b, *compliment, rg, rb, gb;
    Mat *tmp;
    
    Size dSize(Camera::WIDTH/2,Camera::HEIGHT/2);
	
	IplImage *frame;

	//camera = cvCaptureFromCAM(-1);
	
    parseArgs(argc, argv);
    setupWindows();
    
	cout << "Opening video file " << VIDEO_FILE << endl;
	camera = cvCaptureFromFile(VIDEO_FILE);
	//videoStream = new VideoWriter(videoFile, CV_FOURCC('D', 'I', 'V', 'X'), 30, cvSize(320,240),true);
	
	cout << "Loading target settings..." << endl;
    SingleBlob pit;
	pit.LoadIniSettings(ini,"Pit");
	
    SingleBlob gate;
	gate.LoadIniSettings(ini,"Gate");
	
    MultiBlob walls;
	walls.LoadIniSettings(ini,"Wall");
	cout << "Done" << endl;
    
    vector<BlobTarget*> targets;
    targets.push_back((BlobTarget*)&pit);
    targets.push_back((BlobTarget*)&gate);
    targets.push_back((BlobTarget*)&walls);
    	
	if(interactive)
    {
        cout << "Hold the space bar to advance each frame" << endl;
        cout << "Press ctrl-c to end" << endl;
    }

    struct timeval start, end;
    long mtime, seconds, useconds;
    double fps, minFPS, maxFPS, avgFPS=0;
    minFPS = 1000;
    maxFPS = -1;
    int numFailedFrames = 0;
    long numFrames = 0;
	for(;;)	
	{
		if(cvGrabFrame(camera))
		{
            gettimeofday(&start, NULL);
            
			frame = cvRetrieveFrame(camera);
			mat = Mat(frame);
            if(scanIndividually)
            {
                wallImg = Mat::zeros(mat.rows, mat.cols, mat.type());
                gateImg = Mat::zeros(mat.rows, mat.cols, mat.type());
                pitImg = Mat::zeros(mat.rows, mat.cols, mat.type());
            }

			if(processed == NULL)
			{
				processed = new Mat(mat.rows,mat.cols,mat.type());
                eroded = new Mat(mat.rows,mat.cols,mat.type());
                dilated = new Mat(mat.rows,mat.cols,mat.type());
			}
				
			GaussianBlur(mat, blur, Size(3,3), 0);
            cvtColor(blur,rgb,CV_YCrCb2RGB);
			compliment = Target::CreateCompliment(rgb);
			
			tmp = Target::ExtractChannel(blur,0);
            resize(*tmp,y,dSize);
            delete tmp;
            
			tmp = Target::ExtractChannel(blur,1);
            resize(*tmp,u,dSize);
            delete tmp;
            
			tmp = Target::ExtractChannel(blur,2);
            resize(*tmp,v,dSize);
            delete tmp;
            
            tmp = Target::ExtractChannel(rgb,0);
            resize(*tmp,r,dSize);
            delete tmp;
            
			tmp = Target::ExtractChannel(rgb,1);
            resize(*tmp,g,dSize);
            delete tmp;
            
			tmp = Target::ExtractChannel(rgb,2);
            resize(*tmp,b,dSize);
            delete tmp;
            
			tmp = Target::ExtractChannel(*compliment,0);
            resize(*tmp,rg,dSize);
            delete tmp;
            
			tmp = Target::ExtractChannel(*compliment,1);
            resize(*tmp,rb,dSize);
            delete tmp;
            
			tmp = Target::ExtractChannel(*compliment,2);
            resize(*tmp,gb,dSize);
            delete tmp;

            if(!scanIndividually)
            {
                BlobTarget::FindTargets(targets,blur,processed,subsample);
                
                if(twoPass)
                {
                    // erode & dilate processed, and then scan again
                    erode(*processed, *eroded, Mat(),Point(-1,-1),1);
                    dilate(*processed, *dilated, Mat(),Point(-1,-1),1);
                    
                    BlobTarget::FindTargets(targets,*dilated,eroded,subsample);
                }
            }

			if((scanIndividually && walls.FindInFrame(blur, &wallImg)) || (!scanIndividually && walls.GetBoundingBoxes()->size()>0))
			{
				walls.Print();
				walls.Draw(mat);
                walls.Draw(rgb);
                walls.Draw(*compliment);
                if(scanIndividually)
                    walls.Draw(wallImg);
                else
                    walls.Draw(*processed);
			}
            else
                cout << "Walls not found" << endl;
			
			if((scanIndividually && gate.FindInFrame(blur, &gateImg)) || (!scanIndividually && gate.WasFound()))
			{
				gate.Print();
				gate.Draw(mat);
                gate.Draw(rgb);
                gate.Draw(*compliment);
                if(scanIndividually)
                    gate.Draw(gateImg);
                else
                    gate.Draw(*processed);
			}
            else
                cout << "Gate not found" << endl;
			
            if((scanIndividually && pit.FindInFrame(blur, &pitImg)) || (!scanIndividually && pit.WasFound()))
			{
				pit.Print();
				pit.Draw(mat);
                pit.Draw(rgb);
                pit.Draw(*compliment);
                if(scanIndividually)
                    pit.Draw(pitImg);
                else
                    pit.Draw(*processed);
			}
            else
                cout << "Pit not found" << endl;
			
			imshow("YUV",mat);
            imshow("RGB*",*compliment);
            imshow("RGB",rgb);
            if(scanIndividually)
            {
                imshow("Wall",wallImg);
                imshow("Gate",gateImg);
                imshow("Pit",pitImg);
            }
            else
                imshow("Scanline",*processed);
            if(twoPass)
            {
                imshow("Filter",*dilated);
                imshow("Second Pass",*eroded);
            }
			imshow("Y",y);
			imshow("U",u);
			imshow("V",v);
			imshow("R",r);
			imshow("G",g);
			imshow("B",b);
            imshow("R-G",rg);
			imshow("R-B",rb);
			imshow("G-B",gb);
			
            gettimeofday(&end, NULL);
            seconds  = end.tv_sec  - start.tv_sec;
            useconds = end.tv_usec - start.tv_usec;
            mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
            fps = 1.0/mtime * 1000.0;
            cout << fps << " FPS" << endl;
            
            if(fps < minFPS)
                minFPS = fps;
            if(fps > maxFPS)
                maxFPS = fps;
            avgFPS += fps;
            numFrames++;
			
			waitKey(interactive ? 0:1);
			
			delete compliment;
			
			//(*videoStream) << *processed;
			
			//delete processed;
		}
        else
        {
            numFailedFrames++;
            
            if(numFailedFrames > 10)
                break;
        }
	}
    
    cout << "Max FPS: " << maxFPS << endl;
    cout << "Min FPS: " << minFPS << endl;
    cout << "Avg FPS: " << (avgFPS/numFrames) << endl;
}
