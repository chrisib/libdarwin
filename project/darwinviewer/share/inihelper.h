//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef INIHELPER_H
#define INIHELPER_H


//------------------------------------------------------------------------------
// INCLUDE HEADERS AND FORWARD DECLARATION
//------------------------------------------------------------------------------
#include <QString>
class QByteArray;
class QFile;


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
char ini_getc( QFile& file, const QString& fieldname, const QString& section = QString( ), char defValue = 0 );
char ini_getc( const QString& filename, const QString& fieldname, const QString& section = QString( ), char defValue = 0 );
double ini_getd( QFile& file, const QString& fieldname, const QString& section = QString( ), double defValue = 0.0 );
double ini_getd( const QString& filename, const QString& fieldname, const QString& section = QString( ), double defValue = 0.0 );
int ini_geti( QFile& file, const QString& fieldname, const QString& section = QString( ), int defValue = 0 );
int ini_geti( const QString& filename, const QString& fieldname, const QString& section = QString( ), int defValue = 0 );
QString ini_gets( QFile& file, const QString& fieldname, const QString& section = QString( ), QString defValue = QString( ) );
QString ini_gets( const QString& filename, const QString& fieldname, const QString& section = QString( ), QString defValue = QString( ) );

QByteArray ini_setc( const char value, const QByteArray& data, const QString& fieldname, const QString& section = QString( ) );
QByteArray ini_setc( const char value, const QString& filename, const QString& fieldname, const QString& section = QString( ) );
QByteArray ini_setd( const double value, const QByteArray& data, const QString& fieldname, const QString& section = QString( ) );
QByteArray ini_setd( const double value, const QString& filename, const QString& fieldname, const QString& section = QString( ) );
QByteArray ini_seti( const int value, const QByteArray& data, const QString& fieldname, const QString& section = QString( ) );
QByteArray ini_seti( const int value, const QString& filename, const QString& fieldname, const QString& section = QString( ) );
QByteArray ini_sets( const QString& value, const QByteArray& data, const QString& fieldname, const QString& section = QString( ) );
QByteArray ini_sets( const QString& value, const QString& filename, const QString& fieldname, const QString& section = QString( ) );


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
