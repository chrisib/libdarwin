//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "netpacket.h"


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define NETPACKET_HEADER_0 0xFFFFFFFF
#define NETPACKET_HEADER_1 0x00000000


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
unsigned int npChecksum( PNETPACKET pNetPacket )
{
    unsigned int sum = 0;
    unsigned int i;

    for( i = 0; i < pNetPacket->data_length; i += 4 )
    {
        sum += toInt( pNetPacket->data + i );
    }
    return sum;
}


unsigned int npIsValid( PNETPACKET pNetPacket )
{
    return pNetPacket->header0 == NETPACKET_HEADER_0
        && pNetPacket->header1 == NETPACKET_HEADER_1
        && pNetPacket->checksum == npChecksum( pNetPacket )
        && pNetPacket->message < NM__LENGTH
        && pNetPacket->data_length + NETPACKET_EXTRA_SIZE == pNetPacket->buffer_length;
}


void npInitialize( PNETPACKET pNetPacket, NETMESSAGE netMessage )
{
    pNetPacket->header0 = NETPACKET_HEADER_0;
    pNetPacket->header1 = NETPACKET_HEADER_1;
    pNetPacket->checksum = 0;
    pNetPacket->message = netMessage;
    pNetPacket->data_length = 0;
}


void npFinalize( PNETPACKET pNetPacket )
{
    pNetPacket->checksum = npChecksum( pNetPacket );
    pNetPacket->buffer_length = pNetPacket->data_length + NETPACKET_EXTRA_SIZE;
}


void npAppendData( PNETPACKET pNetPacket, unsigned char* data, unsigned int length )
{
    unsigned int i;

    for( i = 0; i < length; ++i )
    {
        pNetPacket->data[pNetPacket->data_length++] = data[i];
    }
}


void npAppendInt( PNETPACKET pNetPacket, int value )
{
    npAppendData( pNetPacket, (unsigned char*)(&value), sizeof( int ) );
}


void npAppendUInt( PNETPACKET pNetPacket, unsigned int value )
{
    npAppendData( pNetPacket, (unsigned char*)(&value), sizeof( unsigned int ) );
}


int toInt( unsigned char* buffer )
{
    return *((int*)buffer);
}

