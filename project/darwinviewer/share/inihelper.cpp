//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "inihelper.h"
#include <QBuffer>
#include <QByteArray>
#include <QFile>
#include <QString>
#include <QStringList>


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define INI_COMMENT_CHAR '#'


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
static QString ini_iterate( QIODevice& file, const QString& fieldname, const QString& section, qint64* pLinePos = 0, qint64* pSecPos = 0 );


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
char ini_getc( QFile& file, const QString& fieldname, const QString& section, char defValue )
{
    QString str = ini_gets( file, fieldname, section, QString( ) );

    if( !str.isNull( ) && !str.isEmpty( ) && str.length( ) == 1 )
    {
        defValue = str[0].toLatin1();
    }
    return defValue;
}


char ini_getc( const QString& filename, const QString& fieldname, const QString& section, char defValue )
{
    QFile file( filename );

    if( file.open( QIODevice::ReadOnly ) )
    {
        defValue = ini_getc( file, fieldname, section, defValue );
        file.close( );
    }
    return defValue;
}


double ini_getd( QFile& file, const QString& fieldname, const QString& section, double defValue )
{
    QString str = ini_gets( file, fieldname, section, QString( ) );
    double value = 0.0;
    bool okay = false;

    if( !str.isNull( ) && !str.isEmpty( ) )
    {
        value = str.toDouble( &okay );
        if( okay )
        {
            defValue = value;
        }
    }
    return defValue;
}


double ini_getd( const QString& filename, const QString& fieldname, const QString& section, double defValue )
{
    QFile file( filename );

    if( file.open( QIODevice::ReadOnly ) )
    {
        defValue = ini_getd( file, fieldname, section, defValue );
        file.close( );
    }
    return defValue;
}


int ini_geti( QFile& file, const QString& fieldname, const QString& section, int defValue )
{
    QString str = ini_gets( file, fieldname, section, QString( ) );
    int value = 0;
    bool okay = false;

    if( !str.isNull( ) && !str.isEmpty( ) )
    {
        value = str.toInt( &okay );
        if( okay )
        {
            defValue = value;
        }
    }
    return defValue;
}


int ini_geti( const QString& filename, const QString& fieldname, const QString& section, int defValue )
{
    QFile file( filename );

    if( file.open( QIODevice::ReadOnly ) )
    {
        defValue = ini_geti( file, fieldname, section, defValue );
        file.close( );
    }
    return defValue;
}


QString ini_gets( QFile& file, const QString& fieldname, const QString& section, QString defValue )
{
    QString value = ini_iterate( file, fieldname, section );
    return (value.isNull( ) ? defValue : value);
}


QString ini_gets( const QString& filename, const QString& fieldname, const QString& section, QString defValue )
{
    QFile file( filename );

    if( file.open( QIODevice::ReadOnly ) )
    {
        defValue = ini_gets( file, fieldname, section, defValue );
        file.close( );
    }
    return defValue;
}


QByteArray ini_setc( const char value, const QByteArray& data, const QString& fieldname, const QString& section )
{
    return ini_sets( QString( "%1" ).arg( value ), data, fieldname, section );
}


QByteArray ini_setc( const char value, const QString& filename, const QString& fieldname, const QString& section )
{
    return ini_sets( QString( "%1" ).arg( value ), filename, fieldname, section );
}


QByteArray ini_setd( const double value, const QByteArray& data, const QString& fieldname, const QString& section )
{
    return ini_sets( QString( "%1" ).arg( value ), data, fieldname, section );
}


QByteArray ini_setd( const double value, const QString& filename, const QString& fieldname, const QString& section )
{
    return ini_sets( QString( "%1" ).arg( value ), filename, fieldname, section );
}


QByteArray ini_seti( const int value, const QByteArray& data, const QString& fieldname, const QString& section )
{
    return ini_sets( QString( "%1" ).arg( value ), data, fieldname, section );
}


QByteArray ini_seti( const int value, const QString& filename, const QString& fieldname, const QString& section )
{
    return ini_sets( QString( "%1" ).arg( value ), filename, fieldname, section );
}


QByteArray ini_sets( const QString& value, const QByteArray& data, const QString& fieldname, const QString& section )
{
    QBuffer memory;
    QByteArray result;
    qint64 linepos = -1;
    qint64 secpos = -1;
    QString fieldvalue;
    QString line;
    int commentIndex;

    memory.setData( data );
    memory.open( QIODevice::ReadOnly );
    fieldvalue = ini_iterate( memory, fieldname, section, &linepos, &secpos );
    memory.seek( 0 );
    if( fieldvalue.isNull( ) )
    {   // field is not found
        if( secpos == -1 )
        {   // no section found
            result.append( memory.readAll( ) );
            result.append( QString( "[%1]\n" ).arg( section ) ); // section
            result.append( QString( "%1=%2\n" ).arg( fieldname ).arg( value ) ); // field
            result.append( QString( "\n" ) );
        }
        else
        {   // section exist
            result.append( memory.read( secpos ) );
            result.append( memory.readLine( ) ); // section
            result.append( QString( "%1=%2\n" ).arg( fieldname ).arg( value ) ); // field
            result.append( memory.readAll( ) );
        }
    }
    else
    {   // field exist
        result.append( memory.read( linepos ) );
        line = memory.readLine( ); // emit field on the file
        commentIndex = line.indexOf( INI_COMMENT_CHAR );
        if( 0 <= commentIndex && commentIndex < line.length( ) )
        {   // comment is exist
            line = line.mid( commentIndex );
            line = line.replace( QString( "\r" ), QString( "" ) ).replace( QString( "\n" ), QString( "" ) );
            result.append( QString( "%1=%2 %3\n" ).arg( fieldname ).arg( value ).arg( line ) );
        }
        else
        {   // no comment
            result.append( QString( "%1=%2\n" ).arg( fieldname ).arg( value ) ); // field
        }
        result.append( memory.readAll( ) );
    }
    memory.close( );
    return result;
}


QByteArray ini_sets( const QString& value, const QString& filename, const QString& fieldname, const QString& section )
{
    QByteArray result;
    QFile file( filename );

    if( file.open( QIODevice::ReadOnly ) )
    {
        result = ini_sets( value, file.readAll( ), fieldname, section );
        file.close( );
    }
    return result;
}


static QString ini_iterate( QIODevice& file, const QString& fieldname, const QString& section, qint64* pLinePos, qint64* pSecPos )
{
    QStringList tokens;
    QString line; // current processing line
    QString value; // found field value
    qint64 linepos = -1; // found line beginning position
    qint64 secpos = -1; // found section beginning position
    bool found_section = section.isNull( );
    bool finished = false;
    int commentIndex;

    if( file.isOpen( ) && (file.openMode( ) & QIODevice::ReadOnly) != 0 && file.seek( 0 ) )
    {   // check file is opened and go to the beginning
        while( !finished && file.bytesAvailable( ) )
        {
            linepos = file.pos( );
            line = QString( file.readLine( ) );
            commentIndex = line.indexOf( INI_COMMENT_CHAR ); // comment
            if( 0 <= commentIndex && commentIndex < line.length( ) )
            {   // get rid of the comment
                line = line.left( commentIndex );
            }
            line = line.simplified( ); // no space/tab characters
            if( line.isNull( ) || line.isEmpty( ) )
            {   // empty line, do nothing
            }
            else if( line.startsWith( QChar( '[' ) ) && line.endsWith( QChar( ']' ) ) )
            {   // section name
                if( found_section )
                {   // reached next section
                    // if section is null, then we go through whole file
                    finished = !section.isNull( );
                }
                else
                {
                    line = line.mid( 1, line.length( ) - 2 );
                    if( line.compare( section ) == 0 )
                    {   // target section has been found
                        found_section = true;
                        secpos = linepos;
                    }
                }
            }
            else if( found_section )
            {   // check the field name
                tokens = line.split( QChar( '=' ), QString::SkipEmptyParts );
                if( tokens.length( ) >= 1 && fieldname.compare( tokens[0].simplified( ) ) == 0 )
                {   // this field name is what we are looking for
                    value = (tokens.length( ) >= 2 ? tokens[1].simplified( ) : QString( "" ));
                    finished = true;
                }
            }
            else
            {   // something on line, skip
            }
        }
    }
    if( pLinePos != 0 )
    {
        *pLinePos = linepos;
    }
    if( pSecPos != 0 )
    {
        *pSecPos = secpos;
    }
    return value;
}

