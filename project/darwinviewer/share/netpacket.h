//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef NETPACKET_H
#define NETPACKET_H


//------------------------------------------------------------------------------
// EXTERN C
//------------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define NETWORK_PORT 2011
#define NETPACKET_MAX_SIZE (1024 * 1024 * 2) // 2MB
#define NETPACKET_EXTRA_SIZE 20

enum _NETMESSAGE
{
    NM_NONE,
    NM_IMAGE,
    NM_SIZE,
    NM__LENGTH
};
typedef enum _NETMESSAGE NETMESSAGE;

struct _NETPACKET
{
    unsigned int buffer_length;
    union
    {
        unsigned char buffer[NETPACKET_MAX_SIZE];
        struct
        {
            unsigned int header0;
            unsigned int header1;
            unsigned int checksum;
            unsigned int message;
            unsigned int data_length;
            unsigned char data[];
        };
    };
};
typedef struct _NETPACKET NETPACKET, * PNETPACKET;


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
unsigned int npChecksum( PNETPACKET pNetPacket );
unsigned int npIsValid( PNETPACKET pNetPacket );
void npInitialize( PNETPACKET pNetPacket, NETMESSAGE netMessage );
void npFinalize( PNETPACKET pNetPacket );
void npAppendData( PNETPACKET pNetPacket, unsigned char* data, unsigned int length );
void npAppendInt( PNETPACKET pNetPacket, int value );
void npAppendUInt( PNETPACKET pNetPacket, unsigned int value );
int toInt( unsigned char* buffer );


//------------------------------------------------------------------------------
// END OF EXTERN C
//------------------------------------------------------------------------------
#ifdef __cplusplus
} // end extern "C"
#endif


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
