//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "imageprocess.h"
#include <assert.h>
#include <opencv/cv.h>
#include <QColor>
#include <QImage>


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
ImageProcess::ImageProcess( void )
{
    strcpy( _targets[0].name, "Yellow" );
    _targets[0].colour_marker = cvScalar( 0, 255, 255 );
    strcpy( _targets[1].name, "Red" );
    _targets[1].colour_marker = cvScalar( 0, 0, 255 );
    strcpy( _targets[2].name, "Green" );
    _targets[2].colour_marker = cvScalar( 0, 255, 0 );
    strcpy( _targets[3].name, "Blue" );
    _targets[3].colour_marker = cvScalar( 255, 0, 0 );
    strcpy( _targets[4].name, "Pink" );
    _targets[4].colour_marker = cvScalar( 255, 0, 255 );
    strcpy( _targets[5].name, "Aqua" );
    _targets[5].colour_marker = cvScalar( 255, 255, 0 );
    for( int i = 0; i < CI__LENGTH; ++i )
    {
        _targets[i].imgMark = 0;
        resetTarget( i );
    }
    _imgPreYUV = 0;
    _imgPreRGB = 0;
    _imgMarkAll = 0;
    _markOnlyAll = false;
    _markOnlyTarget = false;
}


ImageProcess::ImageProcess( const ImageProcess& other )
{
    for( int i = 0; i < CI__LENGTH; ++i )
    {
        memcpy( &(_targets[i]), &(other._targets[i]), sizeof( COLOUR_TARGET ) );
        _targets[i].imgMark = 0;
    }
    _imgPreYUV = 0;
    _imgPreRGB = 0;
    _imgMarkAll = 0;
    _markOnlyAll = other._markOnlyAll;
    _markOnlyTarget = other._markOnlyTarget;
}


ImageProcess::~ImageProcess( void )
{
    for( int i = 0; i < CI__LENGTH; ++i )
    {
        if( _targets[i].imgMark != 0 )
        {
            cvReleaseImage( &_targets[i].imgMark );
            _targets[i].imgMark = 0;
        }
    }
    if( _imgMarkAll != 0 )
    {
        cvReleaseImage( &_imgMarkAll );
        _imgMarkAll = 0;
    }
    if( _imgPreRGB != 0 )
    {
        cvReleaseImage( &_imgPreRGB );
        _imgPreRGB = 0;
    }
    if( _imgPreYUV != 0 )
    {
        cvReleaseImage( &_imgPreYUV );
        _imgPreYUV = 0;
    }
}


ImageProcess& ImageProcess::operator=( const ImageProcess& other )
{
    for( int i = 0; i < CI__LENGTH; ++i )
    {
        setTargetYUV( i, other._targets[i].colour_yuv );
    }
    _markOnlyAll = other._markOnlyAll;
    _markOnlyTarget = other._markOnlyTarget;
    return *this;
}


void ImageProcess::addPixelToTarget( int index, int x, int y )
{
    unsigned char* pYuv;

    if( _imgPreYUV != 0 &&
        0 <= index && index < CI__LENGTH &&
        0 <= x && x < _imgPreYUV->width &&
        0 <= y && y < _imgPreYUV->height )
    {
        pYuv = ((unsigned char*)_imgPreYUV->imageData) + (_imgPreYUV->widthStep * y) + (_imgPreYUV->nChannels * x);
        if( _targets[index].colour_yuv[YUVI_Y_MIN] > pYuv[0] )
        {
            _targets[index].colour_yuv[YUVI_Y_MIN] = pYuv[0];
        }
        else if( _targets[index].colour_yuv[YUVI_Y_MAX] < pYuv[0] )
        {
            _targets[index].colour_yuv[YUVI_Y_MAX] = pYuv[0];
        }
        if( _targets[index].colour_yuv[YUVI_U_MIN] > pYuv[1] )
        {
            _targets[index].colour_yuv[YUVI_U_MIN] = pYuv[1];
        }
        else if( _targets[index].colour_yuv[YUVI_U_MAX] < pYuv[1] )
        {
            _targets[index].colour_yuv[YUVI_U_MAX] = pYuv[1];
        }
        if( _targets[index].colour_yuv[YUVI_V_MIN] > pYuv[2] )
        {
            _targets[index].colour_yuv[YUVI_V_MIN] = pYuv[2];
        }
        else if( _targets[index].colour_yuv[YUVI_V_MAX] < pYuv[2] )
        {
            _targets[index].colour_yuv[YUVI_V_MAX] = pYuv[2];
        }
    }
}


void ImageProcess::resetTarget( int index )
{
    if( 0 <= index && index < CI__LENGTH )
    {
        _targets[index].colour_yuv[YUVI_Y_MAX] = 0;
        _targets[index].colour_yuv[YUVI_Y_MIN] = 255;
        _targets[index].colour_yuv[YUVI_U_MAX] = 0;
        _targets[index].colour_yuv[YUVI_U_MIN] = 255;
        _targets[index].colour_yuv[YUVI_V_MAX] = 0;
        _targets[index].colour_yuv[YUVI_V_MIN] = 255;
    }
}


void ImageProcess::setTargetYUV( int index, const unsigned char colour_yuv[YUVI__LENGTH] )
{
    if( 0 <= index && index < CI__LENGTH )
    {
        memcpy( _targets[index].colour_yuv, colour_yuv, YUVI__LENGTH * sizeof( unsigned char ) );
    }
}


const char* ImageProcess::targetName( int index ) const
{
    if( 0 <= index && index < CI__LENGTH )
    {
        return _targets[index].name;
    }
    return 0;
}


void ImageProcess::targetYUV( int index, unsigned char colour_yuv[YUVI__LENGTH] ) const
{
    if( 0 <= index && index < CI__LENGTH )
    {
        memcpy( colour_yuv, _targets[index].colour_yuv, YUVI__LENGTH * sizeof( unsigned char ) );
    }
}


void ImageProcess::process( const IplImage* src )
{
    unsigned char* pYuv;
    unsigned char* pRgb;
    unsigned char* pMark;
    unsigned char* pTargets[CI__LENGTH];
    CvScalar colour;

    if( src == 0 || src->width <= 0 || src->height <= 0 )
    {
        return ;
    }

    _imgBuffer = _procInitImage( _imgBuffer, cvGetSize( src ), src->depth, src->nChannels );
    _imgPreYUV = _procInitImage( _imgPreYUV, cvGetSize( src ), src->depth, src->nChannels );
    _imgPreRGB = _procInitImage( _imgPreRGB, cvGetSize( src ), src->depth, src->nChannels );
    _imgMarkAll = _procInitImage( _imgMarkAll, cvGetSize( src ), src->depth, src->nChannels );
    for( int i = 0; i < CI__LENGTH; ++i )
    {
        _targets[i].imgMark = _procInitImage( _targets[i].imgMark, cvGetSize( src ), src->depth, src->nChannels );
    }

    _procPreprocessing( src );
    for( int row = 0; row < _imgPreYUV->height; ++row )
    {
        pYuv = ((unsigned char*)_imgPreYUV->imageData) + (_imgPreYUV->widthStep * row);
        pRgb = ((unsigned char*)_imgPreRGB->imageData) + (_imgPreRGB->widthStep * row);
        pMark = ((unsigned char*)_imgMarkAll->imageData) + (_imgMarkAll->widthStep * row);
        for( int i = 0; i < CI__LENGTH; ++i )
        {
            pTargets[i] = ((unsigned char*)_targets[i].imgMark->imageData) + (_targets[i].imgMark->widthStep * row);
        }
        for( int col = 0; col < _imgPreYUV->width; ++col )
        {
            colour = cvtYCrCb2RGB( cvScalar( pYuv[0], pYuv[1], pYuv[2] ) );
            pRgb[0] = colour.val[0];
            pRgb[1] = colour.val[1];
            pRgb[2] = colour.val[2];
            if( _markOnlyAll )
            {
                pMark[0] = 0x00;
                pMark[1] = 0x00;
                pMark[2] = 0x00;
            }
            else
            {
                pMark[0] = colour.val[0];
                pMark[1] = colour.val[1];
                pMark[2] = colour.val[2];
            }
            for( int i = 0; i < CI__LENGTH; ++i )
            {
                if( _targets[i].colour_yuv[YUVI_Y_MIN] <= pYuv[0] && pYuv[0] <= _targets[i].colour_yuv[YUVI_Y_MAX] &&
                    _targets[i].colour_yuv[YUVI_U_MIN] <= pYuv[1] && pYuv[1] <= _targets[i].colour_yuv[YUVI_U_MAX] &&
                    _targets[i].colour_yuv[YUVI_V_MIN] <= pYuv[2] && pYuv[2] <= _targets[i].colour_yuv[YUVI_V_MAX] )
                {
                    pMark[0] = _targets[i].colour_marker.val[0];
                    pMark[1] = _targets[i].colour_marker.val[1];
                    pMark[2] = _targets[i].colour_marker.val[2];
                    pTargets[i][0] = _targets[i].colour_marker.val[0];
                    pTargets[i][1] = _targets[i].colour_marker.val[1];
                    pTargets[i][2] = _targets[i].colour_marker.val[2];
                }
                else if( _markOnlyTarget )
                {
                    pTargets[i][0] = 0x00;
                    pTargets[i][1] = 0x00;
                    pTargets[i][2] = 0x00;
                }
                else
                {
                    pTargets[i][0] = colour.val[0];
                    pTargets[i][1] = colour.val[1];
                    pTargets[i][2] = colour.val[2];
                }
                pTargets[i] += _targets[i].imgMark->nChannels;
            }
            pMark += _imgMarkAll->nChannels;
            pRgb += _imgPreRGB->nChannels;
            pYuv += _imgPreYUV->nChannels;
        }
    }
}


void ImageProcess::setPreprocessing( const PREPROCESSING_OPTION& option )
{
    memcpy( &_preopt, &option, sizeof( PREPROCESSING_OPTION ) );
}


IplImage* ImageProcess::_procInitImage( IplImage* image, const CvSize& size, int depth, int nChannels )
{
    if( image != 0 )
    {
        if( image->width != size.width || image->height != size.height ||
            image->depth != depth || image->nChannels != nChannels )
        {
            cvReleaseImage( &image );
            image = 0;
        }
    }
    if( image == 0 )
    {
        image = cvCreateImage( size, depth, nChannels );
    }
    return image;
}


void ImageProcess::_procPreprocessing( const IplImage* source )
{
    IplImage* src = _imgPreYUV;
    IplImage* dst = _imgBuffer;
    IplImage* ptr;

    cvCopyImage( source, _imgPreYUV );
    if( _preopt.flip )
    {
        cvFlip( src, dst, _preopt.filp_mode );
        SWAP( src, dst, ptr );
    }
    if( _preopt.erode )
    {
        cvErode( src, dst );
        SWAP( src, dst, ptr );
    }
    if( _preopt.dilate )
    {
        cvDilate( src, dst );
        SWAP( src, dst, ptr );
    }
    if( _preopt.morph )
    {
        IplConvKernel* element = 0;
        switch( _preopt.morph_operation )
        {
        case CV_MOP_OPEN:
        case CV_MOP_CLOSE:
        case CV_MOP_GRADIENT:
        case CV_MOP_TOPHAT:
        case CV_MOP_BLACKHAT:
            cvMorphologyEx( src, dst, _imgPreRGB/*temp*/, element, _preopt.morph_operation );
            break;
        default:
            cvMorphologyEx( src, dst, 0, element, CV_MOP_OPEN );
            cvMorphologyEx( src, dst, 0, element, CV_MOP_CLOSE );
            break;
        }
        SWAP( src, dst, ptr );
    }
    if( _preopt.smooth )
    {
        switch( _preopt.smooth_mode )
        {
        case CV_BLUR_NO_SCALE:
        case CV_BLUR:
        case CV_GAUSSIAN:
        case CV_MEDIAN:
        case CV_BILATERAL:
            cvSmooth( src, dst, _preopt.smooth_mode );
            break;
        default:
            cvSmooth( src, dst );
            break;
        }
        SWAP( src, dst, ptr );
    }
    if( src != _imgPreYUV )
    {
        SWAP( _imgPreYUV, _imgBuffer, ptr );
    }
}


QImage* ImageProcess::cvtIplImage2QImage( const IplImage& src )
{
    QImage* image = 0;
    QRgb* imagePtr;
    unsigned char* srcPtr;

    switch( src.depth )
    {
    case IPL_DEPTH_8U:
        image = new QImage( src.width, src.height, QImage::Format_RGB32 );
        for( int row = 0; row < src.height; ++row )
        {
            imagePtr = (QRgb*)image->scanLine( row );
            srcPtr = (unsigned char*)src.imageData + (src.widthStep * row);
            for( int col = 0; col < src.width; ++col )
            {
                *imagePtr = cvtBGRA2QRgba( srcPtr, src.nChannels );
                imagePtr++;
                srcPtr += src.nChannels;
            }
        }
        break;
    default:
        break;
    }
    return image;
}


QRgb ImageProcess::cvtBGRA2QRgba( const unsigned char* ptr, int channel )
{
    unsigned char a = 0, r = 0, g = 0, b = 0;

    switch( channel )
    {
    case 1:
        r = g = b = ptr[0];
        a = 0xFF;
        break;
    case 3:
        b = ptr[0];
        g = ptr[1];
        r = ptr[2];
        a = 0xFF;
        break;
    case 4:
        b = ptr[0];
        g = ptr[1];
        r = ptr[2];
        a = ptr[3];
        break;
    default:
        assert( "Unhandled camera image channel" == 0 );
        break;
    }
    return qRgba( r, g, b, a );
}


CvScalar ImageProcess::cvtYCrCb2RGB( const CvScalar& color_YCrCb )
{
    const double delta = 128.0;
    const double y = color_YCrCb.val[0];
    const double cr = color_YCrCb.val[1];
    const double cb = color_YCrCb.val[2];
    double r = y + 1.403 * (cr - delta);
    double g = y - 0.344 * (cr - delta) - 0.714 * (cb - delta);
    double b = y + 1.773 * (cb - delta);

    r = (r > 255.0 ? 255.0 : (r < 0.0 ? 0.0 : r));
    g = (g > 255.0 ? 255.0 : (g < 0.0 ? 0.0 : g));
    b = (b > 255.0 ? 255.0 : (b < 0.0 ? 0.0 : b));
    return cvScalar( r, g, b );
}

