#ifndef SCANLINE_H
#define SCANLINE_H

#include <opencv/cv.h>
#include <darwin/framework/ThresholdRange.h>
#include <darwin/framework/Camera.h>
#include <darwin/framework/Target.h>
#include "imageprocess.h"
#include <vector>
#include <darwin/framework/MultiBlob.h>

class Scanline
{
public:
    Scanline();
    ~Scanline();

    void setSubsample(int subsample);
    void setMarkColour(int index, int r, int g, int b){_targets[index].SetMarkColour(r,g,b);}

    void setFillThreshold(int index, int x){_targets[index].SetFillThreshold(x);}
    void setScanThreshold(int index, int x){_targets[index].SetScanThreshold(x);}
    void setMinScanLength(int index, int length){_targets[index].SetMinScanLength(length);}
    void setMinPixelHeight(int index, int height){_targets[index].SetMinPixelHeight(height);}
    void setMinPixelWidth(int index, int width){_targets[index].SetMinPixelWidth(width);}

    void setTargetY(int index, int min, int max){_targets[index].SetYRange(min,max);}
    void setTargetU(int index, int min, int max){_targets[index].SetURange(min,max);}
    void setTargetV(int index, int min, int max){_targets[index].SetVRange(min,max);}

    void setDimensions(int index, int width, int height){_targets[index].SetDimensions(width,height);}
    void setKeyChannel(int index, int channel){_targets[index].SetKeyChannel(channel);}

    void setProportionsDisabled(int index, bool b){_targets[index].SetProportionsDisabled(b);}
    void setFillDisabled(int index, bool b){_targets[index].SetFillDisabled(b);}
    void setSizeDisabled(int index, bool b){_targets[index].SetSizeDisabled(b);}
    void setYRangeDisabled(int index, bool b){_targets[index].SetYRangeDisabled(b);}
    void setURangeDisabled(int index, bool b){_targets[index].SetURangeDisabled(b);}
    void setVRangeDisabled(int index, bool b){_targets[index].SetVRangeDisabled(b);}
    void setKeyRangeDisabled(int index, bool b){_targets[index].SetKeyRangeDisabled(b);}


    int getSubsample(){return _targets[0].GetSubsample();}
    cv::Scalar* getMarkColour(int index){return _targets[index].GetMarkColour();}

    int getFillThreshold(int index){return _targets[index].GetFillThreshold();}
    int getScanThreshold(int index){return _targets[index].GetScanThreshold();}
    int getMinScanLength(int index){return _targets[index].GetMinScanLength();}
    int getMinPixelHeight(int index){return _targets[index].GetMinPixelHeight();}
    int getMinPixelWidth(int index){return _targets[index].GetMinPixelWidth();}

    int getHeight(int index){return _targets[index].GetRealHeight();}
    int getWidth(int index){return _targets[index].GetRealWidth();}
    int getKeyChannel(int index){return _targets[index].GetKeyChannel();}

    int getProportionsDisabled(int index){return _targets[index].GetProportionsDisabled() ? 1 : 0;}
    int getFillDisabled(int index){return _targets[index].GetFillDisabled() ? 1 : 0;}
    int getSizeDisabled(int index){return _targets[index].GetSizeDisabled() ? 1 : 0;}
    int getYRangeDisabled(int index){return _targets[index].GetYRangeDisabled() ? 1 : 0;}
    int getURangeDisabled(int index){return _targets[index].GetURangeDisabled() ? 1 : 0;}
    int getVRangeDisabled(int index){return _targets[index].GetVRangeDisabled() ? 1 : 0;}
    int getKeyRangeDisabled(int index){return _targets[index].GetKeyRangeDisabled() ? 1 : 0;}

    void configLoad(minIni* ini, int index, const char* section);


    virtual void process( const IplImage* src );

    IplImage scanlineResult;

    virtual void setPreprocessing( const PREPROCESSING_OPTION& option );
    virtual const PREPROCESSING_OPTION& preprocessing( void ) const { return _preopt; }

private:
    Robot::MultiBlob _targets[CI__LENGTH];
    std::vector<Robot::BlobTarget*> _scanTargets;

    cv::Mat scanlineOutput;

    PREPROCESSING_OPTION _preopt;

    void preprocess(cv::Mat &img);
};

#endif // SCANLINE_H
