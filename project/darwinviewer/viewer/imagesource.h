//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef IMAGESOURCE_H
#define IMAGESOURCE_H


//------------------------------------------------------------------------------
// INCLUDE HEADERS AND FORWARD DECLARATION
//------------------------------------------------------------------------------
#include "netpacket.h"
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <QTcpSocket>
class QHostAddress;


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
enum _IMAGE_SOURCE
{
    IS_CAMERA,
    IS_ROBOT,
    IS_VIDEO,
};
typedef enum _IMAGE_SOURCE IMAGE_SOURCE;


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class ImageSource
{
private:
    IMAGE_SOURCE _source; // current source
    bool _isValid; // true, if source connected/opened; otherwise, false
    bool _isPause; // pause capture

    QTcpSocket _socket;
    NETPACKET _netpacket;
    IplImage* _imgNetSrc; // source image (network)
    unsigned int _sReadSize; // socket read size

    cv::VideoCapture _capture;
    cv::Mat _matCam; // camera image
    IplImage _imgCamSrc; // source image (camera/video)

public:
    ImageSource( void );
    virtual ~ImageSource( void );
    virtual void close( void );
    virtual bool connectRobot( const QHostAddress& address );
    virtual bool openCamera( int deviceNum );
    virtual bool openVideo( const char* filename );
    virtual double frameCount( void );
    virtual double framePosition( void );
    virtual void frameSetPosition( double frame );
    virtual IplImage* next( void );

    virtual bool isValid( void ) const { return _isValid; }
    virtual bool isPaused( void ) const { return _isPause; }
    virtual void pause( void ) { _isPause = true; }
    virtual void play( void ) { _isPause = false; }

private:
    ImageSource( const ImageSource& ) { }
    virtual ImageSource& operator=( const ImageSource& ) { return *this; }

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
