//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "mainwindow.h"
#include "imageprocess.h"
#include "imagesource.h"
#include "qimageview.h"
#include "inihelper.h"
#include <QtGui>
#include <QFile>
#include <QHostAddress>
#include <darwin/linux/CvCamera.h>
#include <darwin/framework/minIni.h>
#include <iostream>

using namespace std;


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define STR_DIALOG_CONFIG_TITLE_O       "Select ini file to load configuration"
#define STR_DIALOG_CONFIG_TITLE_S       "Select ini file to save configuration"
#define STR_DIALOG_CONFIG_DIRECTORY
#define STR_DIALOG_CONFIG_FILTER        "INI file (*.INI);;All files (*)"
#define STR_DIALOG_ROBOT_TITLE          "Enter IP address"
#define STR_DIALOG_ROBOT_LABEL          "Please enter robot's IP address"
#define STR_DIALOG_ROBOT_IP_ADDRESS     "192.168.123.1"
#define STR_DIALOG_VIDEO_TITLE          "Select video file"
#define STR_DIALOG_VIDEO_DIRECTORY
#define STR_DIALOG_VIDEO_FILTER         "AVI format (*.AVI);;MP4 format (*.MP4);;All files (*.*)"
#define STR_INI_SECTION_VIEWER          "DarwinViewer"
#define STR_INI_SHOWRGB                 "SrcImgShowRGB"
#define STR_INI_SHOWMARKONLY_ALL        "AllMarkOnly"
#define STR_INI_SHOWMARKONLY_SINGLE     "SingleMarkOnly"
#define STR_INI_FLIP                    "Flip"
#define STR_INI_FLIP_MODE               "FlipMode"
#define STR_INI_ERODE                   "Erode"
#define STR_INI_DILATE                  "Dilate"
#define STR_INI_MORPH                   "Morphology"
#define STR_INI_MORPH_OP                "MorphologyOperation"
#define STR_INI_SMOOTH                  "Smooth"
#define STR_INI_SMOOTH_MODE             "SmoothMode"
#define STR_INI_MAX_Y                   "MaxY"
#define STR_INI_MIN_Y                   "MinY"
#define STR_INI_MAX_U                   "MaxU"
#define STR_INI_MIN_U                   "MinU"
#define STR_INI_MAX_V                   "MaxV"
#define STR_INI_MIN_V                   "MinV"
#define STR_MSG_CAMERA_OPEN_SUCCESS     "Camera device has been opened"
#define STR_MSG_CAMERA_OPEN_FAIL        "Failed to open camera device"
#define STR_MSG_LOAD_CONFIG_SUCCESS     "Configuration has been loaded from %1"
#define STR_MSG_LOAD_CONFIG_FAIL        "Failed to load configuration from %1"
#define STR_MSG_SAVE_CONFIG_SUCCESS     "Configuration has been saved into %1"
#define STR_MSG_SAVE_CONFIG_FAIL        "Failed to save configuration into %1"
#define STR_MSG_ROBOT_CONNECT_SUCCESS   "Robot has been connected (%1)"
#define STR_MSG_ROBOT_CONNECT_FAIL      "Failed to connect to robot (%1)"
#define STR_MSG_VIDEO_OPEN_SUCCESS      "Video file has been opened (%1)"
#define STR_MSG_VIDEO_OPEN_FAIL         "Failed to open video file (%1)"
#define STR_MSG_SOURCE_CLOSED           "Image source has been closed"

#define DEFAULT_CONFIG_EXTENSION        ".ini"
#define DEFAULT_CONFIG_FILE_PATH        "config.ini"
#define STATUS_MESSAGE_TIMEOUT          5000
#define UPDATE_INTERVAL                 25


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
MainWindow::MainWindow( QWidget* parent )
    : QMainWindow( parent )
{
    _isDialogOpened = false;
    _lastImgSrcIsValid = false;
    _timer = new QTimer( this );
    _timer->setObjectName( QString( "_timer" ) );
    setupUi( );
    QMetaObject::connectSlotsByName( this );
    _timer->start( UPDATE_INTERVAL );
    configLoad( QString( DEFAULT_CONFIG_FILE_PATH ) );
}


MainWindow::~MainWindow( void )
{
    _timer->stop( );
}


void MainWindow::colourChanged( void )
{
    unsigned char colour_yuv[YUVI__LENGTH];

    colour_yuv[YUVI_Y_MAX] = _sbObjYMax->value( );
    colour_yuv[YUVI_Y_MIN] = _sbObjYMin->value( );
    colour_yuv[YUVI_U_MAX] = _sbObjUMax->value( );
    colour_yuv[YUVI_U_MIN] = _sbObjUMin->value( );
    colour_yuv[YUVI_V_MAX] = _sbObjVMax->value( );
    colour_yuv[YUVI_V_MIN] = _sbObjVMin->value( );
    _imgproc.setTargetYUV( _cbObjSelect->currentIndex( ), colour_yuv );
    setScanlineColours();
}

void MainWindow::setScanlineColours()
{
    _scanline.setTargetY(_cbObjSelect->currentIndex(), _sbObjYMin->value(), _sbObjYMax->value());
    _scanline.setTargetU(_cbObjSelect->currentIndex(), _sbObjUMin->value(), _sbObjUMax->value());
    _scanline.setTargetV(_cbObjSelect->currentIndex(), _sbObjVMin->value(), _sbObjVMax->value());
}

void MainWindow::scanlineChanged()
{
    int index = _cbObjSelect->currentIndex();

    _scanline.setSubsample(_sbSubsample->value());
    _scanline.setFillThreshold(index, _sbFillThreshold->value());
    _scanline.setScanThreshold(index, _sbScanThreshold->value());
    _scanline.setMinScanLength(index, _sbScanLength->value());

    _scanline.setSizeDisabled(index, _cbSizeDisabled->isChecked());
    _scanline.setProportionsDisabled(index, _cbProportionsDisabled->isChecked());
    _scanline.setFillDisabled(index, _cbFillDisabled->isChecked());
    _scanline.setYRangeDisabled(index, _cbYRangeDisabled->isChecked());
    _scanline.setURangeDisabled(index, _cbURangeDisabled->isChecked());
    _scanline.setVRangeDisabled(index, _cbVRangeDisabled->isChecked());
    _scanline.setKeyRangeDisabled(index, _cbKeyRangeDisabled->isChecked());

    _scanline.setKeyChannel(index, _cbKeyChannel->currentIndex());

    _scanline.setDimensions(index, _sbRealWidth->value(), _sbRealHeight->value());
    _scanline.setMinPixelHeight(index, _sbMinHeight->value());
    _scanline.setMinPixelWidth(index, _sbMinWidth->value());

    _scanline.setMarkColour(index, _sbMarkR->value(), _sbMarkG->value(), _sbMarkB->value());
}

void MainWindow::cameraSettingsChanged()
{
    Robot::CameraSettings settings;

    settings.brightness = _sbBrightness->value();
    settings.contrast = _sbContrast->value();
    settings.exposure = _sbExposure->value();
    settings.gain = _sbGain->value();
    settings.saturation = _sbSaturation->value();
    settings.hue = _sbHue->value();

    settings.autoExposure = _cbAEx->isChecked();
    settings.autoGain = _cbAGain->isChecked();
    settings.autoHue = _cbAHue->isChecked();
    settings.autoWhiteBalance = _cbAWB->isChecked();

    Robot::CvCamera::GetInstance()->SetCameraSettings(settings);
}


void MainWindow::preprocessChanged( void )
{
    PREPROCESSING_OPTION option;

    option.flip = _cbProcFlip->checkState( ) == Qt::Checked;
    option.erode = _cbProcErode->checkState( ) == Qt::Checked;
    option.dilate = _cbProcDilate->checkState( ) == Qt::Checked;
    option.morph = _cbProcMorph->checkState( ) == Qt::Checked;
    option.smooth = _cbProcSmooth->checkState( ) == Qt::Checked;
    option.filp_mode = _cbProcFilpMode->currentIndex( ) - 1; // -1 both, 0 vertical, 1 horizontal
    option.morph_operation = _cbProcMorphOp->currentIndex( ) + 1; // invalid = open-close
    option.smooth_mode = _cbProcSmoothType->currentIndex( );
    _imgproc.setPreprocessing( option );
    _scanline.setPreprocessing( option );
}


void MainWindow::imageMouseMoved( Qt::MouseButtons buttons, int x, int y )
{
    if( (buttons & Qt::LeftButton) == Qt::LeftButton )
    {
        _imgproc.addPixelToTarget( _cbObjSelect->currentIndex( ), x, y );
        on__cbObjSelect_currentIndexChanged( _cbObjSelect->currentIndex( ) );
    }
}


void MainWindow::imageMousePressed( Qt::MouseButtons buttons, int x, int y )
{
    if( (buttons & Qt::LeftButton) == Qt::LeftButton )
    {
        _imgproc.addPixelToTarget( _cbObjSelect->currentIndex( ), x, y );
        on__cbObjSelect_currentIndexChanged( _cbObjSelect->currentIndex( ) );
    }
}


void MainWindow::on__cbMarkAllOnly_clicked( bool checked )
{
    _imgproc.setMarkOnlyAll( checked );
}


void MainWindow::on__cbMarkSingleOnly_clicked( bool checked )
{
    _imgproc.setMarkOnlyTarget( checked );
}


void MainWindow::on__pbSrcOpen_clicked( void )
{
    QString filename;
    QString ipaddress;
    bool okay = false;

    if( _cbSrcSelect->currentText( ) == QString( "Camera" ) )
    {
        _hsSrcSlider->setEnabled( false );
        if( _imgsrc.openCamera(_spCamDevNum->value()) )
        {
            showStatusMessage( QString( STR_MSG_CAMERA_OPEN_SUCCESS ) );
        }
        else
        {
            showStatusMessage( QString( STR_MSG_CAMERA_OPEN_FAIL ) );
        }
    }
    else if( _cbSrcSelect->currentText( ) == QString( "Robot" ) )
    {
        ipaddress = QInputDialog::getText( this,
            QString( STR_DIALOG_ROBOT_TITLE ),
            QString( STR_DIALOG_ROBOT_LABEL ),
            QLineEdit::Normal,
            QString( STR_DIALOG_ROBOT_IP_ADDRESS ),
            &okay );
        if( okay && !ipaddress.isNull( ) && !ipaddress.isEmpty( ) )
        {
            _hsSrcSlider->setEnabled( false );
            if( _imgsrc.connectRobot( QHostAddress( ipaddress ) ) )
            {
                showStatusMessage( QString( STR_MSG_ROBOT_CONNECT_SUCCESS ).arg( ipaddress ) );
            }
            else
            {
                showStatusMessage( QString( STR_MSG_ROBOT_CONNECT_FAIL ).arg( ipaddress ) );
            }
        }
    }
    else if( _cbSrcSelect->currentText( ) == QString( "Video" ) )
    {
        _isDialogOpened = true;
        filename = QFileDialog::getOpenFileName( this,
            QString( STR_DIALOG_VIDEO_TITLE ),
            QString( STR_DIALOG_VIDEO_DIRECTORY ),
            QString( STR_DIALOG_VIDEO_FILTER ) );
        _isDialogOpened = false;
        if( !filename.isNull( ) && !filename.isEmpty( ) )
        {
            if( _imgsrc.openVideo( filename.toLatin1( ).data( ) ) )
            {
                _hsSrcSlider->setEnabled( true );
                _hsSrcSlider->setMaximum( (int)_imgsrc.frameCount( ) );
                showStatusMessage( QString( STR_MSG_VIDEO_OPEN_SUCCESS ).arg( filename ) );
            }
            else
            {
                showStatusMessage( QString( STR_MSG_VIDEO_OPEN_FAIL ).arg( filename ) );
            }
        }
    }
}


void MainWindow::on__pbSrcPlay_clicked( void )
{
    _imgsrc.play( );
}


void MainWindow::on__pbSrcStop_clicked( void )
{
    _imgsrc.pause( );
}


void MainWindow::on__hsSrcSlider_sliderMoved( int position )
{
    _imgsrc.frameSetPosition( position );
}


void MainWindow::on__pbLoadConfig_clicked( void )
{
    QString filename;

    _isDialogOpened = true;
    filename = QFileDialog::getOpenFileName( this,
        QString( STR_DIALOG_CONFIG_TITLE_O ),
        QString( STR_DIALOG_CONFIG_DIRECTORY ),
        QString( STR_DIALOG_CONFIG_FILTER ) );
    _isDialogOpened = false;
    if( !filename.isNull( ) && !filename.isEmpty( ) )
    {
        if( configLoad( filename ) )
        {
            showStatusMessage( QString( STR_MSG_LOAD_CONFIG_SUCCESS ).arg( filename ) );
        }
        else
        {
            showStatusMessage( QString( STR_MSG_LOAD_CONFIG_FAIL ).arg( filename ) );
        }
    }
}


void MainWindow::on__pbSaveConfig_clicked( void )
{
    QString filename;

    _isDialogOpened = true;
    filename = QFileDialog::getSaveFileName( this,
        QString( STR_DIALOG_CONFIG_TITLE_S ),
        QString( STR_DIALOG_CONFIG_DIRECTORY ),
        QString( STR_DIALOG_CONFIG_FILTER ) );
    _isDialogOpened = false;
    if( !filename.isNull( ) && !filename.isEmpty( ) )
    {
        if( filename.indexOf( '.' ) == -1 )
        {   // no extension, add default extension
            filename.append( DEFAULT_CONFIG_EXTENSION );
        }
        if( configSave( filename ) )
        {
            showStatusMessage( QString( STR_MSG_SAVE_CONFIG_SUCCESS ).arg( filename ) );
        }
        else
        {
            showStatusMessage( QString( STR_MSG_SAVE_CONFIG_FAIL ).arg( filename ) );
        }
    }
}


void MainWindow::on__cbObjSelect_currentIndexChanged( int index )
{
    unsigned char colour_yuv[YUVI__LENGTH];

    _imgproc.targetYUV( index, colour_yuv );
    _sbObjYMax->setValue( colour_yuv[YUVI_Y_MAX] );
    _sbObjYMin->setValue( colour_yuv[YUVI_Y_MIN] );
    _sbObjUMax->setValue( colour_yuv[YUVI_U_MAX] );
    _sbObjUMin->setValue( colour_yuv[YUVI_U_MIN] );
    _sbObjVMax->setValue( colour_yuv[YUVI_V_MAX] );
    _sbObjVMin->setValue( colour_yuv[YUVI_V_MIN] );
    switch( index )
    {
    case CI_YELLOW:
    case CI_RED:
    case CI_GREEN:
    case CI_BLUE:
    case CI_PINK:
    case CI_AQUA:
        _lMarkSingle->setText( QString::fromUtf8( "%1 Object" ).arg( _imgproc.targetName( index ) ) );
        break;
    case CI__LENGTH:
    default:
        _lMarkSingle->setText( QString::fromUtf8( "Single Object" ) );
        break;
    }

    _cbSizeDisabled->setChecked(_scanline.getSizeDisabled(index));
    _cbProportionsDisabled->setChecked(_scanline.getProportionsDisabled(index));
    _cbFillDisabled->setChecked(_scanline.getFillDisabled(index));
    _cbYRangeDisabled->setChecked(_scanline.getYRangeDisabled(index));
    _cbURangeDisabled->setChecked(_scanline.getURangeDisabled(index));
    _cbVRangeDisabled->setChecked(_scanline.getVRangeDisabled(index));
    _cbKeyRangeDisabled->setChecked(_scanline.getKeyRangeDisabled(index));
    _cbKeyChannel->setCurrentIndex(_scanline.getKeyChannel(index));

    _sbSubsample->setValue(_scanline.getSubsample());
    _sbScanThreshold->setValue(_scanline.getScanThreshold(index));
    _sbFillThreshold->setValue(_scanline.getFillThreshold(index));
    _sbScanLength->setValue(_scanline.getMinScanLength(index));

    _sbMinHeight->setValue(_scanline.getMinPixelHeight(index));
    _sbMinWidth->setValue(_scanline.getMinPixelWidth(index));

    _sbRealHeight->setValue(_scanline.getHeight(index));
    _sbRealWidth->setValue(_scanline.getWidth(index));

    _sbMarkR->setValue((*_scanline.getMarkColour(index))[2]);
    _sbMarkG->setValue((*_scanline.getMarkColour(index))[1]);
    _sbMarkB->setValue((*_scanline.getMarkColour(index))[0]);
}


void MainWindow::on__pbObjReset_clicked( void )
{
    _imgproc.resetTarget( _cbObjSelect->currentIndex( ) );
    on__cbObjSelect_currentIndexChanged( _cbObjSelect->currentIndex( ) );
}


void MainWindow::on__timer_timeout( void )
{
    IplImage* pImg;
    QImage* qImage;

    if( _isDialogOpened )
    {   // disable image update while a dialog is opened
        return ;
    }
    pImg = _imgsrc.next( );
    if( pImg == 0 )
    {
        if( _lastImgSrcIsValid && !_imgsrc.isValid( ) )
        {
            showStatusMessage( QString( STR_MSG_SOURCE_CLOSED ) );
        }
    }
    else
    {
        _scanline.process( pImg );
        qImage = ImageProcess::cvtIplImage2QImage(_scanline.scanlineResult);
        _ivScanline->setImage(*qImage);
        delete qImage;

        _imgproc.process( pImg );

        switch( _cbSrcShowRGB->checkState( ) )
        {
        case Qt::Checked:
            qImage = ImageProcess::cvtIplImage2QImage( _imgproc.imagePreprocessedRGB( ) );
            break;
        case Qt::Unchecked:
        default:
            qImage = ImageProcess::cvtIplImage2QImage( _imgproc.imagePreprocessedYUV( ) );
            break;
        }
        _ivSrcImage->setImage( *qImage );
        delete( qImage );

        qImage = ImageProcess::cvtIplImage2QImage( _imgproc.imageAllMarked( ) );
        _ivMarkAll->setImage( *qImage );
        delete( qImage );

        qImage = ImageProcess::cvtIplImage2QImage( _imgproc.imageTargetMarked( _cbObjSelect->currentIndex( ) ) );
        _ivMarkSingle->setImage( *qImage );
        delete( qImage );
    }
    _hsSrcSlider->setSliderPosition( (int)_imgsrc.framePosition( ) );
    _lastImgSrcIsValid = _imgsrc.isValid( );
}


bool MainWindow::configLoad( QString filename )
{
    QFile file( filename );
    QString section;
    bool success = false;
    unsigned char colour_yuv[YUVI__LENGTH];

    cout << "Loading settings from " << filename.toStdString() << endl;

    if( file.open( QIODevice::ReadOnly ) )
    {
        section = QString( STR_INI_SECTION_VIEWER );
        _cbSrcShowRGB->setChecked( ini_getd( file, QString( STR_INI_SHOWRGB ), section, 1 ) != 0 );
        _cbMarkAllOnly->setChecked( ini_geti( file, QString( STR_INI_SHOWMARKONLY_ALL ), section, 0 ) != 0 );
        _cbMarkSingleOnly->setChecked( ini_geti( file, QString( STR_INI_SHOWMARKONLY_SINGLE ), section, 0 ) != 0 );
        _cbProcFlip->setChecked( ini_geti( file, QString( STR_INI_FLIP ), section, 0 ) != 0 );
        _cbProcFilpMode->setCurrentIndex( ini_geti( file, QString( STR_INI_FLIP_MODE ), section, 0 ) );
        _cbProcErode->setChecked( ini_geti( file, QString( STR_INI_ERODE ), section, 0 ) != 0 );
        _cbProcDilate->setChecked( ini_geti( file, QString( STR_INI_DILATE ), section, 0 ) != 0 );
        _cbProcMorph->setChecked( ini_geti( file, QString( STR_INI_MORPH ), section, 1 ) != 0 );
        _cbProcMorphOp->setCurrentIndex( ini_geti( file, QString( STR_INI_MORPH_OP ), section, 0 ) );
        _cbProcSmooth->setChecked( ini_geti( file, QString( STR_INI_SMOOTH ), section, 1 ) != 0 );
        _cbProcSmoothType->setCurrentIndex( ini_geti( file, QString( STR_INI_SMOOTH_MODE ), section, 3 ) );
        file.close( );

        // load the INI settings for the YuvCamera if possible
        minIni *ini = new minIni(filename.toStdString());
        Robot::CvCamera::GetInstance()->LoadINISettings(ini);

        // load the scanline properties from config
        for(int i=0; i<CI__LENGTH; i++)
        {
            _imgproc.resetTarget( i );
            colour_yuv[YUVI_Y_MAX] = (unsigned char)ini->geti(_imgproc.targetName(i), "MaxY", 0);
            colour_yuv[YUVI_Y_MIN] = (unsigned char)ini->geti(_imgproc.targetName(i), "MinY", 255);
            colour_yuv[YUVI_U_MAX] = (unsigned char)ini->geti(_imgproc.targetName(i), "MaxU", 0);
            colour_yuv[YUVI_U_MIN] = (unsigned char)ini->geti(_imgproc.targetName(i), "MinU", 255);
            colour_yuv[YUVI_V_MAX] = (unsigned char)ini->geti(_imgproc.targetName(i), "MaxV", 0);
            colour_yuv[YUVI_V_MIN] = (unsigned char)ini->geti(_imgproc.targetName(i), "MinV", 255);
            _imgproc.setTargetYUV( i, colour_yuv );

            _scanline.configLoad(ini, i, _imgproc.targetName(i));
        }

        _sbBrightness->setValue(ini->geti("Camera","Brightness",-1));
        _sbContrast->setValue(ini->geti("Camera","Contrast",-1));
        _sbExposure->setValue(ini->geti("Camera","Exposure",1000));
        _sbGain->setValue(ini->geti("Camera","Gain",-1));
        _sbSaturation->setValue(ini->geti("Camera","Saturation",-1));
        _cbAWB->setChecked(ini->geti("Camera","AutoWhiteBalance",0));
        cameraSettingsChanged();
        delete ini;

        success = true;
    }
    on__cbObjSelect_currentIndexChanged( _cbObjSelect->currentIndex( ) );
    preprocessChanged( );
    return success;
}


bool MainWindow::configSave( QString filename )
{
    QByteArray data;
    QFile file( filename );
    bool success = false;
    unsigned char colour_yuv[YUVI__LENGTH];

    if( file.open( QIODevice::ReadOnly ) )
    {
        data = file.readAll( );
        file.close( );
    }
    data = ini_seti( _cbProcSmoothType->currentIndex( ), data, STR_INI_SMOOTH_MODE, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbProcSmooth->checkState( ), data, STR_INI_SMOOTH, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbProcMorphOp->currentIndex( ), data, STR_INI_MORPH_OP, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbProcMorph->checkState( ), data, STR_INI_MORPH, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbProcDilate->checkState( ), data, STR_INI_DILATE, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbProcErode->checkState( ), data, STR_INI_ERODE, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbProcFilpMode->currentIndex( ), data, STR_INI_FLIP_MODE, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbProcFlip->checkState( ), data, STR_INI_FLIP, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbMarkSingleOnly->checkState( ), data, STR_INI_SHOWMARKONLY_SINGLE, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbMarkAllOnly->checkState( ), data, STR_INI_SHOWMARKONLY_ALL, STR_INI_SECTION_VIEWER );
    data = ini_seti( _cbSrcShowRGB->checkState( ), data, STR_INI_SHOWRGB, STR_INI_SECTION_VIEWER );
    for( int i = 0; i < CI__LENGTH; ++i )
    {
        _imgproc.targetYUV( i, colour_yuv );
        data = ini_seti( colour_yuv[YUVI_V_MIN], data, STR_INI_MIN_V, _imgproc.targetName( i ) );
        data = ini_seti( colour_yuv[YUVI_V_MAX], data, STR_INI_MAX_V, _imgproc.targetName( i ) );
        data = ini_seti( colour_yuv[YUVI_U_MIN], data, STR_INI_MIN_U, _imgproc.targetName( i ) );
        data = ini_seti( colour_yuv[YUVI_U_MAX], data, STR_INI_MAX_U, _imgproc.targetName( i ) );
        data = ini_seti( colour_yuv[YUVI_Y_MIN], data, STR_INI_MIN_Y, _imgproc.targetName( i ) );
        data = ini_seti( colour_yuv[YUVI_Y_MAX], data, STR_INI_MAX_Y, _imgproc.targetName( i ) );

        data = ini_seti( _scanline.getSubsample(), data, "Subsample", _imgproc.targetName(i));
        data = ini_seti( _scanline.getMinScanLength(i), data, "MinScanLength", _imgproc.targetName(i));
        data = ini_seti( _scanline.getScanThreshold(i), data, "ScanThreshold", _imgproc.targetName(i));
        data = ini_seti( _scanline.getFillThreshold(i), data, "FillThreshold", _imgproc.targetName(i));
        data = ini_seti( _scanline.getMinPixelHeight(i), data, "MinPixelHeight", _imgproc.targetName(i));
        data = ini_seti( _scanline.getMinPixelWidth(i), data, "MinPixelWidth", _imgproc.targetName(i));
        data = ini_seti( _scanline.getKeyChannel(i), data, "KeyChannel", _imgproc.targetName(i));
        data = ini_seti( _scanline.getHeight(i), data, "RealHeight", _imgproc.targetName(i));
        data = ini_seti( _scanline.getWidth(i), data, "RealWidth", _imgproc.targetName(i));
        data = ini_seti( (*_scanline.getMarkColour(i))[2], data, "MarkR", _imgproc.targetName(i));
        data = ini_seti( (*_scanline.getMarkColour(i))[1], data, "MarkG", _imgproc.targetName(i));
        data = ini_seti( (*_scanline.getMarkColour(i))[0], data, "MarkB", _imgproc.targetName(i));
        data = ini_seti( _scanline.getSizeDisabled(i), data, "SizeDisabled", _imgproc.targetName(i));
        data = ini_seti( _scanline.getProportionsDisabled(i), data, "ProportionsDisabled", _imgproc.targetName(i));
        data = ini_seti( _scanline.getFillDisabled(i), data, "FillDisabled", _imgproc.targetName(i));
        data = ini_seti( _scanline.getYRangeDisabled(i), data, "YRangeDisabled", _imgproc.targetName(i));
        data = ini_seti( _scanline.getURangeDisabled(i), data, "URangeDisabled", _imgproc.targetName(i));
        data = ini_seti( _scanline.getVRangeDisabled(i), data, "VRangeDisabled", _imgproc.targetName(i));
        data = ini_seti( _scanline.getKeyRangeDisabled(i), data, "KeyRangeDisabled", _imgproc.targetName(i));
    }

    data = ini_seti( _cbAEx->isChecked(), data, "AutoExposure", "Camera");
    data = ini_seti( _cbAGain->isChecked(), data, "AutoGain", "Camera");
    data = ini_seti( _cbAHue->isChecked(), data, "AutoHue", "Camera");
    data = ini_seti( _cbAWB->isChecked(), data, "AutoWhiteBalance", "Camera");
    data = ini_seti( _sbBrightness->value(), data, "Brightness", "Camera");
    data = ini_seti( _sbContrast->value(), data, "Contrast", "Camera");
    data = ini_seti( _sbExposure->value(), data, "Exposure", "Camera");
    data = ini_seti( _sbGain->value(), data, "Gain", "Camera");
    data = ini_seti( _sbHue->value(), data, "Hue", "Camera");
    data = ini_seti( _sbSaturation->value(), data, "Saturation", "Camera");

    if( file.open( QIODevice::WriteOnly ) )
    {
        file.write( data );
        success = true;
    }
    return success;
}


void MainWindow::showStatusMessage( const QString& msg )
{
    _sbStatus->showMessage( msg, STATUS_MESSAGE_TIMEOUT );
}

