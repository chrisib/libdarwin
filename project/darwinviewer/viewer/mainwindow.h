//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef MAINWINDOW_H
#define MAINWINDOW_H


//------------------------------------------------------------------------------
// INCLUDE HEADERS AND FORWARD DECLARATION
//------------------------------------------------------------------------------
#include "imageprocess.h"
#include "imagesource.h"
#include "scanline.h"
#include <QtGui>
#include <QMainWindow>
#include <QtWidgets>
class QImageView;


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QWidget*        cwMain;
    QStatusBar*     _sbStatus;
    QFrame*         _fMain;
    QGridLayout*    _glMain;
    QVBoxLayout*        _vlSrcImage;
    QLabel*                 _lSrcImage;
    QImageView*             _ivSrcImage;
    QCheckBox*              _cbSrcShowRGB;
    QVBoxLayout*        _vlMarkAll;
    QLabel*                 _lMarkAll;
    QImageView*             _ivMarkAll;
    QCheckBox*              _cbMarkAllOnly;
    QVBoxLayout*        _vlMarkSingle;
    QLabel*                 _lMarkSingle;
    QImageView*             _ivMarkSingle;
    QCheckBox*              _cbMarkSingleOnly;
    QVBoxLayout*        _vlSource;
    QHBoxLayout*            _hlSrcSelect;
    QSpinBox*           _spCamDevNum;
    QComboBox*                  _cbSrcSelect;
    QPushButton*                _pbSrcOpen;
    QHBoxLayout*            _hlSrcCntl;
    QPushButton*                _pbSrcPlay;
    QPushButton*                _pbSrcStop;
    QSlider*                _hsSrcSlider;
    QPushButton*            _pbLoadConfig;
    QPushButton*            _pbSaveConfig;
    QVBoxLayout*        _vlObject;
    QComboBox*              _cbObjSelect;
    QPushButton*            _pbObjReset;
    QGridLayout*            _glObjYUV;
    QLabel*                     _lObjMinValue;
    QLabel*                     _lObjMaxValue;
    QLabel*                     _lObjYValue;
    QSpinBox*                   _sbObjYMin;
    QSpinBox*                   _sbObjYMax;
    QLabel*                     _lObjUValue;
    QSpinBox*                   _sbObjUMin;
    QSpinBox*                   _sbObjUMax;
    QLabel*                     _lObjVValue;
    QSpinBox*                   _sbObjVMin;
    QSpinBox*                   _sbObjVMax;
    QGroupBox*          _gbProcess;
    QGridLayout*        _gbProcessLayout;
    QCheckBox*              _cbProcFlip;
    QComboBox*              _cbProcFilpMode;
    QCheckBox*              _cbProcErode;
    QCheckBox*              _cbProcDilate;
    QCheckBox*              _cbProcMorph;
    QComboBox*              _cbProcMorphOp;
    QCheckBox*              _cbProcSmooth;
    QComboBox*              _cbProcSmoothType;

    QVBoxLayout*    _vlScanline;
    QVBoxLayout*    _vlScanlineControls;
    QImageView*     _ivScanline;
    QLabel*         _lScanline;

    QSpinBox*       _sbSubsample;               QLabel*         _lSubsample;
    QSpinBox*       _sbMarkR;                   QLabel*         _lMarkR;
    QSpinBox*       _sbMarkG;                   QLabel*         _lMarkG;
    QSpinBox*       _sbMarkB;                   QLabel*         _lMarkB;

    QSpinBox*       _sbScanThreshold;           QLabel*         _lScanThreshold;
    QSpinBox*       _sbFillThreshold;           QLabel*         _lFillThreshold;
    QSpinBox*       _sbScanLength;              QLabel*         _lScanLength;
    QSpinBox*       _sbMinHeight;               QLabel*         _lMinHeight;
    QSpinBox*       _sbMinWidth;                QLabel*         _lMinWidth;

    QLabel*         _lScanlineDimensions;
    QSpinBox*       _sbRealHeight;              QLabel*         _lRealHeight;
    QSpinBox*       _sbRealWidth;               QLabel*         _lRealWidth;

    QComboBox*      _cbKeyChannel;              QLabel*         _lKeyChannel;

    QLabel*         _lScanlineFilters;
    QCheckBox*      _cbProportionsDisabled;     QLabel*         _lProportionsDisabled;
    QCheckBox*      _cbFillDisabled;            QLabel*         _lFillDisabled;
    QCheckBox*      _cbSizeDisabled;            QLabel*         _lSizeDisabled;
    QCheckBox*      _cbYRangeDisabled;          QLabel*         _lYRangeDisabled;
    QCheckBox*      _cbURangeDisabled;          QLabel*         _lURangeDisabled;
    QCheckBox*      _cbVRangeDisabled;          QLabel*         _lVRangeDisabled;
    QCheckBox*      _cbKeyRangeDisabled;        QLabel*         _lKeyRangeDisabled;

    QLabel*         _lExposure;
    QLabel*         _lBrightness;
    QLabel*         _lGain;
    QLabel*         _lSaturation;
    QLabel*         _lContrast;
    QLabel*         _lHue;
    QSpinBox*       _sbExposure;
    QSpinBox*       _sbBrightness;
    QSpinBox*       _sbGain;
    QSpinBox*       _sbSaturation;
    QSpinBox*       _sbContrast;
    QDoubleSpinBox*       _sbHue;
    QCheckBox*      _cbAWB;
    QCheckBox*      _cbAEx;
    QCheckBox*      _cbAHue;
    QCheckBox*      _cbAGain;


    QTimer*         _timer;
    ImageProcess    _imgproc;
    ImageSource     _imgsrc;
    bool            _isDialogOpened;
    bool            _lastImgSrcIsValid;

    Scanline        _scanline;

public:
    explicit MainWindow( QWidget* parent = 0 );
    virtual ~MainWindow( void );

private slots:
    virtual void colourChanged( void );
    virtual void preprocessChanged( void );
    virtual void imageMouseMoved( Qt::MouseButtons buttons, int x, int y );
    virtual void imageMousePressed( Qt::MouseButtons buttons, int x, int y );
    virtual void on__cbMarkAllOnly_clicked( bool checked );
    virtual void on__cbMarkSingleOnly_clicked( bool checked );
    virtual void on__pbSrcOpen_clicked( void );
    virtual void on__pbSrcPlay_clicked( void );
    virtual void on__pbSrcStop_clicked( void );
    virtual void on__hsSrcSlider_sliderMoved( int position );
    virtual void on__pbLoadConfig_clicked( void );
    virtual void on__pbSaveConfig_clicked( void );
    virtual void on__cbObjSelect_currentIndexChanged( int index );
    virtual void on__pbObjReset_clicked( void );
    virtual void on__timer_timeout( void );
    virtual void scanlineChanged( void );
    virtual void setScanlineColours(void);
    virtual void cameraSettingsChanged();

private:
    virtual bool configLoad( QString filename );
    virtual bool configSave( QString filename );
    virtual void showStatusMessage( const QString& msg );
    virtual void resizeEvent( QResizeEvent* event );
    virtual void retranslateUi( void );
    virtual void setupUi( void );
    virtual void setupUi_SourceImage( void );
    virtual void setupUi_MarkAllImage( void );
    virtual void setupUi_MarkSingleImage( void );
    virtual void setupUi_SourceControls( void );
    virtual void setupUi_ObjectControls( void );
    virtual void setupUi_ProcessControls( void );
    virtual void setupUi_ScanlineImage( void );
    virtual void setupUi_ScanlineControls( void );

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
