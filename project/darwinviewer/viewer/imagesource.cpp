//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

#ifndef DARWIN
#define DARWIN
#endif

#include <iostream>
using namespace std;

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "imagesource.h"
#include "netpacket.h"
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <QHostAddress>
#include <QTcpSocket>
#ifdef DARWIN
#include <minIni.h>
#include <CvCamera.h>
#endif


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define INI_FILE_PATH "config.ini"


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
ImageSource::ImageSource( void )
{
    _source = IS_CAMERA;
    _isValid = false;
    _isPause = false;
    _imgNetSrc = 0;
}


ImageSource::~ImageSource( void )
{
    close( );
}


void ImageSource::close( void )
{
    _isValid = false;
    _isPause = false;
    _capture.release( );
    _socket.close( );
    if( _imgNetSrc != 0 )
    {
        cvReleaseImage( &_imgNetSrc );
        _imgNetSrc = 0;
    }
    _matCam.release( );
}


bool ImageSource::connectRobot( const QHostAddress& address )
{
    int width, height, nChannels;

    close( );
    _source = IS_ROBOT;
    _socket.connectToHost( address, NETWORK_PORT );
    if( _socket.isValid( ) )
    {
        _sReadSize = 12 + NETPACKET_EXTRA_SIZE;
        if( _socket.waitForReadyRead( 1000 ) && _socket.bytesAvailable( ) >= _sReadSize )
        {
            _netpacket.buffer_length = _socket.read( (char*)_netpacket.buffer, _sReadSize );
            if( _netpacket.buffer_length == _sReadSize && npIsValid( &_netpacket ) != 0 )
            {
                width = toInt( _netpacket.data );
                height = toInt( _netpacket.data + 4 );
                nChannels = toInt( _netpacket.data + 8 );
                _sReadSize = width * height * nChannels + NETPACKET_EXTRA_SIZE;
                _imgNetSrc = cvCreateImage( cvSize( width, height ), IPL_DEPTH_8U, nChannels );
                _isValid = true;
            }
        }
        else
        {
            _socket.close( );
        }
    }
    return _isValid;
}


bool ImageSource::openCamera( int deviceNum )
{
    close( );
    _source = IS_CAMERA;
#ifdef DARWIN
    minIni ini( INI_FILE_PATH );
    Robot::CvCamera::GetInstance( )->Initialize( deviceNum );
    Robot::CvCamera::GetInstance( )->LoadINISettings( &ini );
    //Robot::YuvCamera::GetInstance()->SetAutoWhiteBalance( 0 );
    //cout << "AUTO_WHITE_BALANCE: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, 0 ) << endl;
//    cout << "WHITE_BALANCE_TEMPERATURE: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_WHITE_BALANCE_TEMPERATURE, 6000 ) << endl;

/*

//    cout << "setting white balance off" << endl;
//    cout << "EXPOSURE_AUTO_PRIORITY: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_EXPOSURE_AUTO_PRIORITY, 0 ) << endl;
//    cout << "EXPOSURE_AUTO: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL ) << endl;
//    cout << "EXPOSURE_ABSOLUTE: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_EXPOSURE_ABSOLUTE, ini.geti("Camera", "Exposure")) << endl;
    cout << "AUTO_WHITE_BALANCE: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, 0 ) << endl;
//    cout << "AUTOGAIN: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_AUTOGAIN, 0 ) << endl;
//    cout << "HUE_AUTO: " << Robot::YuvCamera::GetInstance()->v4l2SetControl(V4L2_CID_HUE_AUTO, 0     ) << endl;

    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_BRIGHTNESS ) << endl;
    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_CONTRAST ) << endl;
    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_SATURATION ) << endl;
    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_GAIN ) << endl;
    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_EXPOSURE_AUTO ) << endl;
    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_AUTO_WHITE_BALANCE ) << endl;
    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_AUTOGAIN ) << endl;
    cout << Robot::YuvCamera::GetInstance()->v4l2GetControl(V4L2_CID_HUE_AUTO ) << endl;
//*/

    _isValid = true;
#else
    if( _capture.open( 0 ) )
    {
        _matCam.create( 960, 1280, CV_8UC3 );
        _isValid = true;
    }
#endif
    return _isValid;
}


bool ImageSource::openVideo( const char* filename )
{
    close( );
    _source = IS_VIDEO;
    if( _capture.open( filename ) )
    {
        _matCam.create( 960, 1280, CV_8UC3 );
        _isValid = true;
    }
    return _isValid;
}


double ImageSource::frameCount( void )
{
    if( _isValid && _source == IS_VIDEO )
    {
        return _capture.get( CV_CAP_PROP_FRAME_COUNT );
    }
    return 0.0;
}


double ImageSource::framePosition( void )
{
    if( _isValid && _source == IS_VIDEO )
    {
        return _capture.get( CV_CAP_PROP_POS_FRAMES );
    }
    return 0.0;
}


void ImageSource::frameSetPosition( double frame )
{
    if( _isValid && _source == IS_VIDEO )
    {
        _capture.set( CV_CAP_PROP_POS_FRAMES, frame );
        if( _capture.grab( ) && _capture.retrieve( _matCam ) )
        {
        }
    }
}


IplImage* ImageSource::next( void )
{
    IplImage* pImg = 0;

    if( _isValid )
    {
        switch( _source )
        {
        case IS_CAMERA:
#ifdef DARWIN
            if( !_isPause )
            {
                Robot::CvCamera::GetInstance( )->CaptureFrame( );
            }
            _imgCamSrc = Robot::CvCamera::GetInstance( )->yuvFrame;
            pImg = &_imgCamSrc;
            break;
#endif
        case IS_VIDEO:
            if( _isPause || (_capture.grab( ) && _capture.retrieve( _matCam )) )
            {
                _imgCamSrc = _matCam;
                pImg = &_imgCamSrc;
            }
            break;
        case IS_ROBOT:
            if( _isPause )
            {
                pImg = _imgNetSrc;
            }
            else if( !_socket.isValid( ) && _socket.peerAddress( ).isNull( ) )
            {   // disconnected
                close( );
            }
            else if( _socket.bytesAvailable( ) >= _sReadSize )
            {
                _netpacket.buffer_length = _socket.read( (char*)_netpacket.buffer, _sReadSize );
                if( _netpacket.buffer_length == _sReadSize && npIsValid( &_netpacket ) != 0 )
                {
                    for( int i = 0; i < _imgNetSrc->height; ++i )
                    {
                        memcpy( ((unsigned char*)_imgNetSrc->imageData) + (_imgNetSrc->widthStep * i),
                                _netpacket.data + (_imgNetSrc->widthStep * i),
                               _imgNetSrc->widthStep );
                    }
                    pImg = _imgNetSrc;
                }
            }
            break;
        default:
            break;
        }
    }
    return pImg;
}

