#-------------------------------------------------
#
# Project created by QtCreator 2014-02-25T19:09:55
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ../darwinviewer
TEMPLATE = app

INCLUDEPATH+= ../share/ /usr/local/include/darwin/framework /usr/local/include/darwin/linux #../../../darwin/include/framework ../../../darwin/include/linux
LIBS       += -ldl -lopencv_core -lopencv_highgui -lopencv_imgproc -ldarwin -lespeak

##################################################
# Output Directories                             #
##################################################
OBJECTS_DIR = .objects
MOC_DIR     = .moc
UI_DIR      = .ui

##################################################
# Installation Directories                       #
##################################################
INSTALLBASE = /usr/local
bin.path    = $$INSTALLBASE/bin/
bin.files   = $$TARGET
INSTALLS   += bin

##################################################
# Compiling Flags                                #
##################################################
QMAKE_CFLAGS_DEBUG      += -O0 -g #-DDEBUG
QMAKE_CXXFLAGS_DEBUG    += -O0 -g #-DDEBUG
QMAKE_CFLAGS_RELEASE    += -O3 -g #-DNDEBUG
QMAKE_CXXFLAGS_RELEASE  += -O3 -g #-DNDEBUG

SOURCES += main.cpp \
    scanline.cpp \
    qimageview.cpp \
    mainwindow_ui.cpp \
    mainwindow.cpp \
    imagesource.cpp \
    imageprocess.cpp \
    ../share/netpacket.c \
    ../share/inihelper.cpp

HEADERS  += MainWindow.h \
    scanline.h \
    qimageview.h \
    mainwindow.h \
    imagesource.h \
    imageprocess.h \
    ../share/netpacket.h \
    ../share/inihelper.h

FORMS    +=
