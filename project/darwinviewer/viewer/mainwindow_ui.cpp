//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "mainwindow.h"
#include "qimageview.h"
#include <QtGui>


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define INITIAL_WIDTH   1280
#define INITIAL_HEIGHT  600
#define MINIMUM_WIDTH   720
#define MINIMUM_HEIGHT  700


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
void MainWindow::resizeEvent( QResizeEvent* event )
{
    _fMain->setGeometry( 0, 0, event->size( ).width( ), event->size( ).height( ) - _sbStatus->height( ) );
    event->accept( );
}


void MainWindow::retranslateUi( void )
{
    setWindowTitle( QApplication::translate( "MainWindow", "DARWIN Viewer", 0 ) );
    _lSrcImage->setText( QApplication::translate( "MainWindow", "Source Image", 0 ) );
    _cbSrcShowRGB->setText( QApplication::translate( "MainWindow", "Show as RGB Image", 0 ) );
    _lMarkAll->setText( QApplication::translate( "MainWindow", "All Objects", 0 ) );
    _cbMarkAllOnly->setText( QApplication::translate( "MainWindow", "Show marks only", 0 ) );
    _lMarkSingle->setText( QApplication::translate( "MainWindow", "Single Object", 0 ) );
    _cbMarkSingleOnly->setText( QApplication::translate( "MainWindow", "Show marks only", 0 ) );
    _cbSrcSelect->clear( );
    _cbSrcSelect->insertItems( 0, QStringList( )
         << QApplication::translate( "MainWindow", "Camera", 0 )
         << QApplication::translate( "MainWindow", "Robot", 0 )
         << QApplication::translate( "MainWindow", "Video", 0 )
         );
    _cbSrcSelect->setCurrentIndex( 0 );
    _pbSrcOpen->setText( QApplication::translate( "MainWindow", "Open", 0 ) );
    _pbSrcPlay->setText( QApplication::translate( "MainWindow", "Play", 0 ) );
    _pbSrcStop->setText( QApplication::translate( "MainWindow", "Stop", 0 ) );
    _pbLoadConfig->setText( QApplication::translate( "MainWindow", "Load Configuration", 0 ) );
    _pbSaveConfig->setText( QApplication::translate( "MainWindow", "Save Configuration", 0 ) );
    _cbObjSelect->clear( );
    _cbObjSelect->insertItems( 0, QStringList( )
         << QApplication::translate( "MainWindow", "Yellow", 0 )
         << QApplication::translate( "MainWindow", "Red", 0 )
         << QApplication::translate( "MainWindow", "Green", 0 )
         << QApplication::translate( "MainWindow", "Blue", 0 )
         << QApplication::translate( "MainWindow", "Pink", 0 )
         << QApplication::translate( "MainWindow", "Aqua", 0 )
         );
    _pbObjReset->setText( QApplication::translate( "MainWindow", "Reset", 0 ) );
    _lObjMinValue->setText( QApplication::translate( "MainWindow", "Min", 0 ) );
    _lObjMaxValue->setText( QApplication::translate( "MainWindow", "Max", 0 ) );
    _lObjYValue->setText( QApplication::translate( "MainWindow", "Y", 0 ) );
    _lObjUValue->setText( QApplication::translate( "MainWindow", "U", 0 ) );
    _lObjVValue->setText( QApplication::translate( "MainWindow", "V", 0 ) );
    _gbProcess->setTitle( QApplication::translate( "MainWindow", "Image Preprocessing", 0 ) );
    _cbProcFlip->setText( QApplication::translate( "MainWindow", "Flip", 0 ) );
    _cbProcFilpMode->clear( );
    _cbProcFilpMode->insertItems( 0, QStringList( )
        << QApplication::translate( "MainWindow", "Both", 0 )
        << QApplication::translate( "MainWindow", "Vertical", 0 )
        << QApplication::translate( "MainWindow", "Horizontal", 0 )
        );
    _cbProcErode->setText( QApplication::translate( "MainWindow", "Erode", 0 ) );
    _cbProcDilate->setText( QApplication::translate( "MainWindow", "Dilate", 0 ) );
    _cbProcMorph->setText( QApplication::translate( "MainWindow", "Morphology", 0 ) );
    _cbProcMorphOp->clear( );
    _cbProcMorphOp->insertItems( 0, QStringList( )
        << QApplication::translate( "MainWindow", "OpenClose", 0 )
        << QApplication::translate( "MainWindow", "Opening", 0 )
        << QApplication::translate( "MainWindow", "Closing", 0 )
        );
    _cbProcSmooth->setText( QApplication::translate( "MainWindow", "Smooth", 0 ) );
    _cbProcSmoothType->clear( );
    _cbProcSmoothType->insertItems( 0, QStringList( )
        << QApplication::translate( "MainWindow", "Blur (No Scale)", 0 )
        << QApplication::translate( "MainWindow", "Blur", 0 )
        << QApplication::translate( "MainWindow", "Gaussian", 0 )
        << QApplication::translate( "MainWindow", "Median", 0 )
        << QApplication::translate( "MainWindow", "Bilateral", 0 )
        );
    _cbProcSmoothType->setCurrentIndex( 3 );

    _lScanline->setText(QApplication::translate("MainWindow", "Scanline", 0 ) );
    _lSubsample->setText(QApplication::translate("MainWindow","Subsample",0 ));
    _lScanLength->setText(QApplication::translate("MainWindow","Scan Length",0 ));
    _lFillThreshold->setText(QApplication::translate("MainWindow", "Fill Threshold", 0 ) );
    _lScanThreshold->setText(QApplication::translate("MainWindow", "Scan Threshold", 0 ) );
    _lMarkR->setText(QApplication::translate("MainWindow","Mark R",0 ));
    _lMarkG->setText(QApplication::translate("MainWindow","Mark G",0 ));
    _lMarkB->setText(QApplication::translate("MainWindow","Mark B",0 ));
    _lScanlineDimensions->setText(QApplication::translate("MainWindow","Object Dimensions",0 ));
    _lMinHeight->setText(QApplication::translate("MainWindow","Min Height (px)",0 ));
    _lMinWidth->setText(QApplication::translate("MainWindow","Min Width (px)",0 ));
    _lRealHeight->setText(QApplication::translate("MainWindow","Height (mm)",0 ));
    _lRealWidth->setText(QApplication::translate("MainWindow","Width (mm)",0 ));
    _lKeyChannel->setText(QApplication::translate("MainWindow","Key Channel",0 ));
    _lScanlineFilters->setText(QApplication::translate("MainWindow","Disable Filters...",0 ));
    _lSizeDisabled->setText(QApplication::translate("MainWindow","Size",0 ));
    _lProportionsDisabled->setText(QApplication::translate("MainWindow","Aspect",0 ));
    _lFillDisabled->setText(QApplication::translate("MainWindow","Compactness",0 ));
    _lYRangeDisabled->setText(QApplication::translate("MainWindow","Y Channel",0 ));
    _lURangeDisabled->setText(QApplication::translate("MainWindow","Y Channel",0 ));
    _lVRangeDisabled->setText(QApplication::translate("MainWindow","V Channel",0 ));
    _lKeyRangeDisabled->setText(QApplication::translate("MainWindow","Key Channel",0 ));

    _lExposure->setText(QApplication::translate("MainWindow","Exposure",0 ));
    _lSaturation->setText(QApplication::translate("MainWindow","Saturation",0 ));
    _lGain->setText(QApplication::translate("MainWindow","Gain",0 ));
    _lContrast->setText(QApplication::translate("MainWindow","Constrast",0 ));
    _lBrightness->setText(QApplication::translate("MainWindow","Brightness",0 ));
    _lHue->setText(QApplication::translate("MainWindow","Hue",0 ));
    _cbAWB->setText(QApplication::translate("MainWindow","Auto White Balance",0 ));
    _cbAEx->setText(QApplication::translate("MainWindow","Auto Exposure",0 ));
    _cbAHue->setText(QApplication::translate("MainWindow","Auto Hue",0 ));
    _cbAGain->setText(QApplication::translate("MainWindow","Auto Gain",0 ));
}


void MainWindow::setupUi( void )
{
    setObjectName( QString::fromUtf8( "MainWindow" ) );
    resize( INITIAL_WIDTH, INITIAL_HEIGHT );
    setMinimumSize( QSize( MINIMUM_WIDTH, MINIMUM_HEIGHT ) );
    cwMain = new QWidget( this );
    cwMain->setObjectName( QString::fromUtf8( "cwMain" ) );
    _sbStatus = new QStatusBar( this );
    _sbStatus->setObjectName( QString( "_sbStatus" ) );
    _fMain = new QFrame( cwMain );
    _fMain->setObjectName( QString::fromUtf8( "_fMain" ) );
    _fMain->setGeometry( QRect( 0, 0, MINIMUM_WIDTH, MINIMUM_HEIGHT ) );
    _glMain = new QGridLayout( _fMain );
    _glMain->setObjectName( QString::fromUtf8( "_glMain" ) );
    _glMain->setContentsMargins( 0, 0, 0, 0 );
    _glMain->setSpacing( 6 );
    {
        setupUi_SourceImage( );
        _glMain->addLayout( _vlSrcImage, 0, 0 );
        setupUi_MarkAllImage( );
        _glMain->addLayout( _vlMarkAll, 0, 1 );
        setupUi_MarkSingleImage( );
        _glMain->addLayout( _vlMarkSingle, 0, 2 );
        setupUi_ProcessControls( );
        _glMain->addWidget( _gbProcess, 1, 0 );
        setupUi_SourceControls( );
        _glMain->addLayout( _vlSource, 1, 1 );
        setupUi_ObjectControls( );
        _glMain->addLayout( _vlObject, 1, 2 );
        setupUi_ScanlineImage( );
        _glMain->addLayout( _vlScanline, 0, 3 );
        setupUi_ScanlineControls();
        _glMain->addLayout( _vlScanlineControls, 1, 3 );
    }
    setCentralWidget( cwMain );
    setStatusBar( _sbStatus );
    retranslateUi( );
}


void MainWindow::setupUi_SourceImage( void )
{
    QBrush brush( QColor( 0, 0, 0, 255 ) );
    brush.setStyle( Qt::SolidPattern );

    _vlSrcImage = new QVBoxLayout( );
    _vlSrcImage->setObjectName( QString::fromUtf8( "_vlSrcImage" ) );
    _vlSrcImage->setContentsMargins( 9, 9, 9, 9 );
    _vlSrcImage->setSpacing( 6 );
    {
        _lSrcImage = new QLabel( _fMain );
        _lSrcImage->setObjectName( QString::fromUtf8( "_lSrcImage" ) );
        _vlSrcImage->addWidget( _lSrcImage );

        _ivSrcImage = new QImageView( _fMain );
        _ivSrcImage->setObjectName( QString::fromUtf8( "_ivSrcImage" ) );
        _ivSrcImage->setAcceptDrops( false );
        _ivSrcImage->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivSrcImage->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivSrcImage->setBackgroundBrush( brush );
        _vlSrcImage->addWidget( _ivSrcImage );
        connect( _ivSrcImage, SIGNAL( mouseMoved( Qt::MouseButtons, int, int ) ), SLOT( imageMouseMoved( Qt::MouseButtons, int, int ) ) );
        connect( _ivSrcImage, SIGNAL( mousePressed( Qt::MouseButtons, int, int ) ), SLOT( imageMousePressed( Qt::MouseButtons, int, int ) ) );

        _cbSrcShowRGB = new QCheckBox( _fMain );
        _cbSrcShowRGB->setObjectName( QString::fromUtf8( "_cbSrcShowRGB" ) );
        _cbSrcShowRGB->setChecked( true );
        _vlSrcImage->addWidget( _cbSrcShowRGB );
    }
}


void MainWindow::setupUi_MarkAllImage( void )
{
    QBrush brush( QColor( 0, 0, 0, 255 ) );
    brush.setStyle( Qt::SolidPattern );

    _vlMarkAll = new QVBoxLayout( );
    _vlMarkAll->setObjectName( QString::fromUtf8( "_vlMarkAll" ) );
    _vlMarkAll->setContentsMargins( 9, 9, 9, 9 );
    _vlMarkAll->setSpacing( 6 );
    {
        _lMarkAll = new QLabel( _fMain );
        _lMarkAll->setObjectName( QString::fromUtf8( "_lMarkAll" ) );
        _vlMarkAll->addWidget( _lMarkAll );

        _ivMarkAll = new QImageView( _fMain );
        _ivMarkAll->setObjectName( QString::fromUtf8( "_ivMarkAll" ) );
        _ivMarkAll->setAcceptDrops( false );
        _ivMarkAll->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivMarkAll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivMarkAll->setBackgroundBrush( brush );
        _vlMarkAll->addWidget( _ivMarkAll );
        connect( _ivMarkAll, SIGNAL( mouseMoved( Qt::MouseButtons, int, int ) ), SLOT( imageMouseMoved( Qt::MouseButtons, int, int ) ) );
        connect( _ivMarkAll, SIGNAL( mousePressed( Qt::MouseButtons, int, int ) ), SLOT( imageMousePressed( Qt::MouseButtons, int, int ) ) );

        _cbMarkAllOnly = new QCheckBox( _fMain );
        _cbMarkAllOnly->setObjectName( QString::fromUtf8( "_cbMarkAllOnly" ) );
        _vlMarkAll->addWidget( _cbMarkAllOnly );
    }
}


void MainWindow::setupUi_MarkSingleImage( void )
{
    QBrush brush( QColor( 0, 0, 0, 255 ) );
    brush.setStyle( Qt::SolidPattern );

    _vlMarkSingle = new QVBoxLayout( );
    _vlMarkSingle->setObjectName( QString::fromUtf8( "_vlMarkSingle" ) );
    _vlMarkSingle->setContentsMargins( 9, 9, 9, 9 );
    _vlMarkSingle->setSpacing( 6 );
    {
        _lMarkSingle = new QLabel( _fMain );
        _lMarkSingle->setObjectName( QString::fromUtf8( "_lMarkSingle" ) );
        _vlMarkSingle->addWidget( _lMarkSingle );

        _ivMarkSingle = new QImageView( _fMain );
        _ivMarkSingle->setObjectName( QString::fromUtf8( "_ivMarkSingle" ) );
        _ivMarkSingle->setAcceptDrops( false );
        _ivMarkSingle->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivMarkSingle->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivMarkSingle->setBackgroundBrush( brush );
        _vlMarkSingle->addWidget( _ivMarkSingle );
        connect( _ivMarkSingle, SIGNAL( mouseMoved( Qt::MouseButtons, int, int ) ), SLOT( imageMouseMoved( Qt::MouseButtons, int, int ) ) );
        connect( _ivMarkSingle, SIGNAL( mousePressed( Qt::MouseButtons, int, int ) ), SLOT( imageMousePressed( Qt::MouseButtons, int, int ) ) );

        _cbMarkSingleOnly = new QCheckBox( _fMain );
        _cbMarkSingleOnly->setObjectName( QString::fromUtf8( "_cbMarkSingleOnly" ) );
        _vlMarkSingle->addWidget( _cbMarkSingleOnly );
    }
}


void MainWindow::setupUi_SourceControls( void )
{
    _vlSource = new QVBoxLayout( );
    _vlSource->setObjectName( QString::fromUtf8( "_vlSource" ) );
    _vlSource->setContentsMargins( 9, 9, 9, 9 );
    _vlSource->setSpacing( 0 );
    {
        _hlSrcSelect = new QHBoxLayout( );
        _hlSrcSelect->setObjectName( QString::fromUtf8( "_hlSrcSelect" ) );
        _hlSrcSelect->setContentsMargins( 0, 0, 0, 0 );
        _hlSrcSelect->setSpacing( 6 );
        {
            _cbSrcSelect = new QComboBox( _fMain );
            _cbSrcSelect->setObjectName( QString::fromUtf8( "_cbSrcSelect" ) );
            _hlSrcSelect->addWidget( _cbSrcSelect );

            _spCamDevNum = new QSpinBox(_fMain);
            _spCamDevNum->setMinimum(0);
            _spCamDevNum->setValue(0);
            _spCamDevNum->setMaximum(255);
            _spCamDevNum->setObjectName(QString::fromUtf8("_spCamDevNum"));
            _hlSrcSelect->addWidget(_spCamDevNum);

            _pbSrcOpen = new QPushButton( _fMain );
            _pbSrcOpen->setObjectName( QString::fromUtf8( "_pbSrcOpen" ) );
            _hlSrcSelect->addWidget( _pbSrcOpen );
        }
        _hlSrcSelect->setStretch( 0, 5 );
        _hlSrcSelect->setStretch( 1, 1 );
        _vlSource->addLayout( _hlSrcSelect );

        _hlSrcCntl = new QHBoxLayout( );
        _hlSrcCntl->setObjectName( QString::fromUtf8( "_hlSrcCntl" ) );
        _hlSrcCntl->setContentsMargins( 0, 0, 0, 0 );
        _hlSrcCntl->setSpacing( 6 );
        {
            _pbSrcPlay = new QPushButton( _fMain );
            _pbSrcPlay->setObjectName( QString::fromUtf8( "_pbSrcPlay" ) );
            _hlSrcCntl->addWidget( _pbSrcPlay );

            _pbSrcStop = new QPushButton( _fMain );
            _pbSrcStop->setObjectName( QString::fromUtf8( "_pbSrcStop" ) );
            _hlSrcCntl->addWidget( _pbSrcStop );
        }
        _hlSrcCntl->setStretch( 0, 1 );
        _hlSrcCntl->setStretch( 1, 1 );
        _vlSource->addLayout( _hlSrcCntl );

        _hsSrcSlider = new QSlider( _fMain );
        _hsSrcSlider->setObjectName( QString::fromUtf8( "_hsSrcSlider" ) );
        _hsSrcSlider->setEnabled( false );
        _hsSrcSlider->setOrientation( Qt::Horizontal );
        _vlSource->addWidget( _hsSrcSlider );

        _pbLoadConfig = new QPushButton( _fMain );
        _pbLoadConfig->setObjectName( QString::fromUtf8( "_pbLoadConfig" ) );
        _vlSource->addWidget( _pbLoadConfig );

        _pbSaveConfig = new QPushButton( _fMain );
        _pbSaveConfig->setObjectName( QString::fromUtf8( "_pbSaveConfig" ) );
        _vlSource->addWidget( _pbSaveConfig );

        QLabel *l = new QLabel(_fMain);
        l->setText("Camera Settings");
        _vlSource->addWidget(l);

        QHBoxLayout *hl = new QHBoxLayout(_fMain);
        hl->setContentsMargins( 0, 0, 0, 0 );
        hl->setSpacing( 6 );
        {
            _lExposure = new QLabel(_fMain);
            hl->addWidget(_lExposure);

            _sbExposure = new QSpinBox(_fMain);
            _sbExposure->setMinimum(-1);
            _sbExposure->setMaximum(10000);
            _sbExposure->setValue(1000);
            connect(_sbExposure, SIGNAL(editingFinished()), this, SLOT(cameraSettingsChanged()));
            hl->addWidget(_sbExposure);
        }
        _vlSource->addLayout(hl);

        hl = new QHBoxLayout(_fMain);
        hl->setContentsMargins( 0, 0, 0, 0 );
        hl->setSpacing( 6 );
        {
            _lBrightness = new QLabel(_fMain);
            hl->addWidget(_lBrightness);

            _sbBrightness = new QSpinBox(_fMain);
            _sbBrightness->setMinimum(-1);
            _sbBrightness->setMaximum(255);
            _sbBrightness->setValue(-1);
            connect(_sbBrightness, SIGNAL(editingFinished()), this, SLOT(cameraSettingsChanged()));
            hl->addWidget(_sbBrightness);
        }
        _vlSource->addLayout(hl);

        hl = new QHBoxLayout(_fMain);
        hl->setContentsMargins( 0, 0, 0, 0 );
        hl->setSpacing( 6 );
        {
            _lContrast = new QLabel(_fMain);
            hl->addWidget(_lContrast);

            _sbContrast = new QSpinBox(_fMain);
            _sbContrast->setMinimum(-1);
            _sbContrast->setMaximum(255);
            _sbContrast->setValue(-1);
            connect(_sbContrast, SIGNAL(editingFinished()), this, SLOT(cameraSettingsChanged()));
            hl->addWidget(_sbContrast);
        }
        _vlSource->addLayout(hl);

        hl = new QHBoxLayout(_fMain);
        hl->setContentsMargins( 0, 0, 0, 0 );
        hl->setSpacing( 6 );
        {
            _lSaturation = new QLabel(_fMain);
            hl->addWidget(_lSaturation);

            _sbSaturation = new QSpinBox(_fMain);
            _sbSaturation->setMinimum(-1);
            _sbSaturation->setMaximum(255);
            _sbSaturation->setValue(-1);
            connect(_sbSaturation, SIGNAL(editingFinished()), this, SLOT(cameraSettingsChanged()));
            hl->addWidget(_sbSaturation);
        }
        _vlSource->addLayout(hl);

        hl = new QHBoxLayout(_fMain);
        hl->setContentsMargins( 0, 0, 0, 0 );
        hl->setSpacing( 6 );
        {
            _lGain = new QLabel(_fMain);
            hl->addWidget(_lGain);

            _sbGain = new QSpinBox(_fMain);
            _sbGain->setMinimum(-1);
            _sbGain->setMaximum(255);
            _sbGain->setValue(255);
            connect(_sbGain, SIGNAL(editingFinished()), this, SLOT(cameraSettingsChanged()));
            hl->addWidget(_sbGain);
        }
        _vlSource->addLayout(hl);

        hl = new QHBoxLayout(_fMain);
        hl->setContentsMargins( 0, 0, 0, 0 );
        hl->setSpacing( 6 );
        {
            _lHue = new QLabel(_fMain);
            hl->addWidget(_lHue);

            _sbHue = new QDoubleSpinBox(_fMain);
            _sbHue->setMinimum(0.0);
            _sbHue->setMaximum(1.0);
            _sbHue->setValue(0.5);
            connect(_sbHue, SIGNAL(editingFinished()), this, SLOT(cameraSettingsChanged()));
            hl->addWidget(_sbHue);
        }
        _vlSource->addLayout(hl);

        _cbAWB = new QCheckBox(_fMain);
        connect(_cbAWB, SIGNAL(toggled(bool)), this, SLOT(cameraSettingsChanged()));
        _vlSource->addWidget(_cbAWB);

        _cbAEx = new QCheckBox(_fMain);
        connect(_cbAEx, SIGNAL(toggled(bool)), this, SLOT(cameraSettingsChanged()));
        _vlSource->addWidget(_cbAEx);

        _cbAGain = new QCheckBox(_fMain);
        connect(_cbAGain, SIGNAL(toggled(bool)), this, SLOT(cameraSettingsChanged()));
        _vlSource->addWidget(_cbAGain);

        _cbAHue = new QCheckBox(_fMain);
        connect(_cbAHue, SIGNAL(toggled(bool)), this, SLOT(cameraSettingsChanged()));
        _vlSource->addWidget(_cbAHue);
    }
}


void MainWindow::setupUi_ObjectControls( void )
{
    _vlObject = new QVBoxLayout( );
    _vlObject->setObjectName( QString::fromUtf8( "_vlObject" ) );
    _vlObject->setContentsMargins( 9, 9, 9, 9 );
    _vlObject->setSpacing( 6 );
    {
        _cbObjSelect = new QComboBox( _fMain );
        _cbObjSelect->setObjectName( QString::fromUtf8( "_cbObjSelect" ) );
        _vlObject->addWidget( _cbObjSelect );

        _pbObjReset = new QPushButton( _fMain );
        _pbObjReset->setObjectName( QString::fromUtf8( "_pbObjReset" ) );
        _vlObject->addWidget( _pbObjReset );

        _glObjYUV = new QGridLayout( );
        _glObjYUV->setObjectName( QString::fromUtf8( "_glObjYUV" ) );
        _glObjYUV->setContentsMargins( 0, 0, 0, 0 );
        _glObjYUV->setSpacing( 6 );
        {
            _lObjMinValue = new QLabel( _fMain );
            _lObjMinValue->setObjectName( QString::fromUtf8( "_lObjMinValue" ) );
            _lObjMinValue->setAlignment( Qt::AlignCenter );
            _glObjYUV->addWidget( _lObjMinValue, 0, 1, 1, 1 );

            _lObjMaxValue = new QLabel( _fMain );
            _lObjMaxValue->setObjectName( QString::fromUtf8( "_lObjMaxValue" ) );
            _lObjMaxValue->setAlignment( Qt::AlignCenter );
            _glObjYUV->addWidget( _lObjMaxValue, 0, 2, 1, 1 );

            _lObjYValue = new QLabel( _fMain );
            _lObjYValue->setObjectName( QString::fromUtf8( "_lObjYValue" ) );
            _lObjYValue->setAlignment( Qt::AlignCenter );
            _glObjYUV->addWidget( _lObjYValue, 1, 0, 1, 1 );

            _lObjUValue = new QLabel( _fMain );
            _lObjUValue->setObjectName( QString::fromUtf8( "_lObjUValue" ) );
            _lObjUValue->setAlignment( Qt::AlignCenter );
            _glObjYUV->addWidget( _lObjUValue, 2, 0, 1, 1 );

            _lObjVValue = new QLabel( _fMain );
            _lObjVValue->setObjectName( QString::fromUtf8( "_lObjVValue" ) );
            _lObjVValue->setAlignment( Qt::AlignCenter );
            _glObjYUV->addWidget( _lObjVValue, 3, 0, 1, 1 );

            _sbObjYMin = new QSpinBox( _fMain );
            _sbObjYMin->setObjectName( QString::fromUtf8( "_sbObjYMin" ) );
            _sbObjYMin->setMaximum( 255 );
            _sbObjYMin->setValue( 255 );
            _glObjYUV->addWidget( _sbObjYMin, 1, 1, 1, 1 );
            connect( _sbObjYMin, SIGNAL( editingFinished( void ) ), SLOT( colourChanged( void ) ) );
            connect( _sbObjYMin, SIGNAL(valueChanged(int)), SLOT(setScanlineColours(void)));

            _sbObjYMax = new QSpinBox( _fMain );
            _sbObjYMax->setObjectName( QString::fromUtf8( "_sbObjYMax" ) );
            _sbObjYMax->setMaximum( 255 );
            _glObjYUV->addWidget( _sbObjYMax, 1, 2, 1, 1 );
            connect( _sbObjYMax, SIGNAL( editingFinished( void ) ), SLOT( colourChanged( void ) ) );
            connect( _sbObjYMax, SIGNAL(valueChanged(int)), SLOT(setScanlineColours(void)));

            _sbObjUMin = new QSpinBox( _fMain );
            _sbObjUMin->setObjectName( QString::fromUtf8( "_sbObjUMin" ) );
            _sbObjUMin->setMaximum( 255 );
            _sbObjUMin->setValue( 255 );
            _glObjYUV->addWidget( _sbObjUMin, 2, 1, 1, 1 );
            connect( _sbObjUMin, SIGNAL( editingFinished( void ) ), SLOT( colourChanged( void ) ) );
            connect( _sbObjUMin, SIGNAL(valueChanged(int)), SLOT(setScanlineColours(void)));

            _sbObjUMax = new QSpinBox( _fMain );
            _sbObjUMax->setObjectName( QString::fromUtf8( "_sbObjUMax" ) );
            _sbObjUMax->setMaximum( 255 );
            _glObjYUV->addWidget( _sbObjUMax, 2, 2, 1, 1 );
            connect( _sbObjUMax, SIGNAL( editingFinished( void ) ), SLOT( colourChanged( void ) ) );
            connect( _sbObjUMax, SIGNAL(valueChanged(int)), SLOT(setScanlineColours(void)));

            _sbObjVMin = new QSpinBox( _fMain );
            _sbObjVMin->setObjectName( QString::fromUtf8( "_sbObjVMin" ) );
            _sbObjVMin->setMaximum( 255 );
            _sbObjVMin->setValue( 255 );
            _glObjYUV->addWidget( _sbObjVMin, 3, 1, 1, 1 );
            connect( _sbObjVMin, SIGNAL( editingFinished( void ) ), SLOT( colourChanged( void ) ) );
            connect( _sbObjVMin, SIGNAL(valueChanged(int)), SLOT(setScanlineColours(void)));

            _sbObjVMax = new QSpinBox( _fMain );
            _sbObjVMax->setObjectName( QString::fromUtf8( "_sbObjVMax" ) );
            _sbObjVMax->setMaximum( 255 );
            _glObjYUV->addWidget( _sbObjVMax, 3, 2, 1, 1 );
            connect( _sbObjVMax, SIGNAL( editingFinished( void ) ), SLOT( colourChanged( void ) ) );
            connect( _sbObjVMax, SIGNAL(valueChanged(int)), SLOT(setScanlineColours(void)));
        }
        _vlObject->addLayout( _glObjYUV );
    }
}


void MainWindow::setupUi_ProcessControls( void )
{
    _gbProcess = new QGroupBox( _fMain );
    _gbProcess->setObjectName( QString::fromUtf8( "_gbProcess" ) );
    _gbProcessLayout = new QGridLayout( _gbProcess );
    _gbProcessLayout->setObjectName( QString::fromUtf8( "_gbProcessLayout" ) );
    _gbProcessLayout->setContentsMargins( 9, 9, 9, 9 );
    _gbProcessLayout->setSpacing( 6 );
    {
        _cbProcFlip = new QCheckBox( _gbProcess );
        _cbProcFlip->setObjectName( QString::fromUtf8( "_cbProcFlip" ) );
        _gbProcessLayout->addWidget( _cbProcFlip, 0, 0, 1, 1 );
        connect( _cbProcFlip, SIGNAL( clicked( void ) ), SLOT( preprocessChanged( void ) ) );

        _cbProcFilpMode = new QComboBox( _gbProcess );
        _cbProcFilpMode->setObjectName( QString::fromUtf8( "_cbProcFilpMode" ) );
        _gbProcessLayout->addWidget( _cbProcFilpMode, 0, 1, 1, 1 );
        connect( _cbProcFilpMode, SIGNAL( currentIndexChanged( int ) ), SLOT( preprocessChanged( void ) ) );


        _cbProcErode = new QCheckBox( _gbProcess );
        _cbProcErode->setObjectName( QString::fromUtf8( "_cbProcErode" ) );
        _gbProcessLayout->addWidget( _cbProcErode, 1, 0, 1, 1 );
        connect( _cbProcErode, SIGNAL( clicked( void ) ), SLOT( preprocessChanged( void ) ) );


        _cbProcDilate = new QCheckBox( _gbProcess );
        _cbProcDilate->setObjectName( QString::fromUtf8( "_cbProcDilate" ) );
        _gbProcessLayout->addWidget( _cbProcDilate, 2, 0, 1, 1 );
        connect( _cbProcDilate, SIGNAL( clicked( void ) ), SLOT( preprocessChanged( void ) ) );


        _cbProcMorph = new QCheckBox( _gbProcess );
        _cbProcMorph->setObjectName( QString::fromUtf8( "_cbProcMorph" ) );
        _cbProcMorph->setChecked( true );
        _gbProcessLayout->addWidget( _cbProcMorph, 3, 0, 1, 1 );
        connect( _cbProcMorph, SIGNAL( clicked( void ) ), SLOT( preprocessChanged( void ) ) );

        _cbProcMorphOp = new QComboBox( _gbProcess );
        _cbProcMorphOp->setObjectName( QString::fromUtf8( "_cbProcMorphOp" ) );
        _gbProcessLayout->addWidget( _cbProcMorphOp, 3, 1, 1, 1 );
        connect( _cbProcMorphOp, SIGNAL( currentIndexChanged( int ) ), SLOT( preprocessChanged( void ) ) );


        _cbProcSmooth = new QCheckBox( _gbProcess );
        _cbProcSmooth->setObjectName( QString::fromUtf8( "_cbProcSmooth" ) );
        _cbProcSmooth->setChecked( true );
        _gbProcessLayout->addWidget( _cbProcSmooth, 4, 0, 1, 1 );
        connect( _cbProcSmooth, SIGNAL( clicked( void ) ), SLOT( preprocessChanged( void ) ) );

        _cbProcSmoothType = new QComboBox( _gbProcess );
        _cbProcSmoothType->setObjectName( QString::fromUtf8( "_cbProcSmoothType" ) );
        _gbProcessLayout->addWidget( _cbProcSmoothType, 4, 1, 1, 1 );
        connect( _cbProcSmoothType, SIGNAL( currentIndexChanged( int ) ), SLOT( preprocessChanged( void ) ) );
    }
}

void MainWindow::setupUi_ScanlineImage(void)
{
    QBrush brush( QColor(0,0,0,255));
    brush.setStyle(Qt::SolidPattern);

    _vlScanline = new QVBoxLayout();
    _vlScanline->setObjectName(QString::fromUtf8("_vlScanline"));
    _vlScanline->setContentsMargins( 9, 9, 9, 9 );
    _vlScanline->setSpacing( 6 );


    {
        _lScanline = new QLabel( _fMain );
        _lScanline->setObjectName( QString::fromUtf8( "_lScanline" ) );
        _vlScanline->addWidget( _lScanline );

        _ivScanline = new QImageView( _fMain );
        _ivScanline->setObjectName( QString::fromUtf8( "_ivSrcImage" ) );
        _ivScanline->setAcceptDrops( false );
        _ivScanline->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivScanline->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        _ivScanline->setBackgroundBrush( brush );
        _vlScanline->addWidget( _ivScanline );

        _vlScanline->addWidget(new QCheckBox());
    }
}

void MainWindow::setupUi_ScanlineControls()
{
    QHBoxLayout *hbox;

    _vlScanlineControls = new QVBoxLayout();
    _vlScanlineControls->setObjectName( QString::fromUtf8( "_vlScanlineControls" ) );
    _vlScanlineControls->setContentsMargins( 9, 9, 9, 9 );
    _vlScanlineControls->setSpacing( 0 );
    {
        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lSubsample = new QLabel(_fMain);
            _lSubsample->setObjectName(QString::fromUtf8("_lSubsample"));
            hbox->addWidget(_lSubsample);

            _sbSubsample = new QSpinBox(_fMain);
            _sbSubsample->setMaximum(16);
            _sbSubsample->setMinimum(1);
            _sbSubsample->setValue(2);
            hbox->addWidget(_sbSubsample);
            connect( _sbSubsample, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );

            _lScanLength = new QLabel(_fMain);
            hbox->addWidget(_lScanLength);

            _sbScanLength = new QSpinBox(_fMain);
            _sbScanLength->setMaximum(255);
            _sbScanLength->setMinimum(1);
            _sbScanLength->setValue(8);
            hbox->addWidget(_sbScanLength);
            connect( _sbScanLength, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );
        }
        _vlScanlineControls->addLayout(hbox);

        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lScanThreshold = new QLabel(_fMain);
            _lScanThreshold->setObjectName(QString::fromUtf8("_lScanThreshold"));
            hbox->addWidget(_lScanThreshold);

            _sbScanThreshold = new QSpinBox(_fMain);
            _sbScanThreshold->setObjectName( QString::fromUtf8( "_sbScanThreshold" ) );
            _sbScanThreshold->setMaximum( 255 );
            _sbScanThreshold->setMinimum(1);
            _sbScanThreshold->setValue( 24 );
            hbox->addWidget( _sbScanThreshold );
            connect( _sbScanThreshold, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );

            _lFillThreshold = new QLabel(_fMain);
            _lFillThreshold->setObjectName(QString::fromUtf8("_lFillThreshold"));
            hbox->addWidget(_lFillThreshold);

            _sbFillThreshold = new QSpinBox(_fMain);
            _sbFillThreshold->setObjectName( QString::fromUtf8( "_sbFillThreshold" ) );
            _sbFillThreshold->setMaximum( 255 );
            _sbFillThreshold->setMinimum(1);
            _sbFillThreshold->setValue( 32 );
            hbox->addWidget( _sbFillThreshold );
            connect( _sbFillThreshold, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );
        }
        _vlScanlineControls->addLayout(hbox);

        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lMinHeight = new QLabel(_fMain);
            hbox->addWidget(_lMinHeight);

            _sbMinHeight = new QSpinBox(_fMain);
            _sbMinHeight->setMaximum(255);
            _sbMinHeight->setMinimum(1);
            _sbMinHeight->setValue(5);
            hbox->addWidget(_sbMinHeight);
            connect( _sbMinHeight, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );

            _lMinWidth = new QLabel(_fMain);
            hbox->addWidget(_lMinWidth);

            _sbMinWidth = new QSpinBox(_fMain);
            _sbMinWidth->setMaximum(255);
            _sbMinWidth->setMinimum(1);
            _sbMinWidth->setValue(5);
            hbox->addWidget(_sbMinWidth);
            connect( _sbMinWidth, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );
        }
        _vlScanlineControls->addLayout(hbox);

        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lMarkR = new QLabel(_fMain);
            hbox->addWidget(_lMarkR);

            _sbMarkR = new QSpinBox(_fMain);
            _sbMarkR->setMaximum(255);
            _sbMarkR->setMinimum(0);
            _sbMarkR->setValue(0);
            hbox->addWidget(_sbMarkR);
            connect( _sbMarkR, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );

            _lMarkG = new QLabel(_fMain);
            hbox->addWidget(_lMarkG);

            _sbMarkG = new QSpinBox(_fMain);
            _sbMarkG->setMaximum(255);
            _sbMarkG->setMinimum(0);
            _sbMarkG->setValue(0);
            hbox->addWidget(_sbMarkG);
            connect( _sbMarkG, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );

            _lMarkB = new QLabel(_fMain);
            hbox->addWidget(_lMarkB);

            _sbMarkB = new QSpinBox(_fMain);
            _sbMarkB->setMaximum(255);
            _sbMarkB->setMinimum(0);
            _sbMarkB->setValue(0);
            hbox->addWidget(_sbMarkB);
            connect( _sbMarkB, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );
        }
        _vlScanlineControls->addLayout(hbox);

        _lScanlineDimensions = new QLabel(_fMain);
        _vlScanlineControls->addWidget(_lScanlineDimensions);
        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lRealHeight = new QLabel(_fMain);
            hbox->addWidget(_lRealHeight);

            _sbRealHeight = new QSpinBox(_fMain);
            _sbRealHeight->setMaximum(5000);
            _sbRealHeight->setMinimum(1);
            _sbRealHeight->setValue(1);
            hbox->addWidget(_sbRealHeight);
            connect( _sbRealHeight, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );

            _lRealWidth = new QLabel(_fMain);
            hbox->addWidget(_lRealWidth);

            _sbRealWidth = new QSpinBox(_fMain);
            _sbRealWidth->setMaximum(5000);
            _sbRealWidth->setMinimum(1);
            _sbRealWidth->setValue(1);
            hbox->addWidget(_sbRealWidth);
            connect( _sbRealWidth, SIGNAL( editingFinished( void ) ), SLOT( scanlineChanged( void ) ) );
        }
        _vlScanlineControls->addLayout(hbox);

        _lScanlineFilters = new QLabel(_fMain);
        _vlScanlineControls->addWidget(_lScanlineFilters);
        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lSizeDisabled = new QLabel(_fMain);
            hbox->addWidget(_lSizeDisabled);

            _cbSizeDisabled = new QCheckBox(_fMain);
            hbox->addWidget(_cbSizeDisabled);

            _lProportionsDisabled = new QLabel(_fMain);
            hbox->addWidget(_lProportionsDisabled);

            _cbProportionsDisabled = new QCheckBox(_fMain);
            hbox->addWidget(_cbProportionsDisabled);

            _lFillDisabled = new QLabel(_fMain);
            hbox->addWidget(_lFillDisabled);

            _cbFillDisabled = new QCheckBox(_fMain);
            hbox->addWidget(_cbFillDisabled);
        }
        _vlScanlineControls->addLayout(hbox);
        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lYRangeDisabled = new QLabel(_fMain);
            hbox->addWidget(_lYRangeDisabled);

            _cbYRangeDisabled = new QCheckBox(_fMain);
            hbox->addWidget(_cbYRangeDisabled);

            _lURangeDisabled = new QLabel(_fMain);
            hbox->addWidget(_lURangeDisabled);

            _cbURangeDisabled = new QCheckBox(_fMain);
            hbox->addWidget(_cbURangeDisabled);

            _lVRangeDisabled = new QLabel(_fMain);
            hbox->addWidget(_lVRangeDisabled);

            _cbVRangeDisabled = new QCheckBox(_fMain);
            hbox->addWidget(_cbVRangeDisabled);
        }
        _vlScanlineControls->addLayout(hbox);

        hbox = new QHBoxLayout();
        hbox->setContentsMargins(0,0,0,0);
        hbox->setSpacing(6);
        {
            _lKeyChannel = new QLabel(_fMain);
            hbox->addWidget(_lKeyChannel);

            _cbKeyChannel = new QComboBox(_fMain);
            _cbKeyChannel->addItem("Y");
            _cbKeyChannel->addItem("U");
            _cbKeyChannel->addItem("V");
            hbox->addWidget(_cbKeyChannel);

            _lKeyRangeDisabled = new QLabel(_fMain);
            hbox->addWidget(_lKeyRangeDisabled);

            _cbKeyRangeDisabled = new QCheckBox(_fMain);
            hbox->addWidget(_cbKeyRangeDisabled);
        }
        _vlScanlineControls->addLayout(hbox);
    }
}
