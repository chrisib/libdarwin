//-----------------------------------------
// PROJECT: DARWIN Viewer
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef IMAGEPROCESS_H
#define IMAGEPROCESS_H


//------------------------------------------------------------------------------
// INCLUDE HEADERS AND FORWARD DECLARATION
//------------------------------------------------------------------------------
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <QColor>
class QGraphicsView;
class QImage;

#define SWAP( val1, val2, tmp ) (tmp)=(val1); (val1)=(val2); (val2)=(tmp);

//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
enum _COLOUR_INDEX
{
    CI_YELLOW,
    CI_RED,
    CI_GREEN,
    CI_BLUE,
    CI_PINK,
    CI_AQUA,
    CI__LENGTH
};
typedef enum _COLOUR_INDEX COLOUR_INDEX;

enum _YUV_INDEX
{
    YUVI_Y_MAX,
    YUVI_Y_MIN,
    YUVI_U_MAX,
    YUVI_U_MIN,
    YUVI_V_MAX,
    YUVI_V_MIN,
    YUVI__LENGTH
};

struct _COLOUR_TARGET
{
    char name[16]; // name of target
    IplImage* imgMark; // colour marked image
    CvScalar colour_marker; // marker colour
    unsigned char colour_yuv[YUVI__LENGTH]; // yuv min and max
};
typedef struct _COLOUR_TARGET COLOUR_TARGET;

struct _PREPROCESSING_OPTION
{
    bool flip;
    int filp_mode;
    bool erode;
    bool dilate;
    bool morph;
    int morph_operation;
    bool smooth;
    int smooth_mode;
};
typedef struct _PREPROCESSING_OPTION PREPROCESSING_OPTION;


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class ImageProcess
{
private:
    PREPROCESSING_OPTION _preopt; // image preprocessing options
    COLOUR_TARGET _targets[CI__LENGTH]; // targets
    IplImage* _imgBuffer; // buffer for preprocessing
    IplImage* _imgPreYUV; // preprocessed YCrCb image
    IplImage* _imgPreRGB; // preprocessed RGB image
    IplImage* _imgMarkAll; // all colour marked image
    bool _markOnlyAll; // true, to show marked colour only on all image; otherwise, false
    bool _markOnlyTarget; // true, to show marked colour only on target image; otherwise, false

public:
    ImageProcess( void );
    ImageProcess( const ImageProcess& other );
    virtual ~ImageProcess( void );
    virtual ImageProcess& operator=( const ImageProcess& other );

    virtual void addPixelToTarget( int index, int x, int y );
    virtual void resetTarget( int index );
    virtual void setTargetYUV( int index, const unsigned char colour_yuv[YUVI__LENGTH] );
    virtual const char* targetName( int index ) const;
    virtual void targetYUV( int index, unsigned char colour_yuv[YUVI__LENGTH] ) const;

    virtual void process( const IplImage* src );
    virtual void setPreprocessing( const PREPROCESSING_OPTION& option );

    virtual const PREPROCESSING_OPTION& preprocessing( void ) const { return _preopt; }
    virtual IplImage& imagePreprocessedYUV( void ) { return *_imgPreYUV; }
    virtual IplImage& imagePreprocessedRGB( void ) { return *_imgPreRGB; }
    virtual IplImage& imageAllMarked( void ) { return *_imgMarkAll; }
    virtual IplImage& imageTargetMarked( int index ) { return *_targets[index].imgMark; }
    virtual bool markOnlyAll( void ) const { return _markOnlyAll; }
    virtual bool markOnlyTarget( void ) const { return _markOnlyTarget; }
    virtual void setMarkOnlyAll( bool value ) { _markOnlyAll = value; }
    virtual void setMarkOnlyTarget( bool value ) { _markOnlyTarget = value; }

private:
    virtual IplImage* _procInitImage( IplImage* image, const CvSize& size, int depth, int nChannels );
    virtual void _procPreprocessing( const IplImage* source );

public:
    static QImage* cvtIplImage2QImage( const IplImage& src );
    static QRgb cvtBGRA2QRgba( const unsigned char* ptr, int channel );
    static CvScalar cvtYCrCb2RGB( const CvScalar& color_YCrCb );

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
