#include "scanline.h"
#include <opencv2/highgui/highgui.hpp>
#include <darwin/framework/Target.h>
#include <iostream>
#include <vector>

using namespace std;
using namespace cv;

Scanline::Scanline()
{
    scanlineOutput = cv::Mat::zeros(Robot::Camera::HEIGHT, Robot::Camera::WIDTH, CV_8UC3);

    for(int i=0; i<CI__LENGTH; i++)
        _scanTargets.push_back(&_targets[i]);
}

Scanline::~Scanline()
{

}

void Scanline::configLoad(minIni *ini, int index, const char* section)
{
    cout << "Loading ini settings for scanline target #" << index << " from section " << section << endl;
    _targets[index].LoadIniSettings(*ini, section);
}

void Scanline::setSubsample(int subsample)
{
    for(int i=0; i<CI__LENGTH; i++)
        _targets[i].SetSubsample(subsample);
}

void Scanline::process(const IplImage *src)
{
    cv::Mat mat = cv::Mat(src, true);
    preprocess(mat);
    Robot::BlobTarget::FindTargets(_scanTargets, mat, &scanlineOutput, _targets[0].GetSubsample());

    for(int i=0; i<CI__LENGTH; i++)
    {
        _targets[i].Draw(scanlineOutput);
    }

    //_target.FindInFrame(mat,&scanlineOutput);
    //cv::imshow("src",mat);
    //cvWaitKey(1);

    scanlineResult = IplImage(scanlineOutput);
}

void Scanline::setPreprocessing( const PREPROCESSING_OPTION& option )
{
    memcpy( &_preopt, &option, sizeof( PREPROCESSING_OPTION ) );
}

void Scanline::preprocess(cv::Mat &img)
{
    Mat src = Mat::zeros(img.rows, img.cols, img.type());
    Mat dest = Mat::zeros(img.rows, img.cols, img.type());
    img.copyTo(src);

    // flip if necessary
    if(_preopt.flip)
    {
        cv::flip(src, dest, _preopt.filp_mode);
        dest.copyTo(src);
    }

    if(_preopt.erode)
    {
        cv::erode(src, dest, Mat(),Point(-1,-1),1);
        dest.copyTo(src);
    }
    if(_preopt.dilate)
    {
        cv::erode(src, dest, Mat(),Point(-1,-1),1);
        dest.copyTo(src);
    }
    if(_preopt.morph)
    {
        IplConvKernel* element = 0;
        Mat t = Mat::zeros(img.rows, img.cols, img.type());
        IplImage temp = IplImage(t);
        IplImage srcImg = IplImage(src);
        IplImage destImg = IplImage(dest);

        switch(_preopt.morph_operation)
        {
        case CV_MOP_OPEN:
        case CV_MOP_CLOSE:
        case CV_MOP_GRADIENT:
        case CV_MOP_TOPHAT:
        case CV_MOP_BLACKHAT:
            cvMorphologyEx( &srcImg, &destImg, &temp, element, _preopt.morph_operation );
            break;

        default:
            cvMorphologyEx( &srcImg, &destImg, 0, element, CV_MOP_OPEN );
            cvMorphologyEx( &srcImg, &destImg, 0, element, CV_MOP_CLOSE );
            break;
        }

        dest.copyTo(src);
    }

    if(_preopt.smooth)
    {
        IplImage srcImg = IplImage(src);
        IplImage destImg = IplImage(dest);

        switch(_preopt.smooth_mode)
        {
        case CV_BLUR_NO_SCALE:
        case CV_BLUR:
        case CV_GAUSSIAN:
        case CV_MEDIAN:
        case CV_BILATERAL:
            cvSmooth( &srcImg, &destImg, _preopt.smooth_mode );
            break;
        default:
            cvSmooth( &srcImg, &destImg );
            break;
        }

        dest.copyTo(src);
    }

    // copy back to the original image
    src.copyTo(img);
}
