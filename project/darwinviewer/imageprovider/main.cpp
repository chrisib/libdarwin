//-----------------------------------------
// PROJECT: DARWIN Viewer Image Provider
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "process.h"
#include <QtCore/QCoreApplication>


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
    QCoreApplication a( argc, argv );
    Process proc( argc <= 1 ? 0 : argv[1] );

    proc.start( );
    return a.exec( );
}
