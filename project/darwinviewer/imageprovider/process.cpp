//-----------------------------------------
// PROJECT: DARWIN Viewer Image Provider
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#include "process.h"
#include "netpacket.h"
#include <stdio.h>
#include <YuvCamera.h>
#include <minIni.h>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <QFile>
#include <QList>
#include <QTcpServer>
#include <QTcpSocket>
#include <QtCore/QCoreApplication>


//------------------------------------------------------------------------------
// CONSTANTS AND TYPES
//------------------------------------------------------------------------------
#define INI_FILE_PATH "config.ini"
#define SLEEP_INTERVAL 30


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
Process::Process( const char* avifile )
    : QThread( )
{
    _camera = Robot::YuvCamera::GetInstance( );
    _capture = 0;
    _captureImage = 0;
    _stdout = new QFile( this );
    _stdout->setObjectName( QString( "_stdout" ) );
    _stdout->open( stdout, QIODevice::WriteOnly );
    _server = new QTcpServer( this );
    _server->setObjectName( QString( "_server" ) );
    _server->listen( QHostAddress::Any, NETWORK_PORT );
    if( avifile != 0 )
    {
        _capture = new cv::VideoCapture( avifile );
        if( _capture->isOpened( ) )
        {
            _captureImage = new cv::Mat( 960, 1280, CV_8UC3 ); // big enough matrix
        }
        else
        {
            delete( _capture );
            _capture = 0;
        }
    }
    if( _capture == 0 )
    {
        minIni ini( INI_FILE_PATH );
        _camera->Initialize( 0 );
        _camera->LoadINISettings( &ini );
        //_camera->SetAutoWhiteBalance(0);
        //_camera->v4l2SetControl(V4L2_CID_EXPOSURE_AUTO, 0);
    }
    QMetaObject::connectSlotsByName( this );
}


Process::~Process( void )
{
    _server->close( );
    exit( );
    wait( );
    delete( _server );
    delete( _stdout );
    if( _captureImage != 0 )
    {
        delete( _captureImage );
    }
    if( _capture != 0 )
    {
        delete( _capture );
    }
}


void Process::on__server_newConnection( void )
{
    QTcpSocket* socket;
    NETPACKET packet;

    if( _server->hasPendingConnections( ) )
    {
        npInitialize( &packet, NM_SIZE );
        npAppendInt( &packet, _imgWidth );
        npAppendInt( &packet, _imgHeight );
        npAppendInt( &packet, _imgNumChannels );
        npFinalize( &packet );
        socket = _server->nextPendingConnection( );
        socket->write( (const char*)packet.buffer, packet.buffer_length );
        _mutex.lock( );
        _clients.append( socket );
        _mutex.unlock( );
    }
}


IplImage Process::nextFrame( void )
{
    if( _capture == 0 )
    {
        _camera->CaptureFrame( );
        return _camera->img;
    }
    else if( _capture->grab( ) && _capture->retrieve( *_captureImage ) )
    {
        return *_captureImage;
    }
    else
    {
        IplImage image;
        image.width = 0;
        image.height = 0;
        image.depth = 0;
        image.nChannels = 0;
        return image;
    }
}


void Process::print( const QString& str )
{
    _stdout->write( str.toAscii( ) );
    _stdout->flush( );
}


void Process::run( void )
{
    IplImage image = nextFrame( );
    NETPACKET packet;

    print( QString( "FIRA2011 DARWIN Viewer Image Provider\n" ) );
    print( QString( "Stela H. Seo<shb8775@hotmail.com>\n" ) );
    print( QString( "--------------------------------------------------\n" ) );
    _imgWidth = image.width;
    _imgHeight = image.height;
    _imgNumChannels = image.nChannels;
    while( _server->isListening( ) && image.width > 0 && image.height > 0 )
    {
        npInitialize( &packet, NM_IMAGE );
        for( int row = 0; row < image.height; ++row )
        {
            npAppendData( &packet, ((unsigned char*)image.imageData) + (image.widthStep * row), image.widthStep );
        }
        npFinalize( &packet );

        _mutex.lock( );
        int index = _clients.length( );
        while( --index >= 0 )
        {
            if( _clients[index]->isValid( ) && !_clients[index]->peerAddress( ).isNull( ) )
            {
                //print( QString( "Sending image to client %1\n" ).arg( _clients[index]->peerAddress( ).toString( ) ) );
                _clients[index]->write( (const char*)packet.buffer, packet.buffer_length );
            }
            else
            {
                //print( QString( "Client has been disconnected %1\n" ).arg( _clients[index]->peerAddress( ).toString( ) ) );
                delete( _clients[index] );
                _clients.removeAt( index );
            }
        }
        _mutex.unlock( );

        msleep( SLEEP_INTERVAL );
        image = nextFrame( );
    }
    print( QString( "\n*** END OF PROCESS ***\n" ) );
    QCoreApplication::exit( );
}

