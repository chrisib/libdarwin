##################################################
# DARWIN Viewer Image Provider                   #
# Author: Stela H. Seo                           #
##################################################

##################################################
# Project Definitions                            #
##################################################
QT         += core network
QT         -= gui
CONFIG     += console
CONFIG     -= app_bundle
unix:TARGET = ../darwin-image-provider
win32:TARGET= ../../darwin-image-provider
TEMPLATE    = app
INCLUDEPATH+= ../share /usr/local/include/darwin/framework /usr/local/include/darwin/linux
LIBS       += -ldl -lopencv_core -lopencv_highgui -lopencv_imgproc -ldarwin -lespeak


##################################################
# Output Directories                             #
##################################################
OBJECTS_DIR = .objects
MOC_DIR     = .moc
UI_DIR      = .ui

##################################################
# Installation Directories                       #
##################################################
INSTALLBASE = /usr/local
bin.path    = $$INSTALLBASE/bin/
bin.files   = $$TARGET
INSTALLS   += bin

##################################################
# Compiling Flags                                #
##################################################
QMAKE_CFLAGS_DEBUG      += -O0 -DDEBUG
QMAKE_CXXFLAGS_DEBUG    += -O0 -DDEBUG
QMAKE_CFLAGS_RELEASE    += -O3 -DNDEBUG
QMAKE_CXXFLAGS_RELEASE  += -O3 -DNDEBUG


##################################################
# Source Files                                   #
##################################################
SOURCES += main.cpp \
           process.cpp \
           ../share/netpacket.c

HEADERS += process.h \
           ../share/netpacket.h
