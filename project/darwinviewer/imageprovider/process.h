//-----------------------------------------
// PROJECT: DARWIN Viewer Image Provider
// AUTHOR : Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef PROCESS_H
#define PROCESS_H


//------------------------------------------------------------------------------
// INCLUDE HEADERS AND FORWARD DECLARATION
//------------------------------------------------------------------------------
#include <opencv/cv.h>
#include <QThread>
#include <QList>
#include <QMutex>
class QFile;
class QString;
class QTcpServer;
class QTcpSocket;
namespace Robot
{
    class YuvCamera;
}
namespace cv
{
    class Mat;
    class VideoCapture;
}


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class Process : public QThread
{
    Q_OBJECT

private:
    Robot::YuvCamera* _camera;
    cv::VideoCapture* _capture;
    cv::Mat* _captureImage;
    QFile* _stdout;
    QMutex _mutex; // mutex for client list
    QTcpServer* _server;
    QList<QTcpSocket*> _clients;
    int _imgWidth;
    int _imgHeight;
    int _imgNumChannels;

public:
    Process( const char* avifile = 0 );
    virtual ~Process( void );

private slots:
    virtual void on__server_newConnection( void );

private:
    virtual IplImage nextFrame( void );
    virtual void print( const QString& str );
    virtual void run( void );

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
