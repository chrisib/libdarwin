#ifndef BALANCELOGGER_H
#define BALANCELOGGER_H

#include <fstream>
#include <balancer.h>
#include <balancestatus.h>

class BalanceLogger
{
public:
    BalanceLogger(Balancer *balancer, BalanceStatus *status);
    ~BalanceLogger();

    void WriteLog();
    void Open(const char* filename);
    void Close();

private:
    std::ofstream logFile;
    Balancer *balancer;
    BalanceStatus *status;

};

#endif // BALANCELOGGER_H
