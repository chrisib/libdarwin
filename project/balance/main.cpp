#include <LinuxDARwIn.h>
#include <iostream>
#include <darwin/framework/Walking.h>
#include <darwin/framework/Action.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/LeftLeg.h>
#include <darwin/framework/RightLeg.h>
#include <darwin/framework/DoublePidController.h>
#include <balancer.h>
#include <balancestatus.h>
#include <balanceui.h>
#include <balancelogger.h>

//#define NO_NCURSES

using namespace Robot;
using namespace std;

BalanceStatus status;
Balancer balancer(&status);
BalanceLogger logger(&balancer,&status);
BalanceUI ui(&balancer, &status);

bool enableLogging = false;

void initialize(int argc, char** argv)
{
    minIni *ini = NULL;
    for(int i=1; i<argc; i++)
    {
		if(!strcmp(argv[i],"-c"))
		{
			i++;
			cerr << "Will load configuration data from " << argv[i] << endl;
			ini = new minIni(argv[i]);
		}
		else if(!strcmp(argv[i],"-l"))
		{
			cerr << "Logging enabled" << endl;
			enableLogging = true;
		}
		else
		{
			cerr << "Usage: " << endl <<
                "\t" << argv[0] << " [--help] [-c FILE.ini] [-l]" << endl <<
                endl <<
                "\t-c\tSpecify the configuration file (default: config.ini)" << endl <<
                "\t-l\tEnable logging data to a CSV file" << endl <<
                endl;
			exit(0);
		}
	}
    
    if(ini==NULL)
        ini=new minIni("config.ini");

    cerr << "Initializing..." << endl;
    LinuxDARwIn::ChangeCurrentDir();
    LinuxDARwIn::Initialize(ini->gets("Files","Motion","motion.bin").c_str(),Action::DEFAULT_MOTION_SIT_DOWN);
    //LinuxDARwIn::InitializeSignals();
    //Voice::Initialize();
    cerr << "done" << endl << endl;

    balancer.LoadIniSettings(ini);
    status.LoadIniSettings(ini);

    cerr << "Standing up..." << endl;
    Action::GetInstance()->Start(Action::DEFAULT_MOTION_STAND_UP);
    Action::GetInstance()->Finish();
    cerr << "done" << endl << endl;

    cerr << "Setting up additional modules..." << endl;
    LeftLeg::GetInstance()->Initialize();
    RightLeg::GetInstance()->Initialize();

    LeftArm::GetInstance()->m_Joint.SetEnableBody(false);
    RightArm::GetInstance()->m_Joint.SetEnableBody(false);
    LeftLeg::GetInstance()->m_Joint.SetEnableBody(false);
    RightLeg::GetInstance()->m_Joint.SetEnableBody(false);

    MotionManager::GetInstance()->AddModule(LeftArm::GetInstance());
    MotionManager::GetInstance()->AddModule(RightArm::GetInstance());
    MotionManager::GetInstance()->AddModule(LeftLeg::GetInstance());
    MotionManager::GetInstance()->AddModule(RightLeg::GetInstance());

    LeftArm::GetInstance()->m_Joint.SetEnableLeftArmOnly(true,true);
    RightArm::GetInstance()->m_Joint.SetEnableRightArmOnly(true,true);
    LeftLeg::GetInstance()->m_Joint.SetEnableLeftLegOnly(true,true);
    RightLeg::GetInstance()->m_Joint.SetEnableRightLegOnly(true,true);

    balancer.EnterStartingPosition();
    cerr << "done" << endl;

	if(enableLogging)
	{
		cerr << "Creating log file..." << endl;
		logger.Open(ini->gets("Files","Log","log.csv").c_str());
		cerr << "done" << endl;
	}
	
    //cout << endl << "===== START ACTIVE BALANCING NOW =====" << endl;
}

int main(int argc, char** argv)
{
    initialize(argc, argv);
    
    cerr << "Press the middle button" << endl;
    MotionManager::GetInstance()->WaitButton(CM730::MIDDLE_BUTTON);
#ifndef NO_NCURSES
    ui.Start();
#endif

    LeftLeg::GetInstance()->SetMoveMethod(Limb::ImmediateMove);
    LeftArm::GetInstance()->SetMoveMethod(Limb::ImmediateMove);
    RightLeg::GetInstance()->SetMoveMethod(Limb::ImmediateMove);
    RightArm::GetInstance()->SetMoveMethod(Limb::ImmediateMove);
	
    unsigned long lastTick = MotionStatus::ticks;
    for(;;)
    {   
//		cerr << MotionStatus::ticks << endl;
        if(MotionStatus::ticks != lastTick)
        {
            status.ReadSensors();
            balancer.Process();
            
            // keep log data synchronized
            if(enableLogging)
				logger.WriteLog();
#ifndef NO_NCURSES
            ui.Update();
#endif
			
            // update our tick-counter
			lastTick = MotionStatus::ticks;
        }
#ifndef NO_NCURSES
        // update the UI
        ui.Refresh();
#endif
    }
    
#ifndef NO_NCURSES
    ui.Stop();
#endif
    return 0;
}
