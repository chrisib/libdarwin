#ifndef BALANCER_H
#define BALANCER_H

#include <darwin/framework/DoublePidController.h>
#include <darwin/framework/minIni.h>
#include "balancestatus.h"
#include <darwin/framework/Point.h>

class Balancer
{
public:
    Balancer(BalanceStatus *status);
    ~Balancer();

    void LoadIniSettings(minIni *ini);
    void EnterStartingPosition();

    void Process();

    friend class BalanceLogger;
    friend class BalanceUI;

private:
    BalanceStatus *status;

    double hipHeight;
    double stanceWidth;
    double xOffset;
    double hipPitchOffset, anklePitchOffset;
    double elbowAngle;
    double yawOffset;

    // PID controller for adjusting the robot's inclination
    double inclineP, inclineI, inclineD;
    double goalInclination;
    double maxInclination, minInclination;
    Robot::DoublePidController *inclinationControl;
    double inclinationGain;

    // PID controllers for adjusting the leg lengths
    Robot::DoublePidController *leftLegControl, *rightLegControl;
    double legP, legI, legD;
    double goalLeftLeg, goalRightLeg;
    double setLeftLeg, setRightLeg;
    int legPeriod;
    double legAmplitude;

    // PID controllers for the two shoulders
    double neutralShoulderAngle;
    double shoulderPitchAngle;
    double shoulderP, shoulderI, shoulderD;
    double goalLeftShoulder;
    double goalRightShoulder;
    double maxShoulder, minShoulder;
    double shoulderDecay;
    Robot::DoublePidController *rightShoulderControl, *leftShoulderControl;

    // PID controller for the hip translation
    double goalHipTranslation;
    double setHipTranslation;
    double hipTranslationP;
    double hipTranslationI;
    double hipTranslationD;
    Robot::DoublePidController *hipTranslationControl;
    double maxHipTranslation, minHipTranslation;
    double hipTranslationGain;
    double hipAmplitude;
    int hipPeriod;

    // current positions (used for logging and derivative calculations)
    double currentLeftShoulder, currentRightShoulder;
    double currentLeftLeg, currentRightLeg;
    double currentHipTranslation;
    double armAmplitude;
    int armPeriod;

    // calculated change in inclination based on leg motions
    double realDeltaInclination;

    double setLeftShoulder, setRightShoulder, setInclination;
    Robot::Point3D leftFootPosition, rightFootPosition;
    double armGain;
    
    // counters, etc... for regular oscillations
    int armSwayDirection;
    int hipSwayDirection;
    int legSwayDirection;
    int armPeriodCounter, hipPeriodCounter, legPeriodCounter;
    double hipSwayAmount, armSwayAmount, legSwayAmount;
};

#endif // BALANCER_H
