#include "balancer.h"
#include <iostream>
#include <darwin/framework/Action.h>
#include <darwin/framework/LeftArm.h>
#include <darwin/framework/RightArm.h>
#include <darwin/framework/LeftLeg.h>
#include <darwin/framework/RightLeg.h>
#include <darwin/framework/Math.h>
#include <cmath>
#include <darwin/framework/MotionManager.h>
#include <darwin/framework/Kinematics.h>

using namespace std;
using namespace Robot;

Balancer::Balancer(BalanceStatus *status)
{
    this->status = status;

    goalInclination = 0;
    setInclination = 0;
    goalLeftShoulder = 0;
    setLeftShoulder = 0;
    goalRightShoulder = 0;
    setRightShoulder = 0;
    setHipTranslation = 0;
    goalHipTranslation = 0;
    
    armSwayDirection = 1;
    hipSwayDirection = 1;
    legSwayDirection = 1;
}

Balancer::~Balancer()
{
    delete inclinationControl;
    delete leftShoulderControl;
    delete rightShoulderControl;
    delete leftLegControl;
    delete rightLegControl;
    delete hipTranslationControl;
}

void Balancer::LoadIniSettings(minIni *ini)
{
    cerr << "Setting up PID controllers..." << endl;
    inclineP = ini->getd("Inclination","P");
    inclineI = ini->getd("Inclination","I");
    inclineD = ini->getd("Inclination","D");
    inclinationControl = new DoublePidController(&setInclination, &goalInclination, inclineP, inclineI, inclineD);

    shoulderP = ini->getd("Shoulders","P");
    shoulderI = ini->getd("Shoulders","I");
    shoulderD = ini->getd("Shoulders","D");
    leftShoulderControl = new DoublePidController(&setLeftShoulder, &goalLeftShoulder, shoulderP, shoulderI, shoulderD);
    rightShoulderControl = new DoublePidController(&setRightShoulder, &goalRightShoulder, shoulderP, shoulderI, shoulderD);

    legP = ini->getd("Legs","P");
    legI = ini->getd("Legs","I");
    legD = ini->getd("Legs","D");
    leftLegControl = new DoublePidController(&setLeftLeg, &goalLeftLeg, legP, legI, legD);
    rightLegControl = new DoublePidController(&setRightLeg, &goalRightLeg, legP, legI, legD);

    hipTranslationP = ini->getd("Hips","P");
    hipTranslationI = ini->getd("Hips","I");
    hipTranslationD = ini->getd("Hips","D");
    hipTranslationControl = new DoublePidController(&setHipTranslation, &goalHipTranslation, hipTranslationP, hipTranslationI, hipTranslationD);
    cerr << "done" << endl;

    cerr << "Reading other configuration settings..." << endl;
    hipPitchOffset              = ini->getd("Stance","HipPitchOffset",0.0);
    anklePitchOffset            = ini->getd("Stance","AnklePitchOffset",0.0);
    hipHeight                   = ini->getd("Stance","Height",230);
    stanceWidth                 = ini->getd("Stance","Width",200);
    xOffset                     = ini->getd("Stance","XOffset",0.0);
    elbowAngle                  = ini->getd("Stance","ElbowAngle",0);
    yawOffset                   = ini->getd("Stance","YawOffset",0);
    neutralShoulderAngle        = ini->getd("Stance","ShoulderRoll",45.0);
    shoulderPitchAngle          = ini->getd("Stance","ShoulderPitch",0.0);
    
    cerr << 
		"\t" << "    O    " << "Hip Height: " << hipHeight << endl <<
		"\t" << "  +-+-+  " << "     Width: " << stanceWidth << endl <<
		"\t" << " /  |  \\ "<< "Hip Offset: " << hipPitchOffset << endl <<
		"\t" << "  +-+-+  " << "       Yaw: " << yawOffset << endl <<
		"\t" << "  |   |  " << " Shoulders: " << neutralShoulderAngle << " " << shoulderPitchAngle << endl <<
		"\t" << "  |   |  " << "    Ankles: " << anklePitchOffset << endl <<
		"\t" << " -+   +- " << endl <<
		endl;

    maxInclination              = ini->getd("Inclination","Max",30);
    minInclination              = ini->getd("Inclination","Min",-maxInclination);
    inclinationGain             = ini->getd("Inclination","Gain");
    
    legPeriod                   = ini->geti("Legs","Period",1);
    legAmplitude                = ini->getd("Legs","Amplitude");
    
    maxHipTranslation           = ini->getd("Hips","Max");
    minHipTranslation           = ini->getd("Hips","Min");
    hipTranslationGain          = ini->getd("Hips","Gain");
    hipAmplitude                = ini->getd("Hips","Amplitude");
    hipPeriod                   = ini->geti("Hips","Period",1);
    
    maxShoulder                 = ini->getd("Shoulders","Max",90);
    minShoulder                 = ini->getd("Shoulders","Min",20);
    shoulderDecay               = ini->getd("Shoulders","Decay");
    armGain                     = ini->getd("Shoulders","Gain");
    armAmplitude                = ini->getd("Shoulders","Amplitude");
    armPeriod                   = ini->geti("Shoulders","Period",1);
    
    cerr << "\t" << "      Leg Sway: " << legAmplitude << "mm @ " << legPeriod << endl;
    cerr << "\t" << "      Hip Sway: " << hipAmplitude << "mm @ " << hipPeriod << endl;
    cerr << "\t" << " Shoulder Sway: " << armAmplitude << "deg @ " << armPeriod << endl;
    cerr << "\t" << "Shoulder Decay: " << shoulderDecay << endl;

    goalLeftLeg = -hipHeight;
    goalRightLeg = -hipHeight;
    setLeftLeg = -hipHeight;
    setRightLeg = -hipHeight;
    setLeftShoulder = neutralShoulderAngle;
    setRightShoulder = neutralShoulderAngle;
    goalLeftShoulder = neutralShoulderAngle;
    goalRightShoulder = neutralShoulderAngle;
    cerr << "done" << endl;

    cerr << "Applying configuration settings..." << endl;
    leftFootPosition.X = xOffset;
    leftFootPosition.Y = (stanceWidth - Kinematics::HIP_WIDTH) /2;
    leftFootPosition.Z = -hipHeight;

    rightFootPosition.X = xOffset;
    rightFootPosition.Y = (stanceWidth - Kinematics::HIP_WIDTH) /2;
    rightFootPosition.Z = -hipHeight;

    LeftLeg::GetInstance()->ANKLE_PITCH_OFFSET = anklePitchOffset;
    RightLeg::GetInstance()->ANKLE_PITCH_OFFSET = anklePitchOffset;
    LeftLeg::GetInstance()->HIP_PITCH_OFFSET = hipPitchOffset;
    RightLeg::GetInstance()->HIP_PITCH_OFFSET = hipPitchOffset;
    LeftLeg::GetInstance()->HIP_YAW_OFFSET = yawOffset;
    RightLeg::GetInstance()->HIP_YAW_OFFSET = yawOffset;
    cerr << "done" << endl;
}

void Balancer::EnterStartingPosition()
{
    LeftLeg::GetInstance()->SetMoveMethod(Limb::ApproachByOne);
    LeftArm::GetInstance()->SetMoveMethod(Limb::ApproachByOne);
    RightLeg::GetInstance()->SetMoveMethod(Limb::ApproachByOne);
    RightArm::GetInstance()->SetMoveMethod(Limb::ApproachByOne);

    LeftLeg::GetInstance()->SetGoalPosition(leftFootPosition,0);
    RightLeg::GetInstance()->SetGoalPosition(rightFootPosition,0);

    LeftArm::GetInstance()->SetAngles(shoulderPitchAngle,neutralShoulderAngle, elbowAngle);
    RightArm::GetInstance()->SetAngles(shoulderPitchAngle,neutralShoulderAngle, elbowAngle);
}

void Balancer::Process()
{
	currentLeftShoulder = setLeftShoulder;
    currentRightShoulder = setRightShoulder;
    currentHipTranslation = setHipTranslation;
    currentHipTranslation = setHipTranslation;
    currentLeftLeg = setLeftLeg;
    currentRightLeg = setRightLeg;
    
    //double inclinationErr = status->currentInclination - status->prevInclinationForecast;
    //double gyroErr = status->currentGyro - status->prevGyroForecast;

    // use the inclination to give us a baseline hip position
    if(status->FALLING_STATE != BalanceStatus::STABLE)
    {
		// lean opposite to where we're going
		// this *should* oscillate in a narrow band when we're upright
		// and will skew violently when we're unsteady
		//goalInclination = -status->projectedInclination * inclinationGain;
		goalInclination = -status->currentInclination * inclinationGain;

		// adjust arms to counteract additional rotation
		// flail the spinward arm out quickly and bring the other one in quickly
		// move the arms at an angular velocity that corresponds to the torso speed
		// excessGyro is the speed we need to cancel out
        double excessGyro = 0; // = status->currentGyro; //status->projectedGyro;// + gyroErr;
        
        if(status->currentGyro > 0 && status->currentInclination >= 0 && status->prevInclination < 0)
        {
			excessGyro = status->projectedGyro; //status->anticipatedGyro;
		}
		else if(status->currentGyro < 0 && status->currentInclination <= 0 && status->prevInclination > 0)
		{
			excessGyro = status->projectedGyro; //status->anticipatedGyro;
		}
		else
		{
			excessGyro = status->currentGyro;
		}
        
        goalLeftShoulder = currentLeftShoulder - excessGyro * armGain;
        goalRightShoulder = currentRightShoulder + excessGyro * armGain;

		// update the PID controllers
		leftShoulderControl->Update();
		rightShoulderControl->Update();
        inclinationControl->Update();

		// clamp the PID set values within their allowed ranges
		if(setInclination < minInclination)
			setInclination = minInclination;
		else if(setInclination > maxInclination)
			setInclination = maxInclination;

		// calculate the height of each leg in order to achieve the desired inclination
		if(setInclination != 0)
		{
			// calculate the difference in height between the legs
			double legHeightDifference = sin(deg2rad(fabs(setInclination))) * Kinematics::HIP_WIDTH;

			if(setInclination > 0) // need to lean left
			{
				// fully extend right leg
				goalRightLeg = -hipHeight;

				// retract left leg
				goalLeftLeg = -hipHeight + legHeightDifference;

			}
			else // need to lean right
			{
				// fully extend left leg
				goalLeftLeg = -hipHeight;

				// retract right leg
				goalRightLeg = -hipHeight + legHeightDifference;
			}
		}
		else
		{
			// stand straight up; fully extend both legs
			goalLeftLeg = -hipHeight;
			goalRightLeg = -hipHeight;
		}

		leftLegControl->Update();
		rightLegControl->Update();

        // update the left/right position of the hips
        //goalHipTranslation = status->projectedGyro * hipTranslationGain;
        //goalHipTranslation = status->currentGyro * hipTranslationGain;
        //goalHipTranslation = status->projectedInclination * hipTranslationGain;
        goalHipTranslation = status->currentInclination * hipTranslationGain;
        hipTranslationControl->Update();
        
        
	}
	
	// constantly move the hips side-to-side
    hipPeriodCounter += hipSwayDirection;
    if(hipPeriodCounter <= -hipPeriod/2)
		hipSwayDirection = 1;
	else if(hipPeriodCounter >= hipPeriod/2)
		hipSwayDirection = -1;
	hipSwayAmount = hipPeriodCounter * hipAmplitude / (hipPeriod/2);
	setHipTranslation += hipSwayAmount;
	
	//if(setHipTranslation < minHipTranslation)
	//	setHipTranslation = minHipTranslation;
	//else if(setHipTranslation > maxHipTranslation)
	//	setHipTranslation = maxHipTranslation;
	
	leftFootPosition.Y =  (stanceWidth - Kinematics::HIP_WIDTH) /2 - setHipTranslation;
    rightFootPosition.Y = (stanceWidth - Kinematics::HIP_WIDTH) /2 + setHipTranslation;
    
    // regularly sway the legs up and down
    legPeriodCounter += legSwayDirection;
    if(legPeriodCounter <= -legPeriod/2)
		legSwayDirection = 1;
	else if(legPeriodCounter >= legPeriod/2)
		legSwayDirection = -1;
	legSwayAmount = legPeriodCounter * legAmplitude / (legPeriod/2);
	
	setLeftLeg += legSwayAmount;
	setRightLeg -= legSwayAmount;
    
    // swing the arms back and forth
    armPeriodCounter += armSwayDirection;
    if(armPeriodCounter <= -armPeriod/2)
		armSwayDirection = 1;
	else if(armPeriodCounter >= armPeriod/2)
		armSwayDirection = -1;
	armSwayAmount = armPeriodCounter * armAmplitude / (armPeriod/2);
	setLeftShoulder += armSwayAmount;
	setRightShoulder -= armSwayAmount;
	
	if(setLeftShoulder < minShoulder)
		setLeftShoulder = minShoulder;
	else if(setLeftShoulder > maxShoulder)
		setLeftShoulder = maxShoulder;

	if(setRightShoulder < minShoulder)
		setRightShoulder = minShoulder;
	else if(setRightShoulder > maxShoulder)
		setRightShoulder = maxShoulder;
		
	// slowly trend the arms back towards centre
	setRightShoulder -= (setRightShoulder - neutralShoulderAngle) * shoulderDecay;
	setLeftShoulder -= (setLeftShoulder - neutralShoulderAngle) * shoulderDecay;
	
	
	// calculate the change in hip inclination based on the legs' motion
	double oldHipDifference = fabs(leftFootPosition.Z - rightFootPosition.Z);
	double oldAngle = asin(oldHipDifference/Kinematics::HIP_WIDTH) * (leftFootPosition.Z > rightFootPosition.Z ? -1.0 : 1.0);
	double newHipDifference = fabs(setLeftLeg - setRightLeg);
	double newAngle = asin(newHipDifference/Kinematics::HIP_WIDTH) * (setLeftLeg > setRightLeg ? -1.0 : 1.0);
	realDeltaInclination = newAngle - oldAngle;

	leftFootPosition.Z = setLeftLeg;
	rightFootPosition.Z = setRightLeg;

    // send the positions to the limbs
    LeftArm::GetInstance()->SetAngles(shoulderPitchAngle,setLeftShoulder,elbowAngle);
    RightArm::GetInstance()->SetAngles(shoulderPitchAngle,setRightShoulder,elbowAngle);
    LeftLeg::GetInstance()->SetGoalPosition(leftFootPosition,0);
    RightLeg::GetInstance()->SetGoalPosition(rightFootPosition,0);

    // based on our decisions above we can now anticipate what the impact will be on the next round
    // calculate the actual change in inclination (since the legs adjust independently)
    status->Anticipate(realDeltaInclination, setLeftShoulder - currentLeftShoulder, setRightShoulder - currentRightShoulder, setHipTranslation - currentHipTranslation);
}
