#include "balancelogger.h"
#include <darwin/framework/MotionStatus.h>

using namespace std;
using namespace Robot;

BalanceLogger::BalanceLogger(Balancer *balancer, BalanceStatus *status)
{
    this->balancer = balancer;
    this->status = status;
}
BalanceLogger::~BalanceLogger()
{
    Close();
}

void BalanceLogger::Open(const char* filename)
{
    logFile.open(filename, ios_base::out);
    logFile << "Tick," <<
               "Current Inclination,Inclination Prev Prediction,Prev Inclination,Projected Inclination,Anticipated Inclination," <<
               "Current deg/s,deg/s Prev Prediction,Prev deg/s,Projected deg/s,Anticipated deg/s," <<

           // stance data and motion data
               "Delta Inclination," <<
               "Left Leg Height,Delta Left Leg," <<
               "Right Leg Height,Delta Right Leg," <<
               "Hip Translation,Delta Hip," <<
               "Left Arm Angle,Delta Left Arm," <<
               "Right Arm Angle,Delta Right Arm," <<

           // raw sensor data
               "RL Accel," <<
               "FB Accel," <<
               "Z Accel," <<
               "RL Gyro" <<
               endl;
}

void BalanceLogger::Close()
{
    logFile.close();
}

void BalanceLogger::WriteLog()
{
    // update the log file
    // Tick,
    // Current Inclination,
    // Prev Inclination,
    // Projected Inclination,
    // Anticipated Inclination,
    // err (current)
    logFile << 	MotionStatus::ticks << "," <<
    
                status->currentInclination << "," <<
                status->prevInclinationForecast << "," <<
                status->prevInclination << "," <<
                status->projectedInclination << "," <<
                status->anticipatedInclination << "," <<
                

    // Current deg/s,
    // Prev deg/s,
    // Projected deg/s,
    // Anticipated deg/s,
                status->currentGyro << "," <<
                status->prevGyroForecast << "," <<
                status->prevGyro << "," <<
                status->projectedGyro << "," <<
                status->anticipatedGyro << "," <<
				

    // Delta Inclination,
    // Left Leg Height, Delta Left Leg,
    // Right Leg Height, Delta Right Leg,
    // Hip Translation, Delta Hip,
    // Left Arm Angle, Delta Left Arm,
    // Right Arm Angle,Delta Right Arm,
                balancer->realDeltaInclination << "," <<

                balancer->currentLeftLeg << "," <<
                (balancer->setLeftLeg - balancer->currentLeftLeg) << "," <<

                balancer->currentRightLeg << "," <<
                (balancer->setRightLeg - balancer->currentRightLeg) << "," <<

                balancer->currentHipTranslation << "," <<
                (balancer->setHipTranslation - balancer->currentHipTranslation) << "," <<

                balancer->currentLeftShoulder << "," <<
                (balancer->setLeftShoulder - balancer->currentLeftShoulder) << "," <<

                balancer->currentRightShoulder << "," <<
                (balancer->setRightShoulder - balancer->currentRightShoulder) << "," <<

    // Raw sensor data
                MotionStatus::RL_ACCEL << "," <<
                MotionStatus::FB_ACCEL << "," <<
                MotionStatus::Z_ACCEL << "," <<
                MotionStatus::RL_GYRO << "," <<

    // end of logged data
                endl;
}
