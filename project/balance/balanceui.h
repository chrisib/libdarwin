#ifndef BALANCEUI_H
#define BALANCEUI_H

#include "balancer.h"
#include "balancestatus.h"

class BalanceUI
{
public:
    BalanceUI(Balancer *balancer, BalanceStatus *status);
    ~BalanceUI();

    void Start();
    void Stop();

    void Update();
    void Refresh();

    static const int MAX_COLS = 1024;

private:
    void DrawGraph();
    void WriteText();

    // for drawing an ASCII art graph
    int graphIdx;
    double inclinations[MAX_COLS];
    double hipTargets[MAX_COLS];
    double gyroReadings[MAX_COLS];
    double anticipatedGyroReadings[MAX_COLS];

    int numRows, numCols;

    Balancer *balancer;
    BalanceStatus *status;
};

#endif // BALANCEUI_H
