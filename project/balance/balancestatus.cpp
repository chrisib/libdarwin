#include "balancestatus.h"
#include <darwin/framework/MotionStatus.h>
#include <cmath>
#include <darwin/framework/Math.h>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <assert.h>

using namespace std;
using namespace Robot;

#define G 9.81

BalanceStatus::BalanceStatus()
{
    currentInclination = 0;
    prevInclination = 0;
    currentGyro = 0;
    prevGyro = 0;
}

BalanceStatus::~BalanceStatus()
{
    free(oldInclinations);
    free(oldGyros);
}

void BalanceStatus::LoadIniSettings(minIni *ini)
{
    minBalance = ini->getd("Stance","MinBalance",-1);
    maxBalance = ini->getd("Stance","MaxBalance",1);

    armGyroGain = ini->getd("Prediction", "ArmGyroGain");
    armAccelGain = ini->getd("Prediction", "ArmAccelGain");
    inclinationGyroGain = ini->getd("Prediction","InclinationGyroGain");
    inclinationAccelGain = ini->getd("Prediction","InclinationAccelGain");
    hipGyroGain = ini->getd("Prediction","HipGyroGain");
    hipAccelGain = ini->getd("Prediction","HipAccelGain");

    const char *filterAlg = ini->gets("Inclination", "Filter").c_str();
    if(!strcmp(filterAlg,"average"))
    {
        accelFilterAlg = AVERAGE;
    }
    else if(!strcmp(filterAlg,"median"))
    {
        accelFilterAlg = MEDIAN;
    }
    else
    {
        cerr << "Unknown noise filtering algorithm \"" << filterAlg << "\"" << endl;
        accelFilterAlg = AVERAGE;
    }

    saveLastAccel = ini->geti("Inclination","SaveLast",1);
    if(saveLastAccel < 1)
        saveLastAccel = 1;
    oldInclinations = (double*)malloc(sizeof(double)*saveLastAccel);
    oldInclinationsIn = 0;
    for(int i=0; i<saveLastAccel; i++)
        oldInclinations[i] = 0;


    filterAlg = ini->gets("Gyro", "Filter").c_str();
    if(!strcmp(filterAlg,"average"))
    {
        gyroFilterAlg = AVERAGE;
    }
    else if(!strcmp(filterAlg,"median"))
    {
        gyroFilterAlg = MEDIAN;
    }
    else
    {
        cerr << "Unknown noise filtering algorithm \"" << filterAlg << "\"" << endl;
        gyroFilterAlg = AVERAGE;
    }

    saveLastGyro = ini->geti("Gyro","SaveLast",1);
    if(saveLastGyro < 1)
        saveLastGyro = 1;
    oldGyros = (double*)malloc(sizeof(double)*saveLastGyro);
    for(int i=0; i<saveLastGyro; i++)
        oldGyros[i] = 0;

}

void BalanceStatus::ReadSensors()
{
    // convert accelerometer data to m/s^2
    // see http://support.robotis.com/en/product/darwin-op/references/reference/hardware_specifications/electronics/sub_controller_%28cm-730%29.htm
    prevInclination = currentInclination;
    rlAccel = ((MotionStatus::RL_ACCEL/128.0) - 4.0) * G;
    fbAccel = ((MotionStatus::FB_ACCEL/128.0) - 4.0) * G;
    zAccel = ((MotionStatus::Z_ACCEL/128.0) - 4.0) * G;

    // filter out sensor noise
    oldInclinations[oldInclinationsIn] = rad2deg(atan2(rlAccel, zAccel)) - anticipatedInclination;
    oldInclinationsIn = (oldInclinationsIn+1) % saveLastAccel;
    currentInclination = filterData(oldInclinations, saveLastAccel, accelFilterAlg);

    // use the derivative to estimate the next inclination
    projectedInclination = currentInclination + (currentInclination - prevInclination);


    // convert gyroscope data into degrees per second
    // see http://support.robotis.com/en/product/darwin-op/references/reference/hardware_specifications/electronics/sub_controller_%28cm-730%29.htm
    prevGyro = currentGyro;
    //currentGyro = MotionStatus::RL_GYRO/0.32;

    oldGyros[oldGyroIn] = currentInclination - prevInclination - anticipatedGyro;
    oldGyroIn = (oldGyroIn + 1) % saveLastGyro;
    currentGyro = filterData(oldGyros, saveLastGyro, gyroFilterAlg);

    currentGyro = currentInclination - prevInclination;

    projectedGyro = currentGyro + (currentGyro - prevGyro);

    if(projectedInclination > maxBalance && currentInclination > maxBalance)
    {
        FALLING_STATE = LEANING_AND_FALLING_LEFT;
    }
    else if(projectedInclination > maxBalance && currentInclination < maxBalance)
    {
        // leaning right but falling left
        FALLING_STATE = LEANING_RIGHT_FALLING_LEFT;
    }
    else if(projectedInclination < maxBalance && currentInclination < maxBalance)
    {
        // leaning and falling to the right
        FALLING_STATE = LEANING_AND_FALLING_RIGHT;
    }
    else if(projectedInclination < maxBalance && currentInclination > maxBalance)
    {
        // leaning left but falling right
        FALLING_STATE = LEANING_LEFT_FALLING_RIGHT;
    }
    else
    {
        FALLING_STATE = STABLE;
    }
}

void BalanceStatus::Anticipate(double deltaInclination, double deltaLeftShoulder, double deltaRightShoulder, double deltaHip)
{
    prevGyroForecast = anticipatedGyro + projectedGyro;
    prevInclinationForecast = anticipatedInclination + projectedInclination;

    // calculate the likely value of the gyroscope based on all of our current data
    anticipatedGyro = deltaInclination * inclinationGyroGain
                      + deltaLeftShoulder * armGyroGain
                      - deltaRightShoulder * armGyroGain
                      + deltaHip * hipGyroGain;

    // do the same with the gyro (i.e. inclination) using the second derivative
    double ddIncline = deltaInclination - prevDeltaInclination;
    double ddLeft = deltaLeftShoulder - prevDeltaLeftShoulder;
    double ddRight = deltaRightShoulder - prevDeltaRightShoulder;
    double ddHip = deltaHip - prevDeltaHip;
    anticipatedInclination = ddIncline * inclinationAccelGain
            + ddLeft * armAccelGain
            - ddRight * armAccelGain
            + ddHip * hipAccelGain;

    prevDeltaInclination = deltaInclination;
    prevDeltaLeftShoulder = deltaLeftShoulder;
    prevDeltaRightShoulder = deltaRightShoulder;
    prevDeltaHip = deltaHip;
}

int sortCompare(const void* a, const void *b)
{
    double x = *(double*)a;
    double y = *(double*)b;

    if(x < y)
        return -1;
    else if(x > y)
        return 1;
    else
        return 0;

}

double BalanceStatus::filterData(double *data, int numData, int filterAlg)
{
    double result = 0.0;
    switch(filterAlg)
    {
    case MEDIAN:
        double sorted[numData];
        for(int i=0; i<numData; i++)
            sorted[i] = data[i];

        qsort(sorted, numData, sizeof(double),sortCompare);
        result = sorted[numData/2];
        break;

    case AVERAGE:
    default:
        for(int i=0; i<numData; i++)
            result += data[i];

        result /= numData;
        break;
    }

    return result;
}
