#include "balanceui.h"
#include <ncurses.h>
#include <darwin/framework/MotionStatus.h>

using namespace Robot;

BalanceUI::BalanceUI(Balancer *balancer, BalanceStatus *status)
{
    this->balancer = balancer;
    this->status = status;
}

BalanceUI::~BalanceUI()
{
    Stop();
}

void BalanceUI::Start()
{
    // start the ncurses interface
    initscr();
    getmaxyx(stdscr,numRows,numCols);

    if(numCols > MAX_COLS)
        numCols = MAX_COLS;

    // ASCII art rolling graph
    for(int i=0; i<numCols; i++)
    {
        inclinations[i] = 0;
        hipTargets[i] = 0;
        gyroReadings[i] = 0;
        anticipatedGyroReadings[i] = 0;
    }
    refresh();
}

void BalanceUI::Stop()
{
    // end ncurses
    endwin();
}

void BalanceUI::Update()
{
    // update the graph data
    //hipTargets[graphIdx] = balancer->hipPosition;
    inclinations[graphIdx] = status->currentInclination;
    gyroReadings[graphIdx] = status->currentGyro;
    anticipatedGyroReadings[graphIdx] = status->anticipatedGyro;

    // normalize the graph data between reasonable values
    //hipTargets[graphIdx] = hipTargets[graphIdx] / balancer->maxHip * 5;
    inclinations[graphIdx] = inclinations[graphIdx] / 30 * 5;
    gyroReadings[graphIdx] = gyroReadings[graphIdx] / 15 * 5;
    anticipatedGyroReadings[graphIdx] = anticipatedGyroReadings[graphIdx] / 15 * 5;
}

void BalanceUI::DrawGraph()
{
    // draw a rolling graph in the bottom 10 rows
    mvprintw(numRows-12,0,"+ = hip position   . = inclination    * = gyro    x = anticipated gyro");
    for(int i=0; i<numCols; i++)
    {
        // clear the graph by filling it with spaces
        for(int row=0; row<=10; row++)
        {
            if(row==5)
                mvprintw(numRows-row-1, i, "-");
            else
                mvprintw(numRows-row-1, i, " ");
        }

        int row = (int) hipTargets[(graphIdx+i)%numCols] + 5;
        if(row < 0)
            row = 0;
        else if(row > 5)
            row = 5;
        mvprintw(numRows-row-1,i,"+");

        row = (int) inclinations[(graphIdx+i)%numCols] + 5;
        if(row < 0)
            row = 0;
        else if(row > 10)
            row = 10;
        mvprintw(numRows-row-1,i,".");

        row = (int) gyroReadings[(graphIdx+i)%numCols] + 5;
        if(row < 0)
            row = 0;
        else if(row > 10)
            row = 10;
        mvprintw(numRows-row-1,i,"*");

        row = (int) anticipatedGyroReadings[(graphIdx+i)%numCols] + 5;
        if(row < 0)
            row = 0;
        else if(row > 10)
            row = 10;
        mvprintw(numRows-row-1,i,"x");
    }
}

void BalanceUI::WriteText()
{
    const int NUM_LINES = 13;
    const char* toPrint[NUM_LINES] = {
        "ACTIVE BALANCING",                                     //0*
        "",                                                     //1*
        "          O       SENSORS        CURRENT  PROJECTEED  ANTICIPATED  RAW", 	//2*
        "        +-+-+       Inclination: %+0.3f   %+0.3f      %+0.3f      %+d",	//3
        "       /  |  \\             Gyro: %+0.3f   %+0.3f      %+0.3f      %+d    ",   	//4
        "      /   |   \\",                                     //5*
        "          |       STANCE",                             //6*
        "       +--+--+      Inclination: %+0.3f",              //7
        "       |     |       Hip Trans.: %+0.3f\t(%+0.3f)",              //8
        "       |     |         Left Leg: %+0.3f\t(%+0.3f)",              //9
        "       |     |        Right Leg: %+0.3f\t(%0.3f)",              //10
        "   ---------------     Left Arm: %+0.3f\t(%+0.3f)",              //11
        "          O           Right Arm: %+0.3f\t(%+0.3f)",              //12
    };

    for(int i=0; i<NUM_LINES; i++)
    {
        switch(i)
        {
        case 3:
            mvprintw(i,0,toPrint[i],status->currentInclination, status->projectedInclination, status->anticipatedInclination, MotionStatus::Z_ACCEL);
            break;

        case 4:
            mvprintw(i,0,toPrint[i],status->currentGyro, status->projectedGyro, status->anticipatedGyro, MotionStatus::RL_GYRO);
            break;

        case 7:
            mvprintw(i,0,toPrint[i],balancer->setInclination);
            break;

        case 8:
            mvprintw(i,0,toPrint[i],balancer->setHipTranslation, balancer->hipSwayAmount);
            break;

        case 9:
            mvprintw(i,0,toPrint[i],balancer->leftFootPosition.Z, balancer->legSwayAmount);
            break;

        case 10:
            mvprintw(i,0,toPrint[i],balancer->rightFootPosition.Z, -balancer->legSwayAmount);
            break;

        case 11:
            mvprintw(i,0,toPrint[i],balancer->setLeftShoulder, balancer->armSwayAmount);
            break;

        case 12:
            mvprintw(i,0,toPrint[i],balancer->setRightShoulder, -balancer->armSwayAmount);
            break;
            
		default:
            mvprintw(i,0,toPrint[i]);
            break;
        }
    }

    graphIdx++;
    if(graphIdx == numCols)
        graphIdx = 0;

    refresh();
}

void BalanceUI::Refresh()
{
    WriteText();
    //DrawGraph();
}
