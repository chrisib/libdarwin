#ifndef BALANCESTATUS_H
#define BALANCESTATUS_H

#include <darwin/framework/minIni.h>

class BalanceStatus
{
public:
    friend class BalanceLogger;
    friend class BalanceUI;

    enum
    {
        STABLE = 0,
        LEANING_AND_FALLING_LEFT,
        LEANING_AND_FALLING_RIGHT,
        LEANING_LEFT_FALLING_RIGHT,
        LEANING_RIGHT_FALLING_LEFT
    };

    BalanceStatus();
    ~BalanceStatus();

    void LoadIniSettings(minIni *ini);

    void ReadSensors();
    void Anticipate(double deltaInclination, double deltaLeftShoulder, double deltaRightShoulder, double deltaHip);

    int FALLING_STATE;

    // current, projected and anticipated sensor readings
    double projectedInclination;
    double projectedGyro;
    double currentInclination;
    double currentGyro;
    double anticipatedInclination;
    double anticipatedGyro;

    double prevInclinationForecast;
    double prevGyroForecast;
    
    // previous sensor readings (used for derivative calculations)
    double prevInclination;
    double prevGyro;

private:

    // raw X/Y/Z accel data
    double rlAccel;
    double fbAccel;
    double zAccel;

    // range of values we consider to be "upright"
    double minBalance, maxBalance;

    double inclinationGyroGain, inclinationAccelGain;
    double armGyroGain, armAccelGain;
    double hipGyroGain, hipAccelGain;

    double prevDeltaInclination, prevDeltaLeftShoulder, prevDeltaRightShoulder, prevDeltaHip;

    double *oldInclinations;
    int saveLastAccel;
    int accelFilterAlg;
    int oldInclinationsIn;

    double *oldGyros;
    int saveLastGyro;
    int gyroFilterAlg;
    int oldGyroIn;

    double filterData(double *data, int numData, int filterAlg);

    enum
    {
        AVERAGE = 0,
        MEDIAN = 1
    };
};

#endif // BALANCESTATUS_H
