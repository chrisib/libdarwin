/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class provides basic initialization functions for the Darwin
 * most of this code is copied from demo applications that ship with the
 * Darwin.  Additional initialization may be required by your own application
 ************************************************************************/

#include "LinuxDARwIn.h"
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <list>
#include <ctime>

#ifdef BACK_TRACE
#include <execinfo.h>
#endif

using namespace Robot;
using namespace std;

const char* LinuxDARwIn::DEFAULT_TTY = "/dev/ttyUSB0";
const char* LinuxDARwIn::SECONDARY_TTY = "/dev/ttyUSB1";

int LinuxDARwIn::safePosition = -1;

#ifndef DUMMY_CM730
	LinuxCM730 LinuxDARwIn::linux_cm730(LinuxDARwIn::DEFAULT_TTY);
#else
	DummyCM730 LinuxDARwIn::linux_cm730(LinuxDARwIn::DEFAULT_TTY);
#endif
CM730 LinuxDARwIn::cm730(&LinuxDARwIn::linux_cm730);

void LinuxDARwIn::ChangeCurrentDir()
{
    char exepath[1024] = {0};
    if(readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
    {
        if(chdir(dirname(exepath)))
            cerr << "[debug] chdir error!!" << endl;
    }
}

void LinuxDARwIn::InitializeSignals()
{
    signal(SIGABRT, &SignalHandler);
    signal(SIGTERM, &SignalHandler);
    signal(SIGQUIT, &SignalHandler);
    signal(SIGINT, &SignalHandler);
}

void LinuxDARwIn::AddSignalHandler(int sig, void (*handler)(int))
{
    signal(sig, handler);
}

void LinuxDARwIn::SignalHandler(int sig)
{
    cerr << "Caught signal " << sig << endl << "Shutting down cleanly" << endl;
    // stop every module that is currently running
    MotionManager::GetInstance()->StopAllModules();

	EnterSafePosition();

    exit(0);
}

// SBD
void LinuxDARwIn::SegfaultHandler(int sig)
{
    cout << "Caught signal " << sig << endl;

#ifdef BACK_TRACE
	const int stackFrames = 50;
	void *array[stackFrames];
	
	int size = backtrace(array, stackFrames);
	char** strings = backtrace_symbols(array, size);
	
	cout << "A segmentation fault occured; printing backtrace:" << endl;
    cout << "(a copy of this backtrace will be written to crash.log)" << endl;
	
	for(int i = 0; i < size; i++) {
		cout << strings[i] << endl;
	}

    std::ofstream fout("crash.log");
    char buffer[255];
    time_t rawTime;
    struct tm *t = localtime(&rawTime);
    strftime(buffer, 255, "%c", t);
    fout << "Caught SIG" << sig << " at " << buffer << endl;
    fout << "Backtrace:" << endl;
    for(int i=0; i<size; i++)
    {
        fout << strings[i] << endl;
    }
    fout.close();
	
	free(strings);
	
	MotionManager::GetInstance()->StopAllModules();

	MotionManager::GetInstance()->SetEyeColour(255, 0, 0);
	EnterSafePosition();
#else
	cout << "Compile with BACK_TRACE and -rdynamic to use this." << endl;
#endif
	
	exit(EXIT_FAILURE);
}

bool LinuxDARwIn::Initialize()
{
    safePosition = -1;

    if(!InitCM730())
        return false;

    LinuxMotionTimer::Initialize(MotionManager::GetInstance());

    return true;
}

bool LinuxDARwIn::Initialize(const char *motionFilePath, int initialMotion)
{
	safePosition = initialMotion;

    if(!InitCM730())
        return false;
        
#ifdef BACK_TRACE
	// Turn on segfault handling
	// Compile with -rdynamic
    signal(SIGSEGV, &SegfaultHandler);
#endif

    MotionManager::GetInstance()->AddModule((MotionModule*)Action::GetInstance());

    LinuxMotionTimer::Initialize(MotionManager::GetInstance());
    /////////////////////////////////////////////////////////////////////

    //if(CheckFirmware())
    //    Action::GetInstance()->LoadFile((char*)motionFilePath);
    //else
    //    return false;

	Action::GetInstance()->LoadFile(motionFilePath);

    EnterSafePosition();

    return true;
}

bool LinuxDARwIn::InitCM730()
{
#ifndef DUMMY_CM730
    linux_cm730 = LinuxCM730(LinuxDARwIn::DEFAULT_TTY);
#else
	linux_cm730 = DummyCM730(LinuxDARwIn::DEFAULT_TTY);
#endif
	cm730 = CM730(&LinuxDARwIn::linux_cm730);

    // copied from demo
    //////////////////// Framework Initialize ////////////////////////////
    if(MotionManager::GetInstance()->Initialize(&cm730) == false)
    {
        linux_cm730.SetPortName(SECONDARY_TTY);
        if(MotionManager::GetInstance()->Initialize(&cm730) == false)
        {
            cerr << "[ERROR] Failed to initialize Motion Manager!" << endl;
            return false;
        }
    }

    return true;
}

void LinuxDARwIn::EnterSafePosition()
{
    if(safePosition==-1)
    {
        cerr << "[error] No safe position defined" << endl << "\tInitialize with LinuxDARwIn::Initialize(const char*, int)" << endl;
    }
    else
    {
        cerr << "[info] Entering safe position (" << safePosition << ")" << endl;
        Action::GetInstance()->m_Joint.SetEnableBody(true, true);
        MotionManager::GetInstance()->SetEnable(true);

        cm730.WriteByte(CM730::P_LED_PANNEL, 0x01|0x02|0x04, NULL); // 0x07
        Action::GetInstance()->Start(safePosition);
        
        while(Action::GetInstance()->IsRunning())
            usleep(8000);
            
        cerr << "[info] In safe position" << endl; 
    }
}

bool LinuxDARwIn::CheckFirmware()
{
	cerr << "[info] Checking Firmware..." << endl;
    int firm_ver[JointData::NUMBER_OF_JOINTS+1];
    bool firmwareOK = true;

    for(int i=JointData::ID_R_SHOULDER_PITCH; i<JointData::NUMBER_OF_JOINTS; i++)
        firm_ver[i] = -1;

    // read the firmware version from each servo
    for(int i=JointData::ID_R_SHOULDER_PITCH; i<JointData::NUMBER_OF_JOINTS; i++)
    {
        if(cm730.ReadByte(JointData::ID_HEAD_PAN, MX28::P_VERSION, &(firm_ver[i]), 0)  != CM730::SUCCESS)
        {
            cerr << "[error] Can't read firmware version from Dynamixel ID " << i << "!!" << endl;
            firm_ver[i] = -1;
        }
    }

    for(int i=JointData::ID_R_SHOULDER_PITCH; i<JointData::NUMBER_OF_JOINTS; i++)
    {
        if(firm_ver[i]>-1 && RX28::IsRX28(i))
        {
            cerr << "[info] Servo " << i << " is an AX-12" << endl;
            cerr << "[info] Detected firmware version " << firm_ver[i] << " on servo " << i << endl;
        }
        else if(firm_ver[i]>-1 && AX12::IsAX12(i))
        {
            cerr << "[info] Servo " << i << " is an RX-28" << endl;
            cerr << "[info] Detected firmware version " << firm_ver[i] << " on servo " << i << endl;
        }
        else if(firm_ver[i]>-1)
        {
            cerr << "[info] Servo " << i << " is an MX-28" << endl;
            cerr << "[info] Detected firmware version " << firm_ver[i] << " on servo " << i << endl;

            if(0<firm_ver[i] && firm_ver[i] < 27)
            {
#ifndef MX28_1024
                cerr << "[error] MX-28's firmware does not support 4096 resolution!!" << endl;
                cerr << "Upgrade MX-28's firmware to version 27(0x1B) or higher." << endl;
                cerr << "Or add '#define MX28_1024' to 'MX28.h' file and rebuild." << endl;
                firmwareOK = false;
#endif
            }
            else if(27 <= firm_ver[i])
            {
#ifdef MX28_1024
                cerr << "[error] MX-28's firmware does not support 1024 resolution!!" << endl;
                cerr << "Remove '#define MX28_1024' from 'MX28.h' file and rebuild." << endl;
                firmwareOK = false;
#endif
            }
            else
            {
                cerr << "[error] Unknown servo firmware" << endl;
                firmwareOK = false;
            }
        }
    }

	cerr << "[info] Finished checking firmware" << endl;

    return firmwareOK;
}
