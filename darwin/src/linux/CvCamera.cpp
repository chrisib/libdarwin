﻿/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * Alternative to LinuxCamera and CvCamera; this class uses V4L for the image acquisition
 * but stores recorded images in YUV, RGB, and HSV formats
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <opencv/cv.h>

#include "Camera.h"
#include "CvCamera.h"
#include "ImgProcess.h"
#include <cstring>
#include <cstdio>
#include <iostream>

#define CLEAR(x) memset (&(x), 0, sizeof (x))
#define EXPOSURE_ABSOLUTE

#define AUTO_EXPOSURE_OFF 0
#define AUTO_EXPOSURE_ON  1

using namespace Robot;
using namespace cv;
using namespace std;

CvCamera* CvCamera::uniqueInstance = new CvCamera();

CvCamera::CvCamera() :
        settings(CameraSettings()),
        camera_fd(-1),
        buffers(0),
        n_buffers(0)
{
    DEBUG_PRINT = false;
    //fbuffer = new FrameBuffer(Camera::WIDTH, Camera::HEIGHT);
    rgbFrame.create(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    yuvFrame.create(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);
    hsvFrame.create(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);

    RGB_ENABLE = false;
    HSV_ENABLE = false;

    exposureNotSupportedWarningPrinted = false;
}

CvCamera::~CvCamera()
{
    Release();
}

void CvCamera::ErrorExit(const char* s)
{
    fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
    exit(EXIT_FAILURE);
}

int CvCamera::Initialize(int deviceIndex)
{
    struct stat st;
    char devName[15] = {0, };

    sprintf(devName, "/dev/video%d", deviceIndex);

    fprintf(stderr,"Initializing camera on %s\n", devName);
    fflush(stderr);

    if (-1 == stat (devName, &st)) {
        fprintf (stderr, "Cannot identify '%s': %d, %s\n",
                 devName, errno, strerror (errno));
        //exit (EXIT_FAILURE);
        return false;
    }

    if (!S_ISCHR (st.st_mode)) {
        fprintf (stderr, "%s is no device\n", devName);
        //exit (EXIT_FAILURE);
        return false;
    }

    camera_fd = open(devName, O_RDWR | O_NONBLOCK, 0);
    if (-1 == camera_fd) {
        fprintf (stderr, "Cannot open '%s': %d, %s\n",
                 devName, errno, strerror (errno));
        //exit (EXIT_FAILURE);
        return false;
    }

    /* set format */
    fprintf(stderr,"Setting format...\n");
    fflush(stderr);
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;

    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(ioctl(camera_fd, VIDIOC_CROPCAP, &cropcap) == 0)
    {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        ioctl(camera_fd, VIDIOC_S_CROP, &crop);
    }
    else
    {
        /* Errors ignored. */
    }

    struct v4l2_format fmt;
    CLEAR(fmt);

    fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = Camera::WIDTH;
    fmt.fmt.pix.height      = Camera::HEIGHT;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

    if (-1 == ioctl (camera_fd, VIDIOC_S_FMT, &fmt))
    {
        fprintf(stderr,"VIDIOC_S_FMT\n");
        //ErrorExit ("VIDIOC_S_FMT");
        return false;
    }

    unsigned int min = fmt.fmt.pix.width * 2;
    if(fmt.fmt.pix.bytesperline < min)
        fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if(fmt.fmt.pix.sizeimage < min)
        fmt.fmt.pix.sizeimage = min;

    /* set frame rate */
    fprintf(stderr,"Setting frame rate...\n");
    fflush(stderr);

    struct v4l2_streamparm fps;
    CLEAR(fps);

    fps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(ioctl(camera_fd, VIDIOC_G_PARM, &fps) == -1)
    {
        fprintf(stderr,"VIDIOC_G_PARM\n");
        //ErrorExit("VIDIOC_G_PARM");
        return false;
    }

    fps.parm.capture.timeperframe.numerator = 1;
    fps.parm.capture.timeperframe.denominator = 30;
    if(ioctl(camera_fd, VIDIOC_S_PARM, &fps) == -1)
    {
        fprintf(stderr,"VIDIOC_S_PARM\n");
        //ErrorExit("VIDIOC_S_PARM");
        return false;
    }

    /* init mmap */
    fprintf(stderr,"Initializing mmap...\n");
    fflush(stderr);
    struct v4l2_requestbuffers req;
    CLEAR(req);

    req.count           = 4;
    req.type            = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory          = V4L2_MEMORY_MMAP;

    if (-1 == ioctl (camera_fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            fprintf (stderr, "%s does not support "
                     "memory mapping\n", devName);
            //exit (EXIT_FAILURE);
            return false;
        } else {
            fprintf(stderr,"VIDIOC_REQBUFS\n");
            //ErrorExit ("VIDIOC_REQBUFS");
            return false;
        }
    }

    if (req.count < 2) {
        fprintf (stderr, "Insufficient buffer memory on %s\n",
                 devName);
        //exit (EXIT_FAILURE);
        return false;
    }

    buffers = (buffer *)calloc(req.count, sizeof(*buffers));
    if (!buffers) {
        fprintf (stderr, "Out of memory\n");
        //exit (EXIT_FAILURE);
        return false;
    }

    for(n_buffers = 0; n_buffers < req.count; ++n_buffers)
    {
        struct v4l2_buffer buf;
        CLEAR(buf);

        buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory  = V4L2_MEMORY_MMAP;
        buf.index   = n_buffers;

        if (-1 == ioctl (camera_fd, VIDIOC_QUERYBUF, &buf))
        {
            fprintf(stderr,"VIDIOC_QUERYBUF\n");
            //ErrorExit ("VIDIOC_QUERYBUF");
            return false;
        }

        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start =
                mmap(NULL,
                     buf.length,
                     PROT_READ | PROT_WRITE,
                     MAP_SHARED,
                     camera_fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start)
        {
            fprintf(stderr,"mmap\n");
            //ErrorExit ("mmap");
            return false;
        }
    }

    /* queue buffers */
    fprintf(stderr,"Queuing buffers...\n");
    fflush(stderr);
    for(unsigned int i = 0; i < n_buffers; ++i)
    {
        struct v4l2_buffer buf;
        CLEAR(buf);

        buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory  = V4L2_MEMORY_MMAP;
        buf.index   = i;

        if (-1 == ioctl (camera_fd, VIDIOC_QBUF, &buf))
        {
            fprintf(stderr,"VIDIOC_QBUF\n");
            //ErrorExit ("VIDIOC_QBUF");
            return false;
        }
    }

    /* streaming on */
    fprintf(stderr,"Streaming on...\n");
    fflush(stderr);
    enum v4l2_buf_type type;
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == ioctl (camera_fd, VIDIOC_STREAMON, &type))
    {
        fprintf(stderr,"VIDIOC_STREAMON\n");
        //ErrorExit ("VIDIOC_STREAMON");
        return false;
    }

    /* get camera default setting */
    fprintf(stderr,"Loading default v4l2 settings...\n");
    fflush(stderr);

    settings.brightness = v4l2GetControl(V4L2_CID_BRIGHTNESS);
    settings.contrast   = v4l2GetControl(V4L2_CID_CONTRAST);
    settings.exposure   = v4l2GetControl(V4L2_CID_EXPOSURE_ABSOLUTE);
    settings.gain       = v4l2GetControl(V4L2_CID_GAIN);
    settings.hue        = v4l2GetControl(V4L2_CID_HUE);
    settings.saturation = v4l2GetControl(V4L2_CID_SATURATION);

    settings.autoExposure = true;   // CIB this is an awful hack, but it seems to work. if we set auto exposure to true initially it gets flipped automatically by something else later which is what we want
    settings.autoGain = false;
    settings.autoHue = false;
    settings.autoWhiteBalance = false;

    cout << "Setting initial camera settings..." << endl;
    CaptureFrame();
    SetCameraSettings(settings);

#if 0
    /* set camera auto off */
    fprintf(stderr,"Disabling auto...\n");
    fflush(stderr);
    v4l2SetControl(V4L2_CID_EXPOSURE_AUTO_PRIORITY, AUTO_EXPOSURE_OFF);
    v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, 0);
    v4l2SetControl(V4L2_CID_AUTOGAIN, 0);
    v4l2SetControl(V4L2_CID_HUE_AUTO, 0);
#endif

    return 1;
}

void CvCamera::Release()
{
    if(camera_fd != -1)
    {
        /* streaming off */
        enum v4l2_buf_type type;
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if(ioctl(camera_fd, VIDIOC_STREAMOFF, &type) == -1)
            ErrorExit("VIDIOC_STREAMOFF");

        /* unmap buffers */
        for(unsigned int i = 0; i < n_buffers; i++)
            if(munmap(buffers[i].start, buffers[i].length) == -1)
                ErrorExit("munmap");
        free(buffers);


        /* close device */
        close(camera_fd);
        camera_fd = -1;
    }
    preprocessors.clear();
}

void CvCamera::LoadINISettings(minIni* ini)
{
    int value = -2;
    CameraSettings newset = GetCameraSettings();

    // T/F value  (!! to force boolean)
    if((value = ini->geti("Camera", "AutoExposure", -2)) != -2)                 newset.autoExposure = value;
    if((value = ini->geti("Camera", "AutoGain", -2)) != -2)                     newset.autoGain = value;
    if((value = ini->geti("Camera", "AutoWhiteBalance", -2)) != -2)             newset.autoWhiteBalance = value;
    if((value = ini->geti("Camera", "AutoHue", -2)) != -2)                      newset.autoHue = value;
    if((value = ini->geti("Camera", "HFlip", -2)) != -2)                        newset.hFlip = !!value;
    if((value = ini->geti("Camera", "VFlip", -2)) != -2)                        newset.vFlip = !!value;

    if((value = ini->getd("Camera", "Brightness", -2)) != -2)                   newset.brightness = value;
    if((value = ini->getd("Camera", "Contrast", -2)) != -2)                     newset.contrast = value;
    if((value = ini->getd("Camera", "Exposure", -2)) != -2)                     newset.exposure = value;
    if((value = ini->getd("Camera", "Gain", -2)) != -2)                         newset.gain = value;
    if((value = ini->getd("Camera", "Hue", -2)) != -2)                          newset.hue = value;
    if((value = ini->getd("Camera", "Saturation", -2)) != -2)                   newset.saturation = value;

    SetCameraSettings(newset);

    // load the preprocessors
    string preprocs = ini->gets("Camera","Preprocessors");
    preprocessors.clear();
    if(preprocs.size()>0)
    {
        char buffer[512];
        sprintf(buffer, "%s", preprocs.c_str());
        char *ptr = strtok(buffer,",");
        CvPreprocessor *p;
        while(ptr!=NULL)
        {
            p = CvPreprocessor::MakePreprocessor(ptr);
            if(p!=NULL)
            {
                cerr << "Adding " << ptr << " preprocessor" << endl;
                preprocessors.push_back(p);
            }
            ptr = strtok(NULL,",");
        }
    }
}

void CvCamera::SaveINISettings(minIni* ini)
{
    ini->put("Camera", "AutoExposure",                     settings.autoExposure == V4L2_EXPOSURE_AUTO);
    ini->put("Camera", "AutoGain",                         settings.autoGain );
    ini->put("Camera", "AutoHue",                          settings.autoHue);
    ini->put("Camera", "AutoWhiteBalance",                 settings.autoWhiteBalance );

    ini->put("Camera", "Brightness",                       settings.brightness );
    ini->put("Camera", "Contrast",                         settings.contrast );
    ini->put("Camera", "Exposure",                         settings.exposure );
    ini->put("Camera", "Gain",                             settings.gain );
    ini->put("Camera", "Hue",                              settings.hue );
    ini->put("Camera", "Saturation",                       settings.saturation );

    ini->put("Camera", "HFlip",                            settings.hFlip );
    ini->put("Camera", "VFlip",                            settings.vFlip );

    string s = "";
    for(unsigned int i=0; i<preprocessors.size(); i++)
    {
        s = s+preprocessors[i]->Name();
        if(i<preprocessors.size()-1)
            s = s+",";
    }
    ini->put("Camera","Preprocessors",s);
}

const CameraSettings& CvCamera::GetCameraSettings()
{
    int v;

    v = v4l2GetControl(V4L2_CID_EXPOSURE_AUTO_PRIORITY);
    //cout << "Exposure auto priority = " << v << endl;
    if(v < 0)
    {
        // not supported by the driver
        if(!exposureNotSupportedWarningPrinted)
        {
            cout << "[WARNING] Auto-exposure not supported by driver" << endl;
            exposureNotSupportedWarningPrinted = true;
        }
    }
    else
    {
        /* possuble values for v: (pulled from v4l2-controls.h)
        V4L2_EXPOSURE_AUTO = 0,
        V4L2_EXPOSURE_MANUAL = 1,
        V4L2_EXPOSURE_SHUTTER_PRIORITY = 2,
        V4L2_EXPOSURE_APERTURE_PRIORITY = 3
        */

        if(v == AUTO_EXPOSURE_OFF)
        {
            settings.autoExposure = false;
        }
        else
        {
            settings.autoExposure = true;
        }
    }

    v = v4l2GetControl(V4L2_CID_AUTOGAIN);
    if(v == -1)
        settings.autoGain = false;
    else
        settings.autoGain = v;

    v = v4l2GetControl(V4L2_CID_HUE_AUTO);
    if(v == -1)
        settings.autoHue = false;
    else
        settings.autoHue = v;

    v = v4l2GetControl(V4L2_CID_AUTO_WHITE_BALANCE);
    if(v == -1)
        settings.autoWhiteBalance = false;
    else
        settings.autoHue = v;

    settings.brightness = v4l2GetControl(V4L2_CID_BRIGHTNESS);
    settings.contrast   = v4l2GetControl(V4L2_CID_CONTRAST);
    settings.exposure   = v4l2GetControl(V4L2_CID_EXPOSURE_ABSOLUTE);
    settings.gain       = v4l2GetControl(V4L2_CID_GAIN);
    settings.hue        = v4l2GetControl(V4L2_CID_HUE);
    settings.saturation = v4l2GetControl(V4L2_CID_SATURATION);

    return settings;
}

void CvCamera::SetCameraSettings(const CameraSettings &newset)
{
    settings = newset;

    if(newset.brightness != -1)
        v4l2SetControl(V4L2_CID_BRIGHTNESS, newset.brightness);
    else
        v4l2ResetControl(V4L2_CID_BRIGHTNESS);

    if(newset.contrast != -1)
        v4l2SetControl(V4L2_CID_CONTRAST, newset.contrast);
    else
        v4l2ResetControl(V4L2_CID_CONTRAST);

    if(newset.gain != -1)
        v4l2SetControl(V4L2_CID_GAIN, newset.gain);
    else
        v4l2ResetControl(V4L2_CID_GAIN);

    if(newset.hue != -1)
        v4l2SetControl(V4L2_CID_HUE, newset.hue);
    else
        v4l2ResetControl(V4L2_CID_HUE);

    if(newset.saturation != -1)
        v4l2SetControl(V4L2_CID_SATURATION, newset.saturation);
    else
        v4l2ResetControl(V4L2_CID_SATURATION);


    if(newset.autoExposure)
    {
        v4l2ResetControl(V4L2_CID_EXPOSURE_ABSOLUTE);
        v4l2SetControl(V4L2_CID_EXPOSURE_AUTO_PRIORITY, AUTO_EXPOSURE_ON);
    }
    else
    {
        v4l2SetControl(V4L2_CID_EXPOSURE_AUTO_PRIORITY, AUTO_EXPOSURE_OFF);
        if(newset.exposure != -1)
            v4l2SetControl(V4L2_CID_EXPOSURE_ABSOLUTE, newset.exposure);
        else
            v4l2ResetControl(V4L2_CID_EXPOSURE_ABSOLUTE);
    }

    v4l2SetControl(V4L2_CID_AUTOGAIN, newset.autoGain);
    v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, newset.autoWhiteBalance);
    v4l2SetControl(V4L2_CID_HUE_AUTO, newset.autoHue);

    GetCameraSettings();
}

int CvCamera::v4l2GetControl(int control)
{
    struct v4l2_queryctrl queryctrl;
    struct v4l2_control control_s;
    int err;

    queryctrl.id = control;
    if (ioctl(camera_fd, VIDIOC_QUERYCTRL, &queryctrl) < 0)
        return -1;
    control_s.id = control;
    if ((err = ioctl (camera_fd, VIDIOC_G_CTRL, &control_s)) < 0)
        return -1;

    return control_s.value;
}

int CvCamera::v4l2SetControl(int control, int value)
{
    struct v4l2_control control_s;
    struct v4l2_queryctrl queryctrl;
    int min, max, step, val_def;
    int err;

    queryctrl.id = control;
    if(ioctl(camera_fd, VIDIOC_QUERYCTRL, &queryctrl) < 0)
        return -1;
    if(queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
        return -1;

    min = queryctrl.minimum;
    max = queryctrl.maximum;
    step = queryctrl.step;
    val_def = queryctrl.default_value;
    if((value >= min) && (value <= max)) {
        control_s.id = control;
        control_s.value = value;
        if((err = ioctl (camera_fd, VIDIOC_S_CTRL, &control_s)) < 0)
            return -1;
    }
    return 0;
}

int CvCamera::v4l2ResetControl(int control)
{
    struct v4l2_control control_s;
    struct v4l2_queryctrl queryctrl;
    int val_def;
    int err;

    queryctrl.id = control;
    if (ioctl(camera_fd, VIDIOC_QUERYCTRL, &queryctrl) < 0)
        return -1;
    val_def = queryctrl.default_value;
    control_s.id = control;
    control_s.value = val_def;
    if ((err = ioctl (camera_fd, VIDIOC_S_CTRL, &control_s)) < 0)
        return -1;

    return 0;
}

int CvCamera::xioctl(int fh, int request, void *arg)
{
    int r;

    do {
            r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

int CvCamera::ReadFrame()
{
    struct v4l2_buffer buf;

    CLEAR(buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(camera_fd, VIDIOC_DQBUF, &buf)) {
        switch (errno) {
        case EAGAIN:
            return 0;

        case EIO:
            /* Could ignore EIO, see spec. */

            /* fall through */

        default:
            ErrorExit("VIDIOC_DQBUF");
        }
    }

    assert(buf.index < n_buffers);

    ProcessImage(buffers[buf.index].start, buf.bytesused);

    if (-1 == xioctl(camera_fd, VIDIOC_QBUF, &buf))
        ErrorExit("VIDIOC_QBUF");

    return 1;
}

void CvCamera::ProcessImage(const void *p, int size)
{
    //cerr << "Processing image: size=" << size << endl;
    //return;

    assert(size >= Camera::HEIGHT * Camera::WIDTH * 2); // 3-channel image recorded as YUYV, so 2 bytes per pixel effectively

    // copy the data from the buffer into the YUV frame
    uint8_t* srcData = (uint8_t *)p;
    uint8_t* cvData = (uint8_t *)yuvFrame.data;
    uint8_t y1, u, y2, v;
    for(int i=0; i<Camera::HEIGHT; i++)
    {
        for(int j=0; j<Camera::WIDTH/2; j++)
        {
            y1 = srcData[0];
            u = srcData[1];
            y2 = srcData[2];
            v = srcData[3];

            cvData[0] = y1;
            cvData[1] = u;
            cvData[2] = v;
            cvData[3] = y2;
            cvData[4] = u;
            cvData[5] = v;

            srcData += 4;
            cvData += 6;
        }
    }

    ApplyPreprocessors();
}

void CvCamera::ApplyPreprocessors()
{
    // apply flips if needed
    if(settings.hFlip && settings.vFlip)
        cv::flip(yuvFrame,yuvFrame,-1);
    else if(settings.hFlip)
        cv::flip(yuvFrame,yuvFrame,1);
    else if(settings.vFlip)
        cv::flip(yuvFrame,yuvFrame,0);

    // apply preprocessors to the yuv frame only (anything changed should be applied to hsv and rgb as well)
    for(vector<CvPreprocessor*>::iterator it = preprocessors.begin(); it<preprocessors.end(); it++)
    {
        (*it)->Process(yuvFrame);
    }

    // convert to RGB and HSV colour spaces
    // NOTE: we need to convert to RGB in order to go to HSV
    if(RGB_ENABLE || HSV_ENABLE)
        cv::cvtColor(yuvFrame,rgbFrame,CV_YCrCb2RGB);
    if(HSV_ENABLE)
        cv::cvtColor(rgbFrame,hsvFrame,CV_RGB2HSV);
}

bool CvCamera::CaptureFrame()
{
    if(DEBUG_PRINT == true)
    {
        struct timeval tv;
        static double beforeTime = 0;
        double currentTime;
        double durationTime;

        gettimeofday(&tv, NULL);
        currentTime = (double)tv.tv_sec*1000.0 + (double)tv.tv_usec/1000.0;
        durationTime = currentTime - beforeTime;
        fprintf(stderr, "\rCamera: %.1fmsec(%.1ffps)                    ", durationTime, 1000.0 / durationTime);
        beforeTime = currentTime;
    }

    for (;;) {
        fd_set fds;
        struct timeval tv;
        int r;

        FD_ZERO(&fds);
        FD_SET(camera_fd, &fds);

        /* Timeout. */
        tv.tv_sec = 5;  // TODO: configurable?
        tv.tv_usec = 0;

        r = select(camera_fd + 1, &fds, NULL, NULL, &tv);

        if (-1 == r) {
                if (EINTR == errno)
                        continue;
                //ErrorExit("select");
                fprintf(stderr,"select error %d\n",r);
                return false;
        }

        if (0 == r) {
                fprintf(stderr, "select timeout\n");
                //exit(EXIT_FAILURE);
                return false;
        }

        if (ReadFrame())
                break;
        /* EAGAIN - continue select loop. */
    }

    return true;
}

std::string CvCamera::GetControlName(int control)
{
    static string s;

    switch(control)
    {
    case V4L2_CID_AUTOBRIGHTNESS:
        return "Auto Brightness";
    case V4L2_CID_AUTOGAIN:
        return "Auto Gain";
    case V4L2_CID_AUTO_WHITE_BALANCE:
        return "Auto White Balance";
    case V4L2_CID_BACKLIGHT_COMPENSATION:
        return "Backligt Compensation";
    case V4L2_CID_BLUE_BALANCE:
        return "Blue Balance";
    case V4L2_CID_BRIGHTNESS:
        return "Brightness";
    case V4L2_CID_CHROMA_AGC:
        return "Chroma AGC";
    case V4L2_CID_CHROMA_GAIN:
        return "Chroma Gain";
    case V4L2_CID_COLOR_KILLER:
        return "Colour Killer";
    case V4L2_CID_CONTRAST:
        return "Contrast";
    case V4L2_CID_EXPOSURE_ABSOLUTE:
        return "Exposure (absolute)";
    case V4L2_CID_EXPOSURE:
        return "Exposure";
    case V4L2_CID_GAIN:
        return "Gain";
    case V4L2_CID_GAMMA:
        return "Gamma";
    case V4L2_CID_HFLIP:
        return "H-Flip";
    case V4L2_CID_HUE:
        return "Hue";
    case V4L2_CID_RED_BALANCE:
        return "Red Balance";
    case V4L2_CID_ROTATE:
        return "Rotate";
    case V4L2_CID_SATURATION:
        return "Saturation";
    case V4L2_CID_SHARPNESS:
        return "Sharpness";
    case V4L2_CID_VFLIP:
        return "V-Flip";
    case V4L2_CID_WHITE_BALANCE_TEMPERATURE:
        return "White Balance Temperature";
    default:
        s = "";
        stringstream ss(s);
        ss << control;
        return s;
    }
}
