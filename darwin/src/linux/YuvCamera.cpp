/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 * Andres ??
 *
 * Alternative to LinuxCamera; this class uses OpenCV for the image acquisition
 * and uses the YUV colour space instead of RGB
 ************************************************************************/

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <opencv/cv.h>

#include "Camera.h"
#include "YuvCamera.h"
#include "ImgProcess.h"
#include <cstring>
#include <cstdio>
#include <iostream>

#define CLEAR(x) memset (&(x), 0, sizeof (x))

using namespace Robot;
using namespace cv;
using namespace std;

YuvCamera* YuvCamera::uniqueInstance = new YuvCamera();

YuvCamera::YuvCamera() :
        settings(CameraSettings()),
        camera_fd(-1),
        buffers(0),
        n_buffers(0)
{
	DEBUG_PRINT = false;
    fbuffer = new FrameBuffer(Camera::WIDTH, Camera::HEIGHT);
    img.create(Camera::HEIGHT,Camera::WIDTH,CV_8UC3);

    // CIB
    // By default flip the image along both major axes
    flipV = true;
    flipH = true;
}

YuvCamera::~YuvCamera()
{
    /* streaming off */
    enum v4l2_buf_type type;
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(ioctl(camera_fd, VIDIOC_STREAMOFF, &type) == -1)
        ErrorExit("VIDIOC_STREAMOFF");

    /* unmap buffers */
    for(unsigned int i = 0; i < n_buffers; i++)
        if(munmap(buffers[i].start, buffers[i].length) == -1)
            ErrorExit("munmap");
    free(buffers);

    /* close device */
    close(camera_fd);
    camera_fd = -1;

    preprocessors.clear();
}

void YuvCamera::ErrorExit(const char* s)
{
    fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
    exit(EXIT_FAILURE);
}

int YuvCamera::Initialize(int deviceIndex)
{
    struct stat st;
    char devName[15] = {0, };

    sprintf(devName, "/dev/video%d", deviceIndex);

    fprintf(stderr,"Initializing camera on %s\n", devName);
    fflush(stderr);

    if (-1 == stat (devName, &st)) {
        fprintf (stderr, "Cannot identify '%s': %d, %s\n",
                 devName, errno, strerror (errno));
        exit (EXIT_FAILURE);
    }

    if (!S_ISCHR (st.st_mode)) {
        fprintf (stderr, "%s is no device\n", devName);
        exit (EXIT_FAILURE);
    }

    camera_fd = open(devName, O_RDWR | O_NONBLOCK, 0);
    if (-1 == camera_fd) {
        fprintf (stderr, "Cannot open '%s': %d, %s\n",
                 devName, errno, strerror (errno));
        exit (EXIT_FAILURE);
    }

    /* set format */
    fprintf(stderr,"Setting format...\n");
    fflush(stderr);
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;

    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(ioctl(camera_fd, VIDIOC_CROPCAP, &cropcap) == 0)
    {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        ioctl(camera_fd, VIDIOC_S_CROP, &crop);
    }
    else
    {
        /* Errors ignored. */
    }

    struct v4l2_format fmt;
    CLEAR(fmt);

    fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = Camera::WIDTH;
    fmt.fmt.pix.height      = Camera::HEIGHT;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

    if (-1 == ioctl (camera_fd, VIDIOC_S_FMT, &fmt))
        ErrorExit ("VIDIOC_S_FMT");

    unsigned int min = fmt.fmt.pix.width * 2;
    if(fmt.fmt.pix.bytesperline < min)
        fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if(fmt.fmt.pix.sizeimage < min)
        fmt.fmt.pix.sizeimage = min;

    /* set frame rate */
    fprintf(stderr,"Setting frame rate...\n");
    fflush(stderr);

    struct v4l2_streamparm fps;
    CLEAR(fps);

    fps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if(ioctl(camera_fd, VIDIOC_G_PARM, &fps) == -1)
        ErrorExit("VIDIOC_G_PARM");

    fps.parm.capture.timeperframe.numerator = 1;
    fps.parm.capture.timeperframe.denominator = 30;
    if(ioctl(camera_fd, VIDIOC_S_PARM, &fps) == -1)
        ErrorExit("VIDIOC_S_PARM");

    /* init mmap */
    fprintf(stderr,"Initializing mmap...\n");
    fflush(stderr);
    struct v4l2_requestbuffers req;
    CLEAR(req);

    req.count           = 4;
    req.type            = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory          = V4L2_MEMORY_MMAP;

    if (-1 == ioctl (camera_fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            fprintf (stderr, "%s does not support "
                     "memory mapping\n", devName);
            exit (EXIT_FAILURE);
        } else {
            ErrorExit ("VIDIOC_REQBUFS");
        }
    }

    if (req.count < 2) {
        fprintf (stderr, "Insufficient buffer memory on %s\n",
                 devName);
        exit (EXIT_FAILURE);
    }

    buffers = (buffer *)calloc(req.count, sizeof(*buffers));
    if (!buffers) {
        fprintf (stderr, "Out of memory\n");
        exit (EXIT_FAILURE);
    }

    for(n_buffers = 0; n_buffers < req.count; ++n_buffers)
    {
        struct v4l2_buffer buf;
        CLEAR(buf);

        buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory  = V4L2_MEMORY_MMAP;
        buf.index   = n_buffers;

        if (-1 == ioctl (camera_fd, VIDIOC_QUERYBUF, &buf))
            ErrorExit ("VIDIOC_QUERYBUF");

        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start =
                mmap(NULL,
                     buf.length,
                     PROT_READ | PROT_WRITE,
                     MAP_SHARED,
                     camera_fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start)
                ErrorExit ("mmap");
    }

    /* queue buffers */
    fprintf(stderr,"Queuing buffers...\n");
    fflush(stderr);
    for(unsigned int i = 0; i < n_buffers; ++i)
    {
        struct v4l2_buffer buf;
        CLEAR(buf);

        buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory  = V4L2_MEMORY_MMAP;
        buf.index   = i;

        if (-1 == ioctl (camera_fd, VIDIOC_QBUF, &buf))
            ErrorExit ("VIDIOC_QBUF");
    }

    /* streaming on */
    fprintf(stderr,"Streaming on...\n");
    fflush(stderr);
    enum v4l2_buf_type type;
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == ioctl (camera_fd, VIDIOC_STREAMON, &type))
        ErrorExit ("VIDIOC_STREAMON");

    /* get camera default setting */
    fprintf(stderr,"Loading default v4l2 settings...\n");
    fflush(stderr);
    settings.brightness = v4l2GetControl(V4L2_CID_BRIGHTNESS);
    settings.contrast   = v4l2GetControl(V4L2_CID_CONTRAST);
    settings.exposure   = v4l2GetControl(V4L2_CID_EXPOSURE_ABSOLUTE);
    settings.gain       = v4l2GetControl(V4L2_CID_GAIN);
    settings.hue        = v4l2GetControl(V4L2_CID_HUE);
    settings.saturation = v4l2GetControl(V4L2_CID_SATURATION);

    /* set camera auto off */
    fprintf(stderr,"Disabling auto...\n");
    fflush(stderr);
    v4l2SetControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL);
    v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, 0);
    v4l2SetControl(V4L2_CID_AUTOGAIN, 0);
    v4l2SetControl(V4L2_CID_HUE_AUTO, 0);

    return 1;
}

void YuvCamera::LoadINISettings(minIni* ini)
{
    int value = -2;
    CameraSettings newset = GetCameraSettings();

    if((value = ini->geti("Camera", "Brightness", -2)) != -2)   newset.brightness = value;
    if((value = ini->geti("Camera", "Contrast", -2)) != -2)     newset.contrast = value;
    if((value = ini->geti("Camera", "Saturation", -2)) != -2)   newset.saturation = value;
    if((value = ini->geti("Camera", "Gain", -2)) != -2)         newset.gain = value;
    if((value = ini->geti("Camera", "Exposure", -2)) != -2)     newset.exposure = value;

    SetCameraSettings(newset);

    if((value = ini->geti("Camera", "AutoWhiteBalance", -2)) != -2) v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, value);

    // CIB
    // optionally disable flipping the image
    flipV = ini->geti("Camera","FlipV", 1);
    flipH = ini->geti("Camera","FlipH", 1);

    // load the preprocessors
    string preprocs = ini->gets("Camera","Preprocessors");
    preprocessors.clear();
    if(preprocs.size()>0)
    {
        char buffer[512];
        sprintf(buffer, "%s", preprocs.c_str());
        char *ptr = strtok(buffer,",");
        CvPreprocessor *p;
        while(ptr!=NULL)
        {
            p = CvPreprocessor::MakePreprocessor(ptr);
            if(p!=NULL)
            {
                cerr << "Adding " << ptr << " preprocessor" << endl;
                preprocessors.push_back(p);
            }
            ptr = strtok(NULL,",");
        }
    }
}

void YuvCamera::SaveINISettings(minIni* ini)
{
    ini->put("Camera", "Brightness",settings.brightness);
    ini->put("Camera", "Contrast",  settings.contrast);
    ini->put("Camera", "Saturation",settings.saturation);
    ini->put("Camera", "Gain",      settings.gain);
    ini->put("Camera", "Exposure",  settings.exposure);

    // CIB
    ini->put("Camera", "AutoWhiteBalance", v4l2GetControl(V4L2_CID_AUTO_WHITE_BALANCE));

    // CIB
    ini->put("Camera", "FlipV", flipV);
    ini->put("Camera", "FlipH", flipH);

    string s = "";
    for(unsigned int i=0; i<preprocessors.size(); i++)
    {
        s = s+preprocessors[i]->Name();
        if(i<preprocessors.size()-1)
            s = s+",";
    }
    ini->put("Camera","Preprocessors",s);
}

const CameraSettings& YuvCamera::GetCameraSettings()
{
    int v;

    v = v4l2GetControl(V4L2_CID_EXPOSURE_AUTO);
    if(v == V4L2_EXPOSURE_AUTO)
        settings.autoExposure = true;
    else
        settings.autoExposure = false;

    v = v4l2GetControl(V4L2_CID_AUTOGAIN);
    if(v == -1)
        settings.autoGain = false;
    else
        settings.autoGain = v;

    v = v4l2GetControl(V4L2_CID_HUE_AUTO);
    if(v == -1)
        settings.autoHue = false;
    else
        settings.autoHue = v;

    v = v4l2GetControl(V4L2_CID_AUTO_WHITE_BALANCE);
    if(v == -1)
        settings.autoWhiteBalance = false;
    else
        settings.autoHue = v;

    settings.brightness = v4l2GetControl(V4L2_CID_BRIGHTNESS);
    settings.contrast   = v4l2GetControl(V4L2_CID_CONTRAST);
    settings.exposure   = v4l2GetControl(V4L2_CID_EXPOSURE_ABSOLUTE);
    settings.gain       = v4l2GetControl(V4L2_CID_GAIN);
    settings.hue        = v4l2GetControl(V4L2_CID_HUE);
    settings.saturation = v4l2GetControl(V4L2_CID_SATURATION);

    return settings;
}

void YuvCamera::SetCameraSettings(const CameraSettings &newset)
{
    settings = newset;

    if(newset.brightness != -1)
        v4l2SetControl(V4L2_CID_BRIGHTNESS, newset.brightness);
    else
        v4l2ResetControl(V4L2_CID_BRIGHTNESS);

    if(newset.contrast != -1)
        v4l2SetControl(V4L2_CID_CONTRAST, newset.contrast);
    else
        v4l2ResetControl(V4L2_CID_CONTRAST);

    if(newset.exposure != -1)
        v4l2SetControl(V4L2_CID_EXPOSURE_ABSOLUTE, newset.exposure);
    else
        v4l2ResetControl(V4L2_CID_EXPOSURE_ABSOLUTE);

    if(newset.gain != -1)
        v4l2SetControl(V4L2_CID_GAIN, newset.gain);
    else
        v4l2ResetControl(V4L2_CID_GAIN);

    if(newset.hue != -1)
        v4l2SetControl(V4L2_CID_HUE, newset.hue);
    else
        v4l2ResetControl(V4L2_CID_HUE);

    if(newset.saturation != -1)
        v4l2SetControl(V4L2_CID_SATURATION, newset.saturation);
    else
        v4l2ResetControl(V4L2_CID_SATURATION);


    if(newset.autoExposure)
        v4l2SetControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_AUTO);
    else
        v4l2SetControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL);
    v4l2SetControl(V4L2_CID_AUTOGAIN, newset.autoGain);
    v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, newset.autoWhiteBalance);
    v4l2SetControl(V4L2_CID_HUE_AUTO, newset.autoHue);

    GetCameraSettings();
}

int YuvCamera::v4l2GetControl(int control)
{
    struct v4l2_queryctrl queryctrl;
    struct v4l2_control control_s;
    int err;

    queryctrl.id = control;
    if (ioctl(camera_fd, VIDIOC_QUERYCTRL, &queryctrl) < 0)
        return -1;
    control_s.id = control;
    if ((err = ioctl (camera_fd, VIDIOC_G_CTRL, &control_s)) < 0)
        return -1;

    return control_s.value;
}

int YuvCamera::v4l2SetControl(int control, int value)
{
    struct v4l2_control control_s;
    struct v4l2_queryctrl queryctrl;
    int min, max, step, val_def;
    int err;

    queryctrl.id = control;
    if(ioctl(camera_fd, VIDIOC_QUERYCTRL, &queryctrl) < 0)
        return -1;
    if(queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
        return -1;

    min = queryctrl.minimum;
    max = queryctrl.maximum;
    step = queryctrl.step;
    val_def = queryctrl.default_value;
    if((value >= min) && (value <= max)) {
        control_s.id = control;
        control_s.value = value;
        if((err = ioctl (camera_fd, VIDIOC_S_CTRL, &control_s)) < 0)
            return -1;
    }
    return 0;
}

int YuvCamera::v4l2ResetControl(int control)
{
    struct v4l2_control control_s;
    struct v4l2_queryctrl queryctrl;
    int val_def;
    int err;

    queryctrl.id = control;
    if (ioctl(camera_fd, VIDIOC_QUERYCTRL, &queryctrl) < 0)
        return -1;
    val_def = queryctrl.default_value;
    control_s.id = control;
    control_s.value = val_def;
    if ((err = ioctl (camera_fd, VIDIOC_S_CTRL, &control_s)) < 0)
        return -1;

    return 0;
}

int YuvCamera::ReadFrame()
{
    struct v4l2_buffer buf;

    CLEAR (buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == ioctl (camera_fd, VIDIOC_DQBUF, &buf)) {
        switch (errno) {
        case EAGAIN:
            return 0;

        case EIO:
            /* Could ignore EIO, see spec. */

            /* fall through */

        default:
            exit (EXIT_FAILURE);
        }
    }

    assert (buf.index < n_buffers);

    //process_image (buffers[buf.index].start);
    for(int i = 0; i < fbuffer->m_YUVFrame->m_ImageSize; i++)
        fbuffer->m_YUVFrame->m_ImageData[i] = ((unsigned char*)buffers[buf.index].start)[i];

    // CIB
    // optionally disable flipping the image
    if(flipH)
        ImgProcess::HFlipYUV(fbuffer->m_YUVFrame);
    if(flipV)
        ImgProcess::VFlipYUV(fbuffer->m_YUVFrame);

    int i, j;
    int rows=fbuffer->m_YUVFrame->m_Height;
    int rows_2 =rows/2;
    int cols=fbuffer->m_YUVFrame->m_Width;
    uint8_t* p = (uint8_t *)fbuffer->m_YUVFrame->m_ImageData;
    uint8_t* data = (uint8_t *)img.data;
    for ( i=0; i<cols; i++ )
    {
        for ( j=0; j<rows_2; j++ )
        {
            data[0]           = *p++; // y1
            data[1] = data[4] = *p++; // u
            data[3]           = *p++; // y2
            data[2] = data[5] = *p++; // v
            data += 6;
        }
    }


    if (-1 == ioctl (camera_fd, VIDIOC_QBUF, &buf))
        ErrorExit ("VIDIOC_QBUF");

    return 1;
}

void YuvCamera::CaptureFrame()
{
	if(DEBUG_PRINT == true)
	{
		struct timeval tv;
		static double beforeTime = 0;
		double currentTime;
		double durationTime;

		gettimeofday(&tv, NULL);
		currentTime = (double)tv.tv_sec*1000.0 + (double)tv.tv_usec/1000.0;
		durationTime = currentTime - beforeTime;
		fprintf(stderr, "\rCamera: %.1fmsec(%.1ffps)                    ", durationTime, 1000.0 / durationTime);
		beforeTime = currentTime;
	}

    for (;;) {
        fd_set fds;
        struct timeval tv;
        int r;

        FD_ZERO (&fds);
        FD_SET (camera_fd, &fds);

        /* Timeout. */
        tv.tv_sec = 2;
        tv.tv_usec = 0;

        r = select (camera_fd + 1, &fds, NULL, NULL, &tv);

        if (-1 == r) {
            if (EINTR == errno)
                continue;

            exit (EXIT_FAILURE);
        }

        if (0 == r) {
            fprintf (stderr, "select timeout (this frame will be all-black)\n");
//            exit (EXIT_FAILURE);
            img = Mat::zeros(Camera::HEIGHT, Camera::WIDTH, CV_8UC3);
            return;
//            break;
        }

        if (ReadFrame())
            break;

        /* EAGAIN - continue select loop. */
    }

    // apply preprocessors
    for(vector<CvPreprocessor*>::iterator it = preprocessors.begin(); it<preprocessors.end(); it++)
    {
        (*it)->Process(img);
    }
}

