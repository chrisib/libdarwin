#include <darwin/framework/Application.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <cstring>
#include <darwin/framework/Voice.h>
#include <CvCamera.h>
#include <MotionManager.h>
#include <Head.h>
#include <Action.h>
#include <Walking.h>
#include <LinuxMotionTimer.h>
#include <LinuxDARwIn.h>
#include <stdexcept>

using namespace Robot;
using namespace std;

const string Application::SHOW_VIDEO_FLAG = "--show-video";
const string Application::VISION_TEST_FLAG = "--vision-test";
const string Application::CONFIG_FILE_FLAG = "--config";

Application::Application()
{
    // register default arguments
    this->RegisterSwitch(SHOW_VIDEO_FLAG,&this->showVideo,false, "Show the video frames in a GUI");
    this->RegisterSwitch(VISION_TEST_FLAG,&this->visionOnly,false, "Only process video; do nothing else");
    this->RegisterArgument(CONFIG_FILE_FLAG,&this->iniFilePath,"config.ini","Set the path to the configuration file");

    ini = NULL;
}

Application::~Application()
{
    arguments.clear();
    if(ini!=NULL)
        delete ini;
}

bool Application::Initialize(const char* iniFilePath, const char* motionFilePath, int safePosition)
{	
    if(this->iniFilePath.length() == 0 && iniFilePath != NULL && strlen(iniFilePath) > 0)
        this->iniFilePath = string(iniFilePath);

    if(!InitIni(this->iniFilePath.c_str()))
        return false;

    if(!this->visionOnly)
    {

        if(!InitCM730(motionFilePath, safePosition))
            return false;
    }

    if(!InitCamera(this->iniFilePath.c_str()))
        return false;

    cout << "[init] Reading configuration file..." << endl;
    LoadIniSettings();
    cout << "[init] done" << endl;

    StartVisionThread();

    return true;
}

bool Application::InitIni(const char* iniFilePath)
{
    cout << "[init] Initializing ini..." << endl;
    ini=new minIni(string(iniFilePath));
    cout << "[init] done" << endl;

    return true;
}

bool Application::InitCM730(const char* motionFilePath, int safePosition)
{
    cout << "[init] Initializing CM730..." << endl;
    bool result = LinuxDARwIn::Initialize(motionFilePath, safePosition);

    if(result)
        cout << "[init] done" << endl;
    else
        cout << "[init] ERROR: Failed to initialize CM730" << endl;

    // change the LED colour to indicate we're good to go
    //LinuxDARwIn::cm730.SetForeheadColour(255,255,0);

    return result;
}

bool Application::InitCamera(const char *iniFilePath)
{
    if(ini==NULL)
    {
        cout << "[init] Initializing ini..." << endl;
        ini=new minIni(string(iniFilePath));
        cout << "[init] done" << endl;
    }

    int cameraDevice = ini->geti("Camera","Device",0);
    cout << "[init] Initializing Camera on /dev/video" << cameraDevice << "..." << endl;
    bool result = CvCamera::GetInstance()->Initialize(cameraDevice);

    if(result)
    {
        CvCamera::GetInstance()->LoadINISettings(ini);
        cout << "[init] done" << endl;
    }
    else
    {
        cout << "[init] ERROR: failed to initialize CvCamera" << endl;
    }

    return result;
}

void Application::StartVisionThread()
{
    cout << "[info] Starting video thread..." << endl;
    pthread_create(&videoThreadId, NULL, Application::VideoThread, this);
    cout << "[info] done" << endl << endl;
}

/*bool Application::InitCM730(const char* device)
{
    cout << "[info] Initializing CM730 on " << device << "..." << endl;
    linux_cm730 = LinuxCM730(device);
    cm730 = CM730(&linux_cm730);
    cout << "[info] done" << endl;

    cout << "[info] Initializing motion manager..." << endl;
    if(MotionManager::GetInstance()->Initialize(&cm730) == false)
    {
        linux_cm730.SetPortName(CM730_SECONDARY_DEVICE);
        if(MotionManager::GetInstance()->Initialize(&cm730) == false)
        {
            cerr << "[ERROR] Failed to initialize Motion Manager!" << endl;
            return false;
        }
    }
    cout << "[info] done" << endl;

    return true;
}

bool Application::InitModules(const char *motionFilePath)
{
    cout << "[info] Setting up motion manager..." << endl;
    MotionManager::GetInstance()->SetEnable(true);
    cout << "[info] done" << endl;

	cout << "[info] Inserting motion modules..." << endl;
	MotionManager::GetInstance()->AddModule(Head::GetInstance());
	MotionManager::GetInstance()->AddModule(Walking::GetInstance());
	MotionManager::GetInstance()->AddModule(Action::GetInstance());
	cout << "[info] done" << endl;
	
	cout << "[info] Loading motion file (" << motionFilePath << ")..." << endl;
	Action::GetInstance()->LoadFile(motionFilePath);
	cout << "[info] done" << endl;

    return true;
}

void Application::EnterSafePosition()
{
	cout << "[info] Entering safe position (" << safePosition << ")..." << endl;
	Action::GetInstance()->m_Joint.SetEnableBody(true,true);
	Action::GetInstance()->Start(safePosition);
	Action::GetInstance()->Finish();
	cout << "[info] done" << endl;
}*/

void Application::Execute()
{
    for(;;)
    {
        Process();
    }
}

void Application::ParseArguments(int argc, char **argv)
{
    CommandLineArgument arg;
    string flag;

    for(int i=1; i<argc; i++)
    {
        if(!strcmp(argv[i],"--help") || !strcmp(argv[i],"-h"))
        {
            // got the help flag; print the help and exit
            PrintHelp();
            exit(0);
        }
        else
        {
            flag = string(argv[i]);
            // check to see if the current argument is allowed
            try
            {
                arg = arguments.at(flag);

                // int, string, double flags require an argument
                // booleans are toggled by the presence of the flag
                if(arg.type != CommandLineArgument::BOOLEAN)
                {
                    i++;
                    if(i >= argc)
                    {
                        PrintHelp();
                        cerr << "Insufficient arguments for flag " << flag << endl;
                        exit(-1);
                    }
                    else
                    {
                        switch(arg.type)
                        {
                        case CommandLineArgument::INTEGER:
                            *((int*)(arg.value)) = atoi(argv[i]);
                            break;
                        case CommandLineArgument::DOUBLE:
                            *((double*)(arg.value)) = (double)atof(argv[i]);
                            break;
                        case CommandLineArgument::STRING:
                            *((string*)(arg.value)) = string(argv[i]);
                            break;
                        default:
                            PrintHelp();
                            cerr << "Unknown argument type for flag " << flag << endl;
                            exit(-1);
                        }
                    }
                }
                else
                {
                    *((bool*)(arg.value)) = !*((bool*)(arg.value));
                }
            }
            catch(out_of_range& err)
            {
                PrintHelp();
                cerr << "Unknown argument: " << flag << endl;
                exit(-1);
            }
        }
    }

    // do a final sanity check for the default arguments
    if(visionOnly)
        showVideo = true;
}

void Application::PrintHelp()
{
    cout << endl << "Command-line options:" << endl;
    cout << "    -h, --help\tShow this message" << endl;

    for(map<string, CommandLineArgument>::iterator it = arguments.begin(); it!=arguments.end(); it++)
    {
        switch(it->second.type)
        {
        case CommandLineArgument::INTEGER:
            cout << "    " << it->second.flag << " INT\t";
            break;
        case CommandLineArgument::DOUBLE:
            cout << "    " << it->second.flag << " FLOAT\t";
            break;
        case CommandLineArgument::STRING:
            cout << "    " << it->second.flag << " STR\t";
            break;
        case CommandLineArgument::BOOLEAN:
        default:
            cout << "    " << it->second.flag << "\t";
            break;

        }
        cout << it->second.description << endl;
    }
    cout << endl;
}

void Application::RegisterArgument(std::string flag, int* var, int defaultValue, std::string description)
{
    arguments[flag].value = var;
    *((int*)(arguments[flag].value)) = defaultValue;
    arguments[flag].flag = flag;
    arguments[flag].description=description;
    arguments[flag].type = CommandLineArgument::INTEGER;
}

void Application::RegisterArgument(std::string flag, double* var, double defaultValue, std::string description)
{
    arguments[flag].value = var;
    *((double*)(arguments[flag].value)) = defaultValue;
    arguments[flag].flag = flag;
    arguments[flag].description=description;
    arguments[flag].type = CommandLineArgument::DOUBLE;
}

void Application::RegisterArgument(std::string flag, std::string* var, std::string defaultValue, std::string description)
{
    arguments[flag].value = var;
    *((string*)(arguments[flag].value)) = defaultValue;
    arguments[flag].flag = flag;
    arguments[flag].description=description;
    arguments[flag].type = CommandLineArgument::STRING;
}

void Application::RegisterSwitch(std::string flag, bool* var, bool defaultValue, std::string description)
{
    arguments[flag].value = var;
    *((bool*)(arguments[flag].value)) = defaultValue;
    arguments[flag].flag = flag;
    arguments[flag].description=description;
    arguments[flag].type = CommandLineArgument::BOOLEAN;
}

void Application::UnregisterArgument(string flag)
{
    arguments.erase(flag);
}

void *Application::VideoThread(void* arg)
{
    Application *app = (Application*)arg;
    for(;;)
    {
        app->HandleVideo();
        pthread_yield();
    }
    return NULL;
}

void Application::WaitButton(int button, void (*function)(void))
{
    int lastButton = 0;

    //do
    //{
    //	usleep( 50 );
    //	lastButton = MotionStatus::BUTTON;
    //} while( lastButton != button );

    while(lastButton != button)
    {
        lastButton = GetButton();
        if(function!=NULL)
            function();
    }
}

int Application::GetButton()
{
    int lastButton = 0;
    do
    {
        usleep(50);
        lastButton = MotionStatus::BUTTON;
    } while( lastButton == 0 );

    return lastButton;
}
