/*
 *   MotionManager.cpp
 *
 *   Author: ROBOTIS
 *
 */

#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include "FSR.h"
#include "MX28.h"
#include "RX28.h"
#include "AX12.h"
#include "MotionManager.h"
#include "JointData.h"
#include "LeftArm.h"
#include "RightArm.h"
#include <iostream>
#include <stdarg.h>
#include <limits.h>

using namespace std;
using namespace Robot;

MotionManager* MotionManager::m_UniqueInstance = new MotionManager();

MotionManager::MotionManager() :
        m_CM730(0),
        m_ProcessEnable(false),
        m_Enabled(false),
        m_IsRunning(false),
        m_IsThreadRunning(false),
        m_IsLogging(false),
        DEBUG_PRINT(false)
{
    for(int i = 0; i < JointData::NUMBER_OF_JOINTS; i++)
        m_Offset[i] = 0;
}

MotionManager::~MotionManager()
{
}

bool MotionManager::Initialize(CM730 *cm730)
{
    int value, error;

    m_CM730 = cm730;
    m_Enabled = false;
    m_ProcessEnable = true;

    if(m_CM730->Connect() == false)
    {
        if(DEBUG_PRINT == true)
            fprintf(stderr, "Fail to connect CM-730\n");
        return false;
    }

    for(int id=JointData::ID_R_SHOULDER_PITCH; id<JointData::NUMBER_OF_JOINTS; id++)
    {
        if(DEBUG_PRINT == true)
            fprintf(stderr, "ID:%d initializing...", id);

        if(m_CM730->ReadWord(id, MX28::P_PRESENT_POSITION_L, &value, &error) == CM730::SUCCESS)
        {
            // convert the raw position data into the virtual position for non-MX28 motors
            if(AX12::IsAX12(id))
                value = AX12::Real2VirtualValue(value,id);
            else if(RX28::IsRX28(id))
                value = RX28::Real2VirtualValue(value,id);

            MotionStatus::m_CurrentJoints.SetValue(id, value);
            MotionStatus::m_CurrentJoints.SetEnable(id, true);

            if(DEBUG_PRINT == true)
                fprintf(stderr, "[%d] Success\n", value);
        }
        else
        {
            MotionStatus::m_CurrentJoints.SetEnable(id, false);

            if(DEBUG_PRINT == true)
                fprintf(stderr, " Fail\n");
        }
    }

    int err = CM730::RX_TIMEOUT;
    m_CM730->Ping(FSR::ID_R_FSR,&err);
    if(err == CM730::SUCCESS)
        m_FsrPresent = true;
    else
    {
        fprintf(stderr,"No FSR sensors detected. Will be disabled\n");
        m_FsrPresent = false;
    }

    m_CalibrationStatus = 0;
    m_FBGyroCenter = 512;
    m_RLGyroCenter = 512;
    m_ZGyroCenter = 512;

    return true;
}

bool MotionManager::Reinitialize()
{
    m_ProcessEnable = false;

    m_CM730->DXLPowerOn();

    int value, error;
    for(int id=JointData::ID_R_SHOULDER_PITCH; id<JointData::NUMBER_OF_JOINTS; id++)
    {
        if(DEBUG_PRINT == true)
            fprintf(stderr, "ID:%d initializing...", id);

        if(m_CM730->ReadWord(id, MX28::P_PRESENT_POSITION_L, &value, &error) == CM730::SUCCESS)
        {
            MotionStatus::m_CurrentJoints.SetValue(id, value);
            MotionStatus::m_CurrentJoints.SetEnable(id, true);

            if(DEBUG_PRINT == true)
                fprintf(stderr, "[%d] Success\n", value);
        }
        else
        {
            MotionStatus::m_CurrentJoints.SetEnable(id, false);

            if(DEBUG_PRINT == true)
                fprintf(stderr, " Fail\n");
        }
    }

    m_ProcessEnable = true;
    return true;
}

void MotionManager::StartLogging()
{
    char szFile[32] = {0,};

    int count = 0;
    while(1)
    {
        sprintf(szFile, "Log%d.csv", count);
        if(0 != access(szFile, F_OK))
            break;
        count++;
        if(count > 256) return;
    }

    m_LogFileStream.open(szFile, std::ios::out);
    printf("Creating log file %s\n",szFile);
    for(int id = 1; id < JointData::NUMBER_OF_JOINTS; id++)
        m_LogFileStream << "ID_" << id << "_GP,";//ID_" << id << "_PP,";
    m_LogFileStream << "GyroFB,GyroRL,GyroZ,AccelFB,AccelRL,AccelZ,L_FSR_X,L_FSR_Y,R_FSR_X,R_FSR_Y" << std::endl;

    m_IsLogging = true;
}

void MotionManager::StopLogging()
{
    m_IsLogging = false;
    m_LogFileStream.close();
}

void MotionManager::LoadINISettings(minIni* ini)
{
    LoadINISettings(ini, OFFSET_SECTION);
}
void MotionManager::LoadINISettings(minIni* ini, const std::string &section)
{
    int ivalue = INVALID_VALUE;

    for(int i = 1; i < JointData::NUMBER_OF_JOINTS; i++)
    {
        char key[10];
        sprintf(key, "ID_%.2d", i);
        if((ivalue = ini->geti(section, key, INVALID_VALUE)) != INVALID_VALUE)  m_Offset[i] = ivalue;
    }
}
void MotionManager::SaveINISettings(minIni* ini)
{
    SaveINISettings(ini, OFFSET_SECTION);
}
void MotionManager::SaveINISettings(minIni* ini, const std::string &section)
{
    for(int i = 1; i < JointData::NUMBER_OF_JOINTS; i++)
    {
        char key[10];
        sprintf(key, "ID_%.2d", i);
        ini->put(section, key, m_Offset[i]);
    }
}

#define GYRO_WINDOW_SIZE    100
#define ACCEL_WINDOW_SIZE   30
#define MARGIN_OF_SD        2.0
void MotionManager::Process()
{
    if(m_ProcessEnable == false || m_IsRunning == true)
        return;

    m_IsRunning = true;

    // calibrate gyro sensor
    if(m_CalibrationStatus == 0 || m_CalibrationStatus == -1)
    {
        static int fb_gyro_array[GYRO_WINDOW_SIZE] = {512,};
        static int rl_gyro_array[GYRO_WINDOW_SIZE] = {512,};
        static int buf_idx = 0;

        if(buf_idx < GYRO_WINDOW_SIZE)
        {
            if(m_CM730->m_BulkReadData[CM730::ID_CM].error == 0)
            {
                fb_gyro_array[buf_idx] = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_Y_L);
                rl_gyro_array[buf_idx] = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_X_L);
                buf_idx++;
            }
        }
        else
        {
            double fb_sum = 0.0, rl_sum = 0.0;
            double fb_sd = 0.0, rl_sd = 0.0;
            double fb_diff, rl_diff;
            double fb_mean = 0.0, rl_mean = 0.0;

            buf_idx = 0;

            for(int i = 0; i < GYRO_WINDOW_SIZE; i++)
            {
                fb_sum += fb_gyro_array[i];
                rl_sum += rl_gyro_array[i];
            }
            fb_mean = fb_sum / GYRO_WINDOW_SIZE;
            rl_mean = rl_sum / GYRO_WINDOW_SIZE;

            fb_sum = 0.0; rl_sum = 0.0;
            for(int i = 0; i < GYRO_WINDOW_SIZE; i++)
            {
                fb_diff = fb_gyro_array[i] - fb_mean;
                rl_diff = rl_gyro_array[i] - rl_mean;
                fb_sum += fb_diff * fb_diff;
                rl_sum += rl_diff * rl_diff;
            }
            fb_sd = sqrt(fb_sum / GYRO_WINDOW_SIZE);
            rl_sd = sqrt(rl_sum / GYRO_WINDOW_SIZE);

            if(fb_sd < MARGIN_OF_SD && rl_sd < MARGIN_OF_SD)
            {
                m_FBGyroCenter = (int)fb_mean;
                m_RLGyroCenter = (int)rl_mean;
                m_CalibrationStatus = 1;
                if(DEBUG_PRINT == true)
                    fprintf(stderr, "FBGyroCenter:%d , RLGyroCenter:%d \n", m_FBGyroCenter, m_RLGyroCenter);
            }
            else
            {
                m_FBGyroCenter = 512;
                m_RLGyroCenter = 512;
                m_CalibrationStatus = -1;
            }
        }
    }

    // CIB
    // read torques from specific motors
    if(m_Enabled)
    {
        int jointsWeCareAbout[] = {
            JointData::ID_R_ANKLE_ROLL,
            JointData::ID_L_ANKLE_ROLL,
            JointData::ID_R_ANKLE_PITCH,
            JointData::ID_L_ANKLE_PITCH,
            JointData::ID_R_HAND,
            JointData::ID_L_HAND,
            JointData::ID_R_KNEE,
            JointData::ID_L_KNEE,
            0 // dummy
        };
        int NUM_JOINTS_WE_CARE_ABOUT = 8;
        int id;

        for(int i=0; i<NUM_JOINTS_WE_CARE_ABOUT; i++)
        {
            id = jointsWeCareAbout[i];
            int tmp;


            if(AX12::IsAX12(id))
            {
                m_CM730->m_BulkReadData[id].start_address = AX12::P_PRESENT_LOAD_L;
                m_CM730->m_BulkReadData[id].length = 2;
                tmp = m_CM730->m_BulkReadData[id].ReadWord(AX12::P_PRESENT_LOAD_L);
                //std::cout << "Torque for " << id << " is " << MotionStatus::JOINT_TORQUES[id] << std::endl;
            }
            else if(RX28::IsRX28(id))
            {
                m_CM730->m_BulkReadData[id].start_address = RX28::P_PRESENT_LOAD_L;
                m_CM730->m_BulkReadData[id].length = 2;
                tmp = m_CM730->m_BulkReadData[id].ReadWord(RX28::P_PRESENT_LOAD_L);
            }
            else
            {
                m_CM730->m_BulkReadData[id].start_address = MX28::P_PRESENT_LOAD_L;
                m_CM730->m_BulkReadData[id].length = 2;
                tmp = m_CM730->m_BulkReadData[id].ReadWord(MX28::P_PRESENT_LOAD_L);
            }

            // 10th bit is 1 if torque is applied in a clockwise direction
            if(tmp & (1 << 10))
                tmp = tmp & 0x03ff;
            else
                tmp = - (tmp & 0x03ff);

            switch(id)
            {
                case JointData::ID_R_ANKLE_ROLL:
                    MotionStatus::R_ANKLE_ROLL_TORQUE = -tmp;
                    break;

                case JointData::ID_L_ANKLE_ROLL:
                MotionStatus::L_ANKLE_ROLL_TORQUE = tmp;
                    break;

                case JointData::ID_R_ANKLE_PITCH:
                MotionStatus::R_ANKLE_PITCH_TORQUE = -tmp;
                    break;

                case JointData::ID_L_ANKLE_PITCH:
                    MotionStatus::L_ANKLE_PITCH_TORQUE = tmp;
                    break;

                case JointData::ID_R_HAND:
                MotionStatus::R_HAND_TORQUE = -tmp;
                    break;

                case JointData::ID_L_HAND:
                    MotionStatus::L_HAND_TORQUE = tmp;
                    break;

                case JointData::ID_R_KNEE:
                    MotionStatus::R_KNEE_TORQUE = -tmp;
                    break;

                case JointData::ID_L_KNEE:
                    MotionStatus::L_KNEE_TORQUE = tmp;
                default:
                    break;
            }

        } // for id
    } // enabled

    if(m_CalibrationStatus == 1 && m_Enabled == true)
    {
        static int fb_array[ACCEL_WINDOW_SIZE] = {512,};
        static int buf_idx = 0;
        if(m_CM730->m_BulkReadData[CM730::ID_CM].error == 0)
        {
            MotionStatus::FB_GYRO = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_Y_L) - m_FBGyroCenter;
            MotionStatus::RL_GYRO = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_X_L) - m_RLGyroCenter;
            MotionStatus::Z_GYRO = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_Z_L) - m_ZGyroCenter;
            MotionStatus::Z_GYRO_SUM += MotionStatus::Z_GYRO;
            MotionStatus::RL_ACCEL = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_ACCEL_X_L);
            MotionStatus::FB_ACCEL = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_ACCEL_Y_L);
            MotionStatus::Z_ACCEL = m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_ACCEL_Z_L);
            fb_array[buf_idx] = MotionStatus::FB_ACCEL;
            if(++buf_idx >= ACCEL_WINDOW_SIZE) buf_idx = 0;
        }

        int sum = 0, avr = 512;
        for(int idx = 0; idx < ACCEL_WINDOW_SIZE; idx++)
            sum += fb_array[idx];
        avr = sum / ACCEL_WINDOW_SIZE;

        if(avr < MotionStatus::FALLEN_F_LIMIT)
            MotionStatus::FALLEN = FORWARD;
        else if(avr > MotionStatus::FALLEN_B_LIMIT)
            MotionStatus::FALLEN = BACKWARD;
        else
            MotionStatus::FALLEN = STANDUP;

        if(m_Modules.size() != 0)
        {
            for(std::list<MotionModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
            {
                (*i)->Process();
                for(int id=JointData::ID_R_SHOULDER_PITCH; id<JointData::NUMBER_OF_JOINTS; id++)
                {
                    if((*i)->m_Joint.GetEnable(id) == true)
                    {
                        MotionStatus::m_CurrentJoints.SetSlope(id, (*i)->m_Joint.GetCWSlope(id), (*i)->m_Joint.GetCCWSlope(id));
                        MotionStatus::m_CurrentJoints.SetValue(id, (*i)->m_Joint.GetValue(id));
/*
                        if(id == JointData::ID_R_HAND || id == JointData::ID_L_HAND)
                        {
                            MotionStatus::m_CurrentJoints.SetPGain(id, 26);
                            MotionStatus::m_CurrentJoints.SetIGain(id, 0);
                            MotionStatus::m_CurrentJoints.SetDGain(id, 14); // Originally, 28
                        }
                        else
*/
                        {
                            MotionStatus::m_CurrentJoints.SetPGain(id, (*i)->m_Joint.GetPGain(id));
                            MotionStatus::m_CurrentJoints.SetIGain(id, (*i)->m_Joint.GetIGain(id));
                            MotionStatus::m_CurrentJoints.SetDGain(id, (*i)->m_Joint.GetDGain(id));
                        }
                    }
                }
            }
        }

        // CIB
        // read the FSR values
        // NOTE: X/Y axes are rotated compared to the Walking module
        // therefore we read in the values backwards
        if(m_FsrPresent)
        {
            MotionStatus::R_FSR_X = m_CM730->m_BulkReadData[FSR::ID_R_FSR].ReadByte(FSR::P_FSR_Y);
            MotionStatus::R_FSR_Y = m_CM730->m_BulkReadData[FSR::ID_R_FSR].ReadByte(FSR::P_FSR_X);

            // change to -1 to indicate no data
            if(MotionStatus::R_FSR_X == 255)
                MotionStatus::R_FSR_X = -1;
            if(MotionStatus::R_FSR_Y == 255)
                MotionStatus::R_FSR_Y = -1;

            MotionStatus::L_FSR_X = m_CM730->m_BulkReadData[FSR::ID_L_FSR].ReadByte(FSR::P_FSR_Y);
            MotionStatus::L_FSR_Y = m_CM730->m_BulkReadData[FSR::ID_L_FSR].ReadByte(FSR::P_FSR_X);

            // the left X FSR needs to be flipped
            if(MotionStatus::L_FSR_X == 255)
                MotionStatus::L_FSR_X = -1;
            else
                MotionStatus::L_FSR_X = FSR::MAX_VALUE - MotionStatus::L_FSR_X;

            if(MotionStatus::L_FSR_Y == 255)
                MotionStatus::L_FSR_Y = -1;
        }

        // read the CM730's current voltage
        MotionStatus::CM730_VOLTAGE = m_CM730->m_BulkReadData[CM730::ID_CM].ReadByte(CM730::P_VOLTAGE) / 10.0;

        // CIB
        // if no modules are controlling the hands they may be sent a zero-position (which will over-torque them)
        // if this is the case set the motors to the middle position
        int rHandAngle = (int)MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_HAND);
        if(rHandAngle < RightArm::GetInstance()->CLOSE_ANGLE -5 || rHandAngle > RightArm::GetInstance()->OPEN_ANGLE +5)
        {
            MotionStatus::m_CurrentJoints.SetBodyAngle(JointData::ID_R_HAND,RightArm::GetInstance()->NEUTRAL_ANGLE);
            // cerr << "[debug] Right hand set to invalid angle: " << rHandAngle << endl;
        }
        int lHandAngle = (int)MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_L_HAND);
        if(lHandAngle < LeftArm::GetInstance()->CLOSE_ANGLE -5 || lHandAngle > LeftArm::GetInstance()->OPEN_ANGLE +5)
        {
            MotionStatus::m_CurrentJoints.SetBodyAngle(JointData::ID_L_HAND,LeftArm::GetInstance()->NEUTRAL_ANGLE);
            //cerr << "[debug] Left hand set to invalid angle: " << lHandAngle << endl;
        }

        // CIB
        // Separate parameter arrays for each motor type
        // we send these in three separate syncwrites
        int paramMx[JointData::NUMBER_OF_JOINTS * MX28::PARAM_BYTES];
        int mxLen = 0;
        int numMx = 0;
        int paramRx[JointData::NUMBER_OF_JOINTS * RX28::PARAM_BYTES];
        int rxLen = 0;
        int numRx = 0;
        int paramAx[JointData::NUMBER_OF_JOINTS * AX12::PARAM_BYTES];
        int axLen = 0;
        int numAx = 0;
        for(int id=JointData::ID_R_SHOULDER_PITCH; id<JointData::NUMBER_OF_JOINTS; id++)
        {
            // CIB
            // we needed to edit this section to allow for 3 different sets of parameters:
            // MX28 parameters (standard), RX28 parameters (old Darwin), and AX12 parameters

            if(MotionStatus::m_CurrentJoints.GetEnable(id) == true)
            {
                if(AX12::IsAX12(id))
                {
                    paramAx[axLen++] = id;
                    paramAx[axLen++] = MotionStatus::m_CurrentJoints.GetCWSlope(id);
                    paramAx[axLen++] = MotionStatus::m_CurrentJoints.GetCCWSlope(id);
                    paramAx[axLen++] = CM730::GetLowByte(AX12::Virtual2RealValue(MotionStatus::m_CurrentJoints.GetValue(id) + m_Offset[id]));
                    paramAx[axLen++] = CM730::GetHighByte(AX12::Virtual2RealValue(MotionStatus::m_CurrentJoints.GetValue(id) + m_Offset[id]));

                    //std::cerr << "ID " << id << " is AX-12; goal position: " << paramAx[axLen-2] << " " << paramAx[axLen-1] << std::endl;

                    numAx++;
                }
                else if(RX28::IsRX28(id))
                {
                    paramRx[rxLen++] = id;
                    paramRx[rxLen++] = MotionStatus::m_CurrentJoints.GetCWSlope(id);
                    paramRx[rxLen++] = MotionStatus::m_CurrentJoints.GetCCWSlope(id);
                    paramRx[rxLen++] = CM730::GetLowByte(RX28::Virtual2RealValue(MotionStatus::m_CurrentJoints.GetValue(id) + m_Offset[id]));   // convert from MX-28 position space to RX-28 space
                    paramRx[rxLen++] = CM730::GetHighByte(RX28::Virtual2RealValue(MotionStatus::m_CurrentJoints.GetValue(id) + m_Offset[id]));   // convert from MX-28 position space to RX-28 space

                    numRx++;
                }
                else
                {

                    paramMx[mxLen++] = id;
#ifdef MX28_1024
                    paramMx[mxLen++] = MotionStatus::m_CurrentJoints.GetCWSlope(id);
                    paramMx[mxLen++] = MotionStatus::m_CurrentJoints.GetCCWSlope(id);
#else
                    paramMx[mxLen++] = MotionStatus::m_CurrentJoints.GetDGain(id);
                    paramMx[mxLen++] = MotionStatus::m_CurrentJoints.GetIGain(id);
                    paramMx[mxLen++] = MotionStatus::m_CurrentJoints.GetPGain(id);
                    paramMx[mxLen++] = 0;
#endif
                    paramMx[mxLen++] = CM730::GetLowByte(MotionStatus::m_CurrentJoints.GetValue(id) + m_Offset[id]);
                    paramMx[mxLen++] = CM730::GetHighByte(MotionStatus::m_CurrentJoints.GetValue(id) + m_Offset[id]);

                    numMx++;
                }

            }

            if(DEBUG_PRINT == true)
                fprintf(stderr, "ID[%d] : %d \n", id, MotionStatus::m_CurrentJoints.GetValue(id));
        }

        if(numMx > 0)
#ifdef MX28_1024
            m_CM730->SyncWrite(MX28::P_CW_COMPLIANCE_SLOPE, MX28::PARAM_BYTES, numMx, paramMx);
#else
            m_CM730->SyncWrite(MX28::P_D_GAIN, MX28::PARAM_BYTES, numMx, paramMx);
#endif

        if(numRx > 0)
            m_CM730->SyncWrite(RX28::P_CW_COMPLIANCE_SLOPE, RX28::PARAM_BYTES, numRx, paramRx);

        if(numAx > 0)
            m_CM730->SyncWrite(AX12::P_CW_COMPLIANCE_SLOPE, AX12::PARAM_BYTES, numAx, paramAx);
    }

    m_CM730->BulkRead();

    if(m_IsLogging)
    {
        for(int id = 1; id < JointData::NUMBER_OF_JOINTS; id++)
            m_LogFileStream << MotionStatus::m_CurrentJoints.GetBodyAngle(id) << ",";// << m_CM730->m_BulkReadData[id].ReadWord(MX28::P_PRESENT_POSITION_L) << ",";

#if 0
        m_LogFileStream << m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_Y_L) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_X_L) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_GYRO_Z_L) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_ACCEL_Y_L) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_ACCEL_X_L) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[CM730::ID_CM].ReadWord(CM730::P_ACCEL_Z_L) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[FSR::ID_L_FSR].ReadByte(FSR::P_FSR_X) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[FSR::ID_L_FSR].ReadByte(FSR::P_FSR_Y) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[FSR::ID_R_FSR].ReadByte(FSR::P_FSR_X) << ",";
        m_LogFileStream << m_CM730->m_BulkReadData[FSR::ID_R_FSR].ReadByte(FSR::P_FSR_Y) << ",";
#else
        m_LogFileStream << MotionStatus::FB_GYRO << ",";
        m_LogFileStream << MotionStatus::RL_GYRO << ",";
        m_LogFileStream << MotionStatus::Z_GYRO << ",";
        m_LogFileStream << MotionStatus::FB_ACCEL << ",";
        m_LogFileStream << MotionStatus::RL_ACCEL << ",";
        m_LogFileStream << MotionStatus::Z_ACCEL << ",";
        m_LogFileStream << MotionStatus::L_FSR_X << ",";
        m_LogFileStream << MotionStatus::L_FSR_Y << ",";
        m_LogFileStream << MotionStatus::R_FSR_X << ",";
        m_LogFileStream << MotionStatus::R_FSR_Y << ",";
#endif
        m_LogFileStream << std::endl;
    }

    if(m_CM730->m_BulkReadData[CM730::ID_CM].error == 0)
        MotionStatus::BUTTON = m_CM730->m_BulkReadData[CM730::ID_CM].ReadByte(CM730::P_BUTTON);

    m_IsRunning = false;

    // increment the system ticks now that we've finished a single round of processing
    MotionStatus::ticks++;
    if(MotionStatus::ticks == ULONG_MAX)    // prevent overflows by manually "overflowing"
        MotionStatus::ticks = 0;
}

void MotionManager::SetEnable(bool enable)
{
    m_Enabled = enable;
    if(m_Enabled == true)
    {
        m_CM730->WriteWord(CM730::ID_BROADCAST, MX28::P_MOVING_SPEED_L, 0, 0);
    }
}

void MotionManager::AddModule(MotionModule *module)
{
    module->Initialize();
    m_Modules.push_back(module);
}

void MotionManager::RemoveModule(MotionModule *module)
{
    m_Modules.remove(module);
}

void MotionManager::SetJointDisable(int index)
{
    if(m_Modules.size() != 0)
    {
        for(std::list<MotionModule*>::iterator i = m_Modules.begin(); i != m_Modules.end(); i++)
            (*i)->m_Joint.SetEnable(index, false);
    }
}

// CIB
void MotionManager::StopAllModules()
{
    std::list<MotionModule*>::iterator it;
    for(it=m_Modules.begin(); it!=m_Modules.end(); it++)
    {
        (*it)->Stop();
        while((*it)->IsRunning())
            usleep(8000);
    }
}

// CIB
void MotionManager::WaitButton(int button, void (*function)(void))
{
    int lastButton = 0;

    //do
    //{
    //	usleep( 50 );
    //	lastButton = MotionStatus::BUTTON;
    //} while( lastButton != button );

    while(lastButton != button)
    {
        lastButton = GetButton();
        if(function!=NULL)
            function();
    }
}

// SBD
int MotionManager::GetButton()
{
    int lastButton = 0;
    do
    {
        usleep(50);
        lastButton = MotionStatus::BUTTON;
    } while( lastButton == 0 );

    return lastButton;
}

// DLC
void MotionManager::usleep(double waitTime, void (*function)(void))
{
    timeval tim;
    double start, current;

    waitTime = waitTime / 1000000;

    gettimeofday(&tim, NULL);
    start = tim.tv_sec+(tim.tv_usec/1000000.0);
    current = tim.tv_sec+(tim.tv_usec/1000000.0);
    while(current - start < waitTime)
    {
        if(function!=NULL)
            function();
        gettimeofday(&tim, NULL);
        current = tim.tv_sec+(tim.tv_usec/1000000.0);
    }
}

// SBD
void MotionManager::msleep(double waitTime, void (*function)(void))
{
    usleep(waitTime * 1000, function);
}

// SBD
void MotionManager::Sync()
{
    //::usleep(MotionModule::TIME_UNIT * 1000);
    unsigned long int currentTicks = MotionStatus::ticks;
    while(MotionStatus::ticks == currentTicks)
    {
        ::usleep(1000);
    }
}

// SBD
int MotionManager::SetEyeColour(int r, int g, int b)
{
    int err;
    while(m_IsRunning);

    m_IsRunning = true;
    m_CM730->WriteWord(CM730::ID_CM, CM730::P_LED_EYE_L, CM730::MakeColour(r, g, b), &err);
    m_IsRunning = false;

    return err;
}

// CIB
int MotionManager::SetForeheadColour(int r, int g, int b)
{
    int err;
    while(m_IsRunning);

    m_IsRunning = true;
    m_CM730->WriteWord(CM730::ID_CM, CM730::P_LED_HEAD_L, CM730::MakeColour(r, g, b), &err);
    m_IsRunning = false;

    return err;
}

bool MotionManager::GetTorqueEnable(int id)
{
    return MotionStatus::m_CurrentJoints.GetEnable(id);
}

int MotionManager::SetTorqueEnable(int id, int enable)
{
    // enable/disable writing data to the motor
    MotionStatus::m_CurrentJoints.SetEnable(id,enable);

    // set the torque of the motor
    // (if true then writing position data to the motor automagically turns the torque back on)
    while(m_IsRunning);
    m_IsRunning = true;

    int err = CM730::SUCCESS;
    if(!enable)
        m_CM730->WriteByte(id, MX28::P_TORQUE_ENABLE, enable, &err);

    m_IsRunning = false;
    return err;
}

int MotionManager::SetTorqueEnable(std::list<int> &motorIds, std::list<bool> &enable)
{
    int params[512];

    list<int>::iterator idIt = motorIds.begin();
    list<bool>::iterator enIt = enable.begin();
    int i = 0;
    int numMotors = 0;
    while(i<512 && idIt != motorIds.end() && enIt != enable.end())
    {
        params[i] = *idIt;
        i++;
        params[i] = *enIt;
        i++;

        // enable/disable the motor in the MotionManager
        MotionStatus::m_CurrentJoints.SetEnable(*idIt, *enIt);

        idIt++;
        enIt++;
        numMotors++;
    }

    while(m_IsRunning);
    m_IsRunning = true;
    int result = m_CM730->SyncWrite(MX28::P_TORQUE_ENABLE, 2, numMotors, params);
    m_IsRunning = false;

    return result;
}

int MotionManager::SetTorqueEnable(std::list<int> &motorIds, bool enable)
{
    int params[512];

    list<int>::iterator idIt = motorIds.begin();
    int i = 0;
    int numMotors = 0;
    while(i<512 && idIt != motorIds.end())
    {
        params[i] = *idIt;
        i++;
        params[i] = enable;
        i++;

        // enable/disable the motor in the MotionManager
        MotionStatus::m_CurrentJoints.SetEnable(*idIt, enable);

        idIt++;
        numMotors++;
    }

    while(m_IsRunning);
    m_IsRunning = true;
    int result = m_CM730->SyncWrite(MX28::P_TORQUE_ENABLE, 2, numMotors, params);
    m_IsRunning = false;

    return result;
}
