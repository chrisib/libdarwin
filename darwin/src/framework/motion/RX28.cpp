#include "RX28.h"
#include "MX28.h"

using namespace Robot;

bool RX28::SERVO_IDS[JointData::NUMBER_OF_JOINTS+1] =
{
    false,  // 0 is always false since there is no servo 0

    // TRUE indicates that the motor with ID x is an RX28 motor
    false,  // ID_R_SHOULDER_PITCH
    false,  // ID_L_SHOULDER_PITCH
    false,  // ID_R_SHOULDER_ROLL
    false,  // ID_L_SHOULDER_ROLL
    false,  // ID_R_ELBOW
    false,  // ID_L_ELBOW
    false,  // ID_R_HIP_YAW
    false,  // ID_L_HIP_YAW
    false,  // ID_R_HIP_ROLL
    false,  // ID_L_HIP_ROLL
    false,  // ID_R_HIP_PITCH
    false,  // ID_L_HIP_PITCH
    false,  // ID_R_KNEE
    false,  // ID_L_KNEE
    false,  // ID_R_ANKLE_PITCH
    false,  // ID_L_ANKLE_PITCH
    false,  // ID_R_ANKLE_ROLL
    false,  // ID_L_ANKLE_ROLL
    false,  // ID_HEAD_PAN
    false,  // ID_HEAD_TILT
    false   // ID_R_HAND
};
int RX28::Real2VirtualValue( int value, int id )
{   // if ID is negative, caller wants to convert value no matter what
    if( id < 0 || IsRX28( id ) )
    {
        double angle = Value2Angle( value );
        if( angle < MX28::MIN_ANGLE )
        {
            angle = MX28::MIN_ANGLE;
        }
        else if( angle > MX28::MAX_ANGLE )
        {
            angle = MX28::MAX_ANGLE;
        }
        value = MX28::Angle2Value( angle );
    }
    return value;
}


int RX28::Virtual2RealValue( int value, int id )
{   // if ID is negative, caller wants to convert value no matter what
    if( id < 0 || IsRX28( id ) )
    {
        value = Angle2Value( MX28::Value2Angle( value ) );
    }
    return value;
}

bool RX28::IsRX28( int id )
{
    return SERVO_IDS[id];
}
