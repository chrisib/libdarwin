/*
 *   MotionStatus.cpp
 *
 *   Author: ROBOTIS
 *
 */

#include "MotionStatus.h"

using namespace Robot;

unsigned long int MotionStatus::ticks = 0;

JointData MotionStatus::m_CurrentJoints;
int MotionStatus::FB_GYRO(0);
int MotionStatus::RL_GYRO(0);
int MotionStatus::Z_GYRO(0);
int MotionStatus::FB_ACCEL(0);
int MotionStatus::RL_ACCEL(0);
int MotionStatus::Z_ACCEL(0);

int MotionStatus::Z_GYRO_SUM(0);

int MotionStatus::BUTTON(0);
int MotionStatus::FALLEN(0);

// CIB
int MotionStatus::R_ANKLE_ROLL_TORQUE = 0;
int MotionStatus::R_ANKLE_PITCH_TORQUE = 0;
int MotionStatus::L_ANKLE_ROLL_TORQUE = 0;
int MotionStatus::L_ANKLE_PITCH_TORQUE = 0;
int MotionStatus::R_HAND_TORQUE = 0;
int MotionStatus::L_HAND_TORQUE = 0;
int MotionStatus::R_KNEE_TORQUE = 0;
int MotionStatus::L_KNEE_TORQUE = 0;

int MotionStatus::R_FSR_X = -1;
int MotionStatus::R_FSR_Y = -1;
int MotionStatus::L_FSR_X = -1;
int MotionStatus::L_FSR_Y = -1;

double MotionStatus::CM730_VOLTAGE = 0.0;
