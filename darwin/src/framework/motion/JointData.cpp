/*
 *   JointData.cpp
 *
 *   Author: ROBOTIS
 *
 */

#include "MX28.h"
#include "RX28.h"
#include "AX12.h"
#include "JointData.h"
#include "MotionManager.h"
#include "Kinematics.h"
#include <iostream>

using namespace Robot;
using namespace std;

JointData::JointData()
{
    for(int i=0; i<NUMBER_OF_JOINTS; i++)
    {
        m_Enable[i] = true;
        m_Value[i] = MX28::CENTER_VALUE;
        m_Angle[i] = 0.0;
        m_CWSlope[i] = SLOPE_DEFAULT;
        m_CCWSlope[i] = SLOPE_DEFAULT;
        m_PGain[i] = P_GAIN_DEFAULT;
        m_IGain[i] = I_GAIN_DEFAULT;
        m_DGain[i] = D_GAIN_DEFAULT;
    }
}

JointData::~JointData()
{
}

void JointData::operator =(JointData that)
{
    for(int i=0; i<NUMBER_OF_JOINTS; i++)
    {
        this->m_Enable[i] = that.m_Enable[i];
        this->m_Value[i] = that.m_Value[i];
        this->m_Angle[i] = that.m_Angle[i];
        this->m_CWSlope[i] = that.m_CWSlope[i];
        this->m_CCWSlope[i] = that.m_CCWSlope[i];
        this->m_PGain[i] = that.m_PGain[i];
        this->m_IGain[i] = that.m_IGain[i];
        this->m_DGain[i] = that.m_DGain[i];
    }
}

void JointData::SetEnable(int id, bool enable)
{
    m_Enable[id] = enable;
}

void JointData::SetEnable(int id, bool enable, bool exclusive)
{
    if(enable && exclusive) MotionManager::GetInstance()->SetJointDisable(id);
    m_Enable[id] = enable;
}

void JointData::SetEnableHeadOnly(bool enable)
{
    SetEnableHeadOnly(enable, false);
}

void JointData::SetEnableHeadOnly(bool enable, bool exclusive)
{
	SetEnable(ID_HEAD_PAN,          enable, exclusive);
	SetEnable(ID_HEAD_TILT,         enable, exclusive);
}

void JointData::SetEnableRightArmOnly(bool enable)
{
    SetEnableRightArmOnly(enable, false);
}

void JointData::SetEnableRightArmOnly(bool enable, bool exclusive)
{
    SetEnable(ID_R_SHOULDER_PITCH,  enable, exclusive);
    SetEnable(ID_R_SHOULDER_ROLL,   enable, exclusive);
    SetEnable(ID_R_ELBOW,           enable, exclusive);
    SetEnable(ID_R_HAND,            enable, exclusive); //CIB
}

// SBD
void JointData::SetEnableRightHandOnly(bool enable, bool exclusive)
{
	SetEnable(ID_R_HAND, enable, exclusive);
}

void JointData::SetEnableLeftArmOnly(bool enable)
{
    SetEnableLeftArmOnly(enable, false);
}

void JointData::SetEnableLeftArmOnly(bool enable, bool exclusive)
{
    SetEnable(ID_L_SHOULDER_PITCH,  enable, exclusive);
    SetEnable(ID_L_SHOULDER_ROLL,   enable, exclusive);
    SetEnable(ID_L_ELBOW,           enable, exclusive);
    SetEnable(ID_L_HAND,            enable, exclusive); //CIB
}

// SBD
void JointData::SetEnableLeftHandOnly(bool enable, bool exclusive)
{
	SetEnable(ID_L_HAND, enable, exclusive);
}

void JointData::SetEnableRightLegOnly(bool enable)
{
    SetEnableRightLegOnly(enable, false);
}

void JointData::SetEnableRightLegOnly(bool enable, bool exclusive)
{
    SetEnable(ID_R_HIP_YAW,         enable, exclusive);
    SetEnable(ID_R_HIP_ROLL,        enable, exclusive);
    SetEnable(ID_R_HIP_PITCH,       enable, exclusive);
    SetEnable(ID_R_KNEE,            enable, exclusive);
    SetEnable(ID_R_ANKLE_PITCH,     enable, exclusive);
    SetEnable(ID_R_ANKLE_ROLL,      enable, exclusive);
}

void JointData::SetEnableLeftLegOnly(bool enable)
{
    SetEnableLeftLegOnly(enable, false);
}

void JointData::SetEnableLeftLegOnly(bool enable, bool exclusive)
{
    SetEnable(ID_L_HIP_YAW,         enable, exclusive);
    SetEnable(ID_L_HIP_ROLL,        enable, exclusive);
    SetEnable(ID_L_HIP_PITCH,       enable, exclusive);
    SetEnable(ID_L_KNEE,            enable, exclusive);
    SetEnable(ID_L_ANKLE_PITCH,     enable, exclusive);
    SetEnable(ID_L_ANKLE_ROLL,      enable, exclusive);
}

void JointData::SetEnableUpperBodyWithoutHead(bool enable)
{
    SetEnableUpperBodyWithoutHead(enable, false);
}

void JointData::SetEnableUpperBodyWithoutHead(bool enable, bool exclusive)
{
    SetEnableRightArmOnly(enable, exclusive);
    SetEnableLeftArmOnly(enable, exclusive);
}

void JointData::SetEnableLowerBody(bool enable)
{
    SetEnableLowerBody(enable, false);
}

void JointData::SetEnableLowerBody(bool enable, bool exclusive)
{
    SetEnableRightLegOnly(enable, exclusive);
    SetEnableLeftLegOnly(enable, exclusive);
}

void JointData::SetEnableBodyWithoutHead(bool enable)
{
    SetEnableBodyWithoutHead(enable, false);
}

void JointData::SetEnableBodyWithoutHead(bool enable, bool exclusive)
{
    SetEnableRightArmOnly(enable, exclusive);
    SetEnableLeftArmOnly(enable, exclusive);
    SetEnableRightLegOnly(enable, exclusive);
    SetEnableLeftLegOnly(enable, exclusive);
}

void JointData::SetEnableBody(bool enable)
{
    SetEnableBody(enable, false);
}

void JointData::SetEnableBody(bool enable, bool exclusive)
{
    for(int id = 1; id <= ID_HEAD_TILT; id++) {
        SetEnable(id, enable, exclusive);
	}
}

bool JointData::GetEnable(int id)
{
    return m_Enable[id];
}

void JointData::SetValue(int id, int value)
{
    if(value < MX28::MIN_VALUE)
        value = MX28::MIN_VALUE;
    else if(value >= MX28::MAX_VALUE)
        value = MX28::MAX_VALUE;

    /*
    // CIB
    // if the motor is NOT an MX28 translate the value to the appropriate
    // motor's resolution
    if(RX28::IsRX28(id))
        value = RX28::Virtual2RealValue(value);
    else if(AX12::IsAX12(id))
        value = AX12::Virtual2RealValue(value);
    */
    m_Value[id] = value;
    m_Angle[id] = MX28::Value2Angle(value);
}

int JointData::GetValue(int id)
{
    /*
    // CIB
    // if the motor is NOT an MX28 translate the value back
    // to the MX28 resolution
    if(RX28::IsRX28(id))
        return(RX28::Real2VirtualValue(m_Value[id]));
    else if(AX12::IsAX12(id))
        return(AX12::Real2VirtualValue(m_Value[id]));
    */
    return m_Value[id];
}

void JointData::SetAngle(int id, double angle)
{
    if(angle < MX28::MIN_ANGLE)
        angle = MX28::MIN_ANGLE;
    else if(angle > MX28::MAX_ANGLE)
        angle = MX28::MAX_ANGLE;

    m_Angle[id] = angle;

    /*
    // CIB
    // set the value according to the motor type
    if(RX28::IsRX28(id))
        m_Value[id] = RX28::Angle2Value(angle);
    else if(AX12::IsAX12(id))
        m_Value[id] = AX12::Angle2Value(angle);
    else
    */
        m_Value[id] = MX28::Angle2Value(angle);
}

double JointData::GetAngle(int id)
{
    return m_Angle[id];
}

void JointData::SetRadian(int id, double radian)
{
    SetAngle(id, radian * (180.0 / 3.141592));
}

double JointData::GetRadian(int id)
{
    return GetAngle(id) * (180.0 / 3.141592);
}

void JointData::SetBodyAngle(int id, double angle)
{
    switch(id)
    {
    case JointData::ID_R_SHOULDER_ROLL:
        SetAngle(id, angle - Kinematics::SHOULDER_ROLL_OFFSET);
        break;

    case JointData::ID_L_SHOULDER_ROLL:
        SetAngle(id, -angle + Kinematics::SHOULDER_ROLL_OFFSET);
        break;

    case JointData::ID_R_ELBOW:
        SetAngle(id, angle - Kinematics::ELBOW_ANGLE_OFFSET);
        break;

    case JointData::ID_L_ELBOW:
        SetAngle(id, -angle + Kinematics::ELBOW_ANGLE_OFFSET);
        break;

    // no change to normal angle
    case JointData::ID_R_SHOULDER_PITCH:
    case JointData::ID_R_HAND:
    case JointData::ID_R_HIP_YAW:
    case JointData::ID_R_HIP_ROLL:
    case JointData::ID_L_HIP_PITCH:	// yes, it's L
    case JointData::ID_R_KNEE:
    case JointData::ID_R_ANKLE_ROLL:
    case JointData::ID_R_ANKLE_PITCH:
    case JointData::ID_HEAD_PAN:
    case JointData::ID_HEAD_TILT:
        SetAngle(id, angle);
        break;

    // invert angle (motors turn the opposite direction)
    case JointData::ID_L_SHOULDER_PITCH:
    case JointData::ID_L_HAND:
    case JointData::ID_L_HIP_YAW:
    case JointData::ID_L_HIP_ROLL:
    case JointData::ID_R_HIP_PITCH:	// yes, it's R
    case JointData::ID_L_KNEE:
    case JointData::ID_L_ANKLE_ROLL:
    case JointData::ID_L_ANKLE_PITCH:
        SetAngle(id, -angle);
        break;


    default:
        cerr << "[debug] SetBodyAngle: unknown joint ID: " << id << endl;
        SetAngle(id, angle);
        break;
    }
}

void JointData::SetBodyRadians(int id, double radian)
{
    SetBodyAngle(id, radian * (180.0 / 3.141592));
}

double JointData::GetBodyAngle(int id)
{
    switch(id)
    {
    case JointData::ID_R_SHOULDER_ROLL:
        return GetAngle(id) + Kinematics::SHOULDER_ROLL_OFFSET;

    case JointData::ID_L_SHOULDER_ROLL:
        return -GetAngle(id) + Kinematics::SHOULDER_ROLL_OFFSET;

    case JointData::ID_R_ELBOW:
        return GetAngle(id) + Kinematics::ELBOW_ANGLE_OFFSET;

    case JointData::ID_L_ELBOW:
        return -GetAngle(id) + Kinematics::ELBOW_ANGLE_OFFSET;

    // no change to normal angle
    case JointData::ID_R_SHOULDER_PITCH:
    case JointData::ID_R_HAND:
    case JointData::ID_R_HIP_YAW:
    case JointData::ID_R_HIP_ROLL:
    case JointData::ID_L_HIP_PITCH:	// yes, it's L
    case JointData::ID_R_KNEE:
    case JointData::ID_R_ANKLE_ROLL:
    case JointData::ID_R_ANKLE_PITCH:
    case JointData::ID_HEAD_PAN:
    case JointData::ID_HEAD_TILT:
        return GetAngle(id);

    // invert angle (motors turn the opposite direction)
    case JointData::ID_L_SHOULDER_PITCH:
    case JointData::ID_L_HAND:
    case JointData::ID_L_HIP_YAW:
    case JointData::ID_L_HIP_ROLL:
    case JointData::ID_R_HIP_PITCH:	// yes, it's R
    case JointData::ID_L_KNEE:
    case JointData::ID_L_ANKLE_ROLL:
    case JointData::ID_L_ANKLE_PITCH:
        return -GetAngle(id);


    default:
        cerr << "[debug] GetBodyAngle: unknown joint ID: " << id << endl;
        return GetAngle(id);
    }
}

double JointData::GetBodyRadians(int id)
{
    return GetBodyAngle(id) * (3.141592/180.0);
}

void JointData::SetSlope(int id, int cwSlope, int ccwSlope)
{
    SetCWSlope(id, cwSlope);
    SetCCWSlope(id, ccwSlope);
}

void JointData::SetCWSlope(int id, int cwSlope)
{
    m_CWSlope[id] = cwSlope;
}

int JointData::GetCWSlope(int id)
{
    return m_CWSlope[id];
}

void JointData::SetCCWSlope(int id, int ccwSlope)
{
    m_CCWSlope[id] = ccwSlope;
}

int JointData::GetCCWSlope(int id)
{
    return m_CCWSlope[id];
}
