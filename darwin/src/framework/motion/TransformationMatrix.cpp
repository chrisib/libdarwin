#include "TransformationMatrix.h"
#include "Kinematics.h"
#include <cmath>
#include <iostream>

using namespace Robot;
using namespace std;

TransformationMatrix::TransformationMatrix(double linkLength, double linkTwistDegrees, double linkOffset, double jointDegrees)
{
    double jointAngle = deg2rad(jointDegrees);
    double twistAngle = deg2rad(linkTwistDegrees);

    double st = sin(jointAngle); // sin(theta)
    double ct = cos(jointAngle); // cos(theta)
    double sa = sin(twistAngle); // sin(alpha)
    double ca = cos(twistAngle); // cos(alpha)

    // D-H representation of a single joint
    // see Spong et al.
    mtx[0][0] = ct;
    mtx[0][1] = -st * ca;
    mtx[0][2] = st * sa;
    mtx[0][3] = linkLength * ct;

    mtx[1][0] = st;
    mtx[1][1] = ct * ca;
    mtx[1][2] = -ct * sa;
    mtx[1][3] = linkLength * st;

    mtx[2][0] = 0;
    mtx[2][1] = sa;
    mtx[2][2] = ca;
    mtx[2][3] = linkOffset;

    mtx[3][0] = 0;
    mtx[3][1] = 0;
    mtx[3][2] = 0;
    mtx[3][3] = 1;

}

TransformationMatrix::~TransformationMatrix()
{
    //dtor
}

void TransformationMatrix::Multiply(TransformationMatrix &operand)
{
    double tmp;
    double result[4][4];

    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            tmp = 0;
            for(int k=0; k<4; k++)
            {
                tmp = tmp + mtx[i][k] * operand.mtx[k][j];
            }
            result[i][j] = tmp;
        }
    }

    // copy the results back
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            mtx[i][j] = result[i][j];
        }
    }
}

void TransformationMatrix::Print()
{
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            cout << (int)(mtx[i][j]*1000)/1000.0 << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

double TransformationMatrix::GetLinkLength()
{
    return mtx[0][3] / mtx[0][0];
}
double TransformationMatrix::GetLinkTwist()
{
    return rad2deg(asin(mtx[2][1]));
}
double TransformationMatrix::GetLinkOffset()
{
    return mtx[2][3];
}
double TransformationMatrix::GetJointAngle()
{
    return rad2deg(acos(mtx[0][0]));
}
