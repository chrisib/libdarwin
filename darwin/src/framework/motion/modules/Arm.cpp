/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class allows is a generic superclass for a Darwin arm; it will
 * eventually be used to contain inverse-kinematics
 ************************************************************************/

#include "Arm.h"
#include "Kinematics.h"
#include "TransformationMatrix.h"
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <Matrix.h>

//#define DEBUG_KINEMATICS

using namespace Robot;
using namespace std;

Arm::Arm(bool invertYAxis) : Limb(invertYAxis)
{
    OPEN_ANGLE = 90;
    NEUTRAL_ANGLE = 0;
    CLOSE_ANGLE = -30;
    TORQUE_LIMIT = 600;

    isMoving = false;
    handIsMoving = false;

    for(int i=0; i<NUM_JOINTS; i++)
    {
        jointPositions.push_back(Point3D());
    }
}

Arm::~Arm()
{
    //dtor
}

void Arm::Initialize()
{
    // do nothing
}

bool Arm::IsRunning()
{
    //cout << "hand: " << (handIsMoving ? "yes" : "no") << " arm: " << (armIsMoving ? "yes" : "no") << endl;
    return handIsMoving || isMoving;
}

bool aboutEquals(double a, double b, double epsilon) {
    return a - epsilon < b && a + epsilon > b;
}

Point3D Arm::GetPosition(double shoulderPitchAngle, double shoulderRollAngle, double elbowAngle)
{
#ifdef PYTHON_CODE
    shoulderPitchHome = RotationX(90) # pitch motor is the origin
    #shoulderPitchHome = RotationX(deg2rad(90)).dot(Translation(-0.005,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH,-57.5/1000.0))
    shoulderPitchJoint = shoulderPitchHome.dot(RotationZ(shoulderPitch))

    shoulderRollHome = shoulderPitchJoint.dot(Translation(0.0,-Kinematics.SHOULDER_BOTTOM_OFFSET,-Kinematics.SHOULDER_SIDE_OFFSET).dot(RotationY(deg2rad(90)).dot(RotationZ(deg2rad(90)))))
    shoulderRollJoint = shoulderRollHome.dot(RotationZ(shoulderRoll))

    elbowHome = shoulderRollJoint.dot(Translation(-Kinematics.UPPER_ARM_LENGTH,0,Kinematics.ELBOW_OFFSET).dot(RotationX(deg2rad(-90))))
    elbowJoint = elbowHome.dot(RotationZ(elbow))

    fingerHome = elbowJoint.dot(Translation(-Kinematics.LOWER_ARM_LENGTH,Kinematics.ELBOW_OFFSET,0))
    fingerJoint = fingerHome
#endif
    double x, y, z;

    Matrix3D shoulderPitchHome = Matrix3D::RotationX(90);
    Matrix3D shoulderPitchJoint = shoulderPitchHome * Matrix3D::RotationZ(shoulderPitchAngle);

    Matrix3D shoulderRollHome = shoulderPitchJoint * Matrix3D::Translation(0,-Kinematics::SHOULDER_BOTTOM_OFFSET,-Kinematics::SHOULDER_SIDE_OFFSET) * Matrix3D::RotationY(90) * Matrix3D::RotationZ(90);
    Matrix3D shoulderRollJoint = shoulderRollHome * Matrix3D::RotationZ(shoulderRollAngle);

    Matrix3D elbowHome = shoulderRollJoint * Matrix3D::Translation(-Kinematics::UPPER_ARM_LENGTH,0,Kinematics::ELBOW_OFFSET) * Matrix3D::RotationX(-90);
    Matrix3D elbowJoint = elbowHome * Matrix3D::RotationZ(elbowAngle);

    endEffector = elbowJoint * Matrix3D::Translation(-Kinematics::LOWER_ARM_LENGTH,Kinematics::ELBOW_OFFSET,0);

    jointPositions[JOINT_SHOULDER_PITCH] = Point3D(shoulderPitchHome.m[Matrix3D::m03],shoulderPitchHome.m[Matrix3D::m13],shoulderPitchHome.m[Matrix3D::m23]);
    jointPositions[JOINT_SHOULDER_ROLL] = Point3D(shoulderRollHome.m[Matrix3D::m03],shoulderRollHome.m[Matrix3D::m13],shoulderRollHome.m[Matrix3D::m23]);
    jointPositions[JOINT_ELBOW] = Point3D(elbowHome.m[Matrix3D::m03],elbowHome.m[Matrix3D::m13],elbowHome.m[Matrix3D::m23]);
    jointPositions[JOINT_FINGER] = Point3D(endEffector.m[Matrix3D::m03],endEffector.m[Matrix3D::m13],endEffector.m[Matrix3D::m23]);

    x = endEffector.m[Matrix3D::m03];
    y = endEffector.m[Matrix3D::m13];
    z = endEffector.m[Matrix3D::m23];

    Point3D position(x,y,z);
    return position;
}

Point3D Arm::GetAbsolutePosition()
{
    return GetAbsolutePosition(m_Joint);
}

Point3D Arm::GetAbsolutePosition(JointData &position)
{
    //Translation(Kinematics.SHOULDER_FRONT_OFFSET,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH,-Kinematics.SHOULDER_WIDTH/2)
    Point3D psn = GetPosition(position);
    psn.X += Kinematics::SHOULDER_FRONT_OFFSET;
    psn.Y += Kinematics::SHOULDER_WIDTH/2.0;
    psn.Z += Kinematics::TORSO_LENGTH-Kinematics::HIP_DROP_LENGTH;

    if(INVERTED_Y_AXIS)
        psn.Y = -psn.Y;
    return psn;
}

void Arm::SetGoalPosition(Point3D &endEffectorPosition, double handAngle)
{
    (void)handAngle;

    cerr << "[deprecation] Ignoring specified hand angle" << endl;
    SetGoalPosition(endEffectorPosition);
}

// set the 3D position of the end-effector relative to the shoulder
void Arm::SetGoalPosition(Point3D &endEffectorPosition)
{
    if(this->moveMethod == NULL)
        this->moveMethod = Limb::ApproachByOne;

    //ReadJointPositions();
    if(endEffectorPosition.Y < MIN_Y_POSITION) {
        cerr << "Darwin cannot reach that far in front of itself" << endl;
        return;
    }

    // the position of the hand in 3D space
    // note that to keep the trig signs easy this treats positive Z values as down, not up
    // that's okay though, since everything will work out in the end.
    // just be aware that debug output will have the signs for the Z-axis reversed
    Point3D handVector(0,0,0);

    double desiredElbowAngle = 0.0;
    double desiredShoulderRoll = 0.0;
    double desiredShoulderPitch = 0.0;

    // first off calculate the angle the elbow needs to be in order to make the distance from the shoulder to the hand the proper length
    double armRadius = endEffectorPosition.Magnitude();    // the distance from the shoulder bracket to the end of the hand
    const double MAX_ARM_RADIUS = Kinematics::LOWER_ARM_LENGTH + Kinematics::UPPER_ARM_LENGTH;
    if(armRadius > MAX_ARM_RADIUS)
    {
        cout << "[warning] Cannot extend arm beyond " << MAX_ARM_RADIUS << "mm. Requested position needs length of " << armRadius << "mm" << endl;
        armRadius = MAX_ARM_RADIUS;
        desiredElbowAngle = 180.0;    // fully extend the elbow
    }
    else
    {
        // calculate the necessary elbow length for the radius
        desiredElbowAngle = Math::Truncate(Math::GetInternalAngle(Kinematics::UPPER_ARM_LENGTH, Kinematics::LOWER_ARM_LENGTH, armRadius),2);
#ifdef DEBUG_KINEMATICS
        cout << "Calculated elbow angle: " << desiredElbowAngle << endl;
#endif
    }

    // once we know the elbow angle we can start figuring out where the hand actually is
    handVector.Z = Kinematics::UPPER_ARM_LENGTH - cos(deg2rad(desiredElbowAngle)) * Kinematics::LOWER_ARM_LENGTH;
    handVector.X = sin(deg2rad(desiredElbowAngle)) * Kinematics::LOWER_ARM_LENGTH;

#ifdef DEBUG_KINEMATICS
    cout << "After setting elbow angle: (" << handVector.X << " , 0 , " << handVector.Z << ")" << endl;
#endif

    // calculate the necessary shoulder roll angle that will bring the hand to the proper Y position
    // note that this may not be possible, in which case print an error
    // if the hand is above the shoulder rotating the shoulder brings the hand closer in
    if(handVector.Z < 0 && endEffectorPosition.Y > 0)   // (note that handVector has that flipped Z-axis we talked about before!)
    {
        cerr << "Unable to reach specified hand position" << endl;
        return;
    }
    else
    {
        // take our best stab at reaching the proper position
        desiredShoulderRoll = asin(endEffectorPosition.Y/handVector.Z);
#ifdef DEBUG_KINEMATICS
        cout << "Calculated shoulder roll: " << rad2deg(desiredShoulderRoll) << " = asin(" << endEffectorPosition.Y << "/" << handVector.Z << ")" << endl;
#endif
        // adjust the Y and Z positions according to our new roll angle
        handVector.Y = sin(desiredShoulderRoll) * handVector.Z;
        handVector.Z = cos(desiredShoulderRoll) * handVector.Z;

        desiredShoulderRoll = Math::Truncate(rad2deg(desiredShoulderRoll),2);
#ifdef DEBUG_KINEMATICS
        cout << "After setting shoulder roll: (" << handVector.X << " , " << handVector.Y << " , " << handVector.Z << ")" << endl;
#endif
    }

    // finally get the shoulder pitch that will bring the hand to the correct X/Z position
    // for this we need to project the angled triangle formed by the arm onto the X-Z plane
    // (the triangle is inclined by desiredShoulderRoll degrees
    double targetPitch;
    targetPitch = Math::Truncate(90 + rad2deg(atan2(endEffectorPosition.Z,endEffectorPosition.X)),2);    // angle from shoulder motor to hand
#ifdef DEBUG_KINEMATICS
    cout << "Target Pitch: " << targetPitch << " = atan2(" << endEffectorPosition.X << " , " << endEffectorPosition.Z << ")" << endl;
#endif

    // we need to offset the pitch by the angle in the XZ plane between the shoulder and the fingertip
    double pitchOffset = rad2deg(atan2(handVector.X,handVector.Z));
#ifdef DEBUG_KINEMATICS
    cout << "Pitch Offset: " << pitchOffset << " = atan2(" << handVector.X << " , " << handVector.Z << ")" << endl;
#endif

    desiredShoulderPitch = targetPitch - pitchOffset;

#ifdef DEBUG_KINEMATICS
    cout << "Goal angles: elbow: " << Math::NormalizeAngle(180-desiredElbowAngle) << endl <<
            "              roll: " << Math::NormalizeAngle(desiredShoulderRoll) << endl <<
            "             pitch: " << Math::NormalizeAngle(desiredShoulderPitch) << endl <<
            endl;
#endif

    targetElbowAngle = Math::NormalizeAngle(180 - desiredElbowAngle); // treat straight as zero, not 180
    targetShoulderPitchAngle = Math::NormalizeAngle(desiredShoulderPitch);
    targetShoulderRollAngle = Math::NormalizeAngle(desiredShoulderRoll);

    isMoving = true;
}

// These are for default (Open|Close)Hand methods on Arm* values
void Arm::OpenHand(double speed) {
    this->MoveHand(OPEN_ANGLE, speed);
}

void Arm::CloseHand(double speed) {
    this->MoveHand(CLOSE_ANGLE, speed);
}

void Arm::RelaxHand(double speed)
{
    this->MoveHand(NEUTRAL_ANGLE, speed);
}

void Arm::MoveHand(double toAngle, double speed)
{
    handMovingSpeed = speed;
    handTargetAngle = toAngle;
    handIsMoving = true;
}

void Arm::UpdateHandPosition(int motorId)
{
    if(!handIsMoving)
        return;

    bool isMoving = false;

    // only bother setting hand positions if we actually control the hand
    if(m_Joint.GetEnable(motorId) == true)
    {
        handCurrentAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(motorId);
        double handAngle = handCurrentAngle;

        //cout << handCurrentAngle << " --> " << handTargetAngle << endl;

        if(handCurrentAngle < handTargetAngle && handIsMoving)  // we are opening the hand; do not bother worrying about over-torquing the motor
        {
            handAngle += handMovingSpeed;
            if(handAngle >= handTargetAngle)
            {
                handAngle = handTargetAngle;    // stop at the max angle
#ifdef DEBUG
                cout << "Hand open" << endl;
#endif
            }
            else if(handAngle < handTargetAngle) // keep going until we reach the desired angle
            {
                //cout << "A" << endl;
                isMoving = true;
            }
        }
        else if(handCurrentAngle > handTargetAngle && handIsMoving)   // we are closing the hand; move the hand until we reach the goal angle (or pass it) or hit the torque limit
        {
            // check the torque before we move the hand
            int torque = ReadHandTorque();
            if(torque < TORQUE_LIMIT)   // we have not hit the torque limit yet; keep moving the hand
            {
                // we have not yet reached the torque limit; close the hand until we reach the limit
                handAngle -= handMovingSpeed;
                if(handAngle < handTargetAngle)
                    handAngle = handTargetAngle;

                if(handAngle > handTargetAngle)
                {
                    //cout << "B" << endl;
                    isMoving = true;
                }
            }
#ifdef DEBUG
            else    // hit the torque limit; stop moving
            {
                cout << "Reached torque limit" << endl;
            }
#endif
        }

        // clamp the hand angles within the allowable limits just in case something weird has happened
        if(handAngle > OPEN_ANGLE)
            handAngle = OPEN_ANGLE;
        else if(handAngle < CLOSE_ANGLE)
            handAngle = CLOSE_ANGLE;

        m_Joint.SetPGain(motorId, 26);
        m_Joint.SetIGain(motorId, 0);
        m_Joint.SetDGain(motorId, 14);
        m_Joint.SetBodyAngle(motorId, handAngle);
    }
#if 0
    else
    {
        cout << "Not controllng hand " << motorId << endl;
    }
#endif
    this->handIsMoving = isMoving;
}

void Arm::Extend(double speed)
{
    // TODO: calculate the arm's position when fully-extended
    cerr << "Arm::Extend not yet implemented" << endl;

    //extendSpeed = speed;
}
