#include "Arm.h"
#include "RightArm.h"
#include "Kinematics.h"
#include "JointData.h"
#include "CM730.h"
#include "MX28.h"
#include "RX28.h"
#include "AX12.h"
#include "MotionStatus.h"
#include <cstdlib>
#include <iostream>
#include "LinuxDARwIn.h"

using namespace Robot;
using namespace std;

RightArm *RightArm::_uniqueInstance = new RightArm();

RightArm::RightArm() : Arm(true)
{
    handTargetAngle = NEUTRAL_ANGLE;
    handIsMoving = true;
    handMovingSpeed = 1;
    isMoving = false;
}

Point3D RightArm::GetPosition()
{
    return GetPosition(m_Joint);
}

// get the 3D coordinates of the end-effector relative to the robot's shoulder
Point3D RightArm::GetPosition(JointData &position)
{

    double elbowAngle = position.GetBodyAngle(JointData::ID_R_ELBOW);
    double shoulderRollAngle = position.GetBodyAngle(JointData::ID_R_SHOULDER_ROLL);
    double shoulderPitchAngle = position.GetBodyAngle(JointData::ID_R_SHOULDER_PITCH);

    return Arm::GetPosition(shoulderPitchAngle, shoulderRollAngle, elbowAngle);
}

double RightArm::GetShoulderPitch()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_SHOULDER_PITCH);
}

double RightArm::GetShoulderRoll()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_SHOULDER_ROLL);
}

double RightArm::GetElbowAngle()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_ELBOW);
}

double RightArm::GetHandAngle()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_HAND);
}

// Set the angles of the arm directly
void RightArm::SetAngles(double shoulderPitch, double shoulderRoll, double elbow)//, MoveFunc function)
{
    if(this->moveMethod == NULL)
        this->moveMethod = Limb::ApproachByOne;

    targetElbowAngle = elbow;
    targetShoulderPitchAngle = shoulderPitch;
    targetShoulderRollAngle = shoulderRoll;
    isMoving = true;
}

//void RightArm::SetAngles(double shoulderPitch, double shoulderRoll, double elbow)
//{
//    SetAngles(shoulderPitch, shoulderRoll, elbow, Limb::ImmediateMove);
//}

void RightArm::SetAngles(JointData &joints)
{
    SetAngles(joints.GetBodyAngle(JointData::ID_R_SHOULDER_PITCH), joints.GetBodyAngle(JointData::ID_R_SHOULDER_ROLL), joints.GetBodyAngle(JointData::ID_R_ELBOW));
}

// pull the limb inwards such that the torque on all motors is not greater than the provided force
void RightArm::Pull(int force)
{
	(void) force; // Get rid of warning
}

// push the limb outwards such that the torque on all motors is not greater than the provided force
void RightArm::Push(int force)
{
	(void) force; // Get rid of warning
}

void RightArm::Process()
{
    //cerr << "RightArm::Process" << endl;
    if (isMoving) {
        if(extendSpeed > NOT_EXTENDING)
        {
            ProcessExtending();
        }
        else
        {
            double elbowAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_ELBOW);
            double shoulderRollAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_SHOULDER_ROLL);
            double shoulderPitchAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_SHOULDER_PITCH);

            if(moveMethod == NULL)
                moveMethod = Limb::ApproachByOne;

            bool moving = false;

            // set the joint angles if we need to
            if(m_Joint.GetEnable(JointData::ID_R_SHOULDER_PITCH) == true && fabs(shoulderPitchAngle-targetShoulderPitchAngle) > 1) {
                //cout << fabs(shoulderPitchAngle-targetShoulderPitchAngle) << endl;

                shoulderPitchAngle = moveMethod(shoulderPitchAngle, targetShoulderPitchAngle);
                m_Joint.SetBodyAngle(JointData::ID_R_SHOULDER_PITCH, shoulderPitchAngle);
                moving = true;
            }

            if(m_Joint.GetEnable(JointData::ID_R_SHOULDER_ROLL) == true && fabs(shoulderRollAngle-targetShoulderRollAngle) > 1) {
                //cout << fabs(shoulderRollAngle-targetShoulderRollAngle) << endl;

                shoulderRollAngle = moveMethod(shoulderRollAngle, targetShoulderRollAngle);
                m_Joint.SetBodyAngle(JointData::ID_R_SHOULDER_ROLL, shoulderRollAngle);
                moving = true;
            }

            if(m_Joint.GetEnable(JointData::ID_R_ELBOW) == true && fabs(elbowAngle-targetElbowAngle) > 1) {
                //cout << fabs(elbowAngle-targetElbowAngle) << endl;

                elbowAngle = moveMethod(elbowAngle, targetElbowAngle);
                m_Joint.SetBodyAngle(JointData::ID_R_ELBOW, elbowAngle);
                moving = true;
            }

            isMoving = moving;
        }
	}

    // only bother setting hand positions if we actually control the hand
    UpdateHandPosition(JointData::ID_R_HAND);
}

void RightArm::SetEnable(bool enable, bool exclusive)
{
    m_Joint.SetEnableBody(false);
	m_Joint.SetEnableRightArmOnly(enable, exclusive);
}

void RightArm::SetEnableHand(bool enable, bool exclusive)
{
    m_Joint.SetEnableBody(false);
	m_Joint.SetEnableRightHandOnly(enable, exclusive);
}

bool RightArm::IsHandOpen()
{
    return handTargetAngle >= OPEN_ANGLE;
}

int RightArm::ReadHandTorque()
{
    return abs(MotionStatus::R_HAND_TORQUE);
}
