#include "Limb.h"
#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;
using namespace Robot;

Limb::Limb(bool invertYAxis) : INVERTED_Y_AXIS(invertYAxis)
{
    moveMethod = ApproachByOne;
    extendSpeed = NOT_EXTENDING;
    isExtending = false;
    isMoving = false;
}

Limb::~Limb()
{
}

void Limb::ProcessExtending()
{
    if(!isExtending)
        return;

    // see if we've reached our goal position yet
    Point3D presentPosition = GetPosition();

    if(fabs(presentPosition.Magnitude() - extensionGoalPosition.Magnitude()) < extendSpeed)
    {
        // we're within speed of the goal; move to that position and stop extending
        SetGoalPosition(extensionGoalPosition);
        extendSpeed = NOT_EXTENDING;
        isExtending = false;
    }
    else
    {
        // otherwise extend our goal position by speed
        // this is basically just a p-controller to slowly move the goal position toward the fully-extended position
        Point3D vec = extensionGoalPosition-presentPosition;
        double scale = vec.Magnitude()*extendSpeed;

        Point3D tmpGoal = presentPosition + vec * scale;
        SetGoalPosition(tmpGoal);
    }
}

double Limb::ImmediateMove(double currentPosition, double targetPosition)
{
    (void) currentPosition; // Get rid of warning
    return targetPosition;
}

double Limb::HalfMove(double currentPosition, double targetPosition)
{
    const double epsilon = 0.1;
    double p = 0.5;

    double delta = targetPosition - currentPosition;

    double err = currentPosition + delta * p - targetPosition;

    if(fabs(err) < epsilon)
        return currentPosition;

    return currentPosition + delta * p;
}

double Limb::ApproachByOne(double currentPosition, double targetPosition) {
    if(fabs(currentPosition-targetPosition) < 1) {
        currentPosition = targetPosition;
    } else if (currentPosition < targetPosition) {
        currentPosition++;
    } else {
        currentPosition--;
    }
    return currentPosition;
}
