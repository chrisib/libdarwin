#include "darwin/framework/RightLeg.h"
#include "darwin/framework/MotionStatus.h"
#include <iostream>
#include <FSR.h>
#include <MotionManager.h>

using namespace std;
using namespace Robot;

RightLeg *RightLeg::uniqueInstance = new RightLeg();

RightLeg::RightLeg() : Leg(true)
{
    isMoving = false;
    this->moveMethod = Limb::ApproachByOne;	// default method of moving joints
}

void RightLeg::Initialize()
{
    // set the initial angles and target angles to their current values
    targetAnklePitchAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_ANKLE_PITCH);
    targetAnkleRollAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_ANKLE_ROLL);
    targetKneeAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_KNEE);
    targetHipRollAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_HIP_ROLL);
    targetHipPitchAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_HIP_PITCH);
    targetHipYawAngle = MotionStatus::m_CurrentJoints.GetBodyAngle(JointData::ID_R_HIP_YAW);

    m_Joint.SetBodyAngle(JointData::ID_R_HIP_YAW, targetHipYawAngle);
    m_Joint.SetBodyAngle(JointData::ID_R_HIP_PITCH, targetHipPitchAngle);
    m_Joint.SetBodyAngle(JointData::ID_R_HIP_ROLL, targetHipRollAngle);
    m_Joint.SetBodyAngle(JointData::ID_R_KNEE, targetKneeAngle);
    m_Joint.SetBodyAngle(JointData::ID_R_ANKLE_ROLL, targetAnkleRollAngle);
    m_Joint.SetBodyAngle(JointData::ID_R_ANKLE_PITCH, targetAnklePitchAngle);
}

Point3D RightLeg::GetPosition()
{
    return GetPosition(m_Joint);
}

Point3D RightLeg::GetPosition(JointData &position)
{
    double hipYaw = position.GetBodyAngle(JointData::ID_R_HIP_YAW);
    double hipRoll = position.GetBodyAngle(JointData::ID_R_HIP_ROLL);
    double hipPitch = position.GetBodyAngle(JointData::ID_R_HIP_PITCH);
    double knee = position.GetBodyAngle(JointData::ID_R_KNEE);
    double ankleRoll = position.GetBodyAngle(JointData::ID_R_ANKLE_ROLL);
    double anklePitch = position.GetBodyAngle(JointData::ID_R_ANKLE_PITCH);

    return Leg::GetPosition(hipYaw, hipRoll, hipPitch, knee, ankleRoll, anklePitch);
}

void RightLeg::SetEnable(bool enable, bool exclusive)
{
    m_Joint.SetEnableBody(false);
    m_Joint.SetEnableRightLegOnly(enable,exclusive);
}

void RightLeg::Process()
{
	if(isExtending)
		ProcessExtending();
	
    if(isMoving)
    {
		if(moveMethod == NULL)
			moveMethod = Limb::ApproachByOne;

		bool allJointsFinished = true;
		int jointsWeCareAbout[] = {
			JointData::ID_R_HIP_YAW,
			JointData::ID_R_HIP_ROLL,
			JointData::ID_R_HIP_PITCH,
			JointData::ID_R_KNEE,
			JointData::ID_R_ANKLE_ROLL,
			JointData::ID_R_ANKLE_PITCH
		};

		double current, target, next;

		for(int i=0; i<6; i++)
		{
			int id = jointsWeCareAbout[i];

			if(m_Joint.GetEnable(id))
			{
				current = m_Joint.GetBodyAngle(id);

				switch(id)
				{
				case JointData::ID_R_HIP_YAW:
					target = this->targetHipYawAngle;
					break;

				case JointData::ID_R_HIP_ROLL:
					target = this->targetHipRollAngle;
					break;

				case JointData::ID_R_HIP_PITCH:
					target = this->targetHipPitchAngle;
					break;

				case JointData::ID_R_KNEE:
					target = this->targetKneeAngle;
					break;

				case JointData::ID_R_ANKLE_ROLL:
					target = this->targetAnkleRollAngle;
					break;

				case JointData::ID_R_ANKLE_PITCH:
					target = this->targetAnklePitchAngle;
					break;

				default:
					cerr << "[warning] invalid ID for LeftLeg::Process (" << id << ")" << endl;
					target = current;
					break;
				}

				next = moveMethod(current, target);

				if(next != current)
					allJointsFinished = false;

				// write the angle to the joint data
				m_Joint.SetBodyAngle(id,next);
			}
		}

		if(allJointsFinished)
			isMoving = false;
    }
}

void RightLeg::SetAngles(JointData &joints)
{
    Leg::SetAngles(joints.GetBodyAngle(JointData::ID_R_HIP_YAW),joints.GetBodyAngle(JointData::ID_R_HIP_ROLL),joints.GetBodyAngle(JointData::ID_R_HIP_PITCH),
                   joints.GetBodyAngle(JointData::ID_R_KNEE),joints.GetBodyAngle(JointData::ID_R_ANKLE_ROLL), joints.GetBodyAngle(JointData::ID_R_ANKLE_PITCH));
}

double RightLeg::GetToeAngle()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_HIP_YAW);
}

double RightLeg::GetKneeAngle()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_KNEE);
}

double RightLeg::GetHipPitch()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_HIP_PITCH);
}

double RightLeg::GetHipRoll()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_HIP_ROLL);
}

double RightLeg::GetAnkleRoll()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_ANKLE_ROLL);
}

double RightLeg::GetAnklePitch()
{
    return m_Joint.GetBodyAngle(JointData::ID_R_ANKLE_PITCH);
}
