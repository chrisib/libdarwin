/*
 *   Head.cpp
 *
 *   Author: ROBOTIS
 *
 */

#include <stdio.h>
#include <iostream>
#include "MX28.h"
#include "Kinematics.h"
#include "MotionStatus.h"
#include "Head.h"
#include <cmath>
#include "Camera.h"
#include "Target.h"
#include "SingleBlob.h"
#include "MultiBlob.h"

using namespace Robot;
using namespace std;


Head* Head::m_UniqueInstance = new Head();

Head::Head() : Limb(false)
{
	m_Pan_p_gain = 0.1;
	m_Pan_d_gain = 0.22;

    m_Tilt_p_gain = 0.1;
	m_Tilt_d_gain = 0.22;

	m_LeftLimit = 70;
	m_RightLimit = -70;
	m_TopLimit = Kinematics::EYE_TILT_OFFSET_ANGLE + 20;
	m_BottomLimit = Kinematics::EYE_TILT_OFFSET_ANGLE - 65;

	m_Pan_Home = 0.0;
	m_Tilt_Home = Kinematics::EYE_TILT_OFFSET_ANGLE - 30.0;

	m_moving = false;

	m_Joint.SetEnableHeadOnly(true);

    for(int i=0; i<NUM_JOINTS; i++)
        jointPositions.push_back(Point3D());
}

Head::~Head()
{
}

void Head::CheckLimit()
{
	if(m_PanAngle > m_LeftLimit)
		m_PanAngle = m_LeftLimit;
	else if(m_PanAngle < m_RightLimit)
		m_PanAngle = m_RightLimit;

	if(m_TiltAngle > m_TopLimit)
		m_TiltAngle = m_TopLimit;
	else if(m_TiltAngle < m_BottomLimit)
		m_TiltAngle = m_BottomLimit;

	m_moving = true;
}

void Head::Initialize()
{
	m_PanAngle = MotionStatus::m_CurrentJoints.GetAngle(JointData::ID_HEAD_PAN);
	m_TiltAngle = -MotionStatus::m_CurrentJoints.GetAngle(JointData::ID_HEAD_TILT);
	CheckLimit();

	InitTracking();
	MoveToHome();
}

void Head::LoadINISettings(minIni* ini)
{
    LoadINISettings(ini, HEAD_SECTION);
}

void Head::LoadINISettings(minIni* ini, const std::string &section)
{
    double value = INVALID_VALUE;

    if((value = ini->getd(section, "pan_p_gain", INVALID_VALUE)) != INVALID_VALUE)  m_Pan_p_gain = value;
    if((value = ini->getd(section, "pan_d_gain", INVALID_VALUE)) != INVALID_VALUE)  m_Pan_d_gain = value;
    if((value = ini->getd(section, "tilt_p_gain", INVALID_VALUE)) != INVALID_VALUE) m_Tilt_p_gain = value;
    if((value = ini->getd(section, "tilt_d_gain", INVALID_VALUE)) != INVALID_VALUE) m_Tilt_d_gain = value;
    if((value = ini->getd(section, "left_limit", INVALID_VALUE)) != INVALID_VALUE)  m_LeftLimit = value;
    if((value = ini->getd(section, "right_limit", INVALID_VALUE)) != INVALID_VALUE) m_RightLimit = value;
    if((value = ini->getd(section, "top_limit", INVALID_VALUE)) != INVALID_VALUE)   m_TopLimit = value;
    if((value = ini->getd(section, "bottom_limit", INVALID_VALUE)) != INVALID_VALUE)m_BottomLimit = value;
    if((value = ini->getd(section, "pan_home", INVALID_VALUE)) != INVALID_VALUE)    m_Pan_Home = value;
    if((value = ini->getd(section, "tilt_home", INVALID_VALUE)) != INVALID_VALUE)   m_Tilt_Home = value;
}

void Head::SaveINISettings(minIni* ini)
{
    SaveINISettings(ini, HEAD_SECTION);
}

void Head::SaveINISettings(minIni* ini, const std::string &section)
{
    ini->put(section,   "pan_p_gain",   m_Pan_p_gain);
    ini->put(section,   "pan_d_gain",   m_Pan_d_gain);
    ini->put(section,   "tilt_p_gain",  m_Tilt_p_gain);
    ini->put(section,   "tilt_d_gain",  m_Tilt_d_gain);
    ini->put(section,   "left_limit",   m_LeftLimit);
    ini->put(section,   "right_limit",  m_RightLimit);
    ini->put(section,   "top_limit",    m_TopLimit);
    ini->put(section,   "bottom_limit", m_BottomLimit);
    ini->put(section,   "pan_home",     m_Pan_Home);
    ini->put(section,   "tilt_home",    m_Tilt_Home);
}

void Head::MoveToHome()
{
	MoveByAngle(m_Pan_Home, m_Tilt_Home);
}

void Head::MoveByAngle(double pan, double tilt)
{
	m_PanAngle = pan;
	m_TiltAngle = tilt;

	CheckLimit();
}

void Head::MoveByAngleOffset(double pan, double tilt)
{
	MoveByAngle(m_PanAngle + pan, m_TiltAngle + tilt);
}

void Head::InitTracking()
{
	m_Pan_err = 0;
	m_Pan_err_diff = 0;
	m_Tilt_err = 0;
	m_Tilt_err_diff = 0;
}

void Head::MoveTracking(Point2D err)
{
	m_Pan_err_diff = err.X - m_Pan_err;
	m_Pan_err = err.X;

	m_Tilt_err_diff = err.Y - m_Tilt_err;
	m_Tilt_err = err.Y;

	MoveTracking();
}

void Head::MoveTrackingWithDamping(Point2D err, double dampingFactor)
{
	m_Pan_err_diff = err.X - m_Pan_err;
	m_Pan_err = err.X;

	m_Tilt_err_diff = err.Y - m_Tilt_err;
	m_Tilt_err = err.Y;

	MoveTrackingWithDamping(dampingFactor);
}

void Head::MoveTracking()
{
	double pOffset, dOffset;

	pOffset = m_Pan_err * m_Pan_p_gain;
	pOffset *= pOffset;
	if(m_Pan_err < 0)
		pOffset = -pOffset;
	dOffset = m_Pan_err_diff * m_Pan_d_gain;
	dOffset *= dOffset;
	if(m_Pan_err_diff < 0)
		dOffset = -dOffset;
	m_PanAngle += (pOffset + dOffset);

	pOffset = m_Tilt_err * m_Tilt_p_gain;
	pOffset *= pOffset;
	if(m_Tilt_err < 0)
		pOffset = -pOffset;
	dOffset = m_Tilt_err_diff * m_Tilt_d_gain;
	dOffset *= dOffset;
	if(m_Tilt_err_diff < 0)
		dOffset = -dOffset;
	m_TiltAngle += (pOffset + dOffset);

	CheckLimit();
}

void Head::MoveTrackingWithDamping(double dampingFactor)
{
	double pOffset, dOffset;

	pOffset = m_Pan_err * m_Pan_p_gain;
	pOffset *= pOffset;
	if(m_Pan_err < 0)
		pOffset = -pOffset;
	dOffset = m_Pan_err_diff * m_Pan_d_gain;
	dOffset *= dOffset;
	if(m_Pan_err_diff < 0)
		dOffset = -dOffset;
	m_PanAngle += (pOffset + dOffset)*dampingFactor;

	pOffset = m_Tilt_err * m_Tilt_p_gain;
	pOffset *= pOffset;
	if(m_Tilt_err < 0)
		pOffset = -pOffset;
	dOffset = m_Tilt_err_diff * m_Tilt_d_gain;
	dOffset *= dOffset;
	if(m_Tilt_err_diff < 0)
		dOffset = -dOffset;
	m_TiltAngle += (pOffset + dOffset)*dampingFactor;

	CheckLimit();
}

// SBD
void Head::MoveTo(Point2D& target)
{
	Point2D center(Camera::WIDTH/2, Camera::HEIGHT/2);

	Point2D point = target - center;
	point *= -1; // Invert X-axis, Y-axis
	point.X *= (Camera::VIEW_H_ANGLE / (double) Camera::WIDTH); // pixel per angle
	point.Y *= (Camera::VIEW_V_ANGLE / (double) Camera::HEIGHT); // pixel per angle

	MoveTracking(point);
}

// SBD
void Head::LookAt(SingleBlob& target, double dampingFactor)
{
	if (!target.WasFound())
	{
		cerr << "Target was not found; cannot look at it" << endl;
		return;
	}

	Point2D point = target.GetBoundingBox()->center;
	Point2D center = Point2D(Camera::WIDTH/2, Camera::HEIGHT/2);

	point -= center;
	point *= -1; // Invert X-axis, Y-axis
	point.X *= (Camera::VIEW_H_ANGLE / (double) Camera::WIDTH); // pixel per angle
	point.Y *= (Camera::VIEW_V_ANGLE / (double) Camera::HEIGHT); // pixel per angle

	//std::cout << "Looking at x: " << point.X << ", y: " << point.Y << std::endl;

	MoveTrackingWithDamping(point, dampingFactor);
}

// SBD
// TODO: If this doesn't pan out, delete it. LookAt does the same thing.
void Head::LookAt2(SingleBlob& target)
{
	if(!target.WasFound()) return;

	Point2D point = target.GetBoundingBox()->center;
	Point2D center = Point2D(Camera::WIDTH/2, Camera::HEIGHT/2);

	point -= center;
	point *= -1; // Invert X-axis, Y-axis
	point.X *= (Camera::VIEW_H_ANGLE / (double) Camera::WIDTH); // pixel per angle
	point.Y *= (Camera::VIEW_V_ANGLE / (double) Camera::HEIGHT); // pixel per angle

	MoveByAngleOffset(point.X, point.Y);
}

// SBD
// Try this one more time...
// Only works if vision processing is in another thread
void Head::LookAt3(SingleBlob& target, int speed) {
	static const Point2D centre(Camera::WIDTH/2, Camera::HEIGHT/2); // Not going to change cameras halfway through
	const int vision_offset = (10 > speed) ? 10 : speed;
	Point2D point;

	bool inPosition = false;
	while(!inPosition) {
		if(target.WasFound()) {
			point = target.GetBoundingBox()->center;
			point -= centre;
			point *= -1;

			int moveX, moveY;
			if (point.X < -vision_offset) {
				moveX = -speed;
			} else if (point.X > vision_offset) {
				moveX = speed;
			} else {
				moveX = 0;
			}

			if (point.Y < -vision_offset) {
				moveY = -speed;
			} else if (point.Y > vision_offset) {
				moveY = speed;
			} else {
				moveY = 0;
			}

			if (moveX == 0 && moveY == 0) {
				inPosition = true;
			} else {
				this->MoveByAngleOffset(moveX, moveY);
			}
		} else {
			inPosition = true;
		}

		for(int i = 0; i < 10; i++) usleep(8000);
	}
}

void Head::Process()
{
	if(m_Joint.GetEnable(JointData::ID_HEAD_PAN) == true)
		m_Joint.SetAngle(JointData::ID_HEAD_PAN, m_PanAngle);

	if(m_Joint.GetEnable(JointData::ID_HEAD_TILT) == true)
		m_Joint.SetAngle(JointData::ID_HEAD_TILT, m_TiltAngle);

	m_moving = false;
}

void Head::SetEnable(bool enable, bool exclusive)
{
	m_Joint.SetEnableHeadOnly(enable, exclusive);
}

Point3D Head::GetPosition()
{
    return GetPosition(m_Joint);
}

Point3D Head::GetPosition(JointData &position)
{
    return GetPosition(position.GetBodyAngle(JointData::ID_HEAD_PAN),position.GetBodyAngle(JointData::ID_HEAD_TILT));
}

Point3D Head::GetPosition(double pan, double tilt)
{
#ifdef PYTHON_CODE
    #panHome = Translation(0,0,Kinematics.TORSO_LENGTH-Kinematics.HIP_DROP_LENGTH)
    panHome = np.eye(4)
    panJoint = panHome.dot(RotationZ(pan))

    tiltHome = panJoint.dot(Translation(0,0,Kinematics.NECK_LENGTH).dot(RotationX(deg2rad(90))))
    tiltJoint = tiltHome.dot(RotationZ(tilt))

    cameraHome = tiltJoint.dot(Translation(Kinematics.NECK_TO_FACE,0,0))

    psn = [cameraHome[0][3],cameraHome[1][3],cameraHome[2][3]]
#endif
    Matrix3D panHome;   // measure from the base of the neck as the origin
    Matrix3D panJoint = panHome * Matrix3D::RotationZ(pan);

    Matrix3D tiltHome = panJoint * Matrix3D::Translation(0,0,Kinematics::NECK_LENGTH) * Matrix3D::RotationX(90);
    Matrix3D tiltJoint = tiltHome * Matrix3D::RotationZ(tilt);

    Matrix3D cameraHome = tiltJoint * Matrix3D::Translation(Kinematics::NECK_TO_FACE,0,0);

    Point3D endEffector(cameraHome.m[Matrix3D::m03],
                        cameraHome.m[Matrix3D::m13],
                        cameraHome.m[Matrix3D::m23]);

    Point3D origin;
    Point3D tiltPosition(tiltHome.m[Matrix3D::m03],
                         tiltHome.m[Matrix3D::m13],
                         tiltHome.m[Matrix3D::m23]);

    jointPositions[JOINT_NECK_PAN] = origin;
    jointPositions[JOINT_NECK_TILT] = tiltPosition;
    jointPositions[JOINT_FACE] = endEffector;

    return endEffector;
}

Point3D Head::GetAbsolutePosition()
{
    return GetAbsolutePosition(m_Joint);
}

Point3D Head::GetAbsolutePosition(JointData &position)
{
    Point3D psn = GetPosition(position);
    psn.Z += Kinematics::TORSO_LENGTH-Kinematics::HIP_DROP_LENGTH;
    if(INVERTED_Y_AXIS)
        psn.Y = -psn.Y;

    return psn;
}

void Head::SetAngles(JointData &joints)
{
    m_Joint.SetValue(JointData::ID_HEAD_PAN,joints.GetValue(JointData::ID_HEAD_PAN));
    m_Joint.SetValue(JointData::ID_HEAD_TILT,joints.GetValue(JointData::ID_HEAD_TILT));
}

void Head::SetGoalPosition(Point3D &position)
{
    // first off determine the neck tilt angle that will get the face to the desired Z position
    double tilt = asin(position.Z/Kinematics::NECK_TO_FACE);
    double dx = Kinematics::NECK_TO_FACE*cos(tilt); // distance along x-axis to face after setting the tilt (but not setting pan)
    double pan = asin(position.Y/dx);

    MoveByAngle(rad2deg(pan),rad2deg(tilt));

}

void Head::Extend(double speed)
{
    (void)speed;
    return;
}
