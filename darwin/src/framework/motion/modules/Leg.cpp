/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class allows is a generic superclass for a Darwin leg; it will
 * eventually be used to contain inverse-kinematics
 ************************************************************************/

#include "Leg.h"
#include "Point.h"
#include "Kinematics.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
#include "Matrix.h"

using namespace Robot;
using namespace std;

// length of the fully-extended leg without the ankle and hip drop offsets
const double Leg::MAX_LEG_LENGTH = Kinematics::CALF_LENGTH + Kinematics::THIGH_LENGTH;

// fully retracted leg without the ankle and hip drops
const double Leg::MIN_LEG_LENGTH = 80;  //mm; minimum measured distance when in kneeling position

Leg::Leg(bool invertedYAxis) : Limb(invertedYAxis)
{
    HIP_PITCH_OFFSET = 0.0;
    HIP_ROLL_OFFSET = 0.0;
    HIP_YAW_OFFSET = 0.0;
    KNEE_OFFSET = 0.0;
    ANKLE_ROLL_OFFSET = 0.0;
    ANKLE_PITCH_OFFSET = 0.0;

    isMoving = false;

    for(int i=0; i<NUM_JOINTS; i++)
    {
        jointPositions.push_back(Point3D());
    }

    stopExtendingOnKneeTorque = true;
    stopExtendingWhenFootOnGround = true;
    MAX_KNEE_TORQUE = 400;
}

Point3D Leg::GetPosition(double hipYaw, double hipRoll, double hipPitch, double knee, double ankleRoll, double anklePitch)
{
#ifdef PYTHON_CODE
    hipYawHome = np.eye(4)   # the z-axis goes through the yaw motor already, so nothing to change
    #hipYawHome = Translation(0,Kinematics.HIP_WIDTH/2,0)
    hipYawJoint = hipYawHome.dot(RotationZ(yaw))

    hipRollHome = hipYawJoint.dot(Translation(0,0,-Kinematics.HIP_DROP_LENGTH).dot(RotationX(deg2rad(90)).dot(RotationY(deg2rad(90)))))
    hipRollJoint = hipRollHome.dot(RotationZ(hipRoll))

    hipPitchHome = hipRollJoint.dot(RotationY(deg2rad(-90)))
    hipPitchJoint = hipPitchHome.dot(RotationZ(hipPitch))

    kneeHome = hipPitchJoint.dot(Translation(0,-Kinematics.THIGH_LENGTH,0))
    kneeJoint = kneeHome.dot(RotationZ(-knee))

    anklePitchHome = kneeJoint.dot(Translation(0,-Kinematics.CALF_LENGTH,0))
    anklePitchJoint = anklePitchHome.dot(RotationZ(anklePitch))

    ankleRollHome = anklePitchJoint.dot(RotationY(deg2rad(90)))
    ankleRollJoint = ankleRollHome.dot(RotationZ(-ankleRoll))

    footHome = ankleRollJoint.dot(Translation(0,-Kinematics.ANKLE_LENGTH,0))
    footJoint = footHome
    footJoint = footHome
#endif

    //cout << hipYaw << " " <<
    //        hipRoll << " " <<
    //        hipPitch << " " <<
    //        knee << " " <<
    //        ankleRoll << " " <<
    //        anklePitch << endl << endl;

    // assume we're working with the left leg:
    // +x is forward, +y is left, +z is down
    // and the end we'll flip values as necessary to fit with
    // our standard coordinate system
    double x,y,z;

    Matrix3D hipYawHome; // identity matrix since the hip pitch is the origin with all the axes in the right place
    Matrix3D hipYawJoint = hipYawHome * Matrix3D::RotationZ(hipYaw);

    Matrix3D hipRollHome = hipYawJoint * Matrix3D::Translation(0,0,-Kinematics::HIP_DROP_LENGTH) * Matrix3D::RotationX(90) * Matrix3D::RotationY(90);
    Matrix3D hipRollJoint = hipRollHome * Matrix3D::RotationZ(hipRoll);

    Matrix3D hipPitchHome = hipRollJoint * Matrix3D::RotationY(-90);
    Matrix3D hipPitchJoint = hipPitchHome * Matrix3D::RotationZ(hipPitch);

    Matrix3D kneeHome = hipPitchJoint * Matrix3D::Translation(0,-Kinematics::THIGH_LENGTH,0);
    Matrix3D kneeJoint = kneeHome * Matrix3D::RotationZ(-knee);

    Matrix3D anklePitchHome = kneeJoint * Matrix3D::Translation(0,-Kinematics::CALF_LENGTH,0);
    Matrix3D anklePitchJoint = anklePitchHome * Matrix3D::RotationZ(anklePitch);

    Matrix3D ankleRollHome = anklePitchJoint * Matrix3D::RotationY(90);
    Matrix3D ankleRollJoint = ankleRollHome * Matrix3D::RotationZ(-ankleRoll);

    endEffector = ankleRollJoint * Matrix3D::Translation(0,-Kinematics::ANKLE_LENGTH,0);

    jointPositions[JOINT_HIP_YAW] = Point3D(hipYawHome.m[Matrix3D::m03],hipYawHome.m[Matrix3D::m13],hipYawHome.m[Matrix3D::m23]);
    jointPositions[JOINT_HIP_ROLL] = Point3D(hipRollHome.m[Matrix3D::m03],hipRollHome.m[Matrix3D::m13],hipRollHome.m[Matrix3D::m23]);
    jointPositions[JOINT_HIP_PITCH] = Point3D(hipPitchHome.m[Matrix3D::m03],hipPitchHome.m[Matrix3D::m13],hipPitchHome.m[Matrix3D::m23]);
    jointPositions[JOINT_KNEE] = Point3D(kneeHome.m[Matrix3D::m03],kneeHome.m[Matrix3D::m13],kneeHome.m[Matrix3D::m23]);
    jointPositions[JOINT_ANKLE_PITCH] = Point3D(anklePitchHome.m[Matrix3D::m03],anklePitchHome.m[Matrix3D::m13],anklePitchHome.m[Matrix3D::m23]);
    jointPositions[JOINT_ANKLE_ROLL] = Point3D(ankleRollHome.m[Matrix3D::m03],ankleRollHome.m[Matrix3D::m13],ankleRollHome.m[Matrix3D::m23]);
    jointPositions[JOINT_FOOT] = Point3D(endEffector.m[Matrix3D::m03],endEffector.m[Matrix3D::m13],endEffector.m[Matrix3D::m23]);

    x = endEffector.m[Matrix3D::m03];
    y = endEffector.m[Matrix3D::m13];
    z = endEffector.m[Matrix3D::m23];

    Point3D result(x,y,z);
    return result;
}

Point3D Leg::GetInsideToePosition()
{
    return GetInsideToePosition(endEffector);
}

Point3D Leg::GetOutsideToePosition()
{
    return GetOutsideToePosition(endEffector);
}

Point3D Leg::GetInsideHeelPosition()
{
    return GetInsideHeelPosition(endEffector);
}

Point3D Leg::GetOutsideHeelPosition()
{
    return GetOutsideHeelPosition(endEffector);
}


Point3D Leg::GetInsideToePosition(Matrix3D &endEffector)
{
    Matrix3D m = endEffector * Matrix3D::Translation(Kinematics::ANKLE_TO_OUTSTEP-Kinematics::FOOT_WIDTH, 0, Kinematics::ANKLE_TO_TOE);

    double x = m.m[Matrix3D::m03];
    double y = m.m[Matrix3D::m13];
    double z = m.m[Matrix3D::m23];

    Point3D result(x,y,z);
    return result;
}

Point3D Leg::GetOutsideToePosition(Matrix3D &endEffector)
{
    Matrix3D m = endEffector * Matrix3D::Translation(Kinematics::ANKLE_TO_OUTSTEP, 0, Kinematics::ANKLE_TO_TOE);

    double x = m.m[Matrix3D::m03];
    double y = m.m[Matrix3D::m13];
    double z = m.m[Matrix3D::m23];

    Point3D result(x,y,z);
    return result;
}

Point3D Leg::GetInsideHeelPosition(Matrix3D &endEffector)
{
    Matrix3D m = endEffector * Matrix3D::Translation(Kinematics::ANKLE_TO_OUTSTEP-Kinematics::FOOT_WIDTH, 0, Kinematics::ANKLE_TO_TOE-Kinematics::FOOT_LENGTH);

    double x = m.m[Matrix3D::m03];
    double y = m.m[Matrix3D::m13];
    double z = m.m[Matrix3D::m23];

    Point3D result(x,y,z);
    return result;
}

Point3D Leg::GetOutsideHeelPosition(Matrix3D &endEffector)
{
    Matrix3D m = endEffector * Matrix3D::Translation(Kinematics::ANKLE_TO_OUTSTEP, 0, Kinematics::ANKLE_TO_TOE-Kinematics::FOOT_LENGTH);

    double x = m.m[Matrix3D::m03];
    double y = m.m[Matrix3D::m13];
    double z = m.m[Matrix3D::m23];

    Point3D result(x,y,z);
    return result;
}

Point3D Leg::GetAbsolutePosition()
{
    return GetAbsolutePosition(m_Joint);
}

Point3D Leg::GetAbsolutePosition(JointData &position)
{
    Point3D psn = GetPosition(position);
    psn.Y += Kinematics::HIP_WIDTH/2.0;

    if(INVERTED_Y_AXIS)
        psn.Y = -psn.Y;
    return psn;
}

//void Leg::SetGoalPosition(Point3D &endEffectorPosition, double toeAngle)
//{
//    SetGoalPosition(endEffectorPosition, toeAngle, Limb::ApproachByOne);
//}

void Leg::SetGoalPosition(Point3D &endEffectorPosition)
{
    SetGoalPosition(endEffectorPosition,GetToeAngle());
}

void Leg::SetGoalPosition(Point3D &endEffectorPosition, double toeAngle)//, MoveFunc moveFunc)
{
    // calculate the joint angles for each motor based on a few constraints:
    // 1- knee controls leg length
    // 2- foot should be parallel to hip (TODO: allow configurable offsets, or use FSR to adjust ankle positions)
    // 3- hip roll and pitch are decomposed into sin/cos components based on hip yaw
    // 4- hip yaw is fixed according to toeAngle

    if(this->moveMethod == NULL)
        this->moveMethod = Limb::ApproachByOne;

    // length of the adjustable portion of the leg (i.e. shin + thigh, but not ankle and hip drop)
    // these non-configurable offsets are removed from the Z position
    double adjustableHeight = fabs(endEffectorPosition.Z) - Kinematics::ANKLE_LENGTH - Kinematics::HIP_DROP_LENGTH;
    double hipToAnkle = sqrt(pow(endEffectorPosition.X,2.0) + pow(endEffectorPosition.Y, 2.0 )+ pow(adjustableHeight, 2.0));    // length of straight line from robot's hip to ankle motors
    double kneeAngle = CalculateKneeAngle(hipToAnkle);

    // Calculate the hip pitch and roll angles (which function at 90-degrees to each other,
    // but may not be aligned parallel to the X and Y axes of the robot)
    // Because the thigh and calf are the same length the math it simplified;
    // the leg forms an isoceles triangle with the knee as the apex.
    // First off get the overall angles imagining the leg is a straight line of the desired length
    // (discarding the fixed vertical drops for the hip and ankle brackets)
    double xAngle = -rad2deg(atan2(endEffectorPosition.X, hipToAnkle));
    double yAngle = rad2deg(atan2(endEffectorPosition.Y, hipToAnkle));

    // calculate the mirrored angles of the isosceles triangle formed by the bent leg
    // NOTE: this only works because the thigh and shin are the same lengths
    double interiorAngle = (180.0 - kneeAngle)/2.0;

    // now we need to calculate the actual motor angles, using all of the above
    double pitchAngle = 90 - (cos(deg2rad(toeAngle)) * xAngle + sin(deg2rad(toeAngle)) * yAngle + cos(deg2rad(toeAngle)) * interiorAngle);
    double rollAngle = sin(deg2rad(toeAngle)) * xAngle + cos(deg2rad(toeAngle)) * yAngle + sin(deg2rad(toeAngle)) * interiorAngle;

    // write the target angles to out member variables
    this->targetHipYawAngle = toeAngle + HIP_YAW_OFFSET;
    this->targetHipRollAngle = rollAngle + HIP_ROLL_OFFSET;
    this->targetHipPitchAngle = pitchAngle + HIP_PITCH_OFFSET;
    this->targetKneeAngle = kneeAngle + KNEE_OFFSET;

    // set the ankle angles to the same as the hips
    // TODO: allow optional offsets and/or use FSR to fine-tune ankle motors
    this->targetAnkleRollAngle = rollAngle + ANKLE_ROLL_OFFSET;
    this->targetAnklePitchAngle = -pitchAngle + kneeAngle + ANKLE_PITCH_OFFSET;

    //this->moveMethod = moveFunc;
    isMoving = true;

#ifdef DEBUG_KINEMATICS
    cerr << "Target Angles:" << endl <<
            "\t[forward]   : " << xAngle << endl <<
            "\t[lateral]   : " << yAngle << endl <<
            "\tHip Yaw     : " << targetHipYawAngle << endl <<
            "\tHip Roll    : " << targetHipRollAngle << " ( + " << HIP_ROLL_OFFSET << ")" << endl <<
            "\tHip Pitch   : " << targetHipPitchAngle << " ( + " << HIP_PITCH_OFFSET << ")" << endl <<
            "\tKnee        : " << targetKneeAngle << " ( + " << KNEE_OFFSET << ")" << endl <<
            "\tAnkle Roll   : " << targetAnkleRollAngle << " ( + " << ANKLE_ROLL_OFFSET << ")" << endl <<
            "\tAnkle Pitch : " << targetAnklePitchAngle << " ( + " << ANKLE_PITCH_OFFSET << ")" << endl <<
            endl;
#endif
}

void Leg::SetGoalPosition(double length, double pitch, double roll, double yaw)//, MoveFunc moveFunc)
{
    if(this->moveMethod == NULL)
        this->moveMethod = Limb::ApproachByOne;

    double kneeAngle = CalculateKneeAngle(length);

    // write the target angles to out member variables
    this->targetHipYawAngle = yaw + HIP_YAW_OFFSET;
    this->targetHipRollAngle = roll + HIP_ROLL_OFFSET;
    this->targetHipPitchAngle = pitch + HIP_PITCH_OFFSET;
    this->targetKneeAngle = kneeAngle + KNEE_OFFSET;

    // set the ankle angles to the same as the hips
    // TODO: allow optional offsets and/or use FSR to fine-tune ankle motors
    this->targetAnkleRollAngle = roll + ANKLE_ROLL_OFFSET;
    this->targetAnklePitchAngle = pitch + ANKLE_PITCH_OFFSET;

    //this->moveMethod = moveFunc;
    isMoving = true;

#ifdef DEBUG_KINEMATICS
    cerr << "Target Angles:" << endl <<
            "\tHip Yaw     : " << targetHipYawAngle << endl <<
            "\tHip Roll    : " << targetHipRollAngle << " ( + " << HIP_ROLL_OFFSET << ")" << endl <<
            "\tHip Pitch   : " << targetHipPitchAngle << " ( + " << HIP_PITCH_OFFSET << ")" << endl <<
            "\tKnee        : " << targetKneeAngle << " ( + " << KNEE_OFFSET << ")" << endl <<
            "\tAnkle Roll   : " << targetAnkleRollAngle << " ( + " << ANKLE_ROLL_OFFSET << ")" << endl <<
            "\tAnkle Pitch : " << targetAnklePitchAngle << " ( + " << ANKLE_PITCH_OFFSET << ")" << endl <<
            endl;
#endif
}

//void Leg::SetGoalPosition(double length, double pitch, double roll, double yaw)
//{
//    SetGoalPosition(length, pitch, roll, yaw, Limb::ImmediateMove);
//}

double Leg::CalculateKneeAngle(double adjustableLegLength)
{
    double kneeAngle;

    if(adjustableLegLength > MAX_LEG_LENGTH)
    {
        //cerr << "[warning]: cannot extend leg beyond " << MAX_LEG_LENGTH << "mm; requested position needs length of " << adjustableLegLength << "mm" << endl;
        return 0.0; // extend the knee as far as possible
    }
    else if(adjustableLegLength < MIN_LEG_LENGTH)
    {
        //cerr << "[warning]: cannot extend leg less than " << MIN_LEG_LENGTH << "mm; requested position needs length of " << adjustableLegLength << "mm" << endl;
        adjustableLegLength = MIN_LEG_LENGTH;
    }

    // figure out what length the knee needs to create the desired leg length
    // this gives us the interior angle of the knee
    kneeAngle = Math::GetInternalAngle(Kinematics::CALF_LENGTH, Kinematics::THIGH_LENGTH, adjustableLegLength);

    // the knee h/w angle is measured in-line with the thigh, so convert it
    kneeAngle = 180 - kneeAngle;

    return kneeAngle;
}

void Leg::SetAngles(double hipYaw, double hipRoll, double hipPitch, double knee, double ankleRoll, double anklePitch)//, MoveFunc function)
{
    if(this->moveMethod == NULL)
        this->moveMethod = Limb::ApproachByOne;

    targetHipYawAngle = hipYaw + HIP_YAW_OFFSET;
    targetHipRollAngle = hipRoll + HIP_ROLL_OFFSET;
    targetHipPitchAngle = hipPitch + HIP_PITCH_OFFSET;
    targetKneeAngle = knee + KNEE_OFFSET;
    targetAnkleRollAngle = ankleRoll + ANKLE_ROLL_OFFSET;
    targetAnklePitchAngle = anklePitch + ANKLE_PITCH_OFFSET;

    isMoving = true;
}

//void Leg::SetAngles(double hipYaw, double hipRoll, double hipPitch, double knee, double ankleRoll, double anklePitch)
//{
//    SetAngles(hipYaw, hipRoll, hipPitch, knee, ankleRoll, anklePitch, Limb::ImmediateMove);
//}

bool Leg::IsRunning()
{
    return isMoving;
}

void Leg::Extend(double speed)
{
    Extend(speed, true);
}

void Leg::Extend(double speed, bool stopOnFSR)
{
#if 0
    double toeAngle = GetToeAngle();
    Point3D presentPosition = GetPosition();
    Point3D goalPosition;

    // rotate the present position back to a toe angle of zero to make the math easier
    presentPosition.X /= sin(deg2rad(toeAngle));
    presentPosition.Y /= cos(deg2rad(toeAngle));

    // figure out the pitch and roll angles to the toe from the hip to the foot
    double xAngle = rad2deg(asin(presentPosition.X/Point2D(presentPosition.X,fabs(presentPosition.Z)).Magnitude()));
    double yAngle = rad2deg(asin(presentPosition.Y/Point2D(presentPosition.Y,fabs(presentPosition.Z)).Magnitude()));

    // use the sine law to calculate the angle between the hip-toe line and the thigh-toe lines when the knee is fully extended
    double gamma = xAngle/(Kinematics::THIGH_LENGTH+Kinematics::CALF_LENGTH) * Kinematics::HIP_DROP_LENGTH;

    // use the cosine law to calculate the distance between the hip and the foot when the leg is fully extended in the X-Z plane
    double thighToToe = sqrt(
                pow(Kinematics::HIP_DROP_LENGTH,2) +
                pow(Kinematics::THIGH_LENGTH+Kinematics::CALF_LENGTH,2) -
                2 * Kinematics::HIP_DROP_LENGTH * (Kinematics::THIGH_LENGTH+Kinematics::CALF_LENGTH) * cos(deg2rad(gamma))
    );
    goalPosition.X = sin(deg2rad(xAngle)) * thighToToe;
    goalPosition.Z = cos(deg2rad(xAngle)) * thighToToe;

    // repeat the same, but working in the Y-Z plane
    gamma = yAngle/(Kinematics::THIGH_LENGTH+Kinematics::CALF_LENGTH) * Kinematics::HIP_DROP_LENGTH;
    thighToToe = sqrt(
                pow(Kinematics::HIP_DROP_LENGTH,2) +
                pow(Kinematics::THIGH_LENGTH+Kinematics::CALF_LENGTH,2) -
                2 * Kinematics::HIP_DROP_LENGTH * (Kinematics::THIGH_LENGTH+Kinematics::CALF_LENGTH) * cos(deg2rad(gamma))
    );
    goalPosition.Y = sin(deg2rad(yAngle)) * thighToToe;

    // finally rotate the goal position back based on our initial toe angle
    goalPosition.X *= sin(deg2rad(toeAngle));
    goalPosition.Y *= cos(deg2rad(toeAngle));

    extensionGoalPosition = goalPosition;
#else
    // rather than calculate a goal position just flatten the knee joint
    // this doesn't actually involve any calculations here
#endif
    stopExtendingWhenFootOnGround = stopOnFSR;
    isMoving = true;
    extendSpeed = speed;
    isExtending = true;
}

void Leg::ProcessExtending()
{
    if(!isExtending)
        return;

#if 0
    // see if we've reached our goal position yet
    Point3D presentPosition = GetPosition();

    if(FootOnGround())
    {
        SetGoalPosition(presentPosition);
        extendSpeed = NOT_EXTENDING;
    }
    else if(fabs(presentPosition.Magnitude() - extensionGoalPosition.Magnitude()) < extendSpeed)
    {
        // we're within speed of the goal; move to that position and stop extending
        SetGoalPosition(extensionGoalPosition);
        extendSpeed = NOT_EXTENDING;
    }
    else
    {
        // otherwise extend our goal position by speed
        // this is basically just a p-controller to slowly move the goal position toward the fully-extended position
        Point3D vec = extensionGoalPosition-presentPosition;
        double scale = vec.Magnitude()*extendSpeed;

        Point3D tmpGoal = presentPosition + vec * scale;
        SetGoalPosition(tmpGoal);
    }
#else
	// if the foot is on the ground OR the knee angle is 0 then we've fully extended
	if(FootOnGround() || GetKneeAngle() == 0)
	{

        // finished extending
        extendSpeed = NOT_EXTENDING;
        isExtending = false;
        isMoving = false;
        GetPosition();
	}
	else
	{
		double knee = GetKneeAngle();
		knee -= extendSpeed;
		
		double hip = GetHipPitch();
		hip -= extendSpeed/2.0;
		
		double ankle = GetAnklePitch();
        ankle -= extendSpeed/2.0;
	
		SetAngles(GetToeAngle(), GetHipRoll(), hip, knee, GetAnkleRoll(), ankle);
	}
#endif
}

bool Leg::FootOnGround()
{
    //cout << GetXFSR() << " " << GetYFSR() << " " << GetKneeTorque() << endl;

    bool fsr = stopExtendingWhenFootOnGround && (GetXFSR() != -1 || GetYFSR() != -1);
    bool knee = stopExtendingOnKneeTorque && (GetKneeTorque() >= MAX_KNEE_TORQUE);

    return fsr || knee;
}

void Leg::LoadIniSettings(minIni *ini, const char *section)
{
    stopExtendingOnKneeTorque = !!ini->geti(section,"StopOnKneeTorque",1);
    stopExtendingWhenFootOnGround = !!ini->geti(section,"StopOnFSR",1);
    MAX_KNEE_TORQUE = ini->geti(section,"MaxKneeTorque",400);
}
