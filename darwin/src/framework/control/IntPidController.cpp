/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is a PID controller used to control the value of an int
 * Note: setValue is the value to be adjusted by the controller
 * targetValue is the value setValue is being adjusted towards
 * kP, kI, kD are the standard PID controller coefficients.
 ************************************************************************/

#include "IntPidController.h"

using namespace Robot;

IntPidController::IntPidController(int *setValue, int *targetValue, double kP, double kI, double kD)
    : PidController(kP,kI,kD)
{
    this->setValue = setValue;
    this->targetValue = targetValue;

    lastErrorIndex = 0;
    for(int i=0; i<MAX_INTEGRAL_ERRORS; i++)
    {
        previousErrors[i] = 0;
    }

    lastError = 0;
}

IntPidController::~IntPidController()
{
    //dtor
}

void IntPidController::Update()
{
    // calculate the current proportional error
    int proportionalError = *targetValue - *setValue;

    // calculate the current integral error by summing the previous errors, including the current one
    // first calculate the proportional, integral, and derivative errors
    lastErrorIndex = (lastErrorIndex + 1) % MAX_INTEGRAL_ERRORS;
    previousErrors[lastErrorIndex] = *targetValue - *setValue;
    int integralError = 0;
    for(int i=0; i<MAX_INTEGRAL_ERRORS; i++)
        integralError += previousErrors[i];
    int derivativeError = lastError - proportionalError;


    // now apply the formula m(t) = m(t-1) + kP * e(t) + kI * SUM(e(t)) + kD * e(t-1)
    double mT = *setValue + KP() * proportionalError + KI() * integralError + KD() * derivativeError;
    *setValue = (int)mT;


    // save the current proportional error; it is used when calculating the next derivative error
    lastError = proportionalError;
}

double IntPidController::MoveFunction(double currentPosition, double targetPosition)
{
    (void)currentPosition;
    (void)targetPosition;
    Update();
    return (double) *setValue;
}
