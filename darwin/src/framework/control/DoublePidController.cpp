/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is a PID controller used to control the value of a double
 * Note: setValue is the value to be adjusted by the controller
 * targetValue is the value setValue is being adjusted towards
 * kP, kI, kD are the standard PID controller coefficients.
 ************************************************************************/

#include "DoublePidController.h"
#include <iostream>

using namespace Robot;
using namespace std;

//#define DEBUG

DoublePidController::DoublePidController(double *setValue, double *targetValue, double kP, double kI, double kD)
    : PidController(kP,kI,kD)
{
    this->setValue = setValue;
    this->targetValue = targetValue;

    lastErrorIndex = 0;
    for(int i=0; i<MAX_INTEGRAL_ERRORS; i++)
    {
        previousErrors[i] = 0;
    }

    lastError = 0;
    lastErrorIndex = 0;
}

DoublePidController::~DoublePidController()
{
    //dtor
}

void DoublePidController::Update()
{
//    cout << *setValue << " --> " << *targetValue << endl;


    // calculate the current proportional error
    double proportionalError = *targetValue - *setValue;

    // calculate the current integral error by summing the previous errors, including the current one
    // first calculate the proportional, integral, and derivative errors
    lastErrorIndex = (lastErrorIndex + 1) % MAX_INTEGRAL_ERRORS;
    previousErrors[lastErrorIndex] = *targetValue - *setValue;
    double integralError = 0;
    for(int i=0; i<MAX_INTEGRAL_ERRORS; i++)
        integralError += previousErrors[i];
    double derivativeError = lastError - proportionalError;

    if(DEBUG_PRINT)
        cout << "goal = " << (*targetValue) << endl <<
                "set = " << (*setValue) << endl <<
                "kP = " << kP << endl <<
                "kI = " << kI << endl <<
                "kD = " << kD << endl <<
                "eP = " << proportionalError << endl <<
                "eI = " << integralError << endl <<
                "eD = " << derivativeError << endl;

    // now apply the formula m(t) = m(t-1) + kP * e(t) + kI * SUM(e(t)) + kD * e(t-1)
    double mT = *setValue + kP * proportionalError + kI * integralError + kD * derivativeError;
    if(DEBUG_PRINT)
        cout << "mT = " << mT << endl;
    *setValue = mT;


    // save the current proportional error; it is used when calculating the next derivative error
    lastError = proportionalError;
}

double DoublePidController::MoveFunction(double currentPosition, double targetPosition)
{
    (void)currentPosition;
    (void)targetPosition;
    Update();
    return *setValue;
}
