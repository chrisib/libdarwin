/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is a generic PID controller superclass.  It provides no
 * useful control mechanisms, but should be inherited to allow control
 * of a specific variable (e.g. DoublePidController, IntPidController)
 ************************************************************************/

#include "PidController.h"

using namespace Robot;

PidController::PidController(double kP, double kI, double kD)
{
    this->kP = kP;
    this->kI = kI;
    this->kD = kD;

    DEBUG_PRINT = false;
}

PidController::~PidController()
{
    //dtor
}
