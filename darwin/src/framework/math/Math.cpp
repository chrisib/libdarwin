/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Simon Barber-Dueck 
 *
 * This file provides general math functions that could be useful
 * in more than one place
 ************************************************************************/

#include "Math.h"
#include <assert.h>

//using namespace std;

// calculate the size of the third edge of a triangle using
// the cosine rule:
//   c^2 = a^2 + b^2 - 2ab*cos(AB)
double Math::GetMissingLength(double lenA, double lenB, double degrees)
{
    double len = pow(lenA,2.0) + pow(lenB,2.0) - 2*lenA*lenB*cos(deg2rad(degrees));
    return sqrt(len);
}

// calculate the angle AB based on the lengths of all sides in a triangle
// using the cosine rule refactored:
// c^2 = a^2 + b^2 - 2ab*cos(C)
// -2ab*cos(C) = c^2 - a^2 - b^2
// 2ab*cos(C) = a^2 + b^2 - c^2
// cos(C) = (a^2 + b^2 - c^2)/2ab
double Math::GetInternalAngle(double lenA, double lenB, double opposingLength)
{
    if(lenA == 0 || lenB == 0 || opposingLength == 0)
        return 0;

    double cosC = (pow(lenA,2.0) + pow(lenB,2.0) - pow(opposingLength,2.0)) / (2*lenA*lenB);
    return rad2deg(acos(cosC));
}

double Math::NormalizeAngle(double degrees)
{
    // clap the angle to 2 decimal places
    degrees = Truncate(degrees,2);
    
    while(degrees > 180)
        degrees -=360;

    while(degrees < -180)
        degrees += 360;

    assert(degrees > -180);
    assert(degrees <= 180);

    return degrees;
}

double Math::Truncate(double n, int decimalPlaces)
{
    double multiplier = pow(10,decimalPlaces);
    int intN = (int)(n * multiplier);

    return (double)intN/multiplier;
}
