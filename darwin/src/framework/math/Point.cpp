/*
 *   Point.cpp
 *
 *   Author: ROBOTIS
 *
 */

#include <math.h>
#include "Point.h"

using namespace Robot;

////////////////////////////////////////
// Point2D
////////////////////////////////////////

Point2D::Point2D() : X(0), Y(0) { }

Point2D::Point2D(double x, double y) :
	X(x),
	Y(y)
{ }

Point2D::Point2D(const Point2D &point) :
	X(point.X),
	Y(point.Y)
{ }

Point2D::~Point2D()
{
}

double Point2D::Distance(const Point2D &pt1, const Point2D &pt2)
{
	double x = pt1.X - pt2.X;
	double y = pt1.Y - pt2.Y;
	return sqrt(x * x + y * y);
}

Point2D & Point2D::operator = (const Point2D &point)
{
	X = point.X;
	Y = point.Y;
	return *this;
}

Point2D & Point2D::operator += (const Point2D &point)
{
	X += point.X;
	Y += point.Y;
	return *this;
}

Point2D & Point2D::operator -= (const Point2D &point)
{
	X -= point.X;
	Y -= point.Y;
	return *this;
}

Point2D & Point2D::operator += (double value)
{
	X += value;
	Y += value;
	return *this;
}

Point2D & Point2D::operator -= (double value)
{
	X -= value;
	Y -= value;
	return *this;
}

Point2D & Point2D::operator *= (double value)
{
	X *= value;
	Y *= value;
	return *this;
}

Point2D & Point2D::operator /= (double value)
{
	X /= value;
	Y /= value;
	return *this;
}

Point2D Point2D::operator + (const Point2D &point) const
{
	return Point2D(X + point.X, Y + point.Y);
}

Point2D Point2D::operator - (const Point2D &point) const
{
	return Point2D(X - point.X, Y - point.Y);
}

Point2D Point2D::operator + (double value) const
{
	return Point2D(X + value, Y + value);
}

Point2D Point2D::operator - (double value) const
{
	return Point2D(X - value, Y - value);
}

Point2D Point2D::operator * (double value) const
{
	return Point2D(X * value, Y * value);
}

Point2D Point2D::operator / (double value) const
{
	return Point2D(X / value, Y / value);
}

bool Point2D::operator == (const Point2D &point) const
{
    return this->X == point.X && this->Y == point.Y;
}

double Point2D::Magnitude() const
{
	Point2D temp(0, 0);
	return Distance(*this, temp);
}

double Point2D::Dot(const Point2D &point)
{
    return X*point.X + Y*point.Y;
}

Point3D Point2D::Cross(Point2D &point) const
{
    Point3D me(X,Y,0);
    Point3D them(point.X,point.Y,0);
    return me.Cross(them);
}

int Point2D::Orientation(Point2D &p, Point2D &q, Point2D &r)
{
    // see http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
    int val = (q.Y - p.Y) * (r.X - q.X) -
              (q.X - p.X) * (r.Y - q.Y);

    if (val == 0) return ORIENTATION_COLINEAR;  // colinear

    return (val > 0)? ORIENTATION_CLOCKWISE: ORIENTATION_ANTICLOCKWISE; // clock or counterclock wise
}

///////////////////////////////////////////////
// Point3D
///////////////////////////////////////////////

Point3D::Point3D() : X(0), Y(0), Z(0) {}

Point3D::Point3D(double x, double y, double z) :
	X(x),
	Y(y),
	Z(z)
{ }

Point3D::Point3D(const Point3D &point) :
	X(point.X),
	Y(point.Y),
	Z(point.Z)
{ }

Point3D::~Point3D()
{
}

double Point3D::Distance(const Point3D &pt1, const Point3D &pt2)
{
	double x = pt1.X - pt2.X;
	double y = pt1.Y - pt2.Y;
	double z = pt1.Z - pt2.Z;
	return sqrt(x * x + y * y + z * z);
}

Point3D & Point3D::operator = (const Point3D &point)
{
	X = point.X;
	Y = point.Y;
	Z = point.Z;
	return *this;
}

Point3D & Point3D::operator += (const Point3D &point)
{
	X += point.X;
	Y += point.Y;
	Z += point.Z;
	return *this;
}

Point3D & Point3D::operator -= (const Point3D &point)
{
	X -= point.X;
	Y -= point.Y;
	Z -= point.Z;
	return *this;
}

Point3D & Point3D::operator += (double value)
{
	X += value;
	Y += value;
	Z += value;
	return *this;
}

Point3D & Point3D::operator -= (double value)
{
	X -= value;
	Y -= value;
	Z -= value;
	return *this;
}

Point3D & Point3D::operator *= (double value)
{
	X *= value;
	Y *= value;
	Z *= value;
	return *this;
}

Point3D & Point3D::operator /= (double value)
{
	X /= value;
	Y /= value;
	Z /= value;
	return *this;
}

Point3D Point3D::operator + (const Point3D &point) const
{
	return Point3D(X + point.X, Y + point.Y, Z + point.Z);
}

Point3D Point3D::operator - (const Point3D &point) const
{
	return Point3D(X - point.X, Y - point.Y, Z - point.Z);
}

Point3D Point3D::operator + (double value) const
{
	return Point3D(X + value, Y + value, Z + value);
}

Point3D Point3D::operator - (double value) const
{
	return Point3D(X - value, Y - value, Z - value);
}

Point3D Point3D::operator * (double value) const
{
	return Point3D(X * value, Y * value, Z * value);
}

Point3D Point3D::operator / (double value) const
{
	return Point3D(X / value, Y / value, Z / value);
}

bool Point3D::operator == (const Point3D &point) const
{
    return this->X == point.X && this->Y == point.Y && this->Z == point.Z;
}

double Point3D::Magnitude() const
{
	Point3D temp(0, 0, 0);
	return Distance(*this, temp);
}

double Point3D::Dot(const Point3D &point)
{
    return X*point.X + Y*point.Y + Z*point.Z;
}

Point3D Point3D::Cross(Point3D &point) const
{
    Point3D A = *this;
    Point3D B = point;

    return Point3D(A.Y * B.Z - B.Y * A.Z,
                   B.X * A.Z - A.X * B.Z,
                   A.X * B.Y - A.Y * B.X);
}
