/*
 *   Matrix.cpp
 *
 *   Author: ROBOTIS
 *
 */

#include <math.h>
#include "Matrix.h"
#include <Math.h>
#include <iostream>

using namespace std;
using namespace Robot;


Matrix3D::Matrix3D()
{
	Identity();
}

Matrix3D::Matrix3D(const Matrix3D &mat)
{
	*this = mat;
}

Matrix3D::~Matrix3D()
{
}

void Matrix3D::Identity()
{
	m[m00] = 1; m[m01] = 0; m[m02] = 0; m[m03] = 0;
	m[m10] = 0; m[m11] = 1; m[m12] = 0; m[m13] = 0;
	m[m20] = 0; m[m21] = 0; m[m22] = 1; m[m23] = 0;
	m[m30] = 0; m[m31] = 0; m[m32] = 0; m[m33] = 1;
}

bool Matrix3D::Inverse()
{
	Matrix3D src, dst, tmp;
	double det;

    /* transpose matrix */
    for (int i = 0; i < 4; i++)
    {
        src.m[i] = m[i*4];
        src.m[i + 4] = m[i*4 + 1];
        src.m[i + 8] = m[i*4 + 2];
        src.m[i + 12] = m[i*4 + 3];
    }

    /* calculate pairs for first 8 elements (cofactors) */
    tmp.m[0] = src.m[10] * src.m[15];
    tmp.m[1] = src.m[11] * src.m[14];
    tmp.m[2] = src.m[9] * src.m[15];
    tmp.m[3] = src.m[11] * src.m[13];
    tmp.m[4] = src.m[9] * src.m[14];
    tmp.m[5] = src.m[10] * src.m[13];
    tmp.m[6] = src.m[8] * src.m[15];
    tmp.m[7] = src.m[11] * src.m[12];
    tmp.m[8] = src.m[8] * src.m[14];
    tmp.m[9] = src.m[10] * src.m[12];
    tmp.m[10] = src.m[8] * src.m[13];
    tmp.m[11] = src.m[9] * src.m[12];
    /* calculate first 8 elements (cofactors) */
    dst.m[0] = (tmp.m[0]*src.m[5] + tmp.m[3]*src.m[6] + tmp.m[4]*src.m[7]) - (tmp.m[1]*src.m[5] + tmp.m[2]*src.m[6] + tmp.m[5]*src.m[7]);
    dst.m[1] = (tmp.m[1]*src.m[4] + tmp.m[6]*src.m[6] + tmp.m[9]*src.m[7]) - (tmp.m[0]*src.m[4] + tmp.m[7]*src.m[6] + tmp.m[8]*src.m[7]);
    dst.m[2] = (tmp.m[2]*src.m[4] + tmp.m[7]*src.m[5] + tmp.m[10]*src.m[7]) - (tmp.m[3]*src.m[4] + tmp.m[6]*src.m[5] + tmp.m[11]*src.m[7]);
    dst.m[3] = (tmp.m[5]*src.m[4] + tmp.m[8]*src.m[5] + tmp.m[11]*src.m[6]) - (tmp.m[4]*src.m[4] + tmp.m[9]*src.m[5] + tmp.m[10]*src.m[6]);
    dst.m[4] = (tmp.m[1]*src.m[1] + tmp.m[2]*src.m[2] + tmp.m[5]*src.m[3]) - (tmp.m[0]*src.m[1] + tmp.m[3]*src.m[2] + tmp.m[4]*src.m[3]);
    dst.m[5] = (tmp.m[0]*src.m[0] + tmp.m[7]*src.m[2] + tmp.m[8]*src.m[3]) - (tmp.m[1]*src.m[0] + tmp.m[6]*src.m[2] + tmp.m[9]*src.m[3]);
    dst.m[6] = (tmp.m[3]*src.m[0] + tmp.m[6]*src.m[1] + tmp.m[11]*src.m[3]) - (tmp.m[2]*src.m[0] + tmp.m[7]*src.m[1] + tmp.m[10]*src.m[3]);
    dst.m[7] = (tmp.m[4]*src.m[0] + tmp.m[9]*src.m[1] + tmp.m[10]*src.m[2]) - (tmp.m[5]*src.m[0] + tmp.m[8]*src.m[1] + tmp.m[11]*src.m[2]);
    /* calculate pairs for second 8 elements (cofactors) */
    tmp.m[0] = src.m[2]*src.m[7];
    tmp.m[1] = src.m[3]*src.m[6];
    tmp.m[2] = src.m[1]*src.m[7];
    tmp.m[3] = src.m[3]*src.m[5];
    tmp.m[4] = src.m[1]*src.m[6];
    tmp.m[5] = src.m[2]*src.m[5];
    //Streaming SIMD Extensions - Inverse of 4x4 Matrix 8
    tmp.m[6] = src.m[0]*src.m[7];
    tmp.m[7] = src.m[3]*src.m[4];
    tmp.m[8] = src.m[0]*src.m[6];
    tmp.m[9] = src.m[2]*src.m[4];
    tmp.m[10] = src.m[0]*src.m[5];
    tmp.m[11] = src.m[1]*src.m[4];
    /* calculate second 8 elements (cofactors) */
    dst.m[8] = (tmp.m[0]*src.m[13] + tmp.m[3]*src.m[14] + tmp.m[4]*src.m[15]) - (tmp.m[1]*src.m[13] + tmp.m[2]*src.m[14] + tmp.m[5]*src.m[15]);
    dst.m[9] = (tmp.m[1]*src.m[12] + tmp.m[6]*src.m[14] + tmp.m[9]*src.m[15]) - (tmp.m[0]*src.m[12] + tmp.m[7]*src.m[14] + tmp.m[8]*src.m[15]);
    dst.m[10] = (tmp.m[2]*src.m[12] + tmp.m[7]*src.m[13] + tmp.m[10]*src.m[15]) - (tmp.m[3]*src.m[12] + tmp.m[6]*src.m[13] + tmp.m[11]*src.m[15]);
    dst.m[11] = (tmp.m[5]*src.m[12] + tmp.m[8]*src.m[13] + tmp.m[11]*src.m[14]) - (tmp.m[4]*src.m[12] + tmp.m[9]*src.m[13] + tmp.m[10]*src.m[14]);
    dst.m[12] = (tmp.m[2]*src.m[10] + tmp.m[5]*src.m[11] + tmp.m[1]*src.m[9]) - (tmp.m[4]*src.m[11] + tmp.m[0]*src.m[9] + tmp.m[3]*src.m[10]);
    dst.m[13] = (tmp.m[8]*src.m[11] + tmp.m[0]*src.m[8] + tmp.m[7]*src.m[10]) - (tmp.m[6]*src.m[10] + tmp.m[9]*src.m[11] + tmp.m[1]*src.m[8]);
    dst.m[14] = (tmp.m[6]*src.m[9] + tmp.m[11]*src.m[11] + tmp.m[3]*src.m[8]) - (tmp.m[10]*src.m[11] + tmp.m[2]*src.m[8] + tmp.m[7]*src.m[9]);
    dst.m[15] = (tmp.m[10]*src.m[10] + tmp.m[4]*src.m[8] + tmp.m[9]*src.m[9]) - (tmp.m[8]*src.m[9] + tmp.m[11]*src.m[10] + tmp.m[5]*src.m[8]);
    /* calculate determinant */
    det = src.m[0]*dst.m[0] + src.m[1]*dst.m[1] + src.m[2]*dst.m[2] + src.m[3]*dst.m[3];
    /* calculate matrix inverse */
    if (det == 0)
    {
        det = 0;
        return false;
    }
    else
    {
        det = 1 / det;
    }

    for (int i = 0; i < MAXNUM_ELEMENT; i++)
        m[i] = dst.m[i] * det;

    return true;
}

void Matrix3D::Scale(Vector3D scale)
{
	Matrix3D mat;
	mat.m[m00] = scale.X;
	mat.m[m11] = scale.Y;
	mat.m[m22] = scale.Z;

	*this *= mat;
}

void Matrix3D::Rotate(double angle, Vector3D axis)
{
	double rad = angle * 3.141592 / 180.0;
	double C = cos(rad);
	double S = sin(rad);
	Matrix3D mat;

	mat.m[m00] = C + axis.X * axis.X * (1 - C);
	mat.m[m01] = axis.X * axis.Y * (1 - C) - axis.Z * S;
	mat.m[m02] = axis.X * axis.Z * (1 - C) + axis.Y * S;
	mat.m[m10] = axis.X * axis.Y * (1 - C) + axis.Z * S;
	mat.m[m11] = C + axis.Y * axis.Y * (1 - C);
	mat.m[m12] = axis.Y * axis.Z * (1 - C) - axis.X * S;
	mat.m[m20] = axis.X * axis.Z * (1 - C) - axis.Y * S;
	mat.m[m21] = axis.Y * axis.Z * (1 - C) + axis.X * S;
	mat.m[m22] = C + axis.Z * axis.Z * (1 - C);

	*this *= mat;
}

void Matrix3D::Translate(Vector3D offset)
{
	Matrix3D mat;
	mat.m[m03] = offset.X;
	mat.m[m13] = offset.Y;
	mat.m[m23] = offset.Z;

	*this *= mat;
}

Point3D Matrix3D::Transform(Point3D point)
{
	Point3D result;
	result.X = m[m00]*point.X + m[m01]*point.Y + m[m02]*point.Z + m[m03];
	result.Y = m[m10]*point.X + m[m11]*point.Y + m[m12]*point.Z + m[m13];
	result.Z = m[m20]*point.X + m[m21]*point.Y + m[m22]*point.Z + m[m23];

    return result;
}

Vector3D Matrix3D::Transform(Vector3D vector)
{
	Vector3D result;
	result.X = m[m00]*vector.X + m[m01]*vector.Y + m[m02]*vector.Z + m[m03];
	result.Y = m[m10]*vector.X + m[m11]*vector.Y + m[m12]*vector.Z + m[m13];
	result.Z = m[m20]*vector.X + m[m21]*vector.Y + m[m22]*vector.Z + m[m23];

    return result;
}

void Matrix3D::SetTransform(Point3D point, Vector3D angle)
{
	double Cx = cos(angle.X * 3.141592 / 180.0);
	double Cy = cos(angle.Y * 3.141592 / 180.0);
	double Cz = cos(angle.Z * 3.141592 / 180.0);
	double Sx = sin(angle.X * 3.141592 / 180.0);
	double Sy = sin(angle.Y * 3.141592 / 180.0);
	double Sz = sin(angle.Z * 3.141592 / 180.0);
	
	Identity();
	m[0] = Cz * Cy;
    m[1] = Cz * Sy * Sx - Sz * Cx;
    m[2] = Cz * Sy * Cx + Sz * Sx;
	m[3] = point.X;
    m[4] = Sz * Cy;
    m[5] = Sz * Sy * Sx + Cz * Cx;
    m[6] = Sz * Sy * Cx - Cz * Sx;
    m[7] = point.Y;
    m[8] = -Sy;
    m[9] = Cy * Sx;
    m[10] = Cy * Cx;
    m[11] = point.Z;
}

Matrix3D & Matrix3D::operator = (const Matrix3D &mat)
{
	for (int i = 0; i < MAXNUM_ELEMENT; i++)
        m[i] = mat.m[i];
	return *this;
}

Matrix3D & Matrix3D::operator *= (const Matrix3D &mat)
{
	Matrix3D tmp = *this * mat;
	*this = tmp;
	return *this;
}

Matrix3D Matrix3D::operator * (const Matrix3D &mat)
{
	Matrix3D result;

    for (int j = 0; j < 4; j++)
    {
        for (int i = 0; i < 4; i++)
        {
            double x = 0.0;
            for (int c = 0; c < 4; c++)
            {
                //result.m[j*4+i] += m[j*4+c] * mat.m[c*4+i];
                x += m[j*4+c] * mat.m[c*4+i];
            }
            result.m[j*4+i] = x;
        }
    }

	return result;
}

void Matrix3D::Print()
{
    cout << "[[" << m[m00] << "\t" << m[m01] << "\t" << m[m02] << "\t" << m[m03] << "]" << endl <<
            " [" << m[m10] << "\t" << m[m11] << "\t" << m[m12] << "\t" << m[m13] << "]" << endl <<
            " [" << m[m20] << "\t" << m[m21] << "\t" << m[m22] << "\t" << m[m23] << "]" << endl <<
            " [" << m[m30] << "\t" << m[m31] << "\t" << m[m32] << "\t" << m[m33] << "]]";
}

Matrix3D Matrix3D::Translation(double dx, double dy, double dz)
{
    Matrix3D m;
    m.m[m03] = dx;
    m.m[m13] = dy;
    m.m[m23] = dz;
    return m;
}

Matrix3D Matrix3D::RotationX(double angle)
{
    angle = deg2rad(angle);

    Matrix3D m;
    m.m[m11] = cos(angle);
    m.m[m12] = -sin(angle);
    m.m[m21] = sin(angle);
    m.m[m22] = cos(angle);
    return m;
}

Matrix3D Matrix3D::RotationY(double angle)
{
    angle = deg2rad(angle);

    Matrix3D m;
    m.m[m00] = cos(angle);
    m.m[m02] = sin(angle);
    m.m[m20] = -sin(angle);
    m.m[m22] = cos(angle);
    return m;
}

Matrix3D Matrix3D::RotationZ(double angle)
{
    angle = deg2rad(angle);

    Matrix3D m;
    m.m[m00] = cos(angle);
    m.m[m01] = -sin(angle);
    m.m[m10] = sin(angle);
    m.m[m11] = cos(angle);
    return m;
}
