#include "Line.h"

using namespace Robot;

void Line2D::SortTopToBottom()
{
    if(start.Y < end.Y)
    {
        Point2D tmp = start;
        start = end;
        end = tmp;
    }
}

void Line2D::SortLeftToRight()
{
    if(start.X > end.X)
    {
        Point2D tmp = start;
        start = end;
        end = tmp;
    }
}

bool Line2D::Intersects(Line2D &l)
{
    // given lines [p1,p2] and [q1,q2] they will intersect
    // if the orientation (clockwise/counterclockwise) of
    // (p1 p2 q1) and (p1 p2 q2) are different AND
    // (p2 q2 p1) and (p2 q2 q1) are different
    // see: http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/

    int o1 = Point2D::Orientation(start,end,l.start);
    int o2 = Point2D::Orientation(start,end,l.end);
    int o3 = Point2D::Orientation(end,l.end,start);
    int o4 = Point2D::Orientation(end,l.end,l.start);

    return (o1 != o2 && o3 != o4);
}

Point2D Line2D::ToVector()
{
    return Point2D(end.X-start.X,end.Y-start.Y);
}
