/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class allows access to the Espeak library, allowing Darwin to
 * "speak" strings.  Useful for debugging.
 ************************************************************************/

#include "Voice.h"
#include <espeak/speak_lib.h>
#include <cstring>
#include <pthread.h>
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace Robot;
using namespace std;

const char* Voice::m_voice_name = "default";

t_espeak_callback *Voice::m_synth_callback;
espeak_PARAMETER Voice::m_parm;
espeak_POSITION_TYPE Voice::m_position_type = POS_CHARACTER;
espeak_AUDIO_OUTPUT Voice::m_output = AUDIO_OUTPUT_PLAYBACK;
unsigned int Voice::m_flags = espeakCHARS_AUTO;
int Voice::m_playback_buff_length = 500;
int Voice::m_playback_options = 0x00;
bool Voice::initialized = false;

#define USE_SYSTEM

static bool equalsIgnoreCase(string a, string b)
{
    const char* x = a.c_str();
    const char* y = b.c_str();

    char* p1 = (char*)x;
    char* p2 = (char*)y;
    char c1, c2;

    while(*p1!='\0' && *p2!='\0')
    {
        c1 = *p1;
        c2 = *p2;

        if(c1 >= 'a' && c1 <= 'z')
            c1 = c1 - 'a' + 'A';

        if(c2 >= 'a' && c2 <= 'z')
            c2 = c2 - 'a' + 'A';

        if(c1 != c2)
        {
            return false;
        }

        p1++;
        p2++;
    }

    // if we get to here it's because one char is null.
    // compare them and return
    return *p1 == *p2;
}

void Voice::Initialize()
{
    Initialize("default");
}

void Voice::Initialize(const char* voice)
{
    // each robot has a different default voice so check
    // /etc/hostname and choose an appropriate voice
    if(!strcmp(voice,"default"))
    {
        ifstream hostFile;
        hostFile.open("/etc/hostname");
        string hostname;
        hostFile >> hostname;
        hostFile.close();

        if(equalsIgnoreCase(hostname,"Jeff"))
        {
            voice = "en-us+m3";
        }
        else if(equalsIgnoreCase(hostname,"Jennifer"))
        {
            voice = "en-us+f2";
        }
        else if(equalsIgnoreCase(hostname,"Jimmy"))
        {
            voice = "en-gb+m1";
        }
        else if(equalsIgnoreCase(hostname,"Jose"))
        {
            voice = "es-mx";    // TODO: decide if we really want the mexican spanish pronunciation
        }
    }

    m_voice_name = voice;

#ifndef USE_SYSTEM
    espeak_SetSynthCallback(Voice::SynthCallback);
    espeak_Initialize(m_output, m_playback_buff_length, NULL, m_playback_options);
    espeak_SetVoiceByName(m_voice_name);
#endif


    initialized = true;
}

void Voice::Speak(const char* message)
{
    if(!initialized)
        return;

    // create and execute a new thread that will do the speaking for us
    // so that we don't block the main routine that called us
    pthread_t threadId;
    pthread_create(&threadId, NULL, (void* (*)(void*))&SpeakThread, (void*)message);
    //pthread_join(threadId, NULL);
}

void* Voice::SpeakThread(void* msg)
{
    const char* message = (const char*)msg;

#ifndef USE_SYSTEM
    int size = strlen(message)+1;
    int position = 0;
    int end_position = 0;
    unsigned int* unique_id = NULL;
    void *user_data = NULL;

    espeak_Synth( message, size, position, m_position_type, end_position, m_flags, unique_id, user_data );
    espeak_Synchronize( );
#else
    char buffer[strlen(message)+2+strlen("espeak -v ") + strlen(m_voice_name) + strlen(" &> /dev/null")];
    sprintf(buffer,"espeak -v%s \"%s\" &> /dev/null",m_voice_name,message);
    //cout << buffer << endl;
    system(buffer);
#endif

    return NULL;
}

Voice::Voice()
{
    // private
    //Initialize();
}

Voice::~Voice()
{
    // private
}
