/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is used to locate a single uniformly-coloured target
 ************************************************************************/

#include "SingleBlob.h"
#include "Camera.h"
#include "Math.h"
#include <iostream>
#include <limits.h>
#include "minIni.h"

using namespace std;
using namespace Robot;
using namespace cv;

void SingleBlob::Init(const char* name)
{
    this->name = new string(name);
    this->foundInLastFrame = false;

    this->positionDisabled = false;

    this->SetYRange(0,0);
    this->SetURange(0,0);
    this->SetVRange(0,0);
    this->SetDimensions(0,0);

    this->SetIgnoreTop(0);
    this->SetIgnoreBottom(0);
    this->SetIgnoreLeft(0);
    this->SetIgnoreRight(0);

    this->SetMinPixelHeight(10);
    this->SetMinPixelWidth(10);
    this->SetKeyChannel(1);
    this->SetMinScanLength(8);
    this->SetScanThreshold(50);

    this->SetLeftRange(0,Camera::WIDTH);
    this->SetTopRange(0,Camera::HEIGHT);

    // default to drawing a yellow bounding box
    this->SetMarkColour(255, 255, 0);

    this->realHeight = -1;
    this->realWidth = -1;

    scanlineParam->push_back(this);
}

SingleBlob::SingleBlob() : BlobTarget()
{
    Init("Unnamed Target");
}

SingleBlob::SingleBlob(const char* name) : BlobTarget()
{
    Init(name);
}

void SingleBlob::Print()
{
    cout << *(this->name) << " ";
    boundingBox.Print();
    //cout << " Distance " << GetDistance();
    cout << endl;
}

void SingleBlob::Draw(Mat& image)
{
    boundingBox.Draw(image, markColour, 1);
}

int SingleBlob::FindInFrame(cv::Mat &image, cv::Mat *dest)
{
    //vector<BoundingBox*> *candidates = TargetProcessing::ScanLine(image, minScanLength, scanThreshold, *keyChannelRange, keyChannel, dest, ignoreTop, Camera::HEIGHT-ignoreBottom-1, ignoreLeft, Camera::WIDTH-ignoreRight-1, fillThreshold, subsample);
    //ProcessCandidates(*candidates);

    // clean up
    //while(!candidates->empty())
    //    delete (*candidates)[candidates->size()-1];
    //delete candidates;

    BlobTarget::FindTargets(*scanlineParam, image, dest, this->subsample);

    return foundInLastFrame ? 1:0;
}

int SingleBlob::FindInFrame(CvCamera *camera, cv::Mat *dest)
{
    BlobTarget::FindTargets(*scanlineParam,camera->yuvFrame, camera->rgbFrame, camera->hsvFrame,dest,this->subsample);

    return foundInLastFrame ? 1:0;
}

void SingleBlob::ProcessCandidates(std::vector<BoundingBox*> &candidates)
{
    BoundingBox *bestFit = NULL;
    BoundingBox *current;
    int votes;

    double targetRatio = (double)realHeight/(double)realWidth;
    double currentRatio;
    double bestRatio = 0;
    double bestFill = 0;

    int currentSize;
    int bestSize = 0;

    int mostVotes = 0;

    for(unsigned int i=0; i<candidates.size(); i++)
    {
        current = candidates[i];

        // ignore anything smaller than a certain threshold
        if(current->height >= minPixelHeight && current->width >= minPixelWidth)
        {
            votes = 0;

            currentRatio = (double) current->height / (double) current->width;
            currentSize = current->height * current->width;

            if(!proportionsDisabled)
            {
                // better proportions
                if(abs(currentRatio - targetRatio) < abs(bestRatio - targetRatio))
                    votes++;

                // proportions within [0.75,1.25] times the correct size && colours that match
                if((currentRatio < targetRatio && currentRatio * 1.25 >= targetRatio) || (currentRatio > targetRatio && currentRatio * 0.75 <= targetRatio))
                    votes++;

                // proportions are WAY off
                if((currentRatio < targetRatio && currentRatio * 1.25 < targetRatio) || (currentRatio > targetRatio && currentRatio * 0.75 > targetRatio))
                    votes--;
            }


            // each colour channel is acceptable
            if(!yRangeDisabled && yRange.Check(current->avgY))
                votes++;
            if(!uRangeDisabled && uRange.Check(current->avgU))
                votes++;
            if(!vRangeDisabled && vRange.Check(current->avgV))
                votes++;

            // extra vote if the key channel range is good
            if(!keyRangeDisabled)
            {
                switch(keyChannel)
                {
                    case 2:
                        if(keyChannelRange->Check(current->avgV))
                            votes++;
                        break;
                    case 1:
                        if(keyChannelRange->Check(current->avgU))
                            votes++;
                        break;
                    case 0:
                    default:
                    if(keyChannelRange->Check(current->avgY))
                            votes++;
                        break;
                }
            }


            if(!fillDisabled)
            {
                // fill is sufficient
                if(current->fill >= 0.5)
                    votes++;
                if(current->fill >= 0.6)
                    votes++;
                if(current->fill >= 0.7)
                    votes++;

                // fill is insufficient
                if(current->fill <0.5)
                    votes--;
                if(current->fill <0.4)
                    votes--;
                if(current->fill <0.3)
                    votes--;

                // current fill is better
                if(current->fill > bestFill)
                    votes++;
            }

            // current is a larger target (eliminates small pieces of noise)
            if(!sizeDisabled && currentSize > bestSize)
                votes++;

            // check the position to see if it matches the last known position of the object
            if(!positionDisabled &&
               current->center.X >= boundingBox.center.X - boundingBox.width/2 && current->center.X <= boundingBox.center.X + boundingBox.width/2 &&
               current->center.Y >= boundingBox.center.Y - boundingBox.height/2 && current->center.Y <= boundingBox.center.Y + boundingBox.height/2)
            {
                votes++;
            }
            else if(!positionDisabled &&
               (current->center.X < boundingBox.center.X - boundingBox.width || current->center.X > boundingBox.center.X + boundingBox.width ||
               current->center.Y < boundingBox.center.Y - boundingBox.height || current->center.Y > boundingBox.center.Y + boundingBox.height))
            {
                votes--;
            }

            // found a better match; use this one
            //if(votes >= GetMaxVotes()/2 && votes > mostVotes)
            if(votes > mostVotes)
            {
                bestFit = current;
                bestRatio = currentRatio;
                bestSize = current->width * current->height;
                bestFill = current->fill;
            }
        }
    }

    if(bestFit!=NULL)
    {
        foundInLastFrame = true;

        boundingBox = *bestFit;
    }
    else
        foundInLastFrame = false;
}


int SingleBlob::GetMaxVotes()
{
    int votes = 0;

    if(!proportionsDisabled)
        votes+=2;   // 1 for best proportions, 1 for acceptable proportions
    if(!fillDisabled)
        votes+=4;   // 1 for >.5, >.6, >.7, + 1 for best fill
    if(!sizeDisabled)
        votes++;
    if(!yRangeDisabled)
        votes++;
    if(!yRangeDisabled)
        votes++;
    if(!uRangeDisabled)
        votes++;
    if(!vRangeDisabled)
        votes++;
    if(!keyRangeDisabled)
        votes++;
    if(!positionDisabled)
        votes++;
    return votes;
}

bool SingleBlob::CheckPosition()
{
    return WasFound() && topRange.Check(boundingBox.center.Y) && leftRange.Check(boundingBox.center.X);
}

double SingleBlob::GetAngle(double heightAboveGround)
{
    return this->boundingBox.GetAngle(heightAboveGround);
}

double SingleBlob::GetRange(double heightAboveGround)
{
    CameraPosition cameraPosition;

    Point2D tl, tr, bl, br;
    tl = Point2D(this->boundingBox.center.X - this->boundingBox.width/2, this->boundingBox.center.Y - this->boundingBox.height/2);
    tr = Point2D(this->boundingBox.center.X + this->boundingBox.width/2, this->boundingBox.center.Y - this->boundingBox.height/2);
    bl = Point2D(this->boundingBox.center.X - this->boundingBox.width/2, this->boundingBox.center.Y + this->boundingBox.height/2);
    br = Point2D(this->boundingBox.center.X + this->boundingBox.width/2, this->boundingBox.center.Y + this->boundingBox.height/2);

    double range = (
                cameraPosition.CalculateRange(tl, heightAboveGround + this->realHeight) +
                cameraPosition.CalculateRange(tr, heightAboveGround + this->realHeight) +
                cameraPosition.CalculateRange(bl, heightAboveGround + 0) +
                cameraPosition.CalculateRange(br, heightAboveGround + 0) +
                cameraPosition.CalculateRange(this->boundingBox.center, heightAboveGround + this->realHeight/2.0)
    ) / 5.0;

    return range;
}

#if 0
int SingleTarget::GetDistance()
{
    const double pixels = 260;
    double d1 = pixels * realWidth / (this->boundingBox.width);
    double d2 = pixels * realHeight / (this->boundingBox.height);

    return ((int) (d1 + d2))/2;
    //return (int) d1;
}

bool SingleTarget::CheckDistance(int minDistance, int maxDistance, double headAngle) {
    if (!WasFound()) return false; // No need to do all the work...

    int headDistance = this->GetDistance();
    //double temp = headAngle;
    headAngle = deg2rad(headAngle+60);

    int distance = (int) (headDistance * sin(headAngle));

    //cout << "Check Distance: " << headDistance << ", " << headAngle;
    //cout << ", " << temp << " -> " << distance << " <= " << maxDistance << endl;

    return minDistance < distance && distance <= maxDistance;
}

Point2D SingleTarget::GetPosition(double panAngle, double tiltAngle) {
    if (!WasFound()) return Point2D(-1, -1); // No need to do all the work...

    int directDistance = this->GetDistance();

    tiltAngle = deg2rad(tiltAngle+60);
    panAngle = deg2rad(panAngle);

    double xTilt, xPan, yTilt, yPan;
    sincos(tiltAngle, &xTilt, &yTilt);
    sincos(panAngle, &yPan, &xPan);

    double x = directDistance * xPan * xTilt;
    double y = directDistance * yPan ;// * yTilt;

    cout << "Target Position stuff: " << directDistance << ", " << xPan;
    cout << ", " << xTilt << "; " << yPan << ", " << yTilt << endl;
    cout << "Target angles: " << panAngle/PI << "pi, " << tiltAngle/PI << "pi" << endl;
    cout << "Target result: " << x << ", " << y << endl;


    return Point2D(x, y);
}

Point3D SingleTarget::GetPosition3D(double panAngle, double tiltAngle)
{
    if (!WasFound()) return NULL; // No need to do all the work...

    int distance = this->GetDistance();

    tiltAngle = deg2rad(tiltAngle+60);
    panAngle = deg2rad(panAngle);

    double x = distance * sin(tiltAngle) * cos(-panAngle);
    double y = distance * sin(-panAngle);
    double z = distance * cos(panAngle) * -cos(tiltAngle);

    cout << "Target distance: " << distance << endl;
    cout << "Target angles: " << panAngle/PI << "pi, " << tiltAngle/PI << "pi" << endl;
    cout << "Target result: " << x << ", " << y << ", " << z << endl;

    return Point3D(x, y, z);
}
#endif

void SingleBlob::LoadIniSettings(minIni &ini, const string &iniSection)
{
    BlobTarget::LoadIniSettings(ini, iniSection);

    topRange.min = ini.geti(iniSection,"MinTop",0);
    topRange.max = ini.geti(iniSection,"MaxTop",Camera::HEIGHT);

    leftRange.min = ini.geti(iniSection,"MinLeft",0);
    leftRange.max = ini.geti(iniSection,"MaxLeft",Camera::WIDTH);

    realHeight = ini.geti(iniSection, "RealHeight", -1);
    realWidth = ini.geti(iniSection, "RealWidth", -1);

    positionDisabled = (bool)ini.geti(iniSection,"PositionDisabled", 0);
}
