#include "BoundingBox.h"
#include <iostream>
#include <MotionManager.h>
#include <Head.h>
#include <Kinematics.h>
#include <Camera.h>
#include <CameraPosition.h>

using namespace Robot;
using namespace std;
using namespace cv;

CameraPosition BoundingBox::cameraPosition;

BoundingBox::BoundingBox()
{
    center.X = 0;
    center.Y = 0;
    width = 0;
    height = 0;
    avgY = 0;
    avgU = 0;
    avgV = 0;
    fill = 0;
}

BoundingBox::BoundingBox(const BoundingBox &src)
{
    center.X = src.center.X;
    center.Y = src.center.Y;
    width = src.width;
    height = src.height;
    avgY = src.avgY;
    avgU = src.avgU;
    avgV = src.avgV;
    fill = src.fill;
}

BoundingBox& BoundingBox::operator =(const BoundingBox &src)
{
    center.X = src.center.X;
    center.Y = src.center.Y;
    width = src.width;
    height = src.height;
    avgY = src.avgY;
    avgU = src.avgU;
    avgV = src.avgV;
    fill = src.fill;

    return *this;
}

void BoundingBox::Draw(Mat &img, cv::Scalar &colour, int thickness)
{
    Point p = Point(this->center.X - this->width/2, this->center.Y - this->height/2);
    Point q = Point(this->center.X + this->width/2, this->center.Y + this->height/2);

    rectangle(img, p, q, colour, thickness);
}

void BoundingBox::Print()
{
    cout << (this->GetRange()) << "mm @ " << (this->GetAngle()) << "° (" << center.X << "," << center.Y << ")  [" << width << " x " << height << "] F:" << fill << " YUV:" << avgY << " " << avgU << " " << avgV;
}

double BoundingBox::GetRange(double heightAboveGround)
{
    cameraPosition.RecalculatePosition();
    return cameraPosition.CalculateRange(*this, heightAboveGround);
}

double BoundingBox::GetAngle(double heightAboveGround)
{
    cameraPosition.RecalculatePosition();
    return cameraPosition.CalculateAngle(*this, heightAboveGround);
}

// does this bounding box completely contain another one?
bool BoundingBox::Contains(BoundingBox &other)
{
    //cout << "Checking targets..." << endl;
    //cout << "L: " << (center.X - width/2) << " <= " << (other.center.X - other.width/2) << endl <<
    //        "R: " << (center.X + width/2) << " >= " << (other.center.X + other.width/2) << endl <<
    //        "T: " << (center.Y - height/2) << " <= " << (other.center.Y - other.height/2) << endl <<
    //        "B: " << (center.Y + height/2) << " >= " << (other.center.Y + other.height/2) << endl;


    bool result = center.Y - height/2 <= other.center.Y - other.height/2 && center.Y + height/2 >= other.center.Y + other.height/2 &&
            center.X - width/2 <= other.center.X - other.width/2 && center.X + width/2 >= other.center.X + other.width/2;

    //if(result)
    //    cout << "Target contained" << endl;
    //else
    //    cout << "Distinct targets" << endl;

    return result;
}

bool BoundingBox::ContainsPoint(Point2D &point)
{
    return point.X >= center.X - width/2 && point.X <= center.X + width/2 &&
            point.X >= center.Y - height/2 && point.Y <= center.Y + height/2;
}

bool BoundingBox::Intersects(BoundingBox &other)
{
    // see if any of the perimeter line segments of either bounding box intersects with the other box
    int thisTop = center.Y - height/2;
    int thisBottom = center.Y + height/2;
    int thisLeft = center.X - width/2;
    int thisRight = center.X + width/2;

    int thatTop = other.center.Y - other.height/2;
    int thatBottom = other.center.Y + other.height/2;
    int thatLeft = other.center.X - other.width/2;
    int thatRight = other.center.X + other.width/2;

    if((thisTop <= thatTop && thisBottom >= thatTop) ||
       (thisTop <= thatBottom && thisBottom >= thatBottom))
    {
        // see if top/bottom boundary intersects with left/right
        return (thisLeft <= thatLeft && thisRight >= thatLeft) ||
                (thisLeft <= thatRight && thisRight >= thatRight);
    }
    else if((thisLeft <= thatLeft && thisRight >= thatLeft) ||
             (thisLeft <= thatRight && thisRight >= thatRight))
    {
        // see if left/right boundary intersects with top/bottom
        return (thisTop <= thatTop && thisBottom >= thatTop) ||
                (thisTop <= thatBottom && thisBottom >= thatBottom);
    }
    else
    {
        return false;
    }
}

bool BoundingBox::CompareBySize(void *a, void *b)
{
    BoundingBox *boxA = (BoundingBox*)a;
    BoundingBox *boxB = (BoundingBox*)b;

    int areaA = boxA->height * boxA->width;
    int areaB = boxB->height * boxB->width;

    return areaA < areaB;
}

bool BoundingBox::CompareByLocation(void *a, void *b)
{
    BoundingBox *boxA = (BoundingBox*)a;
    BoundingBox *boxB = (BoundingBox*)b;

    if (boxA->center.Y < boxB->center.Y - boxB->height/2)
    {
        // boxA is much higher
        return true;
    }
    else if (boxB->center.Y < boxA->center.Y - boxA->height/2)
    {
        // boxB is much higher
        return false;
    }
    else
    {
        // they're relatively level, so sort by X values
        if (boxA->center.X <= boxB->center.X)
        {
            // boxA is further left
            return true;
        }
        else if (boxB->center.X < boxA->center.X)
        {
            // boxB is further left
            return false;
        }
        else
        {
            cerr << "[BoundingBox] You should not be seeing this." << endl;
            return false;
        }
    }
}
