#include <Target.h>
#include <string>
#include <opencv/cv.h>

using namespace Robot;
using namespace std;
using namespace cv;

Target::Target()
{
    name = NULL;

    // number of pixels around each edge of the frame where we can ignore results (since they will be noise)
    ignoreTop = 0;
    ignoreLeft = 0;
    ignoreBottom = 0;
    ignoreRight = 0;

    // max/min yuv parameters
    yRange.min=0;
    yRange.max=0;
    uRange.min=0;
    uRange.max=0;
    vRange.min=0;
    vRange.max=0;

    // colour used to mark the target's bounding box on the image
    markColour = cvScalar(0,255,255);
}

Target::~Target()
{
    if(name!=NULL)
        delete name;
}



void Target::LoadIniSettings(minIni &ini, const string &iniSection)
{
    if(name==NULL)
        delete name;
    name = new string(ini.gets(iniSection,"Name"));

    ignoreTop = ini.geti(iniSection,"IgnoreTop",0);
    ignoreLeft = ini.geti(iniSection,"IgnoreLeft",0);
    ignoreBottom = ini.geti(iniSection,"IgnoreBottom",0);
    ignoreRight = ini.geti(iniSection,"IgnoreRight",0);

    yRange.min = ini.geti(iniSection,"MinY",0);
    yRange.max = ini.geti(iniSection,"MaxY",255);
    uRange.min = ini.geti(iniSection,"MinU",0);
    uRange.max = ini.geti(iniSection,"MaxU",255);
    vRange.min = ini.geti(iniSection,"MinV",0);
    vRange.max = ini.geti(iniSection,"MaxV",255);

    markColour = Scalar(ini.geti(iniSection,"MarkB",0), ini.geti(iniSection,"MarkG",255), ini.geti(iniSection,"MarkR",255));
}

void Target::DrawScanRange(Mat &image)
{
    Point p = Point(ignoreLeft, ignoreTop);
    Point q = Point(Camera::WIDTH-ignoreRight, Camera::HEIGHT-ignoreBottom);

    rectangle(image, p, q, markColour, 1, 4);
}

void Target::FastFindTargets(vector<Target*> &targets, Mat &src, Mat *dest)
{
    Target *t;

    for(vector<Target*>::iterator it=targets.begin(); it!=targets.end(); it++)
    {
        t = *it;

        // threshold the image
        Mat binary(dest->rows,dest->cols,CV_8UC1);
        inRange(src,Scalar(t->yRange.min, t->uRange.min, t->vRange.min),
                Scalar(t->yRange.max, t->uRange.max, t->vRange.max),
                binary);
        t->ProcessCandidateImage(binary);
        //imshow("debugging",binary);
        //cvWaitKey(0);

        if(dest != NULL)
        {
            // if there is an output image copy the binary data back into it using the average colour
            Scalar avgColour = Scalar((t->yRange.max+t->yRange.min)/2,(t->uRange.max+t->uRange.min)/2,(t->vRange.max+t->vRange.min)/2);

            IplImage iplImg(*dest);
            IplImage binImg(binary);
            uint8_t *yuvData = (uint8_t*) (iplImg.imageData);

            uint8_t *binData = (uint8_t*) (binImg.imageData);

            for(int i=0; i<src.rows; i++)
            {
                for(int j=0; j<src.cols; j++)
                {
                    int idx = RowCol2Index(i,j,binary);
                    int outIdx = RowCol2Index(i,j,src);
                    if(binData[idx] != 0x00)
                    {
                        yuvData[outIdx+0] = avgColour[0];
                        yuvData[outIdx+1] = avgColour[1];
                        yuvData[outIdx+2] = avgColour[2];
                    }
                }
            }
        }
    }
}

// convert a row/column into an index used to access a specific pixel in the data array
int Target::RowCol2Index(int row, int col, Mat &mat)
{
    int index;

    index = row * mat.cols + col;
    index *= mat.channels();  // multiply by the number of channels in the image

    return index;
}

// convert an index back into row/column
void Target::Index2RowCol(int index, int *row, int *col, Mat &mat)
{
    index /= mat.channels();
    *row = index / mat.cols;
    *col = index % mat.cols;
}

// return true if the provided YUV values are within the right range
bool Target::IsTargetPixel(uint8_t y, uint8_t u, uint8_t v, ThresholdRange &yRange, ThresholdRange &uRange, ThresholdRange &vRange)
{
    return (y >= yRange.min && y <= yRange.max &&
            u >= uRange.min && u <= uRange.max &&
            v >= vRange.min && v <= vRange.max);
}

// create a new binary image that represents the scene
cv::Mat* Target::Yuv2Binary(cv::Mat &src, ThresholdRange &yRange, ThresholdRange &uRange, ThresholdRange &vRange)
{
    // create a new 8-bit, 1-channel image of the same size as the original to store the binary image
    Mat *binaryMat = new Mat(src.rows, src.cols, CV_8UC1);
    cv::inRange(src,Scalar(yRange.min,uRange.min,vRange.min),Scalar(yRange.max,uRange.max,vRange.max),*binaryMat);

    return binaryMat;
}

Mat *Target::ExtractChannel(Mat &src, int channel)
{
    if(channel < 0 || channel >= src.channels())
        return NULL;

    Mat *dest = new Mat(src.rows, src.cols, CV_8UC1);

    //IplImage *srcImg = new IplImage(src);
    //IplImage *destImg = new IplImage(*dest);

    IplImage srcImg(src);
    IplImage destImg(*dest);

    uint8_t *srcData = (uint8_t*) (srcImg.imageData);
    uint8_t *destData = (uint8_t*) (destImg.imageData);

    for(int i = 0; i < srcImg.width; i++) {
        for(int j = 0; j < srcImg.height; j++) {
            *destData = *(srcData+channel);
            destData++;
            srcData += src.channels();
        }
    }

    //delete srcImg;
    //delete destImg;

    return dest;
}

Mat *Target::CreateCompliment(Mat &src)
{
    Mat *dest = new Mat(src.rows, src.cols, src.type());

    //IplImage *srcImg = new IplImage(src);
    //IplImage *destImg = new IplImage(*dest);
    IplImage srcImg(src);
    IplImage destImg(*dest);

    uint8_t *srcData = (uint8_t*) (srcImg.imageData);
    uint8_t *destData = (uint8_t*) (destImg.imageData);

    uint8_t y,u,v;

    for(int i=0; i < srcImg.width; i++)
    {
        for(int j=0; j < srcImg.height; j++)
        {
            y = srcData[0];
            u = srcData[1];
            v = srcData[2];

            destData[0] = (uint8_t)(((int)y - (int)u)/2+128);
            destData[1] = (uint8_t)(((int)y - (int)v)/2+128);
            destData[2] = (uint8_t)(((int)u - (int)v)/2+128);

            srcData += src.channels();
            destData += dest->channels();
        }
    }

    //delete srcImg;
    //delete destImg;

    return dest;
}
