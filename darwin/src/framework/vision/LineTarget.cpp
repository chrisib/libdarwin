#include <LineTarget.h>
#include <Line.h>

using namespace cv;
using namespace std;
using namespace Robot;

LineTarget::LineTarget() : Target()
{
    name = NULL;

    minAngle = 0.0;
    maxAngle = 180.0;

    minHoughVotes = 0;

    minLength = 0;
    maxLength = sqrt(pow(Camera::HEIGHT,2)+pow(Camera::WIDTH,2));

    maxGap = 0;

    zeroIsVertical = false;

    slopeMergeTolerance = 0.12;		// (SLOPE_MERGE_TOLERANCE * 100) percent slope similarity
    endpointMergeTolerance = 35;	// any end points must be within END_POINT_MERGE_TOLERANCE pixels nearby
    minVotesPerMerge = 3;			// one merged line requires a minimum of MIN_VOTES_PER_MERGE lines
    endpointRadius = 15;            // lines within this range of each other will be merged

    // add standard colours to the spectrum vector
    // these get used to draw the buckets in a colour-coded way
    spectrum.push_back(Scalar(  0,  0,255));    //Red
    spectrum.push_back(Scalar(  0,180,255));    //Orange
    spectrum.push_back(Scalar(  0,255,255));    //Yellow
    spectrum.push_back(Scalar(  0,255,  0));    //Green
    spectrum.push_back(Scalar(255,255,  0));    //Blue
    spectrum.push_back(Scalar(255,  0,180));    //Purple

    DRAW_BUCKETS = false;
    DRAW_RAW_LINES = false;
}

LineTarget::~LineTarget()
{

}

void LineTarget::Draw(Mat &canvas)
{
    if(DRAW_RAW_LINES)
    {
        //for_each(rawLines.begin(), rawLines.end(), [&](Line2D &l){
        //    cv::line(canvas,Point(l.start.X,l.start.X),Point(l.end.X,l.end.Y),Scalar(255,255,255),1);
        //});
        for(vector<Line2D>::iterator it=rawLines.begin(); it!=rawLines.end(); it++)
        {
            Line2D l=*it;
            cv::line(canvas,Point(l.start.X,l.start.Y),Point(l.end.X,l.end.Y),Scalar(255,255,255),1);
        }
    }

    if(DRAW_BUCKETS)
    {
        for(unsigned int i=0; i<buckets.size(); i++)
        {
            vector<Line2D> b=buckets[i];
            for(vector<Line2D>::iterator it=b.begin(); it!=b.end(); it++)
            {
                Line2D l=*it;
                cv::line(canvas,Point(l.start.X,l.start.Y),Point(l.end.X,l.end.Y),spectrum[i%spectrum.size()],1);
            }
        }
    }
}

void LineTarget::LoadIniSettings(minIni &ini, const string &section)
{
    Target::LoadIniSettings(ini,section);

    minAngle = ini.getd(section,"MinAngle",minAngle);
    maxAngle = ini.getd(section,"MaxAngle",maxAngle);
    minHoughVotes = ini.getd(section,"MinVotes",minHoughVotes);
    minLength = ini.getd(section,"MinLength",minLength);
    maxLength = ini.getd(section,"MaxLength",maxLength);
    maxGap = ini.getd(section,"MaxGap",maxGap);

    slopeMergeTolerance = ini.getd(section,"SlopeMergeTolerance",slopeMergeTolerance);
    endpointMergeTolerance = ini.getd(section,"EndpointMergeTolerance",endpointMergeTolerance);
    minVotesPerMerge = ini.geti(section,"MinVotesPerMerge",minVotesPerMerge);
    endpointRadius = ini.geti(section,"EndpointRadius",endpointRadius);

    zeroIsVertical = !!ini.geti(section,"ZeroIsVertical",zeroIsVertical);

}

// use OpenCV's hough line detection to find candidate lines in the frame
// then filter them using a subclass's processCandidates function
void LineTarget::ProcessCandidateImage(Mat &binaryImage)
{
    vector<Line2D*> candidates;
    rawLines.clear();

    Canny(binaryImage,binaryImage,160,200,3);
    //imshow("canny",binaryImage);
    dilate(binaryImage,binaryImage,Mat());
    //imshow("dilate",binaryImage);
    //cvWaitKey(1);

    // do a hough line detection
    vector<Vec4i> cvlines;
    HoughLinesP(binaryImage, cvlines, 1, CV_PI/180, minHoughVotes, minLength, maxGap);

    // convert the OpenCV line to our Line class
    for(vector<Vec4i>::iterator i=cvlines.begin(); i!=cvlines.end(); i++)
    {
        rawLines.push_back(Line2D(Point2D((*i)[0],(*i)[1]),Point2D((*i)[2],(*i)[3])));
    }

    // now we need to filter the lines and merge close ones together
    SlopeAndEndPointMergeLines(rawLines, candidates);

    // sort the lines from bottom to top (so the closest one is first)
    sort(candidates.begin(), candidates.end(), LineTarget::IsLineLower);

    ProcessCandidates(candidates);

    for(vector<Line2D*>::iterator it=candidates.begin(); it!=candidates.end(); it++)
        delete *it;
}

// this method performs line merging based on end-points and slope similarity; any lines
// that have a similar enough slope and have end-points near enough to each other are
// averaged and added to a list of merged lines
void LineTarget::SlopeAndEndPointMergeLines(vector<Line2D> &lines, vector<Line2D*> &merged)
{
    // we'll place all the lines into buckets based on their similarity
    // each line is placed into a single bucket
    // at the end all buckets of a minimum size or larger are merged together
    // (buckets with too few items have all their items added to the final candidates)
    buckets.clear();
    map<int,int> keys; // keys[i] = j, where j is the bucket line i is in
    map<int,bool> placedInBucket;
    for(unsigned int i=0; i<lines.size(); i++)
        placedInBucket.insert(std::pair<int,bool>(i,false));

    for(unsigned int i=0; i<lines.size(); i++)
    {
        if(!placedInBucket[i])
        {
            // line i hasn't been placed yet so create a new bucket
            placedInBucket[i] = true;
            keys.insert(std::pair<int,int>(i,buckets.size()));
            buckets.push_back(vector<Line2D>());
            buckets[buckets.size()-1].push_back(lines[i]);
        }

        Line2D &l1 = lines[i];

        for(unsigned int j=i+1; j<lines.size(); j++)
        {
            Line2D &l2 = lines[j];

            Point2D v1 = l1.ToVector();
            v1 = v1/v1.Magnitude();
            Point2D v2 = l2.ToVector();
            v2 = v2/v2.Magnitude();

            double cosTheta = v1.Dot(v2);

            if(!placedInBucket[j] &&
               1-fabs(cosTheta) < slopeMergeTolerance &&
               (l1.Intersects(l2) || EndpointsNearby(l1,l2,endpointMergeTolerance))
            )
            {
                // the lines are roughly parallel and either intersecting or have endpoints near each other
                // put j in the same bucket as i
                placedInBucket[j] = true;
                int bucketIndex = keys.at(i);
                keys.insert(std::pair<int,int>(j, bucketIndex));
                buckets[bucketIndex].push_back(lines[j]);
            }
        }
    }

    // now that we have all the lines grouped into buckets merge the lines together
    for(unsigned int i=0; i<buckets.size(); i++)
    {
        if(buckets[i].size() >= (unsigned int)minVotesPerMerge)
        {
            vector<Line2D> &bucket = buckets[i];
            Line2D *mergedLine = MergeBucket(bucket);
            merged.push_back(mergedLine);
        }
        else
        {
            // add all lines in the bucket to the list
            for(unsigned int j=0; j<buckets[i].size(); j++)
            {
                merged.push_back(new Line2D(buckets[i][j].start,buckets[i][j].end));
            }
        }
    }
}

Line2D *LineTarget::MergeBucket(vector<Line2D> &bucket)
{
    // average all of the lines
    // note: this isn't the prettiest solution, but it should work for now

    Point2D start(0,0);
    Point2D end(0,0);

    for(vector<Line2D>::iterator it=bucket.begin(); it!=bucket.end(); it++)
    {
        Line2D l = *it;
        l.SortTopToBottom();

        start = start+l.start;
        end = end+l.end;
    }
    start = start/(int)bucket.size();
    end = end/(int)bucket.size();

    return new Line2D(start,end);

}

bool LineTarget::IsLineLower(Line2D *a, Line2D *b)
{
    return (a->start.Y + a->end.Y)/2.0 > (b->start.Y + b->end.Y)/2.0;
}

bool LineTarget::EndpointsNearby(Line2D &l1, Line2D &l2, double radius)
{
    return (l1.start-l2.start).Magnitude() < radius ||
           (l1.start-l2.end).Magnitude() < radius ||
           (l1.end-l2.start).Magnitude() < radius ||
           (l1.end-l2.end).Magnitude() < radius;
}
