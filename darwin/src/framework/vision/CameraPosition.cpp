#include "include/darwin/framework/CameraPosition.h"

#include <iostream>
#include <MotionStatus.h>
#include <JointData.h>
#include <cmath>
#include <Math.h>
#include <Kinematics.h>
#include <Head.h>
#include <Camera.h>
#include <BoundingBox.h>

using namespace std;
using namespace Robot;

CameraPosition::CameraPosition()
{
    RecalculatePosition();
}

CameraPosition::~CameraPosition()
{
    // do nothing
}

// NOTE: the left leg calculations have not been thoroughly tested yet; there may be bugs
// angles are measured in degrees from horizontal (and should all be positive and <180)
// offsets are measured in mm horizontally from the ankle.  positive values are in front of the ankle
void CameraPosition::RecalculatePosition(JointData &joints)
{
    double rAnkleAngle, lAnkleAngle;

    double rKneeHeight, lKneeHeight;
    double rKneeAngle, lKneeAngle;
    double rKneeOffset, lKneeOffset;

    double rHipHeight, lHipHeight;
    double rHipOffset, lHipOffset;
    double rHipAngle, lHipAngle;

    // calculate the camera's height above ground
    // these include some magic number offsets to translate the raw motor angles into convenient body angles
    // first the ankle...
    rAnkleAngle = 90 - joints.GetAngle(JointData::ID_R_ANKLE_PITCH);
    rKneeHeight = sin(deg2rad(rAnkleAngle)) * Kinematics::CALF_LENGTH + Kinematics::ANKLE_LENGTH;
    rKneeOffset = cos(deg2rad(rAnkleAngle)) * Kinematics::CALF_LENGTH;

    lAnkleAngle = 90 + joints.GetAngle(JointData::ID_L_ANKLE_PITCH);
    lKneeHeight = sin(deg2rad(lAnkleAngle)) * Kinematics::CALF_LENGTH + Kinematics::ANKLE_LENGTH;
    lKneeOffset = cos(deg2rad(lAnkleAngle)) * Kinematics::CALF_LENGTH;

    // ...then the knee (which is offset by the ankle) and the hip (which is offset by the knee)
    rKneeAngle = joints.GetAngle(JointData::ID_R_KNEE) + rAnkleAngle;
    rHipHeight = sin(deg2rad(rKneeAngle)) * Kinematics::THIGH_LENGTH + rKneeHeight;
    rHipOffset = cos(deg2rad(rKneeAngle)) * Kinematics::THIGH_LENGTH + rKneeOffset;
    rHipAngle = joints.GetAngle(JointData::ID_R_HIP_PITCH) + rKneeAngle;

    lKneeAngle = -joints.GetAngle(JointData::ID_L_KNEE) + lAnkleAngle;
    lHipHeight = sin(deg2rad(lKneeAngle)) * Kinematics::THIGH_LENGTH + lKneeHeight;
    lHipOffset = cos(deg2rad(lKneeAngle)) * Kinematics::THIGH_LENGTH + lKneeOffset;
    lHipAngle = -joints.GetAngle(JointData::ID_L_HIP_PITCH) + lKneeAngle;

    // determine which leg is the support leg, use the hip parameters it generated for further calculations
    if(rHipHeight >= lHipHeight)
    {
        // right leg is more extended; it is the support leg
        ankleAngle = rAnkleAngle;
        kneeHeight = rKneeHeight;
        kneeOffset = rKneeOffset;

        kneeAngle = rKneeAngle;
        hipHeight = rHipHeight;
        hipOffset = rHipOffset;

        hipAngle = rHipAngle;
    }
    else
    {
        // left leg is more extended; it is the support leg
        ankleAngle = lAnkleAngle;
        kneeHeight = lKneeHeight;
        kneeOffset = lKneeOffset;

        kneeAngle = lKneeAngle;
        hipHeight = lHipHeight;
        hipOffset = lHipOffset;

        hipAngle = lHipAngle;
    }

	// Using these numbers makes GetRange significantly more accurate. They were determined empirically, but seem to work. JDJ
	if(kneeAngle < 130)
    	hipAngle += 8.5;
    else {
    	hipAngle += 5.5;
	}

    // then the neck (which is offset by the hip
    neckHeight = sin(deg2rad(hipAngle)) * (Kinematics::TORSO_LENGTH + Kinematics::NECK_LENGTH) + hipHeight;
    neckOffset = cos(deg2rad(hipAngle)) * (Kinematics::TORSO_LENGTH + Kinematics::NECK_LENGTH) + hipOffset;

    // and finally the camera
    neckAngle = hipAngle - 90;
    cameraHeight = sin(deg2rad(neckAngle)) * Kinematics::NECK_TO_FACE + neckHeight + sin(deg2rad(neckAngle+joints.GetAngle(JointData::ID_HEAD_TILT))) * Kinematics::CAMERA_DISTANCE;
    cameraOffset = cos(deg2rad(neckAngle)) * Kinematics::NECK_TO_FACE + neckOffset + cos(deg2rad(neckAngle+joints.GetAngle(JointData::ID_HEAD_TILT))) * Kinematics::CAMERA_DISTANCE;
    cameraAngle = neckAngle + joints.GetAngle(JointData::ID_HEAD_TILT) - Kinematics::EYE_TILT_OFFSET_ANGLE;

    cameraPan = joints.GetAngle(JointData::ID_HEAD_PAN);
//cout<<lKneeHeight<<" "<<lHipHeight<<" "<<neckHeight<<" "<<cameraHeight<<endl;
//cout<<lAnkleAngle<<" "<<lKneeAngle<<" "<<hipAngle<<" "<<neckAngle<<" "<<joints.GetAngle(JointData::ID_HEAD_TILT)<<" "<<cameraAngle<<endl;
#ifdef DEBUG
    cout << "\tLeft\tRight" << endl
         << "Angles" << endl
         << "Ankle\t" << lAnkleAngle << "\t" << rAnkleAngle << endl
         << "Knee\t" << lKneeAngle << "\t" << rKneeAngle << endl
         << "Hip\t" << lHipAngle << "\t" << rHipAngle << endl
         << "Neck\t" << neckAngle << endl
         << "Cam\t" << cameraAngle << endl
         << endl << "Heights" << endl
         << "Knee\t" << lKneeHeight << "\t" << rKneeHeight << endl
         << "Hip\t" << lHipHeight << "\t" << rHipHeight << endl
         << "Neck\t" << neckHeight << endl
         << "Cam\t" << cameraHeight << endl
         << endl << "Offsets" << endl
         << "Knee\t" << lKneeOffset << "\t" << rKneeOffset << endl
         << "Hip\t" << lHipOffset << "\t" << rHipOffset << endl
         << "Neck\t" << neckOffset << endl
         << "Cam\t" << cameraOffset << endl
         << endl;
#endif
}

double CameraPosition::CalculateRange(Point2D &objectInFrame, double heightAboveGround)
{

	const double DISTANCE_MULTIPLIER = 1.0;

    // first calculate the angles (pan, tilt) of the object within the frame
    // (copied from BallFollower)
    Point2D frameCenter = Point2D(Camera::WIDTH/2, Camera::HEIGHT/2);
    Point2D anglesInFrame = objectInFrame - frameCenter;
    anglesInFrame *= -1; // Inverse X-axis, Y-axis
    anglesInFrame.X *= (Camera::VIEW_H_ANGLE / (double)Camera::WIDTH); // pixel per angle
    anglesInFrame.Y *= (Camera::VIEW_V_ANGLE / (double)Camera::HEIGHT); // pixel per angle

    // do some simple trig to calculate where the object is in the frame
    double objectDownAngle = anglesInFrame.Y + cameraAngle;
    double objectRange = fabs(tan(deg2rad(abs(90+objectDownAngle)))) * abs(cameraHeight-heightAboveGround) + cameraOffset;

#ifdef DEBUG
    cout << "ankle: " << ankleAngle << endl;
    cout << "knee: " << kneeAngle << " " << kneeHeight << " " << kneeOffset << endl;
    cout << "hip: " << hipAngle << " " << hipHeight << " " << hipOffset << endl;
    cout << "neck: " << neckAngle << " " << neckHeight << " " << neckOffset << endl;
    cout << "camera: " << cameraAngle << " " << cameraHeight << " " << cameraOffset << endl;
    cout << "frame: " << anglesInFrame.X << " " << anglesInFrame.Y << endl;
    cout << "object angle: " << objectDownAngle << " = " << anglesInFrame.Y << " + " << cameraAngle << endl;
    cout << "object range: " << objectRange << " = " << "abs(" << (tan(deg2rad(90+objectDownAngle))) << " * " << (cameraHeight-heightAboveGround) << ") + " << cameraOffset << endl;
#endif
    
    return (objectRange - Kinematics::ANKLE_TO_TOE) / cos(deg2rad(anglesInFrame.X)) * DISTANCE_MULTIPLIER;
}

// average the four corners and centre to approximate the range
double CameraPosition::CalculateRange(BoundingBox &objectInFrame, double heightAboveGround)
{
    Point2D tl, tr, bl, br, ctr;
    ctr = objectInFrame.center;
    tl.X = ctr.X - objectInFrame.width/2;
    tl.Y = ctr.Y - objectInFrame.height/2;
    tr.X = ctr.X + objectInFrame.width/2;
    tr.Y = ctr.Y - objectInFrame.height/2;
    bl.X = ctr.X - objectInFrame.width/2;
    bl.Y = ctr.Y + objectInFrame.height/2;
    br.X = ctr.X + objectInFrame.width/2;
    br.Y = ctr.Y + objectInFrame.height/2;

    double range = (CalculateRange(ctr, heightAboveGround) +
                    CalculateRange(tl, heightAboveGround) +
                    CalculateRange(tr, heightAboveGround) +
                    CalculateRange(bl, heightAboveGround) +
                    CalculateRange(br, heightAboveGround))/5.0;
    return range - Kinematics::ANKLE_TO_TOE;
}

double CameraPosition::CalculateAngle(Point2D &objectInFrame, double heightAboveGround)
{
    // TODO: actually do something with heightAboveGround ??
    (void) heightAboveGround;

    // calculate the angles (pan, tilt) of the object within the frame
    // (copied from BallFollower)
    Point2D frameCenter = Point2D(Camera::WIDTH/2, Camera::HEIGHT/2);

    Point2D pt = objectInFrame - frameCenter;

    // multiply by pixels per angle
    const double H_PIXELS_PER_ANGLE = (Camera::VIEW_H_ANGLE / (double)Camera::WIDTH);
    const double V_PIXELS_PER_ANGLE = (Camera::VIEW_V_ANGLE / (double)Camera::HEIGHT);

    pt.X *= H_PIXELS_PER_ANGLE;
    pt.Y *= V_PIXELS_PER_ANGLE;

    // invert the axes
    pt *= -1;

    // offset by the head angle
    double angle = pt.X + cameraPan;
    return angle;
}

// average the four corners and centre to approximate the angle
double CameraPosition::CalculateAngle(BoundingBox &objectInFrame, double heightAboveGround)
{
    Point2D tl, tr, bl, br, ctr;
    ctr = objectInFrame.center;
    tl.X = ctr.X - objectInFrame.width/2;
    tl.Y = ctr.Y - objectInFrame.height/2;
    tr.X = ctr.X + objectInFrame.width/2;
    tr.Y = ctr.Y - objectInFrame.height/2;
    bl.X = ctr.X - objectInFrame.width/2;
    bl.Y = ctr.Y + objectInFrame.height/2;
    br.X = ctr.X + objectInFrame.width/2;
    br.Y = ctr.Y + objectInFrame.height/2;

    double angle = (CalculateAngle(ctr, heightAboveGround) +
                    CalculateAngle(tl, heightAboveGround) +
                    CalculateAngle(tr, heightAboveGround) +
                    CalculateAngle(bl, heightAboveGround) +
                    CalculateAngle(br, heightAboveGround))/5.0;
    return angle;
}

//NOTE: Downward from the horizontal is positive. I'm not changing this for fear of what it might affect. - JDJ
double CameraPosition::CalculateVerticalAngle(Point2D &objectInFrame, double heightAboveGround)
{
    (void)heightAboveGround;

    // first calculate the angles (pan, tilt) of the object within the frame
    // (copied from BallFollower)
    Point2D frameCenter = Point2D(Camera::WIDTH/2, Camera::HEIGHT/2);
    Point2D anglesInFrame = objectInFrame - frameCenter;
    anglesInFrame *= -1; // Inverse X-axis, Y-axis
    anglesInFrame.X *= (Camera::VIEW_H_ANGLE / (double)Camera::WIDTH); // pixel per angle
    anglesInFrame.Y *= (Camera::VIEW_V_ANGLE / (double)Camera::HEIGHT); // pixel per angle

    return -(anglesInFrame.Y + cameraAngle);
}
