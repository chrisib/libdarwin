/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * OpenCV Preprocessor classes that can be used with the YuvCamera class
 ************************************************************************/

#include "CvPreprocessor.h"
#include <cstring>
#include <iostream>

using namespace Robot;
using namespace std;
using namespace cv;

CvPreprocessor *CvPreprocessor::MakePreprocessor(const char* name)
{
    const int MAX_PARAMS = 5;

    char cpy[strlen(name)];
    strcpy(cpy, name);

    // find out if there is a size associated with the preprocessor
    char *params[MAX_PARAMS];
    for(int i=0; i<MAX_PARAMS; i++)
        params[i] = NULL;

    int nParams = 0;
    for(char* ch = cpy; *ch!=0 && nParams < MAX_PARAMS; ch++)
    {
        if(*ch==' ')
        {
            *ch = 0;
            ch++;
            params[nParams] = ch;
            nParams++;
        }
    }
#ifdef DEBUG
    for(int i=0; i<nParams; i++)
        cout << ">" << params[i] << "<" << endl;
#endif
    if(!strcmp(cpy,"gaussian"))
    {
        int param = atoi(params[0]);
        if(param < 1)
            param = DEFAULT_FILTER_SIZE;
        return new GaussianPreprocessor(param);
    }
    else if(!strcmp(cpy,"median"))
    {
        int param = atoi(params[0]);
        if(param < 1)
            param = DEFAULT_FILTER_SIZE;
        return new MedianPreprocessor(param);
    }
    else if(!strcmp(cpy,"erode"))
    {
        int param = atoi(params[0]);
        if(param < 1)
            param = DEFAULT_FILTER_SIZE;
        return new ErodePreprocessor(param);
    }
    else if(!strcmp(cpy,"dilate"))
    {
        int param = atoi(params[0]);
        if(param < 1)
            param = DEFAULT_FILTER_SIZE;
        return new DilatePreprocessor(param);
    }
    else if(!strcmp(cpy,"contrast"))
    {
        double a = atof(params[0]);
        double b = atof(params[1]);

        return new ContrastPreprocessor(a,b);
    }
    else if(!strcmp(cpy,"background"))
    {
        int param = atoi(params[0]);
        if(param < 1)
            param = 0;
        return new BackgroundRemover(param);
    }
    else
    {
        cerr << "Unknown CvPreprocessor: " << name << endl;
        return NULL;
    }
}

const char *GaussianPreprocessor::Name()
{
    stringstream ss;

    ss << "gaussian " << this->filterSize;
    return ss.str().c_str();
}

const char *MedianPreprocessor::Name()
{
    stringstream ss;

    ss << "median " << this->filterSize;
    return ss.str().c_str();
}

const char *DilatePreprocessor::Name()
{
    stringstream ss;

    ss << "dilate " << this->filterSize;
    return ss.str().c_str();
}

const char *ErodePreprocessor::Name()
{
    stringstream ss;

    ss << "erode " << this->filterSize;
    return ss.str().c_str();
}

const char *ContrastPreprocessor::Name()
{
    stringstream ss;

    ss << "contrast " << alpha << " " << beta;
    return ss.str().c_str();
}

const char *BackgroundRemover::Name()
{
    stringstream ss;

    ss << "background " << this->filterSize;
    return ss.str().c_str();
}

void GaussianPreprocessor::Process(cv::Mat &image)
{
    cv::Mat dst(image.rows, image.cols, image.type());
    cv::GaussianBlur(image, dst, Size(filterSize,filterSize),0);
    dst.copyTo(image);
}

void MedianPreprocessor::Process(cv::Mat &image)
{
    cv::Mat dst(image.rows, image.cols, image.type());
    cv::medianBlur(image, dst, filterSize);
    dst.copyTo(image);
}

void ErodePreprocessor::Process(cv::Mat &image)
{
    cv::Mat dst(image.rows, image.cols, image.type());
    cv::erode(image, dst, Mat(),Point(-1,-1),filterSize);   // filterSize == numIterations
    dst.copyTo(image);
}

void DilatePreprocessor::Process(cv::Mat &image)
{
    cv::Mat dst(image.rows, image.cols, image.type());
    cv::dilate(image, dst, Mat(),Point(-1,-1),filterSize);   // filterSize == numIterations
    dst.copyTo(image);
}

ContrastPreprocessor::ContrastPreprocessor(double alpha, double beta) : CvPreprocessor()
{
    this->alpha = alpha;
    this->beta = beta;
}

void ContrastPreprocessor::Process(cv::Mat &image)
{
#if 1
    Mat tmp;
    image.convertTo(tmp,image.type(),alpha, beta);
    tmp.copyTo(image);
#else
    for( int y = 0; y < image.rows; y++ )
    {
        for( int x = 0; x < image.cols; x++ )
        {
            for( int c = 0; c < 3; c++ )
            {
                image.at<Vec3b>(y,x)[c] = saturate_cast<uchar>( alpha*( image.at<Vec3b>(y,x)[c] ) + beta );
            }
        }
    }
#endif
}

void BackgroundRemover::Process(Mat &image)
{
    cv::Scalar avg = mean(image);

    // black in YCrCb (used to fill in the background to reduce noise)
    const Scalar black(0,128,128);

    int v;

    const int channels = image.channels();
    for( int y = 0; y < image.rows; y++ )
    {
        for( int x = 0; x < image.cols; x++ )
        {
            bool matchesBG = true;
            for( int c = 0; c < channels && matchesBG; c++ )
            {
                v = image.at<Vec3b>(y,x)[c];

                if(v < avg[c] - filterSize || v > avg[c] + filterSize)
                    matchesBG = false;
            }

            // if the pixel matched the average then we assume it's the background and can remove it (set it to black)
            if(matchesBG)
            {
                for( int c = 0; c < channels; c++ )
                    image.at<Vec3b>(y,x)[c] = black[c];
            }
        }
    }
}
