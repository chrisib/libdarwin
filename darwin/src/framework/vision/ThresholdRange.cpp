#include "ThresholdRange.h"
#include <iostream>

using namespace Robot;
using namespace std;

ThresholdRange::ThresholdRange()
{
    min=0;
    max=0;
}

ThresholdRange::ThresholdRange(int min, int max)
{
    this->max=max;
    this->min=min;
}

ThresholdRange::~ThresholdRange()
{

}

bool ThresholdRange::Check(int v)
{
    return min <= max && v>=min && v<=max;
}

void ThresholdRange::Print()
{
    cout << "[" << min << "," << max << "]" << endl;
}
