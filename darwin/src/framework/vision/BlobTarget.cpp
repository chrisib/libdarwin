#include "BlobTarget.h"
#include <iostream>

using namespace std;
using namespace Robot;
using namespace cv;

BlobTarget::BlobTarget() : Target()
{
    scanlineParam = new vector<BlobTarget*>();
    name = NULL;

    // NOTE: if these are not set elsewhere we may encounter divide-by-zero errors
    realHeight = 0;
    realWidth = 0;

    keyChannelRange = &yRange;

    // scanline parameters
    keyChannel = 0;
    minScanLength = 5;
    scanThreshold = 24;
    fillThreshold = 24;

    minPixelWidth = 0;
    minPixelHeight = 0;

    // enable/disable different properties to check for
    proportionsDisabled = false;
    fillDisabled = false;
    sizeDisabled = false;
    yRangeDisabled = false;
    uRangeDisabled = false;
    vRangeDisabled = false;
    keyRangeDisabled = false;
}

BlobTarget::~BlobTarget()
{
    delete scanlineParam;
}

void BlobTarget::LoadIniSettings(minIni &ini, const string &iniSection)
{
    Target::LoadIniSettings(ini,iniSection);

    realHeight = ini.geti(iniSection,"RealHeight");
    realWidth = ini.geti(iniSection,"RealWidth");

    SetKeyChannel(ini.geti(iniSection,"KeyChannel",1));
    minScanLength = ini.geti(iniSection,"MinScanLength",8);
    scanThreshold = ini.geti(iniSection,"ScanThreshold",24);
    fillThreshold = ini.geti(iniSection,"FillThreshold",24);
    minPixelHeight = ini.geti(iniSection,"MinPixelHeight",10);
    minPixelWidth = ini.geti(iniSection,"MinPixelWidth",10);

    subsample = ini.geti(iniSection,"Subsample",2);

    sizeDisabled = ini.geti(iniSection,"SizeDisabled",0);
    proportionsDisabled = ini.geti(iniSection,"ProportionsDisabled",0);
    fillDisabled = ini.geti(iniSection,"FillDisabled",0);
    yRangeDisabled = ini.geti(iniSection,"YRangeDisabled",0);
    yRangeDisabled = ini.geti(iniSection,"URangeDisabled",0);
    yRangeDisabled = ini.geti(iniSection,"VRangeDisabled",0);
    keyRangeDisabled = ini.geti(iniSection,"KeyRangeDisabled",0);
}

void BlobTarget::SetKeyChannel(int x)
{
    keyChannel = x;

    switch(x)
    {
        case 2:
            keyChannelRange = &vRange;
            break;

        case 1:
            keyChannelRange = &uRange;
            break;

        default:
        case 0:
            keyChannelRange = &yRange;
            break;
    }
}

void BlobTarget::ProcessCandidateImage(Mat &binaryImage)
{
    std::vector<std::vector<Point> > contours;
    std::vector<BoundingBox*> *boundingBoxes = new vector<BoundingBox*>;

    // find the contours of the objects
    //cout << "finding contours" << endl;
    cv::findContours(binaryImage,contours,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);
    //cout << "done" << endl;

    // go through and turn the contours into bounding boxes
    // NOTE: we can't determine the compactness of objects with this algorithm
    unsigned int numContours = contours.size();
    for(unsigned int c=0; c<numContours; c++)
    {
        unsigned int numPoints = contours[c].size();
        int maxX = -1;
        int minX = 99999;
        int maxY = -1;
        int minY = 99999;
        for(unsigned int p=0; p<numPoints; p++)
        {
            Point pt = contours[c][p];

            if(pt.x < minX)
                minX = pt.x;
            if(pt.x > maxX)
                maxX = pt.x;
            if(pt.y < minY)
                minY = pt.y;
            if(pt.y > maxY)
                maxY = pt.y;
        }

        if(minX >= ignoreLeft && maxX <= (binaryImage.cols - ignoreRight) && minY >= ignoreTop && maxY <= (binaryImage.rows - ignoreBottom))
        {

            BoundingBox *bbox = new BoundingBox();
            bbox->center.X = (maxX + minX)/2;
            bbox->center.Y = (maxY + minY)/2;
            bbox->height = maxY - minY + 1;
            bbox->width = maxX - minX + 1;

            // just hard-set all the other parameters for the bounding box since we have no idea what they really are
            bbox->fill = 1.0;   // hack because we really have no idea
            bbox->avgY = (yRange.max + yRange.min)/2;
            bbox->avgU = (uRange.max + uRange.min)/2;
            bbox->avgV = (vRange.max + vRange.min)/2;

            //cout << minX << " " << maxX << " :: " << minY << " " << maxY << endl;

            boundingBoxes->push_back(bbox);
        }
    }

    ProcessCandidates(*boundingBoxes);

    for(vector<BoundingBox*>::iterator it=boundingBoxes->begin(); it!=boundingBoxes->end(); it++)
        delete *it;
    delete boundingBoxes;
}

void BlobTarget::FindTargets(vector<BlobTarget*> &targets, Mat &src, Mat *dest, int subsample)
{
    // create RGB and RGB* images to look for additional edges
    Mat rgb = cv::Mat::zeros(src.rows, src.cols, src.type());
    cv::cvtColor(src,rgb,CV_YCrCb2RGB);
    Mat *rgbCompliment = Robot::Target::CreateCompliment(rgb);

    FindTargets(targets, src, rgb, *rgbCompliment, dest, subsample);
    delete rgbCompliment;
}

void BlobTarget::FindTargets(vector<BlobTarget*> &targets, Mat &src, Mat &rgb, Mat &rgbc, Mat *dest, int subsample)
{
    // quick-access to loop-boundary variables so we can avoid unnecessary function calls
    int numChannels = src.channels();
    unsigned int numTargets = targets.size();

    // kick out of there are no targets to look for
    if(numTargets<=0)
        return;
    if(subsample<1)
        subsample = 1;

    // find the largest scan & fill thresholds
    // find the most extreme ignore ranges (i.e. scan the largest area possible)
    // use the shortest scan length
    int scanThreshold = targets[0]->GetScanThreshold();
    int minLength = targets[0]->GetMinScanLength();
    int minRow = targets[0]->GetIgnoreTop();
    int maxRow = Camera::HEIGHT - targets[0]->GetIgnoreBottom()-1;
    int minCol = targets[0]->GetIgnoreLeft();
    int maxCol = Camera::WIDTH - targets[0]->GetIgnoreRight()-1;
    for(unsigned int i=1; i<numTargets; i++)
    {
        if(targets[i]->GetScanThreshold() > scanThreshold)
            scanThreshold = targets[i]->GetScanThreshold();

        if(targets[i]->GetMinScanLength() < minLength)
            minLength = targets[i]->GetMinScanLength();

        if(targets[i]->GetIgnoreTop() < minRow)
            minRow = targets[i]->GetIgnoreTop();

        if(targets[i]->GetIgnoreLeft() < minCol)
            minCol = targets[i]->GetIgnoreLeft();

        if(Camera::HEIGHT - targets[i]->GetIgnoreBottom()-1 > maxRow)
            maxRow = Camera::HEIGHT - targets[i]->GetIgnoreBottom()-1;

        if(Camera::WIDTH - targets[i]->GetIgnoreLeft()-1 > maxCol)
            maxCol = Camera::WIDTH - targets[i]->GetIgnoreRight()-1;
    }
/*
    if(minRow < 0)
        minRow = 0;
    if(maxRow >= Camera::HEIGHT)
        maxRow = Camera::HEIGHT-1;
    if(minCol < 0)
        minCol = 0;
    if(maxCol >= Camera::WIDTH)
        maxCol = Camera::WIDTH-1;
*/
#ifdef DEBUG
    cerr << "Doing multiple target scanline" << endl <<
            "\tScan Threshold: " << scanThreshold << endl <<
            "\tScan Length: " << minLength << endl <<
            "\tRow Range: " << minRow << " " << maxRow << endl <<
            "\tCol Range: " << minCol << " " << maxCol << endl;
#endif

    Mat processed = Mat::zeros(src.rows, src.cols, src.type());

    // create a vector of bounding boxes for each target
    vector<BoundingBox*> boundingBoxes[numTargets];
    BoundingBox fillResult;

    bool done = false;

    // get byte arrays for the three input images (YUV, RGB, RGB*), and the output image
    //IplImage *yuvImg = new IplImage(src);
    //IplImage *rgbImg = new IplImage(rgb);
    //IplImage *rgbcImg = new IplImage(*rgbCompliment);
    //IplImage *processedImg = new IplImage(processed);
    IplImage yuvImg(src);
    IplImage rgbImg(rgb);
    IplImage rgbcImg(rgbc);
    IplImage processedImg(processed);
    uint8_t *yuvData = (uint8_t*) (yuvImg.imageData);
    uint8_t *rgbData = (uint8_t*) (rgbImg.imageData);
    uint8_t *rgbcData = (uint8_t*) (rgbcImg.imageData);
    uint8_t *processedData = (uint8_t*) (processedImg.imageData);

    int lineStart = minCol;
    int lineEnd = minCol;
    int currentRow = minRow;

    int maxIndex = src.rows * src.cols * numChannels;

    // average YUV channel values
    int avgY, avgU, avgV, y,u,v;

    // average RGB channel values
    int avgR, avgG, avgB, r,g,b;

    // average RGB* channel values
    int avgRG, avgRB, avgGB, rg,rb,gb;

    int index;

    int blobsFound = 0;
    while(!done)
    {
        index = RowCol2Index(currentRow,lineEnd,processed);

        if(index < 0 || index >= maxIndex)
        {
            //cerr << "Invalid index" << endl;
            //cerr << "(" << currentRow << "," << lineEnd << ") --> " << index << "[" << maxIndex << "]" << endl;
        }
        else if(processedData[index]==0x00)    // if we have not already processed this pixel
        {
            y = yuvData[index];
            u = yuvData[index+1];
            v = yuvData[index+2];

            r = rgbData[index];
            g = rgbData[index+1];
            b = rgbData[index+2];

            rg = rgbcData[index];
            rb = rgbcData[index+1];
            gb = rgbcData[index+2];

            avgY = y*subsample;
            avgU = u*subsample;
            avgV = v*subsample;

            avgR = r*subsample;
            avgG = g*subsample;
            avgB = b*subsample;

            avgRG = rg*subsample;
            avgRB = rb*subsample;
            avgGB = gb*subsample;

            int startIndex = RowCol2Index(currentRow,0,processed);


            // expand the current line as far as possible
            // keep going until we hit a pixel that has been processed already, or one that is out of bounds, or is the wrong colour
            for(lineEnd = lineStart+subsample;
                lineEnd <= maxCol && processedData[startIndex+lineEnd] == 0x00 &&
                abs(y - avgY/(lineEnd-lineStart)) < scanThreshold && abs(u - avgU/(lineEnd-lineStart)) < scanThreshold && abs(v - avgV/(lineEnd-lineStart)) < scanThreshold &&
                abs(r - avgR/(lineEnd-lineStart)) < scanThreshold && abs(g - avgG/(lineEnd-lineStart)) < scanThreshold && abs(b - avgB/(lineEnd-lineStart)) < scanThreshold &&
                abs(rg - avgRG/(lineEnd-lineStart)) < scanThreshold && abs(rb - avgRB/(lineEnd-lineStart)) < scanThreshold && abs(gb - avgGB/(lineEnd-lineStart)) < scanThreshold;
                lineEnd+=subsample)
            {
                index = RowCol2Index(currentRow,lineEnd,processed);
                //index+=src.channels();
                y = yuvData[index];
                u = yuvData[index+1];
                v = yuvData[index+2];

                r = rgbData[index];
                g = rgbData[index+1];
                b = rgbData[index+2];

                rg = rgbcData[index];
                rb = rgbcData[index+1];
                gb = rgbcData[index+2];

                avgY += yuvData[index]*subsample;
                avgU += yuvData[index+1]*subsample;
                avgV += yuvData[index+2]*subsample;

                avgR += rgbData[index]*subsample;
                avgG += rgbData[index+1]*subsample;
                avgB += rgbData[index+2]*subsample;

                avgRG += rgbcData[index]*subsample;
                avgRB += rgbcData[index+1]*subsample;
                avgGB += rgbcData[index+2]*subsample;
            }
#ifdef DEBUG
            if(lineEnd-lineStart > subsample)
                cout << "Found line of length " << (lineEnd - lineStart) << endl;
#endif
            if(lineEnd - lineStart >= minLength)
            {
                avgY = avgY/(lineEnd-lineStart);
                avgU = avgU/(lineEnd-lineStart);
                avgV = avgV/(lineEnd-lineStart);

                avgR = avgR/(lineEnd-lineStart);
                avgG = avgG/(lineEnd-lineStart);
                avgB = avgB/(lineEnd-lineStart);

                avgRG = avgRG/(lineEnd-lineStart);
                avgRB = avgRB/(lineEnd-lineStart);
                avgGB = avgGB/(lineEnd-lineStart);

                // if this segment matches any of the targets do a flood fill
                for(unsigned int i=0; i<numTargets; i++)
                {
                    if(targets[i]->GetYRange()->Check(avgY) && targets[i]->GetURange()->Check(avgU) && targets[i]->GetVRange()->Check(avgV) &&
                       currentRow >= targets[i]->GetIgnoreTop() && currentRow <= Camera::HEIGHT - targets[i]->GetIgnoreBottom()-1)
                    {
#ifdef DEBUG
                        cout << "Found a line of length " << (lineEnd-lineStart) << " YUV: " <<
                                avgY << " ("   << targets[i]->GetYRange()->min << "," << targets[i]->GetYRange()->max << ") " <<
                                avgU << " ("   << targets[i]->GetURange()->min << "," << targets[i]->GetURange()->max << ") " <<
                                avgV << " ("   << targets[i]->GetVRange()->min << "," << targets[i]->GetVRange()->max << ") " <<
                                endl;
#endif
                        FloodFill(currentRow, (lineStart+lineEnd)/2,
                              avgY, avgU, avgV, yuvData,
                              avgR, avgG, avgB, rgbData,
                              avgRG, avgRB, avgGB, rgbcData,
                              (targets[i]->GetFillThreshold() < 0 ? targets[i]->GetScanThreshold() : targets[i]->GetFillThreshold()),
                              src, processedData, &fillResult, minRow, maxRow, minCol, maxCol, subsample);

                        if(targets[i]->GetYRange()->Check(fillResult.avgY) && targets[i]->GetURange()->Check(fillResult.avgU) && targets[i]->GetVRange()->Check(fillResult.avgV))
                        {
                            boundingBoxes[i].push_back(new BoundingBox(fillResult));
                        }

                        blobsFound++;
                    }
                }
            }
        }
        else
        {
            lineEnd = lineStart+subsample;
        }

        if(lineEnd >= maxCol)
        {
            currentRow+=subsample;
            lineStart = minCol;
        }
        else
        {
            lineStart = lineEnd;
        }

        if(currentRow > maxRow)
            done = true;
    }

    // copy the processed image to the dest matrix if necessary
    if(dest!=NULL)
    {
        //processed.copyTo(*dest);
        // copy the non-ignored area to dest
        int i;

        IplImage destImg(*dest);
        uint8_t *destData = (uint8_t*) (destImg.imageData);

        for(int row = minRow; row <= maxRow; row++)
        {
            for(int col=minCol; col<=maxCol; col++)
            {
                for(int c=0; c<numChannels; c++)
                {
                    i = RowCol2Index(row, col, *dest)+c;
                    destData[i] = processedData[i];
                }
            }
        }
    }

    // process the candidate targets for every target
    for(unsigned int i=0; i<numTargets; i++)
    {
        targets[i]->ProcessCandidates(boundingBoxes[i]);
        unsigned int numBoxes = boundingBoxes[i].size();
        for(unsigned int j = 0; j < numBoxes; j++)
            delete boundingBoxes[i][j];
    }

#ifdef DEBUG
    cerr << "ScanLine found " << blobsFound << " blobs" << endl;
#endif
}

// flood-fill a region based on an average Y value
void BlobTarget::FloodFill(int row, int col,
                                 int avgY, int avgU, int avgV, uint8_t *yuvData,
                                 int avgR, int avgG, int avgB, uint8_t *rgbData,
                                 int avgRG, int avgRB, int avgGB, uint8_t *rgbcData,
                                 int threshold, cv::Mat &src, uint8_t *destData, BoundingBox *result,
                                 int minRow, int maxRow, int minCol, int maxCol,
                                 int subsample)
{
    if(row < minRow || row > maxRow || col < minCol || col > maxCol)
    {
        //cerr << "Flood fill origin out of range: " << row << " " << col << endl;
        return;
    }

    std::list<int> stack;
    stack.push_back(RowCol2Index(row,col,src));
    uint8_t y,u,v,r,g,b,rg,rb,gb;

    // used to calculate average YUV values for the filled region
    int sumY = 0;
    int sumU = 0;
    int sumV = 0;
    int sumR = 0;
    int sumG = 0;
    int sumB = 0;
    int sumRG = 0;
    int sumRB = 0;
    int sumGB = 0;

    // bounding box dimensions
    int boxTop = src.rows;
    int boxBottom = 0;
    int boxLeft = src.cols;
    int boxRight = 0;

    // how many pixels have we coloured in?
    // divided by box size = fill percentage
    int filledPixels = 0;

    int index;
    while(stack.size() != 0)
    {
        index = stack.back();
        stack.pop_back();
        Index2RowCol(index,&row,&col,src);

        // get the colours at this position
        y = yuvData[index];
        u = yuvData[index+1];
        v = yuvData[index+2];

        r = rgbData[index];
        g = rgbData[index+1];
        b = rgbData[index+2];

        rg = rgbcData[index];
        rb = rgbcData[index+1];
        gb = rgbcData[index+2];

        // update the colour totals
        sumY += y;
        sumU += u;
        sumV += v;

        sumR += r;
        sumG += g;
        sumB += b;

        sumRG += rg;
        sumRB += rb;
        sumGB += gb;

        // colour in the current index
        assert(destData!=NULL);
        assert(yuvData!=NULL);
        filledPixels++;
        memcpy(destData+index, yuvData+index, src.channels());
        // copy each channel (does the same thing as memcpy, but explicitly)
        //for(int i=0; i<src.channels(); i++)
        //{
        //    destData[index+i] = yuvData[index+i];
        //}

        // update the bounding box dimensions
        if(row<boxTop)
            boxTop = row;
        else if(row>boxBottom)
            boxBottom = row;
        if(col<boxLeft)
            boxLeft = col;
        else if(col>boxRight)
            boxRight = col;

        // look at the 4 pixels orthogonal to this one and add them to the stack if they are valid and
        // have not been processed already
        if(row-subsample >= minRow)
            ProcessPixel(row-subsample,col,stack,yuvData,avgY,avgU,avgV,rgbData,avgR,avgG,avgB,rgbcData,avgRG,avgRB,avgGB,threshold,destData,src);
        if(row+subsample <= maxRow)
            ProcessPixel(row+subsample,col,stack,yuvData,avgY,avgU,avgV,rgbData,avgR,avgG,avgB,rgbcData,avgRG,avgRB,avgGB,threshold,destData,src);
        if(col-subsample >= minCol)
            ProcessPixel(row,col-subsample,stack,yuvData,avgY,avgU,avgV,rgbData,avgR,avgG,avgB,rgbcData,avgRG,avgRB,avgGB,threshold,destData,src);
        if(col+subsample <= maxCol)
            ProcessPixel(row,col+subsample,stack,yuvData,avgY,avgU,avgV,rgbData,avgR,avgG,avgB,rgbcData,avgRG,avgRB,avgGB,threshold,destData,src);

        /*
        // analyze the 8 pixels surrounding the current one
        for(int i=-1; i<=1; i++)
        {
            for(int j=-1; j<=1; j++)
            {
                index = RowCol2Index(row+i,col+j,src);

                // only bother if this pixel actually exists and it lies within the region we are considering
                if(row+i >=minRow && row+i <=maxRow && col+j >=minCol && col+j <=maxCol)
                {
                    y = yuvData[index];
                    u = yuvData[index+1];
                    v = yuvData[index+2];

                    r = rgbData[index];
                    g = rgbData[index+1];
                    b = rgbData[index+2];

                    rg = rgbcData[index];
                    rb = rgbcData[index+1];
                    gb = rgbcData[index+2];

                    // only add the pixel if it meets the threshold requirement and has not been processed before
                    if(destData[index] == 0x00 && destData[index+1] == 0x00 && destData[index+2] == 0x00 &&
                       abs((int)y - avgY) < threshold && abs((int)u - avgU) < threshold  && abs((int)v - avgV) < threshold &&
                       abs((int)r - avgR) < threshold && abs((int)g - avgG) < threshold  && abs((int)b - avgB) < threshold &&
                       abs((int)rg - avgRG) < threshold && abs((int)rb - avgRB) < threshold  && abs((int)gb - avgGB) < threshold )
                    {
                        stack->push_back(index);

                        // mangle the colour at this pixel to indicate that it has been examined
                        destData[index] = 0x01;
                    }
                }
            }
        }*/
    }

    if(result!=NULL)
    {
        result->center.Y = (boxTop+boxBottom)/2;
        result->center.X = (boxLeft+boxRight)/2;
        result->height = boxBottom-boxTop+1;
        result->width = boxRight-boxLeft+1;

        if(filledPixels>0 && result->height>0 && result->width>0)
        {
            result->avgY = sumY/filledPixels;
            result->avgU = sumU/filledPixels;
            result->avgV = sumV/filledPixels;

            // number of actual pixels filled times subsample^2 (we assume all the subsampled pixels are filled)
            result->fill = ((double)filledPixels*subsample*subsample)/(double)(result->width * result->height);

            //cout << "Filled " << filledPixels << " pixels out of " << result->width*result->height << " (" << result->width << " x " << result->height << ")" << endl;
        }
    }
}

void BlobTarget::ProcessPixel(int row, int col, std::list<int> &stack,
                                    uint8_t *yuvData, int avgY, int avgU, int avgV,
                                    uint8_t *rgbData, int avgR, int avgG, int avgB,
                                    uint8_t *rgbcData, int avgRG, int avgRB, int avgGB,
                                    int threshold, uint8_t *destData, cv::Mat &src)
{
    int index = RowCol2Index(row,col,src);

    uint8_t y = yuvData[index];
    uint8_t u = yuvData[index+1];
    uint8_t v = yuvData[index+2];

    uint8_t r = rgbData[index];
    uint8_t g = rgbData[index+1];
    uint8_t b = rgbData[index+2];

    uint8_t rg = rgbcData[index];
    uint8_t rb = rgbcData[index+1];
    uint8_t gb = rgbcData[index+2];

    if(destData[index] == 0x00 && destData[index+1] == 0x00 && destData[index+2] == 0x00 &&
       abs((int)y - avgY) < threshold && abs((int)u - avgU) < threshold  && abs((int)v - avgV) < threshold &&
       abs((int)r - avgR) < threshold && abs((int)g - avgG) < threshold  && abs((int)b - avgB) < threshold &&
       abs((int)rg - avgRG) < threshold && abs((int)rb - avgRB) < threshold  && abs((int)gb - avgGB) < threshold )
    {
        stack.push_back(index);

        // mangle the colour at this pixel to indicate that it has been examined
        destData[index] = 0xff;
        destData[index+1] = 0xff;
        destData[index+2] = 0xff;
    }
}
