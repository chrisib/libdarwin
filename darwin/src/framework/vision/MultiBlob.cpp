/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is used to locate a multiple uniformly-coloured targets
 * TODO: revise the image-processing algorithm to use the scan-line algorithm
 * instead of the crude methods used currently
 ************************************************************************/

#include "Camera.h"
#include "MultiBlob.h"

using namespace Robot;
using namespace std;
using namespace cv;

MultiBlob::MultiBlob() : BlobTarget()
{
    Init("Unnamed Multi Target");
}

MultiBlob::MultiBlob(const char* name) : BlobTarget()
{
    Init(name);
}

MultiBlob::~MultiBlob()
{
    ClearAllTargets();
}


void MultiBlob::Init(const char* name)
{
    this->name = new string(name);

    SetYRange(255,0);
    SetVRange(255,0);
    SetURange(255,0);

    SetMarkColour(255,0,0);

    this->SetIgnoreTop(0);
    this->SetIgnoreBottom(0);
    this->SetIgnoreLeft(0);
    this->SetIgnoreRight(0);

    this->SetMinPixelHeight(10);
    this->SetMinPixelWidth(10);
    this->SetKeyChannel(1);
    this->SetMinScanLength(8);
    this->SetScanThreshold(24);

    filterIntersections = true;
    filterNested = true;

    scanlineParam->push_back(this);
}

int MultiBlob::FindInFrame(Mat &image, Mat *dest)
{
    // clean up the previous round of targets
    ClearAllTargets();

    //vector<BoundingBox*> *candidates = TargetProcessing::ScanLine(image, minScanLength, scanThreshold, *keyChannelRange, keyChannel, dest, ignoreTop, Camera::HEIGHT-ignoreBottom-1, ignoreLeft, Camera::WIDTH-ignoreRight-1, fillThreshold, subsample);
    //ProcessCandidates(*candidates);


    // clean up
    //while(!candidates->empty())
    //    delete (*candidates)[candidates->size()-1];
    //delete candidates;

    BlobTarget::FindTargets(*scanlineParam, image, dest, this->subsample);

    return allTargets.size();
}

int MultiBlob::FindInFrame(CvCamera *camera, cv::Mat *dest)
{
    ClearAllTargets();

    BlobTarget::FindTargets(*scanlineParam, camera->yuvFrame, camera->rgbFrame, camera->hsvFrame,dest,this->subsample);

    return allTargets.size();
}

void MultiBlob::ProcessCandidates(std::vector<BoundingBox*> &candidates)
{
    ClearAllTargets();

    BoundingBox *current;
    int votes;

    double targetRatio = (double)realHeight/(double)realWidth;
    double currentRatio;

    vector<BoundingBox*> tmpTargets;

    unsigned int numCandidates = candidates.size();
//    cout << "Analyzing " << numCandidates <<" candidates" << endl;
    for(unsigned int i=0; i<numCandidates; i++)
    {
        current = candidates[i];

        // ignore anything smaller than a certain threshold
        if(current->height >= minPixelHeight && current->width >= minPixelWidth)
        {
            votes = 0;

            currentRatio = (double) current->height / (double) current->width;

            if(!proportionsDisabled)
            {
                // proportions within [0.75,1.25] times the correct size && colours that match
                if((currentRatio < targetRatio && currentRatio * 1.25 >= targetRatio) || (currentRatio > targetRatio && currentRatio * 0.75 <= targetRatio))
                    votes++;

                // proportions are WAY off
                if((currentRatio < targetRatio && currentRatio * 1.25 < targetRatio) || (currentRatio > targetRatio && currentRatio * 0.75 > targetRatio))
                    votes--;
            }

            // each colour channel is acceptable
            if(!yRangeDisabled && yRange.Check(current->avgY))
				votes++;
            if(!uRangeDisabled && uRange.Check(current->avgU))
				votes++;
            if(!vRangeDisabled && vRange.Check(current->avgV))
				votes++;

            // extra vote if the key channel range is good
            if(!keyRangeDisabled)
            {
                switch(keyChannel)
                {
                    case 2:
                        if(keyChannelRange->Check(current->avgV))
                            votes++;
                        break;
                    case 1:
                        if(keyChannelRange->Check(current->avgU))
                            votes++;
                        break;
                    case 0:
                    default:
                    if(keyChannelRange->Check(current->avgY))
                            votes++;
                        break;
                }
            }

            if(!fillDisabled)
            {
                // fill is sufficient
                if(current->fill >= 0.5)
                    votes++;
                if(current->fill >= 0.6)
                    votes++;
                if(current->fill >= 0.7)
                    votes++;

                // fill is insufficient
                if(current->fill <0.5)
                    votes--;
                if(current->fill <0.4)
                    votes--;
                if(current->fill <0.3)
                    votes--;
            }

            // found a decent match; add this to the list of candidates
            if(votes >= GetMaxVotes()/2)
            {
                if(filterNested || filterIntersections)
                    tmpTargets.push_back(new BoundingBox(*current));
                else
                    allTargets.push_back(new BoundingBox(*current));
            }
        }
    }

    // do a second pass to filter out intersecting/nested items
    if(filterNested || filterIntersections)
    {

        // sort the candidates in ascending order of size
        sort(tmpTargets.begin(), tmpTargets.end(), BoundingBox::CompareBySize);

        for(unsigned int i=0; i<tmpTargets.size(); i++)
        {
            bool distinct = true;
            for(unsigned int j=tmpTargets.size()-1; distinct && j>i; j--)
            {
                if( (filterIntersections && tmpTargets[j]->Intersects(*(tmpTargets[i]))) ||
                     (filterNested && tmpTargets[j]->Contains(*(tmpTargets[i]))) )
                {
                    distinct = false;
                    //cerr << "Candidate " << i << " is contained by candidate " << j << endl;
                }
            }

            if(distinct)
                allTargets.push_back(tmpTargets[i]);
			else
				delete tmpTargets[i];
        }
    }
}

int MultiBlob::GetMaxVotes()
{
    int votes = 0;

    if(!proportionsDisabled)
        votes++;
    if(!fillDisabled)
        votes+=3;          // +1 for >.5, >.6, >.7
    if(!sizeDisabled)
        votes++;
    if(!yRangeDisabled)
        votes++;
    if(!uRangeDisabled)
        votes++;
    if(!vRangeDisabled)
        votes++;
    if(!keyRangeDisabled)
        votes++;

    return votes;
    /*
    cout << (proportionsDisabled ? 0:1) << endl;
    cout << (fillDisabled ? 0:1) << endl;
    cout << (sizeDisabled ? 0:1) << endl;
    cout << (yRangeDisabled ? 0:1) << endl;
    cout << (uRangeDisabled ? 0:1) << endl;
    cout << (vRangeDisabled ? 0:1) << endl;
    cout << (keyRangeDisabled ? 0:1) << endl << endl;

    return (proportionsDisabled ? 0:1) +
           (fillDisabled ? 0:3) +          // +1 for >.5, >.6, >.7
           (sizeDisabled ? 0:1) +
           (yRangeDisabled ? 0:1) +
           (uRangeDisabled ? 0:1) +
           (vRangeDisabled ? 0:1) +
           (keyRangeDisabled ? 0:1);
    */
}

void MultiBlob::Draw(Mat &img)
{
    Point p, q;
    BoundingBox *ptr;

    for(unsigned int i=0; i<allTargets.size(); i++)
    {
        ptr = allTargets[i];

        ptr->Draw(img, markColour, 1);
    }
}

void MultiBlob::ClearAllTargets()
{
    while(!allTargets.empty())
    {
        delete allTargets.back();
        allTargets.pop_back();
    }
}

void MultiBlob::Print()
{
    cout << *(this->name) << " " << allTargets.size() << " targets:" << endl;
    for(unsigned int i=0; i<allTargets.size(); i++)
    {
        cout << "\t" << (i+1) << " ";
        allTargets[i]->Print();
        cout << endl;
    }
    cout << endl;
}

void MultiBlob::LoadIniSettings(minIni &ini, const string &section)
{
    BlobTarget::LoadIniSettings(ini, section);

    filterNested = ini.geti(section,"FilterNested",1);
    filterIntersections = ini.geti(section,"FilterIntersections",1);
}
