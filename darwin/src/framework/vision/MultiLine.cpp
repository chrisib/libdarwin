#include "MultiLine.h"
#include <iostream>
#include <Math.h>

using namespace Robot;
using namespace std;
using namespace cv;

MultiLine::MultiLine() : LineTarget()
{
}

MultiLine::~MultiLine()
{
    ClearAllLines();
}

int MultiLine::FindInFrame(Mat &image, Mat *dest)
{
    // first off threshold the image to create a binary version
    Mat binary(image.rows,image.cols,CV_8UC1);
    inRange(image,Scalar(yRange.min,uRange.min,vRange.min),
            Scalar(yRange.max,uRange.max,vRange.max),binary);

    // dilate the image to reduce random pixel noise (this also makes the lines thicker)
    dilate(binary,binary,Mat(), Point(-1, -1), 1);
    dilate(binary,binary,Mat(), Point(-1, -1), 1);
    //erode(binary,binary,Mat(), Point(-1, -1), 1);

    ProcessCandidateImage(binary);

    // copy the thresholded image to the destination if required
    // because dest might be multi-channel we can't just use .copyTo since
    // that clobbers the channel data in the destination image
    if(dest!=NULL)
    {
        uchar *dstData = dest->data;
        uchar *srcData = binary.data;

        const int NUM_PIXELS = Camera::HEIGHT * Camera::WIDTH;
        for(int i=0; i<NUM_PIXELS; i++)
        {
            for(int c = 0; c<dest->channels(); c++)
            {
                if(*srcData)
                    dstData[c] = 0xff;
                else
                    dstData[c] = 0x00;
            }

            srcData++;
            dstData += dest->channels();
        }
    }

    return lines.size();
}

void MultiLine::ClearAllLines()
{
    // clean up the existing lines
    while(!lines.empty())
    {
        delete lines.back();
        lines.pop_back();
    }
}

void MultiLine::ProcessCandidates(vector<Line2D*> &candidates)
{
    ClearAllLines();

    // go through all of the candidate lines and ensure that they fit into the required region and have the appropriate angles & lengths
    Line2D *l;
    double angle;
    double length;
    int maxX, minX, maxY, minY;
    for(vector<Line2D*>::iterator it=candidates.begin(); it!=candidates.end(); it++)
    {
        l = *it;

        // calculate the angle of the line and clamp it in the 0-180 range
        if(!zeroIsVertical)
            angle = rad2deg(atan2(l->start.Y-l->end.Y,l->start.X-l->end.X));
        else
            angle = rad2deg(atan2(l->start.X-l->end.X,l->start.Y-l->end.Y));

        while(angle < 0)
            angle += 180.0;
        while(angle > 180)
            angle -= 180.0;

        // determine the bounding box the line resides in
        if(l->start.X > l->end.X)
        {
            maxX = l->start.X;
            minX = l->end.X;
        }
        else
        {
            minX = l->start.X;
            maxX = l->end.X;
        }

        if(l->start.Y > l->end.Y)
        {
            maxY = l->start.Y;
            minY = l->end.Y;
        }
        else
        {
            minY = l->start.Y;
            maxY = l->end.Y;
        }

        length=l->Length();

        //cout << "Candidate: l = " << length << " (" << minLength << " , " << maxLength << ")" << endl <<
        //        "           a = " << angle << " (" << minAngle << " , " << maxAngle << ")" << endl <<
        //        endl;

        if(length >= minLength && length <= maxLength &&    // check the length of the line

                // check the angle
                angle >= minAngle && angle <= maxAngle &&

                // check bounding box; make sure we're in the allowable region of the frame
                minX > ignoreLeft && minY > ignoreTop &&
                maxX < Camera::WIDTH-ignoreRight && maxY < Camera::HEIGHT-ignoreBottom)
        {
            // add a new line to the list
            lines.push_back(new Line2D(Point2D(l->start),Point2D(l->end)));
        }
    }
}

void MultiLine::Print()
{
    cout << "Lines for target " << name << ":" << endl;

    Line2D *l;
    for(vector<Line2D*>::iterator it=lines.begin(); it!=lines.end(); it++)
    {
        l = *it;

        cout << "\t(" << l->start.X << "," << l->start.Y << ") (" << l->end.X << "," << l->end.Y <<  ")" << endl;
    }
}

void MultiLine::Draw(Mat &canvas)
{
    LineTarget::Draw(canvas);
    for(vector<Line2D*>::iterator it=lines.begin(); it!=lines.end(); it++)
    {
        Line2D *l = *it;

        cv::line(canvas,Point(l->start.X,l->start.Y),Point(l->end.X,l->end.Y),markColour,2);
    }
}
