README for libdarwin.so

darwin.cbp is a Code::Blocks project file for libdarwin.  The custom makefile included in
this directory will compile and install libdarwin to /usr/local/lib and copy header files to
/usr/local/include/darwin.

To compile from the command-line cd to libdarwin/darwin and execute
    $ make
    # make install  (or # make reinstall)

libdarwin depends on the following additional shared libraries:
    - libespeak  (voice synthesis library)
    - libcv      (OpenCV vision processing library)
    - libhighgui (used by OpenCV for displaying images)
    - libjpeg
    - libcxcore  (part of OpenCV in later versions)
    - libpthread
    - libdl

Older versions of libdarwin also depended on libmlibrary, the AA Lab homebrew inverse kinematics
library (available on Sculpin as part of the MotionPlayer_v2.0 git project).  Support for
libmlibrary was removed during the last update, but will likely be re-introduced at a later date.

Certain FIRA projects also depends on Qt libraries.  See those individual projects as necessary.

If you know of a library that would be useful (e.g. libfann for artificial neural networks) feel
free to discuss using it in the core library as well.  Generally-speaking we want to use libraries
that are documented and supported wherever possible, instead of writing our own code from scratch
all the time.  This improves code portability, since those libraries are available on multiple
platforms.


Note on editing code:
    Please refrain from editing files authored by Robotis as much as possible.  We do not want
    to break compatibility with the sample software.  If you must edit one of the stock files
    please add a comment with your initials so that we can easily track changes.

    If you must radically change the behaviour of a class consider creating a new class or subclass
    instead.  e.g. YuvCamera is a complete rewrite of LinuxCamera; our own software tends to use
    YuvCamera but the sample software still relies on LinuxCamera.

    New classes/headers you create should contain the following header at the top of the file:

    /************************************************************************
     * (c) University of Manitoba Autonomous Agents Laboratory
     * <Your name(s) here>
     *
     * <Short description of what the class/header does here>
     ************************************************************************/

    To stay consistent with the Robotis code please use upper case for function names, and use
    CamelCase, not underscores.  e.g. GetMirrorValue, not get_mirror_value nor getMirrorValue

    Enum values should be in all caps with underscores.


Branches:
    Master is intended for use on the newer-model Darwins (with MX28 motors).  This should, ideally,
    be based on the latest version of Robotis' stock library, with additional code as necessary.

    The "old-darwin" branch is intended for use on the older model Darwin (with RX-28 motors).
    Because of a glitch in the latest version of the code from Robotis the old Darwin does not work
    with the latest MotionManager class.  Therefore we have downgraded the core library from 1.4 to
    1.0.1 in this branch.  The old-darwin branch should be kept up-to-date with new code we add,
    but will inevitbaly suffer from some limitations.

    Other branches can be created for testing purposes as desired.  But please, PLEASE be careful about
    merging; we don't want to break anything.


TODO LIST:
    - Delete old-darwin branch
    - Finish forward & inverse kinematics for arms & legs
        - arms done, legs not started
    - Improve vision algorithms
        - better object detection
        - more tunable parameters
        - better algorithms than ScanLine?
    - Create a SocketCM750 class that can be used to control a Darwin via TCP or UDP packets
        - requires a server to run on the Darwin, SocketCM750 would connect to the server
          via sockets, but otherwise would act the same way as the current LinuxCM730
        - SocketCM730 would be wrapped up by the CM730 class, just like LinuxCM730
    - Create CM-5 controller class so we can use libdarwin to control bioloids
        - may require additional classes, offsets, etc...
    - Refactor AbstractTarget, Target, MultiTarget, TargetProcessing
        - AbstractTarget class becomes "Target"
        - Target class becomes "SingleTarget"
        - static functions in TargetProcessing move into (new) Target class
    - fire pthread_cond_signals when ever DPS and SSP start (inside Walking)

Finished TODOs:
    - Keep old-darwin branch up-to-date with features and improvements made to Master (ongoing)
        * firwmare update on RX-28M motors eliminates the need for this branch
    - Finish support for RX28, MX28, and AX12 motors all connected to the same robot
        * done

