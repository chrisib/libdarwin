##################################################
# PROJECT: Darwin - University of Manitoba
##################################################

QT       -= core gui

TARGET = darwin
TEMPLATE = lib

OBJECTS_DIR = build/
DESTDIR = lib/

QMAKE_CFLAGS_RELEASE -= -O2
QMAKE_CFLAGS_RELEASE += -O3 -g
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3 -g

QMAKE_LFLAGS_RELEASE -= -Wl,-O1
QMAKE_LFLAGS_RELEASE += -Wl,-O3 -g

INCLUDEPATH +=  include/ \
                include/darwin/framework/ \
                include/darwin/linux/ \

LIBS += -ldl \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc \
        -lespeak \
        -ljpeg \
        -lpthread \

BASE_SRC += src/framework/CM730.cpp \
            src/framework/control/DoublePidController.cpp \
            src/framework/control/IntPidController.cpp \
            src/framework/control/PidController.cpp \
            src/framework/math/Line.cpp \
            src/framework/math/Math.cpp \
            src/framework/math/Matrix.cpp \
            src/framework/math/Plane.cpp \
            src/framework/math/Point.cpp \
            src/framework/math/Vector.cpp \
            src/framework/minIni/minIni.c \
            src/framework/motion/AX12.cpp \
            src/framework/motion/JointData.cpp \
            src/framework/motion/Kinematics.cpp \
            src/framework/motion/MotionManager.cpp \
            src/framework/motion/MotionStatus.cpp \
            src/framework/motion/RX28.cpp \
            src/framework/motion/TransformationMatrix.cpp \
            src/framework/motion/modules/Action.cpp \
            src/framework/motion/modules/Limb.cpp \
            src/framework/motion/modules/Leg.cpp \
            src/framework/motion/modules/LeftLeg.cpp \
            src/framework/motion/modules/RightLeg.cpp \
            src/framework/motion/modules/Arm.cpp \
            src/framework/motion/modules/LeftArm.cpp \
            src/framework/motion/modules/RightArm.cpp \
            src/framework/motion/modules/Head.cpp \
            src/framework/motion/modules/Walking.cpp \
            src/framework/vision/BallFollower.cpp \
            src/framework/vision/BallTracker.cpp \
            src/framework/vision/BoundingBox.cpp \
            src/framework/vision/BlobTarget.cpp \
            src/framework/vision/CameraPosition.cpp \
            src/framework/vision/ColorFinder.cpp \
            src/framework/vision/CvPreprocessor.cpp \
            src/framework/vision/Image.cpp \
            src/framework/vision/ImgProcess.cpp \
            src/framework/vision/MultiBlob.cpp \
            src/framework/vision/SingleBlob.cpp \
            src/framework/vision/LineTarget.cpp \
            src/framework/vision/MultiLine.cpp \
            src/framework/vision/Target.cpp \
            src/framework/vision/ThresholdRange.cpp \
            src/framework/voice/Voice.cpp \
            src/framework/application/Application.cpp \

NIX_SRC +=  src/linux/CvCamera.cpp \
            src/linux/LinuxActionScript.cpp \
            src/linux/LinuxCamera.cpp \
            src/linux/LinuxCM730.cpp \
            src/linux/LinuxDARwIn.cpp \
            src/linux/LinuxMotionTimer.cpp \
            src/linux/LinuxNetwork.cpp \
            src/linux/YuvCamera.cpp \
            src/linux/streamer/httpd.cpp \
            src/linux/streamer/jpeg_utils.cpp \
            src/linux/streamer/mjpg_streamer.cpp \

SOURCES +=  $$BASE_SRC \
            $$NIX_SRC \



BASE_HEADERS +=  include/DARwIn.h \
            include/LinuxDARwIn.h \

FRAMEWORK_HEADERS +=  \
            include/darwin/framework/Action.h \
            include/darwin/framework/Arm.h \
            include/darwin/framework/AX12.h \
            include/darwin/framework/AXS1.h \
            include/darwin/framework/BallFollower.h \
            include/darwin/framework/BallTracker.h \
            include/darwin/framework/BlobTarget.h \
            include/darwin/framework/BoundingBox.h \
            include/darwin/framework/Camera.h \
            include/darwin/framework/CameraSettings.h \
            include/darwin/framework/CM730.h \
            include/darwin/framework/ColorFinder.h \
            include/darwin/framework/CvPreprocessor.h \
            include/darwin/framework/DoublePidController.h \
            include/darwin/framework/Dynamixel.h \
            include/darwin/framework/FSR.h \
            include/darwin/framework/Head.h \
            include/darwin/framework/Image.h \
            include/darwin/framework/ImgProcess.h \
            include/darwin/framework/IntPidController.h \
            include/darwin/framework/JointData.h \
            include/darwin/framework/Kinematics.h \
            include/darwin/framework/LeftArm.h \
            include/darwin/framework/Limb.h \
            include/darwin/framework/Leg.h \
            include/darwin/framework/LeftLeg.h \
            include/darwin/framework/LineTarget.h \
            include/darwin/framework/RightLeg.h \
            include/darwin/framework/Line.h \
            include/darwin/framework/Math.h \
            include/darwin/framework/Matrix.h \
            include/darwin/framework/minGlue.h \
            include/darwin/framework/minIni.h \
            include/darwin/framework/MotionManager.h \
            include/darwin/framework/MotionModule.h \
            include/darwin/framework/MotionStatus.h \
            include/darwin/framework/MultiBlob.h \
            include/darwin/framework/MultiLine.h \
            include/darwin/framework/MX28.h \
            include/darwin/framework/PidController.h \
            include/darwin/framework/Plane.h \
            include/darwin/framework/Point.h \
            include/darwin/framework/RightArm.h \
            include/darwin/framework/RX28.h \
            include/darwin/framework/SingleBlob.h \
            include/darwin/framework/Target.h \
            include/darwin/framework/ThresholdRange.h \
            include/darwin/framework/TransformationMatrix.h \
            include/darwin/framework/Vector.h \
            include/darwin/framework/Voice.h \
            include/darwin/framework/Walking.h \
            include/darwin/framework/wxMinIni.h \
            include/darwin/framework/Application.h \
            include/darwin/framework/CameraPosition.h \

LINUX_HEADERS +=  include/darwin/linux/CvCamera.h \
            include/darwin/linux/httpd.h \
            include/darwin/linux/jpeg_utils.h \
            include/darwin/linux/LinuxActionScript.h \
            include/darwin/linux/LinuxCamera.h \
            include/darwin/linux/LinuxCM730.h \
            include/darwin/linux/LinuxMotionTimer.h \
            include/darwin/linux/LinuxNetwork.h \
            include/darwin/linux/mjpg_streamer.h \
            include/darwin/linux/YuvCamera.h \

HEADERS +=  $$BASE_HEADERS \
            $$FRAMEWORK_HEADERS \
            $$LINUX_HEADERS \


##################################################
# Installation Directories                       #
##################################################
INSTALLBASE = /usr/local

target.path     = $$INSTALLBASE/lib/
#target.commands = ldconfig
INSTALLS   += target

base_headers.path  = $$INSTALLBASE/include/
base_headers.files = $$BASE_HEADERS
INSTALLS   += base_headers

framework_headers.path  = $$INSTALLBASE/include/darwin/framework/
framework_headers.files = $$FRAMEWORK_HEADERS
INSTALLS   += framework_headers

linux_headers.path  = $$INSTALLBASE/include/darwin/linux
linux_headers.files = $$LINUX_HEADERS
INSTALLS   += linux_headers

docs.path = $$INSTALLBASE/share/doc/libdarwin
docs.files = ../doc/html/*
#INSTALLS   += docs

post_install.path = $$INSTALLBASE
post_install.commands = ldconfig
INSTALLS += post_install

OTHER_FILES += \
    README
