/*
 *   DARwIn.h
 *
 *   Author: ROBOTIS
 *
 */
/*! \mainpage libdarwin - DARwIn-OP/Robotis OP Vision & Control Library
 *
 * \section intro_sec Introduction
 * This library is based on the official DARwIn-OP library written by Robotis.  It has been
 * extensively modified and expanded by the students of the University of Manitoba Autonomous
 * Agents Lab, most notably:
 * - Chris Iverach-Brereton
 * - Joshua Jung
 * - Simon Barber-Dueck
 * - Geoff Nagy
 * - Stela H. Seo
 * - Meng Cheng Lau
 * - Chi-Tai Cheng
 *
 * \section deps_sec Dependencies
 * This library uses OpenCV for most of the vision-processing.  Currently OpenCV 2.4 or OpenCV 3.0 are
 * both known to work, but support for the 2.4 branch may eventually be dropped.
 *
 * In addition to OpenCV this library depends on Espeak for voice synthesis, madplay for playing
 * mp3 files in external processes, libjpeg, and pthreads.
 *
 * Some projects associated with this library use the Qt framework for graphical components.
 */
#ifndef _DARWIN_H_
#define _DARWIN_H_

#include "darwin/framework/CameraSettings.h"
#include "darwin/framework/CM730.h"
#include "darwin/framework/MX28.h"
#include "darwin/framework/AX12.h"
#include "darwin/framework/RX28.h"
#include "darwin/framework/MotionModule.h"
#include "darwin/framework/MotionManager.h"
#include "darwin/framework/MotionStatus.h"
#include "darwin/framework/JointData.h"
#include "darwin/framework/Action.h"
#include "darwin/framework/Walking.h"
#include "darwin/framework/Head.h"
#include "darwin/framework/Arm.h" //SBD
#include "darwin/framework/LeftArm.h" // SBD
#include "darwin/framework/RightArm.h" // SBD
#include "darwin/framework/Image.h"
#include "darwin/framework/ImgProcess.h"
#include "darwin/framework/BallTracker.h"
#include "darwin/framework/BallFollower.h"
#include "darwin/framework/ColorFinder.h"
#include "darwin/framework/Camera.h"
#include "darwin/framework/Point.h"
#include "darwin/framework/Vector.h"
#include "darwin/framework/Matrix.h"
#include "darwin/framework/Plane.h"
#include "darwin/framework/minIni.h"

#endif
