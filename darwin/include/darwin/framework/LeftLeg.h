#ifndef LEFTLEG_H
#define LEFTLEG_H

#include "darwin/framework/Limb.h"
#include "darwin/framework/Leg.h"
#include "darwin/framework/MotionStatus.h"

namespace Robot
{

    class LeftLeg : public Leg
    {
    public:
        static LeftLeg *GetInstance(){return uniqueInstance;}

        LeftLeg();

        virtual void Initialize();
        virtual void Process();
        virtual Point3D GetPosition();
        virtual Point3D GetPosition(JointData &position);

        virtual double GetToeAngle();
        virtual double GetKneeAngle();
        virtual double GetHipPitch();
        virtual double GetHipRoll();
        virtual double GetAnkleRoll();
        virtual double GetAnklePitch();
        virtual void SetEnable(bool enable, bool exclusive);

        virtual void SetAngles(JointData &joints);

        virtual int GetKneeTorque() {return MotionStatus::L_KNEE_TORQUE;}
        virtual int GetAnkleRollTorque() {return MotionStatus::L_ANKLE_ROLL_TORQUE;}
        virtual int GetAnklePitchTorque() {return MotionStatus::L_ANKLE_PITCH_TORQUE;}
        virtual int GetXFSR() {return MotionStatus::L_FSR_X;}
        virtual int GetYFSR() {return MotionStatus::L_FSR_Y;}

    private:
        static LeftLeg *uniqueInstance;
    };
}

#endif // LEFTLEG_H
