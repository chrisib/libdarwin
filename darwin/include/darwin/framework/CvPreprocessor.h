/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * OpenCV Preprocessor classes that can be used with the YuvCamera class
 ************************************************************************/

#ifndef CV_PREPROCESS_H_INCLUDED
#define CV_PREPROCESS_H_INCLUDED

#include <opencv/cv.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace Robot
{
    /*!
     * \brief A preprocessor that applies simple OpenCV operations to frames as they are captured by the camera
     */
    class CvPreprocessor
    {
    public:
        static const int DEFAULT_FILTER_SIZE = 3;

        CvPreprocessor(){filterSize = DEFAULT_FILTER_SIZE;}
        CvPreprocessor(int filterSize){this->filterSize = filterSize;}

        virtual void Process(cv::Mat &image) = 0;
        virtual const char* Name() = 0;
        virtual int FilterSize(){return filterSize;}

        static CvPreprocessor *MakePreprocessor(const char* name);

    protected:
        int filterSize;
    };

    /*!
     * \brief Applies a gaussian blur to the image
     */
    class GaussianPreprocessor : public CvPreprocessor
    {
    public:
        GaussianPreprocessor() : CvPreprocessor() {}
        GaussianPreprocessor(int filterSize) : CvPreprocessor(filterSize) {}

        virtual void Process(cv::Mat &image);
        virtual const char* Name();
    };

    /*!
     * \brief Applies a median filter to the image
     */
    class MedianPreprocessor : public CvPreprocessor
    {
    public:
        MedianPreprocessor() : CvPreprocessor() {}
        MedianPreprocessor(int filterSize) : CvPreprocessor(filterSize) {}

        virtual void Process(cv::Mat &image);
        virtual const char* Name();
    };

    /*!
     * \brief Applies an erode filter to the image
     */
    class ErodePreprocessor : public CvPreprocessor
    {
    public:
        ErodePreprocessor() : CvPreprocessor(1) {}
        ErodePreprocessor(int numIterations) : CvPreprocessor(numIterations) {}

        virtual void Process(cv::Mat &image);
        virtual const char* Name();
    };

    /*!
     * \brief Applies a dilate filter to the image
     */
    class DilatePreprocessor : public CvPreprocessor
    {
    public:
        DilatePreprocessor() : CvPreprocessor(1) {}
        DilatePreprocessor(int numIterations) : CvPreprocessor(numIterations) {}

        virtual void Process(cv::Mat &image);
        virtual const char* Name();
    };

    /*!
     * \brief Applies a contrast/brightness filter to the image
     */
    class ContrastPreprocessor : public CvPreprocessor
    {
    public:
        ContrastPreprocessor() : CvPreprocessor() {}
        ContrastPreprocessor(double alpha, double beta);

        virtual void Process(cv::Mat &image);
        virtual const char* Name();

    private:
        double alpha, beta;
    };

    /*!
     * \brief Removes all pixels within threshold of the average colour of the frame
     */
    class BackgroundRemover : public CvPreprocessor
    {
    public:
        BackgroundRemover() : CvPreprocessor() {}
        BackgroundRemover(double threshold) : CvPreprocessor(threshold) {}

        virtual void Process(cv::Mat &image);
        virtual const char* Name();
    };
}

#endif // CV_PREPROCESS_H_INCLUDED
