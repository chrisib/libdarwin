#ifndef BLOBTARGET_H
#define BLOBTARGET_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "darwin/framework/Target.h"
#include "darwin/framework/minIni.h"
#include "darwin/framework/ThresholdRange.h"
#include "darwin/framework/Camera.h"
#include "darwin/framework/BoundingBox.h"
#include <vector>
#include <list>

namespace Robot
{
    /*!
     * \brief A vision target characterized by a uniform-coloured, compact blob within the robot's field of view.
     *
     * Detected objects are represented as BoundingBox instances
     *
     * Blob targets are detected using a scanline/flood-fill algorithm initially developed by Jacky's students for use
     * on Bioloid robots using Nokia phones as their CPUs in the early 2000s.
     *
     * The algorithm works as follows:
     * 1- Begin scanning horizontally across rows of the image looking for a set of adjacent pixels whose delta is less than a threshold
     * 2- Once such a line of pixels is detected, do a 4-connected flood-fill of those pixels using a second threshold
     * 3- Calculate a bounding box around the filled area along with the object's compactness and add the result to the list of candidate objects
     * 4- Continue scanning horizontally, skipping pixels that have been flood-filled already
     * 5- Filter candidate objects based on a series of filters to eliminate false positives
     *
     * Alternatively the ProcessCandidateImage algorithm can be used to perform traditional blob-detection on a binary image. How the binary
     * image is generated is up to the calling function (e.g. hard thresholding based on YUV values)
     *
     * \remarks Currently the scanline algorithm uses YUV, RGB, and RGB* images to detect edges, which is probably serious overkill. We could
     *          probably refactor this to use just a single image instead of all three.
     */
    class BlobTarget : public Target
    {
        public:
            BlobTarget();
            virtual ~BlobTarget();

            /*!
             * \brief Load the object-detection parameters from the configuration file
             * \param ini
             * \param section
             */
            virtual void LoadIniSettings(minIni &ini, const std::string &section);

            // print out the target's properties
            /*!
             * \brief Print the target's properties to stdout
             */
            virtual void Print() = 0;

            /*!
             * \brief Locate the object in the robot's current FoV
             * \param image The image to process (e.g. the current frame from the CvCamera class)
             * \param dest An optional pointer to an image to draw debugging information to
             * \return The number of instances of the object found in the frame
             */
            virtual int FindInFrame(cv::Mat &image, cv::Mat *dest=NULL) = 0;

            /*!
             * \brief Process a list of possible objects found by the object-detection algorithm and filter out false positives
             * \param candidates The list of possible object that have been detected that may include false positives
             */
            virtual void ProcessCandidates(std::vector<BoundingBox*> &candidates) = 0;

            /*!
             * \brief Given a thresholded binary image, process the detected blobs into detected objects
             *
             * This is intended to be a higher-speed version of FindInFrame (which uses a complex scanline/flood-fill algorithm).  The hard
             * thresholding used to produce the binary image is fast, but not robust to varible lighting
             * \param binaryImage A binary image we can perform traditional blob-detection on
             */
            virtual void ProcessCandidateImage(cv::Mat &binaryImage);

            /*!
             * \brief Set the real-world dimensions of the object in mm (e.g. the size of the binders the robot navigates around in the obstacle course)
             * \param realWidth The width of the object
             * \param realHeight The height of the object
             */
            void SetDimensions(int realWidth, int realHeight) {this->realWidth = realWidth; this->realHeight = realHeight;}

            /*!
             * \brief Set the channel in the image that provides the best contrast of the object against the background
             *
             * By default this is the Y channel (brightness) when working in YUV. However, if working in RGB or HSV other channels may provide better contrast
             * \param x The index of the channel in the images to be processed
             */
            void SetKeyChannel(int x);

            /*!
             * \brief Set the minimum scanline length required before starting a flood-fill
             * \param x
             */
            void SetMinScanLength(int x){this->minScanLength = x;}

            /*!
             * \brief Set the maximum threshold between adjacent pixels when doing a scanline
             * \param x
             */
            void SetScanThreshold(int x){this->scanThreshold = x;}

            /*!
             * \brief Set the maximum threshold between adjacent pixels when doing a flood-fill
             * \param x
             */
            void SetFillThreshold(int x){this->fillThreshold = x;}

            /*!
             * \brief Set the minimum pixel height of the object to eliminate noise
             * \param x
             */
            void SetMinPixelHeight(int x){this->minPixelHeight = x;}

            /*!
             * \brief Set the minimum pixel width of the object to eliminate noise
             * \param x
             */
            void SetMinPixelWidth(int x){this->minPixelHeight = x;}

            /*!
             * \brief set the interval at which we skip pixels when doing flood fills and scanlines
             *
             * A value of 1 will examine every single pixel (accurate but slow), while a value of 2 will skip every second row and every second column.
             *
             * Higher values result in higher throughput, but with less accuracy/ability to detect small objects
             * \param x
             */
            void SetSubsample(int x){this->subsample = x;}

            /*!
             * \brief Set whether or not objects are filtered based on their real-world aspect ratio
             * \param b
             */
            void SetProportionsDisabled(bool b){proportionsDisabled = b;}

            /*!
             * \brief Set whether or not objects are filtered based on their compactness (percent of bounding box filled by the object)
             * \param b
             */
            void SetFillDisabled(bool b){fillDisabled = b;}

            /*!
             * \brief Set whether or not objects are filtered based on their perceived size
             * \param b
             */
            void SetSizeDisabled(bool b){sizeDisabled = b;}

            /*!
             * \brief Set whether objects are filtered based on their Y-channel (or 0-channel if working in a different colour space) value
             * \param b
             */
            void SetYRangeDisabled(bool b){yRangeDisabled = b;}

            /*!
             * \brief Set whether objects are filtered based on their U-channel (or 1-channel if working in a different colour space) value
             * \param b
             */
            void SetURangeDisabled(bool b){uRangeDisabled = b;}

            /*!
             * \brief Set whether objects are filtered based on their V-channel (or 2-channel if working in a different colour space) value
             * \param b
             */
            void SetVRangeDisabled(bool b){vRangeDisabled = b;}

            /*!
             * \brief Set whether objects are filtered based on their key channel value
             *
             * \see SetKeyChannel
             * \param b
             */
            void SetKeyRangeDisabled(bool b){keyRangeDisabled = b;}

            /*!
             * \brief Get the real-world height of the object being looked for in mm
             * \return
             */
            int GetRealHeight() {return this->realHeight;}

            /*!
             * \brief Get the real-world width of the object being looked for in mm
             * \return
             */
            int GetRealWidth() {return this->realWidth;}

            /*!
             * \brief Get the key channel that best differentiates the object from the background
             * \return
             */
            int GetKeyChannel(){return this->keyChannel;}

            /*!
             * \brief Get the minimum pixel length of a scanline required before triggering a floodfill
             * \return
             */
            int GetMinScanLength(){return this->minScanLength;}

            /*!
             * \brief Get the maximum threshold between adjacent pixels when doing a scanline
             * \return
             */
            int GetScanThreshold(){return this->scanThreshold;}

            /*!
             * \brief Get the maximum threshold between adjacent pixels when doing a flood-fill
             * \return
             */
            int GetFillThreshold(){return this->fillThreshold;}

            /*!
             * \brief Get the minimum allowed pixel height of detected objects
             * \return
             */
            int GetMinPixelHeight(){return this->minPixelHeight;}

            /*!
             * \brief Get the minimum allowed pixel width of detected objects
             * \return
             */
            int GetMinPixelWidth(){return this->minPixelHeight;}

            /*!
             * \brief Get the scanline/flood-fill subsample value
             * \return
             */
            int GetSubsample(){return this->subsample;}

            /*!
             * \brief Get whether or not objects are filtered based on their real-world aspect ratio
             * \return
             */
            bool GetProportionsDisabled(){return proportionsDisabled;}

            /*!
             * \brief Get whether or not objects are filtered based on their compactness
             * \return
             */
            bool GetFillDisabled(){return fillDisabled;}

            /*!
             * \brief Get whether or not objects are filtered based on their perceived size
             * \return
             */
            bool GetSizeDisabled(){return sizeDisabled;}

            /*!
             * \brief Get whether or not objects are filtered based on their Y-channel (or 0-channel if working in other colour spaces)
             * \return
             */
            bool GetYRangeDisabled(){return yRangeDisabled;}

            /*!
             * \brief Get whether or not objects are filtered based on their U-channel (or 1-channel if working in other colour spaces)
             * \return
             */
            bool GetURangeDisabled(){return uRangeDisabled;}

            /*!
             * \brief Get whether or not objects are filtered based on their V-channel (or 2-channel if working in other colour spaces)
             * \return
             */
            bool GetVRangeDisabled(){return vRangeDisabled;}

            /*!
             * \brief Get whether or not objects are filtered based on their key channel
             *
             * \see SetKeyChannel
             * \return
             */
            bool GetKeyRangeDisabled(){return keyRangeDisabled;}

        protected:
            // the target's real-world dimensions
            int realHeight; //mm
            int realWidth;  //mm

            ThresholdRange *keyChannelRange;

            // scanline parameters
            int keyChannel;
            int minScanLength;
            int scanThreshold;
            int fillThreshold;

            int minPixelWidth;
            int minPixelHeight;

            int subsample;

            // enable/disable different properties to check for
            bool proportionsDisabled;
            bool fillDisabled;
            bool sizeDisabled;
            bool yRangeDisabled;
            bool uRangeDisabled;
            bool vRangeDisabled;
            bool keyRangeDisabled;

            /*!
             * \brief Used in the FindInFrame target as the parameter to pass to the ScanLine function
             */
            std::vector<BlobTarget*> *scanlineParam;



        // Static image processing functions
        public:
            /*!
             * \brief Find multiple targets with a single scanline
             *
             * The largest scan and fill thresholds of any target are used when scanning
             * \param targets The set of blob targets to detect
             * \param src The image to process. Should be in YUV format, and will be internally converted to RGB and RGB* (where RGB* = (R-G)(R-B)(G-B)) to improve accuracy when detecting edges
             * \param dest An optional image to draw debugging information to
             * \param subsample The subsample to use when doing scanline and flood-fill algorithms
             */
            static void FindTargets(std::vector<Robot::BlobTarget*> &targets, cv::Mat &src, cv::Mat *dest=NULL, int subsample=2);

            /*!
             * \brief Find multiple targets with a single scanline
             *
             * The largest scan and fill thresholds of any target are used when scanning
             *
             * RGB and RGB-compliment (RGB*) may be any U8C3 format images (e.g. HSV, RGB, RGB*, etc...)
             * \param targets The set of blob targets to detect
             * \param yuv A YUV image to process; the channels must correspond to the max and min YUV channels defined in the Target class
             * \param rgb An RGB formatted image to assist in detecting edges when performing scanlines
             * \param rgbc An RGB* image (where RGB* = (R-G)(R-B)(G-B)) used to assist in detecting edges when performing scanlines
             * \param dest An optional image to draw debugging information to
             * \param subsample The subsample to use when doing scanline and flood-fill algorithms
             */
            static void FindTargets(std::vector<Robot::BlobTarget*> &targets, cv::Mat &yuv, cv::Mat &rgb, cv::Mat &rgbc, cv::Mat *dest=NULL, int subsample=2);

        private:
            static void FloodFill(int row, int col, int avgY, int avgU, int avgV, uint8_t *yuvData, int avgR, int avgG, int avgB, uint8_t *rgbData, int avgRG, int avgRB, int avgGB, uint8_t *rgbcData, int threshold, cv::Mat &src, uint8_t *destData, BoundingBox *result, int minRow, int maxRow, int minCol, int maxCol, int subsample);
            static void ProcessPixel(int row, int col,std::list<int> &stack, uint8_t *yuvData, int avgY, int avgU, int avgV, uint8_t *rgbData, int avgR, int avgG, int avgB, uint8_t *rgbcData, int avgRG, int avgRB, int avgGB, int threshold, uint8_t *destData, cv::Mat &src);

    };
}

#endif // BLOBTARGET_H
