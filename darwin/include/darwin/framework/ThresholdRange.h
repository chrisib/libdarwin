#ifndef THRESHOLDRANGE_H
#define THRESHOLDRANGE_H

namespace Robot
{
    // a simple Max/Min value pair
    // used to store the YUV, X/Y, Height/Width ranges
    class ThresholdRange
    {
        public:
            ThresholdRange(void);
            ThresholdRange(int min, int max);
            ~ThresholdRange(void);

            int max;
            int min;

            bool Check(int v);
            void Print();
    };
}

#endif // THRESHOLDRANGE_H
