/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class allows access to the Espeak library, allowing Darwin to
 * "speak" strings.  Useful for debugging.
 ************************************************************************/

#ifndef VOICE_H
#define VOICE_H

#include <espeak/speak_lib.h>
#include <pthread.h>

namespace Robot
{
    // Wrapper for libespeak (computer voice synthesis library)
    // Allows debugging messages to be spoken aloud
    // requires the libespeak-dev package (or OS-specific equivalent)
    class Voice
    {
        public:
            // get the unique instance of Voice
            //static Voice* GetInstance(void) {return uniqueInstance;}

            // configure libespeak to control voice output
            static void Initialize(void);
            static void Initialize(const char* voice);

            // speak a string
            static void Speak(const char* message);

        private:
            // Voice is not instantiable
            Voice();
            virtual ~Voice();

            static const char* m_voice_name;

            // does the actual speaking within a pthread
            static void* SpeakThread(void* msg);

            static espeak_POSITION_TYPE m_position_type;
            static espeak_AUDIO_OUTPUT m_output;
            static t_espeak_callback *m_synth_callback;
            static espeak_PARAMETER m_parm;
            static unsigned int m_flags;
            static int m_playback_buff_length;
            static int m_playback_options;

            static bool initialized;
    };
}
#endif // VOICE_H
