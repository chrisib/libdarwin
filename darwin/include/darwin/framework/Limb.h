/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * Generic superclass for Arm and Leg modules
 * Designed to provide IK-supported control over each limb individually
 ************************************************************************/

#ifndef LIMB_H
#define LIMB_H

#include "darwin/framework/MotionModule.h"
#include "darwin/framework/Point.h"
#include "darwin/framework/JointData.h"
#include "darwin/framework/Matrix.h"

#include <vector>

// Current angle, target angle -> new angle
typedef double (*MoveFunc)(double, double);

namespace Robot
{
    /*!
     * \brief Generic superclass for any robot limb (arm, leg, head, etc...)
     *
     * Limb positions are normal described as the end-effector position in body coordinates where:
     * - X is the distance front/back from the limb's origin in mm (positive is forward, negative is backward)
     * - Y is the distance medial/distal from the limb's origin  in mm (positive is distal, positive is medial) (medial = towards the sagittal plane, distal = away from the robot's midline)
     * - Z is the distance above/below the limb's origin in mm (positive is above, negative is below)
     */
    class Limb : public MotionModule
    {
    public:
        virtual ~Limb();

        // get the 3D coordinates of the end-effector relative to the robot's shoulder (in mm)
        // Joint positions are pulled from m_Joint (which may not represent the robot's actual
        // pose if a different module is controlling the motors in question)
        // Updates the jointPositions vector (see below)
        virtual Point3D GetPosition() = 0;
        virtual Point3D GetPosition(JointData &position) = 0; // same as above, but using any arbitrary JointData object for the pose.

        // same as above, but relative to the robot's origin, NOT the base of the limb
        virtual Point3D GetAbsolutePosition() = 0;
        virtual Point3D GetAbsolutePosition(JointData &position) = 0;

        // set this limb's joints from those in a JointData file
        // NOTE: this only sets the joint positions, not enable/disable, PID values, etc....
        virtual void SetAngles(JointData &joints) = 0;

        // move to a given position
        virtual void SetGoalPosition(Point3D &position) = 0;

        // extend the limb until it is straight (moving along the same line drawn from the joint's origin to the end-effector)
        virtual void Extend(double speed=0.1) = 0;

        // To allow you to move the hand from one place to another immediately
        static double ImmediateMove(double currentPosition, double targetPosition);

        // P-controller with P=0.5
        static double HalfMove(double currentPosition, double targetPosition);

        // Approach goal position by 1 degree at a time
        static double ApproachByOne(double currentPosition, double targetPosition);

        void SetMoveMethod(MoveFunc function){this->moveMethod = function;}

        // positions of each joint in the limb starting at the limb's origin (always (0,0,0)) to the end effector
        // the Y axis is measures *away* from the body's midline
        // values in this vector are set by calling GetPosition()
        // GetPosition() returns the same point as the last item in this list
        std::vector<Point3D> jointPositions;

        // the position and orientation of the end effector
        // this is set whenever GetPosition is called
        Matrix3D endEffector;

    protected:
        Limb(bool invertYAxis);
        bool isMoving;
        MoveFunc moveMethod;
        double extendSpeed;
        bool isExtending;   // set True when Extend() is called, set False when ProcessExtending() decides we've finished extending
        static const double NOT_EXTENDING = 0.0;
        Point3D extensionGoalPosition;

        const bool INVERTED_Y_AXIS;

        void ProcessExtending();

    private:

    };
}


#endif // LIMB_H
