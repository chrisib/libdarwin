/*
 *   BallFollower.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _BALL_FOLLOWER_H_
#define _BALL_FOLLOWER_H_

#include "darwin/framework/Point.h"
#include "darwin/framework/BallTracker.h"


namespace Robot
{
    /*!
     * \brief Class that uses the Head, Action and Walking modules to have the robot follow and kick a ball
     *
     * Part of the Robotis demo library (see soccer demo), but can be used for other purposes
     */
	class BallFollower
	{
	public:                         // CIB (originally private, but changed so we can tune parameters
		int m_NoBallMaxCount;
		int m_NoBallCount;
		int m_KickBallMaxCount;
		int m_KickBallCount;

		double m_MaxFBStep;
		double m_MaxRLStep;
		double m_MaxDirAngle;

		double m_KickTopAngle;
		double m_KickRightAngle;
		double m_KickLeftAngle;

		double m_FollowMaxFBStep;
        double m_FollowMinFBStep;
		double m_FollowMaxRLTurn;
        double m_FitFBStep;
		double m_FitMaxRLTurn;
		double m_UnitFBStep;
		double m_UnitRLTurn;

		double m_GoalFBStep;
		double m_GoalRLTurn;
		double m_FBStep;
		double m_RLTurn;

        double m_TiltMinOffset;

	protected:

	public:
		bool DEBUG_PRINT;
		int KickBall;		// 0: No ball 1:Left -1:Right

		BallFollower();
		~BallFollower();

		void Process(Point2D ball_pos);
	};
}

#endif
