#ifndef LINE_H
#define LINE_H

#include <darwin/framework/Point.h>

namespace Robot
{
    /*!
     * \brief A line segment consisting of 2 points (start and end)
     */
    class Line2D
    {
    public:
        /*!
         * \brief Create a new line with undefined endpoints
         */
        Line2D(){}

        /*!
         * \brief Create a new line with the given start and end points
         * \param start The start of the line
         * \param end The end of the line
         */
        Line2D(Point2D start, Point2D end) {this->start = start; this->end = end;}

        /*!
         * \brief The line's starting point
         */
        Point2D start;

        /*!
         * \brief The lines end point
         */
        Point2D end;

        /*!
         * \brief The scalar length of the line
         * \return
         */
        double Length(){return Point2D::Distance(start,end);}

        /*!
         * \brief Does this line intersect with another line segment?
         * \param l The line we're checking if we intersect with
         * \return
         */
        bool Intersects(Line2D &l);

        /*!
         * \brief Convert this line segment to a vector starting at the origin
         * \return \f$ (x', y') = (end.x - start.x, end.y - start.y) \f$
         */
        Point2D ToVector();

        /*!
         * \brief Re-order start and end such that the start point has a smaller Y value than the end point
         *
         * The start and end points are not changed; only their order may be altered
         */
        void SortTopToBottom();

        /*!
         * \brief Re-order stard and end such that the start point has a smaller X value than the end point
         *
         * The start and end points are not changed; only their order may be altered
         */
        void SortLeftToRight();

    };

    /*!
     * \brief A line segment in 3-dimensional space
     */
    class Line3D
    {
    public:
        /*!
         * \brief Create a line segment with undefined start and end points
         */
        Line3D(){}

        /*!
         * \brief Create a line segment with the given start and end points
         * \param start The start of the line segment
         * \param end The end of the line segment
         */
        Line3D(Point3D start, Point3D end) {this->start = start; this->end = end;}

        /*!
         * \brief The start of the line segment
         */
        Point3D start;

        /*!
         * \brief The end of the line segment
         */
        Point3D end;

        /*!
         * \brief The scalar length of the line segment
         * \return
         */
        double Length(){return Point3D::Distance(start,end);}

        /*!
         * \brief Convert the line segment into a 3D vector starting at the origin
         * \return \f$ (x', y', z') = (end.x - start.x, end.y - start.y, end.z - start.z) \f$
         */
        Point3D ToVector(){return Point3D(end.X-start.X,end.Y-start.Y,end.Z-start.Z);}
    };
}

#endif // LINE_H
