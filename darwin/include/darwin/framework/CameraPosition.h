#ifndef CAMERAPOSITION_H
#define CAMERAPOSITION_H

#include "darwin/framework/Point.h"
#include "darwin/framework/JointData.h"
#include "darwin/framework/MotionStatus.h"


namespace Robot
{
    class BoundingBox;

    /*!
     * \brief Calculates the position of the robot's camera in 3D space to make it possible to do vision-based range
     * and bearing estimates
     *
     * Does simple forward-kinematic calculations to determine the position of the camera relative to the robot's feet.
     * X is how far ahead/behind the robot's front-most foot the camera is. Y is how far left/right of the robot's origin the camera
     * is. Z is the height above the ground the camera is (assuming the feet are on the ground).
     */
    class CameraPosition
    {
    public:
        CameraPosition();
        ~CameraPosition();

        /*!
         * \brief Calculate an object's range based on its position in the frame and it's physical height above
         * the ground (where ground = level with the robot's support foot)
         *
         * NOTE: calculations are based on MotionStatus::m_CurrentJoint's values, and may not always
         * reflect the physical position of the motors at any given time (esp. if the robot is actively moving)
         * use with caution when the robot is walking
         *
         * \param objectInFrame The position of the object whose range we are calculating
         * \param heightAboveGround The height of the object above ground level in mm
         * \return The distance measured along the ground in mm
         */
        double CalculateRange(Point2D &objectInFrame, double heightAboveGround=0);

        /*!
         * \brief Calculate an object's range based on its position in the frame and it's physical height above
         * the ground (where ground = level with the robot's support foot)
         *
         * NOTE: calculations are based on MotionStatus::m_CurrentJoint's values, and may not always
         * reflect the physical position of the motors at any given time (esp. if the robot is actively moving)
         * use with caution when the robot is walking
         *
         * \param objectInFrame The position of the object whose range we are calculating
         * \param heightAboveGround The height of the object above ground level in mm
         * \return The distance measured along the ground in mm
         */
        double CalculateRange(BoundingBox &objectInFrame, double heightAboveGround=0);

        /*!
         * \brief Calculate the angle to an object based on its position
         * \param objectInFrame The position of the object whose bearing we are calculating
         * \param heightAboveGround The object's height above ground level in mm
         * \return The bearing to the object in degrees (positive = left of robot, negative = right, 0 = directly ahead)
         */
        double CalculateAngle(Point2D &objectInFrame, double heightAboveGround=0);

        /*!
         * \brief Calculate the angle to an object based on its position
         * \param objectInFrame The position of the object whose bearing we are calculating
         * \param heightAboveGround The object's height above ground level in mm
         * \return The bearing to the object in degrees (positive = left of robot, negative = right, 0 = directly ahead)
         */
        double CalculateAngle(BoundingBox &objectInFrame, double heightAboveGround=0);

        /*!
         * \brief Calculate the downward viewing angle of a point in degrees
         * \param objectInFrame The position of the object in the frame
         * \param heightAboveGround The height of the object above ground level
         * \return The downward angle in degrees (positive = below the horizon, negative = above)
         */
        double CalculateVerticalAngle(Point2D &objectInFrame, double heightAboveGround=0);

        /*!
         * \brief Recalculate the position of the camera based on the robot's pose
         * \param joints The robot's current joint positions
         */
        void RecalculatePosition(JointData &joints = MotionStatus::m_CurrentJoints);

        /*!
         * \brief Get the calculated height of the camera above ground level in mm
         * \see RecalculatePosition
         * \return
         */
        double GetCameraHeight(){return cameraHeight;}

        /*!
         * \brief Get the calculated front-back offset of the camera relative to the feet in mm
         * \see RecalculatePosition
         * \return
         */
        double GetCameraXOffset(){return cameraOffset;}

        /*!
         * \brief get the tilt angle of the camera relative to the ground-plane
         * \see RecalculatePosition
         * \return
         */
        double GetCameraTiltOffset(){return neckAngle;}

        /*!
         * \brief Get the pan angle of the camera relative to the ground-plane
         *
         * NOT IMPLENENTED YET -- is this necessary?
         * \return  Always returns 0
         */
        double GetCameraPanOffset(){return 0;} // TODO ??

    private:
        // angles below are used for forward-kinematic calculations to determine the
        // height, tilt, and front/back offset of the camera relative to the robot's feet
        double ankleAngle;

        double kneeHeight;
        double kneeAngle;
        double kneeOffset;

        double hipHeight;
        double hipAngle;
        double hipOffset;

        double neckHeight;
        double neckAngle;
        double neckOffset;

        double cameraHeight;
        double cameraAngle;
        double cameraOffset;

        // current pan angle of the camera
        double cameraPan;
    };
}
#endif // CAMERAPOSITION_H
