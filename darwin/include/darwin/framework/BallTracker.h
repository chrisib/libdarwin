/*
 *   BallTracker.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _BALL_TRACKER_H_
#define _BALL_TRACKER_H_

#include <string.h>

#include "darwin/framework/Point.h"
#include "darwin/framework/minIni.h"

namespace Robot
{
    /*!
     * \brief Controls the Head module to have the robot's head track an object and look directly at it
     *
     * Part of the Robotis demo library, intended to be used in conjunction with the BallFollower class for the soccer demo
     */
	class BallTracker
	{
	private:
		int NoBallCount;
		static const int NoBallMaxCount = 15;

	public:
        Point2D     ball_position;

		BallTracker();
		~BallTracker();

		void Process(Point2D pos);
	};
}

#endif
