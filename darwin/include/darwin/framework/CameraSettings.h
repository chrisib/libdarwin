/************************************************************************
 * ROBOTIS
 *
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 * Andres ??
 *
 * This class was originally defined in LinuxCamera.h, but was moved to
 * its own file so as not to conflict with YuvCamera
 ************************************************************************/

#include <stdlib.h>
#include <linux/videodev2.h>
#include <sys/time.h>

#include "darwin/framework/Image.h"
#include "darwin/framework/minIni.h"
#include <iostream>

#ifndef _CAM_SETTINGS_H
#define _CAM_SETTINGS_H

namespace Robot
{
    /*!
     * \brief The camera's capture settings, controlled through V4L2 (or equivilent if working on different platforms)
     *
     * This was originally part of Robotis' LinuxCamera class, but has been moved to its own file for re-use with the
     * CvCamera
     *
     * Note that not all are are necessarily supported by the hardware.
     * Setting any value to -1 will act as a reset when using V4L2.
     * Range of the settings is somewhat arbitrary; check your camera hardware.
     * When working on the Darwins typically 0 ~ 1000 is the allowable range for most settings
     * unless otherwise noted.
     */
    class CameraSettings
    {
    private:

    protected:

    public:
        double brightness;
        double contrast;
        double saturation;
        double hue;
        double gamma;
        double backlight;
        /*!
         * \brief 0-255
         */
        double gain;
        /*!
         * \brief 0-10000
         */
        double exposure;

        bool autoExposure;  /* false by default */
        bool autoWhiteBalance;
        bool autoGain;
        bool autoHue;

        // flip the image vertically/horizontally when capturing
        bool vFlip;
        bool hFlip;


        CameraSettings() :
            brightness(-1),
            contrast(-1),
            saturation(-1),
            hue(-1),
            gamma(-1),
            backlight(-1),
            gain(-1),
            exposure(1000),

            autoExposure(false),
            autoWhiteBalance(false),
            autoGain(false),
            autoHue(false),

            vFlip(false),
            hFlip(false)
        {}

        void Print() {
            std::cout << "Camera Settings:" << std::endl <<
                         "\tAuto Exposure: " << autoExposure << std::endl <<
                         "\t    Auto Gain: " << autoGain  << std::endl <<
                         "\t Auto Wht Bal: " << autoWhiteBalance << std::endl <<
                         "\t     Auto Hue: " << autoHue << std::endl <<
                         "\t     Exposure: " << exposure << std::endl <<
                         "\t   Brightness: " << brightness << std::endl <<
                         "\t     Contrast: " << contrast << std::endl <<
                         "\t   Saturation: " << saturation << std::endl <<
                         "\t         Gain: " << gain << std::endl <<
                         "\t          Hue: " << hue << std::endl <<
                         "\t    Backlight: " << backlight << std::endl <<
                         "\t        Gamma: " << gamma << std::endl <<
                          std::endl;
        }
    };
}
#endif
