#ifndef TRANSFORMATIONMATRIX_H
#define TRANSFORMATIONMATRIX_H

namespace Robot
{

    class TransformationMatrix
    {
        public:
            TransformationMatrix(double linkLength, double linkTwistDegrees, double linkOffset, double jointDegrees);
            virtual ~TransformationMatrix();

            void Multiply(TransformationMatrix &operand);
            void Print();

            double GetLinkLength();
            double GetLinkTwist();
            double GetLinkOffset();
            double GetJointAngle();

            double mtx[4][4];

    };
}
#endif // TRANSFORMATIONMATRIX_H
