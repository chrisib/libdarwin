#ifndef RIGHTLEG_H
#define RIGHTLEG_H

#include "darwin/framework/Limb.h"
#include "darwin/framework/Leg.h"
#include "darwin/framework/MotionStatus.h"

namespace Robot
{

    class RightLeg : public Leg
    {
    public:
        static RightLeg *GetInstance(){return uniqueInstance;}

        RightLeg();

        virtual void Initialize();
        virtual void Process();
        virtual Point3D GetPosition();
        virtual Point3D GetPosition(JointData &position);
        virtual double GetToeAngle();
        virtual double GetKneeAngle();
        virtual double GetHipPitch();
        virtual double GetHipRoll();
        virtual double GetAnkleRoll();
        virtual double GetAnklePitch();
        virtual void SetEnable(bool enable, bool exclusive);

        virtual int GetKneeTorque() {return MotionStatus::R_KNEE_TORQUE;}
        virtual int GetAnkleRollTorque() {return MotionStatus::R_ANKLE_ROLL_TORQUE;}
        virtual int GetAnklePitchTorque() {return MotionStatus::R_ANKLE_PITCH_TORQUE;}
        virtual int GetXFSR() {return MotionStatus::R_FSR_X;}
        virtual int GetYFSR() {return MotionStatus::R_FSR_Y;}

        virtual void SetAngles(JointData &joints);

    private:
        static RightLeg *uniqueInstance;
    };
}

#endif // RIGHTLEG_H
