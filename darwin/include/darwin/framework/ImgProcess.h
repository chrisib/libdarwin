/*
 *   ImgProcess.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _IMAGE_PROCESS_H_
#define _IMAGE_PROCESS_H_

#include "darwin/framework/Image.h"

namespace Robot
{
/*!
     * \brief Class for processing images in the original DARwIn-OP demo library
     *
     * This class is only necessary when working with the original Image class; you can
     * ignore this if using OpenCV image and camera classes.
     */
	class ImgProcess
	{
	public:
		static void YUVtoRGB(FrameBuffer *buf);
		static void RGBtoHSV(FrameBuffer *buf);

		static void Erosion(Image* img);
        static void Erosion(Image* src, Image* dest);
		static void Dilation(Image* img);
        static void Dilation(Image* src, Image* dest);

        static void HFlipYUV(Image* img);
        static void VFlipYUV(Image* img);
	};
}

#endif
