#ifndef LINETARGET_H
#define LINETARGET_H

#include <darwin/framework/Point.h>
#include <darwin/framework/minIni.h>
#include <opencv2/opencv.hpp>
#include <darwin/framework/Target.h>
#include <darwin/framework/Line.h>

namespace Robot
{
    // abstract class representing linear targets
    // main superclass for SingleLine and MultiLine targets
    class LineTarget : public Target
    {
    public:
        LineTarget(void);
        virtual ~LineTarget();

        // load target properties from an Ini file
        virtual void LoadIniSettings(minIni &ini, const std::string &section);

        // print out the target's properties
        virtual void Print() = 0;

        // locate this target on the image;
        virtual int FindInFrame(cv::Mat &image, cv::Mat *dest=NULL) = 0;
        //virtual int FindInFrame(CvCamera *camera, cv::Mat *dest=NULL) = 0;

        virtual void ProcessCandidates(std::vector<Line2D*> &candidates) = 0;
        virtual void ProcessCandidateImage(cv::Mat &binaryImage);

        virtual void Draw(cv::Mat &canvas);

        // accessors/mutators specific to line targets

        // is an angle of zero considered to be vertical or not?
        // default is no
        bool GetZeroIsVertical(){return zeroIsVertical;}
        void SetZeroIsVertical(bool b){zeroIsVertical = b;}

        double GetMinAngle(){return minAngle;}
        double GetMaxAngle(){return maxAngle;}
        double GetMaxGap(){return maxGap;}
        double GetMinLength(){return minLength;}
        double GetMaxLength(){return maxLength;}
        int GetMinVotes(){return minHoughVotes;}
        double GetSlopeMergeTolerance(){return slopeMergeTolerance;}
        double GetEndpointMergeTolerance(){return endpointMergeTolerance;}
        int GetMergeMinVotes(){return minVotesPerMerge;}
        int GetEndpointRadius(){return endpointRadius;}


        void SetMinAngle(double v){minAngle=v;}
        void SetMaxAngle(double v){maxAngle=v;}
        void SetMaxGap(double v){maxGap=v;}
        void SetMinLength(double v){minLength=v;}
        void SetMaxLength(double v){maxLength=v;}
        void SetMinVotes(int v){minHoughVotes=v;}
        void SetSlopeMergeTolerance(double v){slopeMergeTolerance=v;}
        void SetEndpointMergeTolerance(double v){endpointMergeTolerance=v;}
        void SetMergeMinVotes(int v){minVotesPerMerge=v;}
        void SetEndpointRadius(int v){endpointRadius=v;}

        bool DRAW_BUCKETS;
        bool DRAW_RAW_LINES;


    protected:
        bool zeroIsVertical;

        double minAngle, maxAngle;      // range of allowable angles for the line (0-180)
        double minLength, maxLength;    // required length for the line
        double maxGap;                  // maximum gap between segments to be considered for joining together
        int minHoughVotes;              // min hough transformation votes

        // treated as constants within the code, but are actually loaded by config and can be edited externally via accessors/mutators
        double slopeMergeTolerance;		 // if 1-abs(cos(theta)) < slopeTolerance then we consider the lines to be parallel [should be in the range [0,1]]
        double endpointMergeTolerance;   // maximum allowed pixel distance between lines' endpoints for them to be merged together
        int minVotesPerMerge;            // one merged line requires a minimum of MIN_VOTES_PER_MERGE lines
        int endpointRadius;              // presently unused

        void SlopeAndEndPointMergeLines(std::vector<Line2D> &lines, std::vector<Line2D*> &merged);

        static bool IsLineLower(Line2D *a, Line2D *b);
        static bool EndpointsNearby(Line2D &l1, Line2D &l2, double radius);

        // raw output from the HoughP transform
        std::vector<Line2D> rawLines;
        std::vector<std::vector<Line2D> > buckets;
        std::vector<cv::Scalar> spectrum;

    private:
        // create and return a new line that is a merging of all lines in the bucket
        Line2D *MergeBucket(std::vector<Line2D> &bucket);
    };
}

#endif // LINETARGET_H
