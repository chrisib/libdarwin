/*
 *   Camera.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _CAMERA_H_
#define _CAMERA_H_

namespace Robot
{
    /*!
     * \brief Hardware-specific attributes of the DARwIn-OP's camera hardware
     */
	class Camera
	{
	public:
        /*!
         * \brief The vertical FOV of the camera in degrees
         */
		static const double VIEW_V_ANGLE = 46.0; //degree

        /*!
         * \brief The horizontal FOV of the camera in degrees
         */
		static const double VIEW_H_ANGLE = 58.0; //degree

        /*!
         * \brief The width of the frames returned by the camera in pixels
         */
        static const int WIDTH  = 320;

        /*!
         * \brief The width of the frames returned by the camera in pixels
         */
        static const int HEIGHT = 240;
	};
}

#endif
