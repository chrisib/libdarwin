/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is used to locate a single uniformly-coloured target
 * TODO: revise the image-processing algorithm to use the scan-line algorithm
 * instead of the crude methods used currently
 ************************************************************************/


#ifndef SINGLEBLOB_H
#define SINGLEBLOB_H

#include "darwin/framework/BlobTarget.h"
#include <string>
#include <iostream>
#include "darwin/framework/minIni.h"
#include "darwin/linux/CvCamera.h"

namespace Robot
{
    // a single-coloured target that appears exactly once in a scene
    // the largest object (determined by the area of its bounding box) is returned
    // with all other objects ignored
    class SingleBlob: public BlobTarget
    {
        public:
            // create a new Target with a specific name (name is only used when printing)
            // all other parameters are set to zero except the bounding box colour (set to RGB(255,255,0))
            SingleBlob(void);
            explicit SingleBlob(const char* name);

            // draw the target's bounding box on the provided image
            virtual void Draw(cv::Mat &image);
            virtual void DrawBoundingBox(cv::Mat &image) __attribute__((deprecated("Use Draw(cv::Mat) instead"))){Draw(image);}

            // print the name, current position, and pixel size of the target in the last frame examined
            virtual void Print();

            // locate this target on the image;
            // return true if the target's location & size are within the set parameters
            // otherwise return false
            virtual int FindInFrame(cv::Mat &image, cv::Mat *dest=NULL);
            virtual int FindInFrame(CvCamera *camera, cv::Mat *dest);

            virtual void ProcessCandidates(std::vector<BoundingBox*> &candidates);

            // T/F indicating if the target was found anywhere in the last frame
            virtual bool WasFound() const {return foundInLastFrame;}

            // return T/F if the target is in the top/left ranges specified
            virtual bool CheckPosition();

            virtual void LoadIniSettings(minIni &ini, const std::string &iniSection);

            // Accessors/Mutators

            // get the target's current pixel dimensions
            BoundingBox *GetBoundingBox() {return &(this->boundingBox);}

            ThresholdRange *GetLeftRange(){return &leftRange;}
            ThresholdRange *GetTopRange(){return &topRange;}

            void SetLeftRange(int min,int max){leftRange.min=min;leftRange.max=max;}
            void SetTopRange(int min,int max){topRange.min=min;topRange.max=max;}

            void setPositionDisabled(bool v){positionDisabled = v;}
            bool getPositionDisabled(){return positionDisabled;}

            //int GetDistance();
            //bool CheckDistance(int minDistance, int maxDistance, double headAngle);
            //Point2D GetPosition(double panAngle, double tiltAngle);
            //Point3D GetPosition3D(double panAngle, double tiltAngle);

            // get the range and angle to the target
            // Optionally the object may be raised up off the ground
            virtual double GetRange(double heightAboveGround = 0.0);
            virtual double GetAngle(double heightAboveGround = 0.0);

        protected:
            bool foundInLastFrame;

            bool positionDisabled;

            // the target's pixel dimensions and position
            BoundingBox boundingBox;

            ThresholdRange topRange;
            ThresholdRange leftRange;

            int realHeight;
            int realWidth;

            void Init(const char* name);
            int GetMaxVotes();
    };
}

#endif // TARGET_H
