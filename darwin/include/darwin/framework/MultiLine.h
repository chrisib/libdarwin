#ifndef MULTILINE_H
#define MULTILINE_H

#include <darwin/framework/LineTarget.h>

namespace Robot
{
    class MultiLine : public LineTarget
    {
    public:
        MultiLine();
        virtual ~MultiLine();

        // print out the target's properties
        virtual void Print();

        // locate this target on the image;
        virtual int FindInFrame(cv::Mat &image, cv::Mat *dest=NULL);
        //virtual int FindInFrame(CvCamera *camera, cv::Mat *dest=NULL) = 0;

        virtual void ProcessCandidates(std::vector<Line2D*> &candidates);

        virtual void Draw(cv::Mat &canvas);

        std::vector<Line2D*> *GetAllLines() {return &lines;}

    protected:
        std::vector<Line2D*> lines;
        void ClearAllLines();
    };
}

#endif // MULTILINE_H
