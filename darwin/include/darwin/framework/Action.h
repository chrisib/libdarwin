/*
 *   Action.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _ACTION_MODULE_H_
#define _ACTION_MODULE_H_

#include <stdio.h>
#include "darwin/framework/MotionModule.h"
#include "darwin/framework/JointData.h"

namespace Robot
{
    /*!
     * \brief A motion module that plays keyframe motion sequences loaded from a file
     *
     * This class is a singleton class that must be added to the MotionManager before it can be used.
     * It requires an external motion file be provided to read keyframes from.
     *
     * The LinuxDARwIn::Initialize function can optionally automatically load and initialize this
     * module and read the motion file.
     *
     * The motion files used by this module are binary files created by the RoboPlus tools.
     * The binary file is a set of 512-byte pages consisting of a 64-byte header and 7 64-byte
     * keyframes per page.
     *
     * Each page contains the absolute positions of every motor in the robot for that pose, as well as
     * information about the speed of the motors, next motion, etc...
     *
     * Consult Robotis' RoboPlus documentation for more about keyframe motions
     */
	class Action : public MotionModule
	{
	public:
		enum
		{
			MAXNUM_PAGE = 256,
			MAXNUM_STEP = 7,
			MAXNUM_NAME = 13
		};

		enum
		{
			SPEED_BASE_SCHEDULE = 0,
			TIME_BASE_SCHEDULE = 0x0a
		};

		enum
		{
			INVALID_BIT_MASK	= 0x4000,
			TORQUE_OFF_BIT_MASK	= 0x2000
		};

        /*!
         * \brief Pre-programmed motions in the DEFAULT motion.bin files
         *
         * These may not be useful if you use a custom motion file
         * not all stock motions are listed here
         * enum names are based on the motion page names
         * some of the names are cryptic; I'm not entirely sure what they're for
         */
		enum
		{
		    DEFAULT_MOTION_INIT         =1,
		    DEFAULT_MOTION_OK           =2,
		    DEFAULT_MOTION_NO           =3,
		    DEFAULT_MOTION_HI           =4,
		    DEFAULT_MOTION_HUH          =5,
		    DEFAULT_MOTION_TALK1        =6,

		    DEFAULT_MOTION_WALKREADY    =9,
		    DEFAULT_MOTION_F_UP         =10,
		    DEFAULT_MOTION_B_UP         =11,
		    DEFAULT_MOTION_R_KICK       =12,
		    DEFAULT_MOTION_L_KICK       =13,

		    DEFAULT_MOTION_SIT_DOWN     =15,
		    DEFAULT_MOTION_STAND_UP     =16
		};

        enum
        {
            MOTION_FILE_1024,
            MOTION_FILE_4096
        };

		typedef struct // Header Structure (total 64unsigned char)
		{
			unsigned char name[MAXNUM_NAME+1]; // Name             0~13
			unsigned char reserved1;        // Reserved1        14
			unsigned char repeat;           // Repeat count     15
			unsigned char schedule;         // schedule         16
			unsigned char reserved2[3];     // reserved2        17~19
			unsigned char stepnum;          // Number of step   20
			unsigned char reserved3;        // reserved3        21
			unsigned char speed;            // Speed            22
			unsigned char reserved4;        // reserved4        23
			unsigned char accel;            // Acceleration time 24
			unsigned char next;             // Link to next     25
			unsigned char exit;             // Link to exit     26
			unsigned char reserved5[4];     // reserved5        27~30
			unsigned char checksum;         // checksum         31
			unsigned char slope[31];        // CW/CCW compliance slope  32~62
			unsigned char reserved6;        // reserved6        63
		} PAGEHEADER;

		typedef struct // Step Structure (total 64unsigned char)
		{
			unsigned short position[31];    // Joint position   0~61
			unsigned char pause;            // Pause time       62
			unsigned char time;             // Time             63
		} STEP;

		typedef struct // Page Structure (total 512unsigned char)
		{
			PAGEHEADER header;          // Page header  0~64
			STEP step[MAXNUM_STEP];		// Page step    65~511
		} PAGE;

	private:
		static Action* m_UniqueInstance;
		FILE* m_ActionFile;
		PAGE m_PlayPage;
		PAGE m_NextPlayPage;
		STEP m_CurrentStep;

		int m_IndexPlayingPage;
		bool m_FirstDrivingStart;
		int m_PageStepCount;
		bool m_Playing;
		bool m_StopPlaying;
		bool m_PlayingFinished;

        int m_FileResolution;

		bool VerifyChecksum( PAGE *pPage );
		void SetChecksum( PAGE *pPage );

	public:
		bool DEBUG_PRINT;

        Action();
		~Action();

		static Action* GetInstance() { return m_UniqueInstance; }

		void Initialize();
		void Process();

        /*!
         * \brief Load a binary motion file
         * \param filename The path of the file to read
         * \param resolution The resolution of the motors in the file (MX-28 motors use 4096 resolution)
         * \return True if the file was successfully loaded, otherwise false
         */
        bool LoadFile(const char* filename, int resolution=MOTION_FILE_4096);

        /*!
         * \brief Write the module's motions to a binary motion file
         * \param filename The path of the file to create
         * \return True if the file was successfully created, otherwise false
         */
		bool CreateFile(const char* filename);

        /*!
         * \brief Start playing a page from the motion file
         * \param iPage The index of the page to start playing
         * \return True if the motion was started, otherwise false
         */
		bool Start(int iPage);
        bool Start(const char* namePage) __attribute_deprecated__;
        bool Start(int index, PAGE *pPage) __attribute_deprecated__;

        /*!
         * \brief Stop the module
         *
         * This signals that the current motion should stop; the actual motion will continue to its designated stop pose
         */
		void Stop();

        /*!
         * \brief Immediately halt the module
         *
         * This will halt the motion immediately, even if the robot is in an unstable position
         */
		void Brake();
		bool IsRunning();
		bool IsRunning(int *iPage, int *iStep);
		virtual void SetEnable(bool enable, bool exclusive = false);
		bool LoadPage(int index, PAGE *pPage);
		bool SavePage(int index, PAGE *pPage);
		void ResetPage(PAGE *pPage);
	};
}

#endif
