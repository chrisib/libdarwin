/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class allows is a generic superclass for a Darwin arm; it will
 * eventually be used to contain inverse-kinematics
 ************************************************************************/

#ifndef ARM_H
#define ARM_H

#include "darwin/framework/MotionModule.h"
#include "darwin/framework/Point.h"
#include "darwin/framework/CM730.h"
#include "darwin/framework/MotionStatus.h"
#include "darwin/framework/Limb.h"

namespace Robot
{
    /*!
     * \brief Generic superclass for LeftArm and RightArm
     *
     * Contians all arm-specific functionality that is distinct from generic limbs
     */
    class Arm : public Limb
    {
        public:
            // technically these are configurable, but they are set in the constructor and shouldn't need to be changed under normal circumstances
            /*!
             * \brief The angle (in degrees) of the hand motor to be considered open
             *
             * Measured as a body angle where 0 is closed, 90 is open to a right-angle, and 180 would be backwards into the forearm.
             * Configurable, but set to a default value of 90 degrees in the constructor.
             */
            double OPEN_ANGLE;// = 90;

            /*!
             * \brief The angle (in degrees) of the hand motor to be considered neutral
             *
             * Measured as a body angle where 0 is closed, 90 is open to a right-angle, and 180 would be backwards into the forearm.
             * Configurable, but set to a default value of 0 degrees in the constructor.
             */
            double NEUTRAL_ANGLE;// = 0;

            /*!
             * \brief The angle (in degrees) of the hand motor to be considered closed firmly
             *
             * Measured as a body angle where 0 is closed, 90 is open to a right-angle, and 180 would be backwards into the forearm.
             * Configurable, but set to a default value of -30 degrees in the constructor.
             */
            double CLOSE_ANGLE;// = -30;

            /*!
             * \brief The maximum allowed torque read on the hand motor when closing the hand
             *
             * While the hand is closing (i.e. moving from a positive angle toward a smaller angle) if the torque meets or
             * exceeds this value the hand's motion is stopped.
             * Configurable, but set to a default value of 600 in the constructor.  Units are defined by the ReadTorque functionality
             * of the hand's servo
             */
            int TORQUE_LIMIT;// = 600;

            /*!
             * \brief The absolute minimum possible Y-axis position of the hand in mm as body coordinates
             *
             * Because of the way the robot is built it absolutely cannot reach a Y position closer
             * to the torso than -35mm from the shoulder (negative being toward the body's centreline)
             */
            static const double MIN_Y_POSITION = -35.0;

            /*!
             * \brief Destructor; cleans up any dynamically-allocated memory
             */
            virtual ~Arm();

            /*!
             * \brief Get the XYZ position of the arm in mm as body coordinates
             * \return
             */
            virtual Point3D GetPosition() = 0;

            /*!
             * \brief Get the XYZ position of the the arm in mm as body coordinates if its limbs were set according to the
             * values provided
             * \param position The positions of all of the joints in the robot
             * \return
             */
            virtual Point3D GetPosition(JointData &position) = 0;

            /*!
             * \brief Get the XYZ position of the arm in body coordinates given the arm angles provided
             * \param shoulderPitchAngle The shoulder pitch angle in degrees where 0 is standard anatomical and 90 is sticking straight ahead
             * \param shoulderRollAngle The shoulder roll angle in degrees where 0 is at the robot's side and 90 is sticking straight out to the side
             * \param elbowAngle The elbow angle where 0 is straight-armed and 90 is a normal right-angle elbow position
             * \return
             */
            virtual Point3D GetPosition(double shoulderPitchAngle, double shoulderRollAngle, double elbowAngle);

            /*!
             * \brief Get the absolute XYZ position of the robot's hand measured from the robot's origin
             * \return
             */
            virtual Point3D GetAbsolutePosition();

            /*!
             * \brief Get the absolute XYZ position of the robot's hand measured from the robot's origin if the arm's joints were as-provided
             * \param position
             * \return
             */
            virtual Point3D GetAbsolutePosition(JointData &position);

            /*!
             * \brief Get the shoulder's roll body angle
             * \return The shoulder roll angle in degrees where 0 is at the robot's side and 90 is sticking straight out
             */
            virtual double GetShoulderRoll() = 0;

            /*!
             * \brief Get the shoulder's pitch body angle
             * \return The shoulder pitch angle in degrees where 0 is at the robot's side and 90 is sticking straight forward
             */
            virtual double GetShoulderPitch() = 0;

            /*!
             * \brief Get the elbow body angle
             * \return The elbow angle in degrees where 0 is straight and 90 is at a natural right angle
             */
            virtual double GetElbowAngle() = 0;

            /*!
             * \brief Get the hand body angle
             * \return The hand angle in degrees where 0 is neutral and 90 is open wide
             */
            virtual double GetHandAngle() = 0;

            /*!
             * \brief Set the 3D position of the hand relative to the shoulder in body coordinates
             * \param endEffectorPosition The XYZ position in mm of the hand where:
             *        - X: positive = forward
             *        - Y: positive = away from the body
             *        - Z: positive = above shoulder level
             */
            virtual void SetGoalPosition(Point3D &endEffectorPosition);

            /*!
             * \brief Set the 3D position of the hand relative to the shoulder in body coordinates
             *
             * This function is deprecated
             * \param endEffectorPosition The XYZ position in mm of the hand where:
             *        - X: positive = forward
             *        - Y: positive = away from the body
             *        - Z: positive = above shoulder level
             * \param handAngle: unused
             */
            virtual void SetGoalPosition(Point3D &endEffectorPosition, double handAngle)  __attribute__((deprecated("Use SetGoalPosition(Point3D) instead")));

            /*!
             * \brief Pull the hand toward the body such that the torque on all motors is not greater than the provided value
             * \param force The maximum allowed torque while pulling (as read form the motors -- see the Dynamixel documentation)
             */
            virtual void Pull(int force) = 0;

            /*!
             * \brief Push the hand away from the body such that the torque on all motors is not greater than the provided value
             * \param force The maximum allowed torque while pushing (as read form the motors -- see the Dynamixel documentation)
             */
            virtual void Push(int force) = 0;

            /*!
             * \brief Set the angles of the arm directly
             *
             * Angles are in degrees and correspond to body angle
             * \param shoulderPitch The shoulder pitch in degrees where 0 is at the robot's side and 90 is straight forward
             * \param shoulderRoll The shoulder roll angle in degrees where 0 is at the robot's side and 90 is straight out sideways
             * \param elbow The elbow angle in degrees where 0 is straight and 90 is bent at a right-angle
             */
            virtual void SetAngles(double shoulderPitch, double shoulderRoll, double elbow) = 0;

            /*!
             * \brief Initialize the arm module
             */
            virtual void Initialize();

            /*!
             * \brief Is the arm module currently moving the arm from one point to another?
             * \return
             */
            virtual bool IsRunning();

            /*!
             * \brief Open the hand from its current angle until its angle is >= OPEN_ANGLE
             * \param speed The number of degrees to open per tick
             */
            virtual void OpenHand(double speed=1);

            /*!
             * \brief Close the hand from its current angle until its angle is <= CLOSE_ANGLE or it encounters torque >= TORQUE_LIMIT
             * \param speed The number of degrees to close per tick
             */
            virtual void CloseHand(double speed=1);

            /*!
             * \brief Move the hand to the designated angle
             * \param toAngle The desired angle
             * \param speed The number of degrees to move per tick
             */
            virtual void MoveHand(double toAngle, double speed=1);

            /*!
             * \brief Move the hand toward NEUTRAL_ANGLE
             * \param speed The number of degrees to move per tick
             */
            virtual void RelaxHand(double speed=1);

            /*!
             * \brief Is the hand currently open?
             * \return True if the hand is open
             */
            virtual bool IsHandOpen() = 0;

            /*!
             * \brief Read the torque of the hand servo
             * \return The raw torque reading from the hand servo
             */
            virtual int ReadHandTorque() = 0;
            
            /*!
             * \brief Give this module control over the shoulder roll & pitch, elbow, and hand motors
             * \param enable If true this module is given control over the motors
             * \param exclusive If true this module is given exclusive control
             */
            virtual void SetEnable(bool enable, bool exclusive) = 0;

            /*!
             * \brief Give this module control over the hand motors
             * \param enable If true this module is given control over the motors
             * \param exclusive If true this module is given exclusive control
             */
            virtual void SetEnableHand(bool enable, bool exclusive) = 0;

            /*!
             * \brief Push the arm forward such that it lies along the same angle it currently is, but the radius grows
             * \param speed How quickly the arm should extend in mm per tick
             */
            virtual void Extend(double speed=0.1);

            // indices for the jointPositions vector
            enum {
                JOINT_SHOULDER_PITCH = 0,
                JOINT_SHOULDER_ROLL,
                JOINT_ELBOW,
                JOINT_FINGER,
                NUM_JOINTS
            };

        protected:
            /*!
             * \brief Arm constructor
             * \param invertYAxis Specifies if this arm's Y-axis is inverted relative to the absolute coordinate system
             */
            Arm(bool invertYAxis);

            double targetElbowAngle;
            double targetShoulderRollAngle;
            double targetShoulderPitchAngle;

            bool handIsMoving;
            double handCurrentAngle;
            double handTargetAngle;
            double handMovingSpeed;

            /*!
             * \brief Change the position of the hand motor.
             *
             * This function is intended to be called as part of the Update function of the limb
             * \param motorId
             */
            virtual void UpdateHandPosition(int motorId);

        private:
    };

}
#endif // ARM_H
