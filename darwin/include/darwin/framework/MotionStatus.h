/*
 *   MotionStatus.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _MOTION_STATUS_H_
#define _MOTION_STATUS_H_

#include "darwin/framework/JointData.h"

namespace Robot
{
    enum {
        BACKWARD    = -1,
        STANDUP     = 0,
        FORWARD     = 1
    };

	class MotionStatus
	{
	private:

	public:

	    static const int FALLEN_F_LIMIT     = 390;
	    static const int FALLEN_B_LIMIT     = 580;
	    static const int FALLEN_MAX_COUNT   = 30;

        static unsigned long int ticks;

		static JointData m_CurrentJoints;
		static int FB_GYRO;
		static int RL_GYRO;
		static int Z_GYRO;
		static int FB_ACCEL;
		static int RL_ACCEL;
		static int Z_ACCEL;
		
		// DLC
		// running sum of Z_GYRO data
		static int Z_GYRO_SUM;

		static int BUTTON;
		static int FALLEN;

		// CIB
		// current positions and loads on the joints
		static int R_ANKLE_ROLL_TORQUE;
		static int R_ANKLE_PITCH_TORQUE;
		static int L_ANKLE_ROLL_TORQUE;
		static int L_ANKLE_PITCH_TORQUE;
		static int R_HAND_TORQUE;
		static int L_HAND_TORQUE;
        static int R_KNEE_TORQUE;
        static int L_KNEE_TORQUE;

		// CIB
        // FSR data
        // [0-254] = load read
        // -1 = no data
        // X = 0 :: load at toe
        // Y = 0 :: load at instep
		static int R_FSR_X;
		static int R_FSR_Y;
		static int L_FSR_X;
		static int L_FSR_Y;

        // CIB
        // current input voltage of the CM730 (in volts)
        static double CM730_VOLTAGE;
	};
}

#endif
