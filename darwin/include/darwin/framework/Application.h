#ifndef DARWINAPPLICATION_H
#define DARWINAPPLICATION_H

#include <string>
#include <map>
#include <pthread.h>
#include <darwin/framework/minIni.h>
#include <darwin/framework/Action.h>
#include <darwin/framework/CM730.h>
#include <darwin/linux/LinuxCM730.h>

namespace Robot
{
    /*!
      \brief A template for creating applications using libdarwin

      Has built-in mechanisms for command-line argument parsing,
      vision-processing threads, etc....

      Users should extend this class and write a main function that
      instantiates it, calls the initialization functions, and calls
      Execute().

      e.g.
      \code{.cpp}
      class Sprint : public Robot::Application {
        void Process() {
            // do whatever needs doing in the sprint
        }

        void HandleVideo() {
            // capture a frame of video and process it
        }
      };

      ...

      int main(int argc, char** argv) {
        Robot::Application app = Sprint();
        app.RegisterArgument("--distance",&distanceToRun,3,"set the number of meters in the race");

        app.parseArguments(argc, argv);
        app.Initialize("config.ini","motion.bin",Action::DEFAULT_MOTION_SIT_DOWN);
        app.Execute();

        return 0;
      }
      \endcode

      This class should be refactored to use TCLAP for the command-line parameters instead of using the homebrew system
      invented for here. It was a fun experiment, but I wish I'd known about TCLAP before...
     */
    class Application{
    public:
        Application();
        virtual ~Application();

        /*!
         * \brief Called in an infinite loop by Execute
         **/
        virtual void Process()=0;

        /*!
         * \brief Initialize the application
         *
         * Calls (in this order):
         * 1- InitIni
         * 2- InitCM730 (if the vision only flag is not set)
         * 3- InitCamera
         * 4- LoadIniSettings
         * 5- StartVisionThread
         *
         * \param iniFilePath The path to the configuration file to read on startup.
         * This parameter is only used if the protected class variable of the same name is not set.
         * Normally the protected variable is set when parsing the command-line arguments (the --config flag)
         * but if the flag has been disabled for a given application (using the UnregisterArgument function)
         * we can support setting the path explicitly as a parameter to this function.
         * \param motionFilePath The path to the binary motion file that contains the robot's safe position
         * \param safePosition The index of the page in the motion file of the robot's default safe position
         * \return True if the application was initialized successfully, otherwise false
         *
         * \see LinuxDARwIn::EnterSafePosition
         * \see Action
         */
        virtual bool Initialize(const char* iniFilePath=NULL, const char* motionFilePath="motion.bin", int safePosition=Robot::Action::DEFAULT_MOTION_SIT_DOWN);

        /*!
         * \brief Initialize the minIni handler for the configuration file
         * \param iniFilePath The path to the configuration file to load
         * \return True if the file was loaded successfully, otherwise false
         */
        virtual bool InitIni(const char* iniFilePath);

        /*!
         * \brief Initialize the CM730, add the Action module to the MotionManager, and initialize the action module
         *
         * Calling this function will cause the robot to enter its default "safe" position
         * \param motionFilePath The path to the motion file to load into the Action module
         * \param safePosition The page in the motion file that the robot should use as its default position
         * \return True if the CM730 was successfully initialized
         */
        virtual bool InitCM730(const char* motionFilePath, int safePosition=Robot::Action::DEFAULT_MOTION_SIT_DOWN);

        /*!
         * \brief Initialize the camera and load the camera settings from the ini file
         * \param iniFilePath The ini file to load.  Ignored if the ini file path has already been set by either a command-line switch or the Initialize function
         * \return True if the camera was initialized, otherwise false
         */
        virtual bool InitCamera(const char* iniFilePath="config.ini");

        /*!
         * \brief Start a background thread that will grab and process frames from the camera
         *
         * \see VideoThread
         * \see HandleVideo
         */
        virtual void StartVisionThread();

        // by default this does nothing
        // clients should write their own which will be called from Initialize
        /*!
         * \brief Read and apply configuration settings from the ini file
         *
         * By default this function does nothing. If this behaviour is undesired, subclasses should implement
         * their own function will will be called by Initialize.  If not inheriting from Application directly
         * subclasses should call their superclass' LoadIniSettings function.
         */
        virtual void LoadIniSettings(){}

        /*!
         * \brief Start an infinite loop that calls Process() over and over
         */
        virtual void Execute();

        /*!
         * \brief Parse the command-line arguments used to invoke this application
         * \param argc The number of command-line arguments
         * \param argv The value of the command-line arguments
         */
        virtual void ParseArguments(int argc, char** argv);

        /*!
         * \brief Print the command-line help (triggered by the -h or --help flags)
         */
        virtual void PrintHelp();

        /*!
         * \brief Register a new command-line argument
         * \param flag The flag that triggers the argument (e.g. --config)
         * \param var A pointer to the variable set by the argument
         * \param defaultValue The default value to assign to var if the argument is omitted by the user
         * \param description The description/summary displayed to the user when PrintHelp() is called
         */
        virtual void RegisterArgument(std::string flag, int* var, int defaultValue, std::string description);

        /*!
         * \brief Register a new command-line argument
         * \param flag The flag that triggers the argument (e.g. --config)
         * \param var A pointer to the variable set by the argument
         * \param defaultValue The default value to assign to var if the argument is omitted by the user
         * \param description The description/summary displayed to the user when PrintHelp() is called
         */
        virtual void RegisterArgument(std::string flag, double* var, double defaultValue, std::string description);

        /*!
         * \brief Register a new command-line argument
         * \param flag The flag that triggers the argument (e.g. --config)
         * \param var A pointer to the variable set by the argument
         * \param defaultValue The default value to assign to var if the argument is omitted by the user
         * \param description The description/summary displayed to the user when PrintHelp() is called
         */
        virtual void RegisterArgument(std::string flag, std::string* var, std::string defaultValue, std::string description);

        /*!
         * \brief Register a new command-line switch that toggles a variable
         * \param flag The flag that triggers the argument (e.g. --vision-only)
         * \param var A pointer to the variable set by the argument
         * \param defaultValue The default value to assign to var if the argument is omitted by the user
         * \param description The description/summary displayed to the user when PrintHelp() is called
         */
        virtual void RegisterSwitch(std::string flag, bool* var, bool defaultValue, std::string description);

        /*!
         * \brief Remove a command-line argument that has been added by RegisterArgument or RegisterSwitch
         * \param flag The flag of the argument we're removing
         */
        virtual void UnregisterArgument(std::string flag);

        // flags for arguments enabled by default
        // automatically registered when Application is instantiated, but can be removed via UnregisterArgument
        /*!
         * \brief The default vision-testing switch; indicates that we should enable and run the camera, but not run the motors
         *
         * Good for testing vision calibration before a competition
         */
        static const std::string VISION_TEST_FLAG;

        /*!
         * \brief A switch that signals that we should display the raw video feed in an X11 window when processing video
         */
        static const std::string SHOW_VIDEO_FLAG;

        /*!
         * \brief Allows setting the configuration file path via a command-line argument
         */
        static const std::string CONFIG_FILE_FLAG;


        /*!
         * \brief Get the current CM730 button status
         *
         * Wrapper for MotionStatus::BUTTON
         * \return Returns the MotionStatus' current button status
         */
        int GetButton();

        /*!
         * \brief Wait for a button to be pressed
         *
         * Blocks execution until the button state we want is reached
         * \param button The button state we're waiting for
         * \param function An optional function to invoke over and over while waiting (e.g. flash an LED)
         *
         * \see MotionStatus::BUTTON
         */
        void WaitButton(int button, void (*function)(void)=NULL);
        
        /*!
         * \brief Force the robot into its safe, stable position
         *
         * \see LinuxDARwIn::EnterSafePosition
         * \see Initialize
         */
        void EnterSafePosition();

        /*!
         * \brief Get the application's configuration file handler
         * \return
         */
        minIni *GetIni(){return ini;}

    protected:
        /*!
         * \brief The command-line argument object used to save the description, type, and value of command-line parameters
         */
        class CommandLineArgument{
        public:
            /*!
             * \brief The types of argument we can store
             */
            enum ArgType
            {
                INTEGER,
                DOUBLE,
                BOOLEAN,
                STRING
            };

            /*!
             * \brief The flag used on the command-line to set the parameter (e.g. --config, --vision-only, --help)
             */
            std::string flag;

            /*!
             * \brief The human-readible description of the parameter shown when we print the application's help
             */
            std::string description;

            /*!
             * \brief A pointer to the variable set by this argument
             */
            void* value;

            /*!
             * \brief The type of variable this argument sets
             *
             * \see ArgType
             */
            int type;
        };
        /*!
         * \brief The set of command-line arguments registered by this application
         */
        std::map<std::string, CommandLineArgument> arguments;

        /*!
         * \brief Has the --show-video flag been set?
         */
        bool showVideo;

        /*!
         * \brief The path to the application's configuration file
         */
        std::string iniFilePath;

        /*!
         * \brief Was the --vision-only flag set?
         *
         * Setting visionOnly to true naturally implies showVideo should be true as well, since
         * --vision-only is intended specifically for vision debugging
         */
        bool visionOnly;

        /*!
         * \brief The configuration file handler
         */
        minIni *ini;

        /*!
         * \brief The ID of the thread that handles the video processing
         */
        pthread_t videoThreadId;

        /*!
         * \brief Function that is called by the VideoThread function to capture and process frames from the camera
         *
         * Should capture a frame from the camera and implement any application-specific object detection that's needed
         */
        virtual void HandleVideo() = 0;

        /*!
         * \brief A static function executed in a background thread that calls HandleVideo over and over
         * \param arg
         * \return
         */
        static void *VideoThread(void* arg);

    };
}
#endif // DARWINAPPLICATION_H
