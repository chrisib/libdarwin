/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * AXS1 sensor module (originally from bioloids, but compatible with DARwIn-OP)
 ************************************************************************/

#include "darwin/framework/Dynamixel.h"

#ifndef AXS1_H_INCLUDED
#define AXS1_H_INCLUDED

namespace Robot
{
    /*!
     * \brief Interface class for the AX-S1 IR/Audio sensor module that ships with the bioloid
     *
     * This hardware is not used on any of our bioloids, but could eventually be used
     */
    class AXS1: public Dynamixel
    {
    public:

        // Address
        enum
        {
            P_MODEL_NUMBER_L            = 0,
            P_MODEL_NUMBER_H            = 1,
            P_VERSION                   = 2,
            P_ID                        = 3,
            P_BAUD_RATE                 = 4,
            P_RETURN_DELAY_TIME         = 5,
            P_HIGH_LIMIT_TEMPERATURE    = 11,
            P_LOW_LIMIT_VOLTAGE         = 12,
            P_HIGH_LIMIT_VOLTAGE        = 13,
            P_RETURN_LEVEL              = 16,
            P_OBSTACLE_DETECT_COMPARE_V = 20,
            P_LIGHT_DETECT_COMPARE_V    = 21,
            P_LEFT_IR_SENSOR            = 26,
            P_CENTER_IR_SENSOR          = 27,
            P_RIGHT_IR_SENSOR           = 28,
            P_LEFT_LUMINOSITY           = 29,
            P_CENTER_LUMINOSITY         = 30,
            P_RIGHT_LUMINOSITY          = 31,
            P_OBSTACLE_DETECT_FLAG      = 32,
            P_LUMINOSITY_DETECT_FLAG    = 33,
            P_SOUND_DATA                = 35,
            P_SOUND_DATA_MAX_HOLD       = 36,
            P_SOUND_DETECTED_COUNT      = 37,
            P_SOUND_DETECTED_TIME_L     = 38,
            P_SOUND_DETECTED_TIME_H     = 39,
            P_BUZZER_INDEX              = 40,
            P_BUZZER_TIME               = 41,
            P_PRESENT_VOLTAGE           = 42,
            P_PRESENT_TEMPERATURE       = 43,
            P_REGISTERED_INSTRUCTION    = 44,
            P_IR_REMOCON_ARRIVED        = 46,
            P_LOCK                      = 47,
            P_IR_REMOCON_RX_DATA_0      = 48,
            P_IR_REMOCON_RX_DATA_1      = 49,
            P_IR_REMOCON_TX_DATA_0      = 50,
            P_IR_REMOCON_TX_DATA_1      = 51,
            P_OBSTACLE_DETECTED_COMPARE = 52,
            P_LIGHT_DETECTED_COMPARE    = 53,

            MAXNUM_ADDRESS
        };
    };
}

#endif // AXS1_H_INCLUDED

