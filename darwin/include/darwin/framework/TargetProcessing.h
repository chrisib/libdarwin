/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class provides basic image processing tools used by the Target and
 * MultiTarget classes
 ************************************************************************/


#ifndef TARGETPROCESSING_H
#define TARGETPROCESSING_H

#include "darwin/framework/Point.h"
#include "darwin/framework/CameraSettings.h"
#include "darwin/framework/BoundingBox.h"
#include <opencv/cv.h>
#include <vector>
#include <list>
#include "darwin/framework/Camera.h"
#include "darwin/framework/AbstractTarget.h"
#include "darwin/framework/ThresholdRange.h"

namespace Robot
{
    class TargetProcessing
    {
        public:

            // return true if the provided YUV values are within the right range
            static bool IsTargetPixel(uint8_t y, uint8_t u, uint8_t v, ThresholdRange &yRange, ThresholdRange &uRange, ThresholdRange &vRange);

            // create a new binary image that represents the scene
            static cv::Mat *Yuv2Binary(cv::Mat &src, ThresholdRange &yRange, ThresholdRange &uRange, ThresholdRange &vRange);

            // find multiple targets with a single scanline
            // the largest scan and fill thresholds of any target are used when scanning
            // if dest in not NULL the processed image is saved to it for debugging purposes
            static void FindTargets(std::vector<Robot::AbstractTarget*> &targets, cv::Mat &src, cv::Mat *dest=NULL, int subsample=2);

            // copy a single channel into a new image
            // the new image is an 8-bit, 1-channel image
            static cv::Mat *ExtractChannel(cv::Mat &src, int channel);

            // create a new Mat with the following channels:
            //  y-u
            //  y-v
            //  u-v
            // new channels are normalized to be in the [0,255] range
            static cv::Mat *CreateCompliment(cv::Mat &src);

            // convert row/column values to a one-dimensional array index (and vice-versa) for finding pixels in an image
            static int RowCol2Index(int row, int col, cv::Mat &mat);
            static void Index2RowCol(int index, int *row, int *col, cv::Mat &mat);

        private:
            TargetProcessing();
            virtual ~TargetProcessing();

            static void FloodFill(int row, int col, int avgY, int avgU, int avgV, uint8_t *yuvData, int avgR, int avgG, int avgB, uint8_t *rgbData, int avgRG, int avgRB, int avgGB, uint8_t *rgbcData, int threshold, cv::Mat &src, uint8_t *destData, BoundingBox *result, int minRow, int maxRow, int minCol, int maxCol, int subsample);
            static void ProcessPixel(int row, int col,std::list<int> &stack, uint8_t *yuvData, int avgY, int avgU, int avgV, uint8_t *rgbData, int avgR, int avgG, int avgB, uint8_t *rgbcData, int avgRG, int avgRB, int avgGB, int threshold, uint8_t *destData, cv::Mat &src);
    };
}

#endif // TARGETPROCESSING_H
