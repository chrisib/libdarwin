/*
 *   MotionManager.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _MOTION_MANGER_H_
#define _MOTION_MANGER_H_

#include <list>
#include <fstream>
#include <iostream>
#include "darwin/framework/MotionStatus.h"
#include "darwin/framework/MotionModule.h"
#include "darwin/framework/CM730.h"
#include "darwin/framework/minIni.h"
#include <list>

#define OFFSET_SECTION "Offset"
#define INVALID_VALUE   -1024.0

namespace Robot
{
	class MotionManager
	{
	private:
        std::list<MotionModule*> m_Modules;
		static MotionManager* m_UniqueInstance;
		CM730 *m_CM730;
		bool m_ProcessEnable;
		bool m_Enabled;
		int m_FBGyroCenter;
		int m_RLGyroCenter;
		int m_ZGyroCenter;
		int m_CalibrationStatus;

		bool m_IsRunning;
		bool m_IsThreadRunning;
		bool m_IsLogging;
        bool m_FsrPresent;

		std::ofstream m_LogFileStream;

        MotionManager();

	public:
        bool DEBUG_PRINT;
        int m_Offset[JointData::NUMBER_OF_JOINTS];

		~MotionManager();

		static MotionManager* GetInstance() { return m_UniqueInstance; }

		bool Initialize(CM730 *cm730);
		bool Reinitialize();
        void Process();
		void SetEnable(bool enable);
		bool GetEnable()				{ return m_Enabled; }
		void AddModule(MotionModule *module);
		void RemoveModule(MotionModule *module);

		// CIB
		// Force all modules currently loaded to stop
		void StopAllModules();

		// DLC
		// Wait for waitTime milliseconds, function will be called continuously while waiting
		static void msleep(double waitTime, void (*function)(void)=NULL);

		// SBD
		// Same as msleep, but with microseconds
		static void usleep(double waitTime, void (*function)(void) = NULL);

		void ResetGyroCalibration() { m_CalibrationStatus = 0; m_FBGyroCenter = 512; m_RLGyroCenter = 512;}
		int GetCalibrationStatus() { return m_CalibrationStatus; }
		void SetJointDisable(int index);

		void StartLogging();
		void StopLogging();

        void LoadINISettings(minIni* ini);
        void LoadINISettings(minIni* ini, const std::string &section);
        void SaveINISettings(minIni* ini);
        void SaveINISettings(minIni* ini, const std::string &section);

        // enable/disable specific motors
        int SetTorqueEnable(int id, int enable);
        int SetTorqueEnable(std::list<int> &motorIds, bool enable);
        int SetTorqueEnable(std::list<int> &motorIds, std::list<bool> &enable); // ID/bool pairs to turn some motors on, others off
        bool GetTorqueEnable(int id);


		// SBD
		// Sync the current thread with other threads, including the robot's motion
		static void Sync();

		// SBD
        int SetEyeColour(int red, int green, int blue);

		// CIB
        int SetForeheadColour(int red, int green, int blue);

        // CIB
        // wait in a busy loop until a button is pressed
        static void WaitButton(int button, void (*function)(void)=NULL) /*__attribute__((deprecated("use Application class")))*/;

		// SBD
		// Get the next button pressed
		// Useful when listening for multiple buttons
        static int GetButton() /*__attribute__((deprecated("use Application class")))*/;
	};
}

#endif
