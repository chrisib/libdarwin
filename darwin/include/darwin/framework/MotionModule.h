/*
 *   MotionModule.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _MOTION_MODULE_H_
#define _MOTION_MODULE_H_

#include "darwin/framework/JointData.h"
#include <unistd.h>

namespace Robot
{
	class MotionModule
	{
	private:

	protected:

	public:
		JointData m_Joint;

		static const int TIME_UNIT = 8; //msec

		virtual void Initialize() = 0;
		virtual void Process() = 0;
        
        // CIB
        virtual bool IsRunning() {return false;}
        virtual void Stop(){}
        
        virtual void SetEnable(bool enable, bool exclusive = false) = 0;
        
        // SBD
        virtual void Finish( void (*func)(void) = 0 ) {
			while(IsRunning()) {
				if(func != NULL) {
					func();
				}
				usleep(TIME_UNIT * 1000);
			}
		}
	};
}

#endif
