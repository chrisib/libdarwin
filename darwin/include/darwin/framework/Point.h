/*
 *   Point.h
 *
 *   Author: ROBOTIS
 *
 *   Edited by Simon Barber-Dueck, making all references const 
 */

#ifndef _POINT_H_
#define _POINT_H_


namespace Robot
{	
    class Point3D; // forward definition
	class Point2D
	{
	private:

	protected:

	public:
		double X;
		double Y;

		Point2D();
		Point2D(double x, double y);
		Point2D(const Point2D &point);		
        ~Point2D();

		static double Distance(const Point2D &pt1, const Point2D &pt2);

		Point2D & operator = (const Point2D &point);
		Point2D & operator += (const Point2D &point);
		Point2D & operator -= (const Point2D &point);
		Point2D & operator += (double value);
		Point2D & operator -= (double value);
		Point2D & operator *= (double value);
		Point2D & operator /= (double value);
		Point2D operator + (const Point2D &point) const;
		Point2D operator - (const Point2D &point) const;
		Point2D operator + (double value) const;
		Point2D operator - (double value) const;
		Point2D operator * (double value) const;
		Point2D operator / (double value) const;

        bool operator == (const Point2D &point) const;

        double Dot(const Point2D &point);
        Point3D Cross(Point2D &point) const;
		
		// SBD
        double Magnitude() const;

        // determine if 3 ordered points are colinear, or form a clockwise/anti-clockwise loop
        enum
        {
            ORIENTATION_COLINEAR = 0,
            ORIENTATION_CLOCKWISE,
            ORIENTATION_ANTICLOCKWISE
        };
        static int Orientation(Point2D &p1, Point2D &p2, Point2D &p3);
	};

	class Point3D
	{
	private:

	protected:

	public:
		double X;
		double Y;
		double Z;
		
		Point3D();
		Point3D(double x, double y, double z);
		Point3D(const Point3D &point);
        ~Point3D();

		static double Distance(const Point3D &pt1, const Point3D &pt2);

		Point3D & operator = (const Point3D &point);
		Point3D & operator += (const Point3D &point);
		Point3D & operator -= (const Point3D &point);
		Point3D & operator += (double value);
		Point3D & operator -= (double value);
		Point3D & operator *= (double value);
		Point3D & operator /= (double value);
		Point3D operator + (const Point3D &point) const;
		Point3D operator - (const Point3D &point) const;
		Point3D operator + (double value) const;
		Point3D operator - (double value) const;
		Point3D operator * (double value) const;
		Point3D operator / (double value) const;

        bool operator == (const Point3D &point) const;

        double Dot(const Point3D &point);
        Point3D Cross(Point3D &point) const;

        Point2D XY() {return Point2D(X,Y);}
        Point2D XZ() {return Point2D(X,Z);}
        Point2D YZ() {return Point2D(Y,Z);}
		
		// SBD
		double Magnitude() const;
    };
}

#endif
