/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is used to locate a multiple uniformly-coloured targets
 * TODO: revise the image-processing algorithm to use the scan-line algorithm
 * instead of the crude methods used currently
 ************************************************************************/

#ifndef MULTIBLOB_H
#define MULTIBLOB_H

#include "darwin/framework/BlobTarget.h"
#include <string>
#include <iostream>
#include <vector>
#include <darwin/linux/CvCamera.h>

namespace Robot
{
    // a target that can appear multiple times in a scene
    // only targets that satisfy the height/width/top/left thresholds are recorded
    // targets that are the wrong dimensions or in the wrong position are skipped
    // by default the size and position thresholds are set to be the entire frame size, meaning ALL
    // objects will be recorded
    class MultiBlob: public BlobTarget
    {
        public:
            MultiBlob();
            explicit MultiBlob(const char* name);
            ~MultiBlob();

            // draw the targets' bounding boxes on the provided image
            virtual void DrawBoundingBoxes(cv::Mat &image) __attribute__((deprecated("Use Draw(cv::Mat) instead"))) {Draw(image);}
            virtual void Draw(cv::Mat &image);

            // print the name, current positions, and pixel sizes of the target in the last frame examined
            virtual void Print();

            // locate this target on the image;
            // returns the number of items found in the scene
            virtual int FindInFrame(cv::Mat &image, cv::Mat *dest);
            virtual int FindInFrame(CvCamera *camera, cv::Mat *dest);

            virtual void ProcessCandidates(std::vector<BoundingBox*> &candidates);

            std::vector<BoundingBox*> *GetBoundingBoxes(){return &allTargets;}

            virtual void LoadIniSettings(minIni &ini, const std::string &section);

        protected:

            // the target's pixel dimensions and position
            std::vector<BoundingBox*> allTargets;

            // filter out intersecting and/or nested candidates
            bool filterIntersections;
            bool filterNested;

            void Init(const char* name);
            void ClearAllTargets();

            int GetMaxVotes();

    };
}
#endif // MULTITARGET_H
