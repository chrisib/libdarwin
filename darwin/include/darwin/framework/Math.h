/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Simon Barber-Dueck 
 *
 * This file provides general math functions that could be useful
 * in more than one place
 ************************************************************************/

#include <cmath>

#ifndef PI
#define PI 3.14159265
#endif

#define deg2rad(x) ((x)*PI/180.0)
#define rad2deg(x) ((x)*180.0/PI)

namespace Math {
	// CIB
	// general-purpose kinematics functions that had nowhere better to go

	// calculate the missing length of a triangle with two known sides and an angle between them
	double GetMissingLength(double lenA, double lenB, double degrees);

	// calculate the missing angle in a triangle with all sides known
    // returns a value in degrees
	double GetInternalAngle(double lenA, double lenB, double opposingLength);

    // normalize an angle to lie in the [-180 180] range
    double NormalizeAngle(double degrees);

    // round a double to a specific number of decimal places
    double Truncate(double n, int decimalPlaces);
}
