/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class allows is a generic superclass for a Darwin leg; it will
 * eventually be used to contain inverse-kinematics
 ************************************************************************/

#ifndef LEG_H
#define LEG_H

#include "darwin/framework/MotionModule.h"
#include "darwin/framework/Point.h"
#include "darwin/framework/Limb.h"
#include "darwin/framework/minIni.h"
#include "darwin/framework/Matrix.h"

namespace Robot
{
    class Leg : public Limb
    {
    public:
        // get the 3D coordinates of the foot relative to the hip (in mm)
        virtual Point3D GetPosition() = 0;
        virtual Point3D GetPosition(JointData &position) = 0;
        virtual Point3D GetAbsolutePosition();
        virtual Point3D GetAbsolutePosition(JointData &position);

        // get 3D coordinates relative to the hip in mm
        // positive Y is AWAY from the body's midline
        virtual Point3D GetInsideToePosition();
        virtual Point3D GetOutsideToePosition();
        virtual Point3D GetInsideHeelPosition();
        virtual Point3D GetOutsideHeelPosition();
        virtual Point3D GetInsideToePosition(Matrix3D &endEffector);
        virtual Point3D GetOutsideToePosition(Matrix3D &endEffector);
        virtual Point3D GetInsideHeelPosition(Matrix3D &endEffector);
        virtual Point3D GetOutsideHeelPosition(Matrix3D &endEffector);

        virtual double GetToeAngle() = 0;   // get the yaw angle of this leg
        virtual double GetKneeAngle() = 0;	// get the knee angle for this leg
        virtual double GetHipPitch() = 0;
        virtual double GetHipRoll() = 0;
        virtual double GetAnkleRoll() = 0;
        virtual double GetAnklePitch() = 0;

        virtual int GetKneeTorque() = 0;
        virtual int GetAnkleRollTorque() = 0;
        virtual int GetAnklePitchTorque() = 0;
        virtual int GetXFSR() = 0;
        virtual int GetYFSR() = 0;

        // do the forward-kinematics calculations to calculate the foot's current position based on all joint angles
        virtual Point3D GetPosition(double hipYaw, double hipRoll, double hipPitch, double knee, double ankleRoll, double anklePitch);

        // set the 3D position of the foot relative to the hip (in mm)
        // toeAngle corresponds to the hip yaw angle (since that is the only motor tha can move the toe in/out)
        // coordinate system is the same as walking:
        //  +X = forwards
        //  +Y = left
        //  +Z = above hip level
        // toeAngle is hip yaw angle (+ to left, - to right)
        virtual void SetGoalPosition(Point3D &endEffectorPosition);
        virtual void SetGoalPosition(Point3D &endEffectorPosition, double toeAngle);
        //virtual void SetGoalPosition(Point3D &endEffectorPosition, double toeAngle, MoveFunc moveFunc);

        // set the goal position abstractly by setting a desired length and then setting the pitch, roll, and yaw angles
        // length is measured from hip to ankle (i.e. hip & ankle drop are NOT included)
        //virtual void SetGoalPosition(double length, double pitch, double roll, double yaw, MoveFunc moveFunc);
        virtual void SetGoalPosition(double length, double pitch, double roll, double yaw=0.0);

        virtual void SetEnable(bool enable, bool exclusive) = 0;

        // set the leg angles manually
        // angles are body angles, not h/w angles (in degrees):
        // yaw: 0: neutral, +: outward, -: inward
        // roll: 0: neutral, +: outward, -: inward
        // pitch 0: neutral, +: forward, -: backward
        virtual void SetAngles(double hipYaw, double hipRoll, double hipPitch, double knee, double ankleRoll, double anklePitch);
        //virtual void SetAngles(double hipYaw, double hipRoll, double hipPitch, double knee, double ankleRoll, double anklePitch, MoveFunc function);

        // optional offsets to adjust final positions
        // NOTE: these offsets are applied AFTER angles are set using the SetAngles or SetGoalPosition functions
        // as such they may introduce additional error into the results of any IK calculations
        double HIP_PITCH_OFFSET;
        double HIP_ROLL_OFFSET;
        double HIP_YAW_OFFSET;
        double KNEE_OFFSET;
        double ANKLE_ROLL_OFFSET;
        double ANKLE_PITCH_OFFSET;

        virtual bool IsRunning();

        // extend the leg until either it is fully extended OR the FSR registers pressure
        virtual void Extend(double speed=0.1);
        virtual void Extend(double speed, bool stopOnFSR);

        // used to determine if we need to stop extending the leg
        virtual bool FootOnGround();

        // indices for jointPositions
        enum{
            JOINT_HIP_YAW = 0,
            JOINT_HIP_ROLL,
            JOINT_HIP_PITCH,
            JOINT_KNEE,
            JOINT_ANKLE_PITCH,
            JOINT_ANKLE_ROLL,
            JOINT_FOOT,
            NUM_JOINTS
        };

        virtual void LoadIniSettings(minIni *ini, const char *section);

    protected:
        Leg(bool invertedYAxis);

        double targetHipYawAngle;
        double targetHipRollAngle;
        double targetHipPitchAngle;
        double targetKneeAngle;
        double targetAnkleRollAngle;
        double targetAnklePitchAngle;
        bool stopExtendingWhenFootOnGround;
        bool stopExtendingOnKneeTorque;
        int MAX_KNEE_TORQUE;

        // length of the fully-extended leg without the ankle and hip drop offsets
        static const double MAX_LEG_LENGTH;

        // fully retracted leg without the ankle and hip drops
        static const double MIN_LEG_LENGTH;

        virtual void ProcessExtending();

    private:
        double CalculateKneeAngle(double adjustableLegLength);
    };

}
#endif // LEG_H
