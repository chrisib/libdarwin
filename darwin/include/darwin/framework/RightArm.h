#ifndef RIGHTARM_H
#define RIGHTARM_H

#include "darwin/framework/Arm.h"

namespace Robot
{
    class RightArm : public Arm
    {
       public:
            static RightArm* GetInstance(){return _uniqueInstance;}

            RightArm();

            // get the 3D coordinates of the end-effector relative to the robot's shoulder (in mm)
            virtual Point3D GetPosition();
            virtual Point3D GetPosition(JointData &position);

            // get the arm's current angles
            virtual double GetShoulderRoll();
            virtual double GetShoulderPitch();
            virtual double GetElbowAngle();
            virtual double GetHandAngle();

			// Set the angles of the arm directly
            virtual void SetAngles(double shoulderPitch, double shoulderRoll, double elbow);
            //virtual void SetAngles(double shoulderPitch, double shoulderRoll, double elbow, MoveFunc function);
            virtual void SetAngles(JointData &joints);

            // pull the limb inwards such that the torque on all motors is not greater than the provided force
            virtual void Pull(int force);

            // push the limb outwards such that the torque on all motors is not greater than the provided force
            virtual void Push(int force);

            // required by MotionModule
            virtual void Process();

            // open & close the hand
            virtual bool IsHandOpen();
            virtual int ReadHandTorque();
            
            virtual void SetEnable(bool enable, bool exclusive);
            virtual void SetEnableHand(bool enable, bool exclusive);

        private:
            static RightArm *_uniqueInstance;

            //virtual Point3D *GetPosition(double elbowAngle, double shoulderRollAngle, double shoulderPitchAngle);

        protected:

    };

}
#endif // RIGHTARM_H
