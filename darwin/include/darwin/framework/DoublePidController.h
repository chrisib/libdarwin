/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is a PID controller used to control the value of a double
 * Note: setValue is the value to be adjusted by the controller
 * targetValue is the value setValue is being adjusted towards
 * kP, kI, kD are the standard PID controller coefficients.
 ************************************************************************/

#ifndef DOUBLEPIDCONTROLLER_H
#define DOUBLEPIDCONTROLLER_H

#include "darwin/framework/PidController.h"

namespace Robot
{
    /*!
     * \brief A PID controller that adjusts a floating-point variable from its current value toward some goal value
     */
    class DoublePidController : public PidController
    {
        public:
            DoublePidController(double *setValue, double *targetValue, double kP, double kI, double kD);
            virtual ~DoublePidController();

            virtual void Update();
            virtual double MoveFunction(double currentPosition, double targetPosition);

        private:
            double *targetValue;
            double *setValue;

            double previousErrors[MAX_INTEGRAL_ERRORS];    // circular queue that overwrites the oldest data
            int lastErrorIndex;                         // circular queue index
            double lastError;                              // quick reference to the most recent proportional error
    };
}

#endif // DOUBLEPIDCONTROLLER_H
