#ifndef ABSTRACTTARGET_H_INCLUDED
#define ABSTRACTTARGET_H_INCLUDED

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "darwin/framework/minIni.h"
#include "darwin/framework/ThresholdRange.h"
#include "darwin/framework/Camera.h"
#include "darwin/framework/BoundingBox.h"
#include <vector>
#include <list>

namespace Robot
{
    // Generic top-level Target class
    // This class represents any visual target that can be found in the field of view
    // In general it should be used for uniformly-coloured objects
    class Target
    {
        public:
            Target();
            virtual ~Target();

            // load target properties from an Ini file
            virtual void LoadIniSettings(minIni &ini, const std::string &section);

            // print out the target's properties
            virtual void Print() = 0;

            // locate this target on the image;
            virtual int FindInFrame(cv::Mat &image, cv::Mat *dest=NULL) = 0;

            // given a binary image where this target's candidate colours are in white, where is the object?
            // when inherited, this function *should* do whatever analysis is appropriate to filter out false positives & negatives
            virtual void ProcessCandidateImage(cv::Mat &binaryImages) = 0;

            // draw a box around the target's non-ignored range
            virtual void DrawScanRange(cv::Mat& image);

            // draw the target onto an image
            virtual void Draw(cv::Mat &canvas) = 0;

            std::string *GetName(){return this->name;}

            // YUV parameters that define the colour of this object
            void SetYRange(int min, int max) {this->yRange.min = min; this->yRange.max = max;}
            void SetURange(int min, int max) {this->uRange.min = min; this->uRange.max = max;}
            void SetVRange(int min, int max) {this->vRange.min = min; this->vRange.max = max;}

            ThresholdRange *GetYRange(){return &(this->yRange);}
            ThresholdRange *GetURange(){return &(this->uRange);}
            ThresholdRange *GetVRange(){return &(this->vRange);}

            // the colour used to mark this object on the frame
            void SetMarkColour(int r, int g, int b) {this->markColour = cv::Scalar(b, g, r);}
            cv::Scalar *GetMarkColour(){return &(this->markColour);}

            // used to set what area of the frame we can ignore when looking for this object
            void SetIgnoreTop(int x){this->ignoreTop = (x >= 0 ? x : 0);}
            void SetIgnoreLeft(int x){this->ignoreLeft = (x >= 0 ? x : 0);}
            void SetIgnoreBottom(int x){this->ignoreBottom = (x >= 0 ? x : 0);}
            void SetIgnoreRight(int x){this->ignoreRight = (x >= 0 ? x : 0);}
            int GetIgnoreTop(){return this->ignoreTop;}
            int GetIgnoreLeft(){return this->ignoreLeft;}
            int GetIgnoreBottom(){return this->ignoreBottom;}
            int GetIgnoreRight(){return this->ignoreRight;}

        protected:
            // the target's name
            std::string *name;

            // number of pixels around each edge of the frame where we can ignore results (since they will be noise)
            int ignoreTop;
            int ignoreLeft;
            int ignoreBottom;
            int ignoreRight;

            // max/min yuv parameters
            ThresholdRange yRange;
            ThresholdRange uRange;
            ThresholdRange vRange;

            // colour used to mark the target's bounding box on the image
            cv::Scalar markColour;


        // Static image processing functions
        public:
            // return true if the provided YUV values are within the right range
            static bool IsTargetPixel(uint8_t y, uint8_t u, uint8_t v, ThresholdRange &yRange, ThresholdRange &uRange, ThresholdRange &vRange);

            // create a new binary image that represents the scene
            static cv::Mat *Yuv2Binary(cv::Mat &src, ThresholdRange &yRange, ThresholdRange &uRange, ThresholdRange &vRange);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // SIMPLE THRESHOLDING ALGORITHMS
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // use standard OpenCV blob detection & hough transformations to find objects in the frame
            // uses same thresholds as scanline, but does not actively look for edges; just does simple thresholding
            // to produce a binary image that is processed
            static void FastFindTargets(std::vector<Robot::Target*> &targets, cv::Mat &src, cv::Mat *dest=NULL);

            // copy a single channel into a new image
            // the new image is an 8-bit, 1-channel image
            static cv::Mat *ExtractChannel(cv::Mat &src, int channel);

            // create a new Mat with the following channels:
            //  y-u
            //  y-v
            //  u-v
            // new channels are normalized to be in the [0,255] range
            static cv::Mat *CreateCompliment(cv::Mat &src);

            // convert row/column values to a one-dimensional array index (and vice-versa) for finding pixels in an image
            static /*inline*/ int RowCol2Index(int row, int col, cv::Mat &mat);
            static /*inline*/ void Index2RowCol(int index, int *row, int *col, cv::Mat &mat);

        private:
            static void FloodFill(int row, int col, int avgY, int avgU, int avgV, uint8_t *yuvData, int avgR, int avgG, int avgB, uint8_t *rgbData, int avgRG, int avgRB, int avgGB, uint8_t *rgbcData, int threshold, cv::Mat &src, uint8_t *destData, BoundingBox *result, int minRow, int maxRow, int minCol, int maxCol, int subsample);
            static void ProcessPixel(int row, int col,std::list<int> &stack, uint8_t *yuvData, int avgY, int avgU, int avgV, uint8_t *rgbData, int avgR, int avgG, int avgB, uint8_t *rgbcData, int avgRG, int avgRB, int avgGB, int threshold, uint8_t *destData, cv::Mat &src);

    };
}

#endif // ABSTRACTTARGET_H_INCLUDED
