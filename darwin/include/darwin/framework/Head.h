/*
 *   Head.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _HEAD_H_
#define _HEAD_H_

#include <string.h>

#include "darwin/framework/minIni.h"
#include "darwin/framework/MotionModule.h"
#include "darwin/framework/Point.h"
#include "darwin/framework/Limb.h"



#define HEAD_SECTION    "Head Pan/Tilt"
#define INVALID_VALUE   -1024.0

namespace Robot
{
    //forward declarations
    class Target;
    class SingleBlob;

    /*!
     * \brief A class that controls the head as if it were a limb of the robot.
     *
     * Can do basing IK and FK calculations to control the camera position in 3D space relative to the torso
     *
     * This is a pseudo-singleton class; we provide a singleton instance that should be used with the MotionManager,
     * but we also have a public constructor that can be used to create utility instances for motion-planning.
     */
    class Head : public Limb
	{
    private:
		static Head* m_UniqueInstance;
		double m_LeftLimit;
		double m_RightLimit;
		double m_TopLimit;
		double m_BottomLimit;
		double m_Pan_Home;
		double m_Tilt_Home;
		double m_Pan_err;
		double m_Pan_err_diff;
		double m_Pan_p_gain;
		double m_Pan_d_gain;
		double m_Tilt_err;
		double m_Tilt_err_diff;
		double m_Tilt_p_gain;
		double m_Tilt_d_gain;
		double m_PanAngle;
		double m_TiltAngle;
		bool m_moving;	// SBD

		void CheckLimit();

	public:
        /*!
         * \brief Get the unique instance of Head that should be used by the MotionManager
         * \return
         */
		static Head* GetInstance() { return m_UniqueInstance; }

        Head();
		virtual ~Head();

        /*!
         * \brief Initialize the module
         */
		void Initialize();

        /*!
         * \brief Calculate the new goal positions for the head motor; intended to be called by the MotionManager
         * as part of the pose-calculations
         */
		void Process();

        /*!
         * \brief Get the maximum upward angle in degrees the head can support
         * \return
         */
		double GetTopLimitAngle()		{ return m_TopLimit; }

        /*!
         * \brief Get the minimum downward angle in degrees the head can support
         * \return
         */
		double GetBottomLimitAngle()	{ return m_BottomLimit; }

        /*!
         * \brief Get the maximum rightward angle in degrees the head can support
         * \return
         */
		double GetRightLimitAngle()		{ return m_RightLimit; }

        /*!
         * \brief Get the maximum leftward angle in degrees the head can support
         * \return
         */
		double GetLeftLimitAngle()		{ return m_LeftLimit; }

        /*!
         * \brief Get the current pan angle of the head in degrees
         * \return
         */
		double GetPanAngle()		{ return m_PanAngle; }

        /*!
         * \brief Get the current tilt angle of the head in degrees
         * \return
         */
		double GetTiltAngle()		{ return m_TiltAngle; }

        /*!
         * \brief Is the head currently moving?
         * \return
         */
		bool IsRunning()	{ return m_moving; }

        /*!
         * \brief Give the head module control over the neck motors
         * \param enable If true, the head module is given control over the motors
         * \param exclusive If true, the head module is given exclusive control over the motors
         */
		virtual void SetEnable(bool enable, bool exclusive);

        /*!
         * \brief Move the head module to the home position
         */
		void MoveToHome();

        /*!
         * \brief Move the head to a given pan/tilt angle
         * \param pan The desired pan angle in degrees
         * \param tilt The desired tilt angle in degrees
         */
		void MoveByAngle(double pan, double tilt);

        /*!
         * \brief Move the head from its current position by the specified amounts
         * \param pan The amount to change the pan angle
         * \param tilt The amount to change the tilt angle
         */
		void MoveByAngleOffset(double pan, double tilt);

        /*!
         * \brief Initialize the head-tracking
         */
		void InitTracking();

        void MoveTracking(Point2D err); // For image processing
		void MoveTrackingWithDamping(double dampingFactor);
		void MoveTrackingWithDamping(Point2D err, double dampingFactor);
		void MoveTracking();
		void MoveTo(Point2D& target);

        /*!
         * \brief Move the head to look at a target
         * \param target The target to look at
         * \param dampingFactor A damping factor to control how quickly the head moves
         */
        void LookAt(SingleBlob& target, double dampingFactor = 1);

        /*!
         * \brief Experimental, can likely be removed
         * \param target
         */
        void LookAt2(SingleBlob& target);

        /*!
         * \brief Experimental, can likely be removed
         * \param target
         * \param speed
         */
        void LookAt3(SingleBlob& target, int speed);

        void LoadINISettings(minIni* ini);
        void LoadINISettings(minIni* ini, const std::string &section);
        void SaveINISettings(minIni* ini);
        void SaveINISettings(minIni* ini, const std::string &section);

        /*!
         * \brief Get the position of the camera in 3D space relative to the base of the neck
         * \return
         */
        virtual Point3D GetPosition();

        /*!
         * \brief Get the position of the camera in 3D space relative to the base of the neck using the provided joint positions
         * \return
         */
        virtual Point3D GetPosition(JointData &position);

        /*!
         * \brief Get the position of the camera in 3D space relative to the robot's origin
         * \return
         */
        virtual Point3D GetAbsolutePosition();

        /*!
         * \brief Get the position of the camera in 3D space relative to the robot's origin using the provided joint positions
         * \return
         */
        virtual Point3D GetAbsolutePosition(JointData &position);

        /*!
         * \brief Get the position of the camera in 3D space relative to the base of the robot's neck using the provided pan and tilt angles
         * \param pan
         * \param tilt
         * \return
         */
        virtual Point3D GetPosition(double pan, double tilt);

        /*!
         * \brief Set the goal positions for the neck motors based on the provided joint data
         * \param joints
         */
        virtual void SetAngles(JointData &joints);

        /*!
         * \brief Set the goal position of the camera relative to the base of the neck
         * \param position
         */
        virtual void SetGoalPosition(Point3D &position);

        /*!
         * \brief Inherited from Limb, but does nothing
         *
         * If we had a telescoping neck that would be cool, but kind of breaks the "humanoid" form factor
         * \param speed Ignored
         */
        virtual void Extend(double speed=0.1);

        enum{
            JOINT_NECK_PAN = 0,
            JOINT_NECK_TILT,
            JOINT_FACE,
            NUM_JOINTS
        };
	};
}
#endif
