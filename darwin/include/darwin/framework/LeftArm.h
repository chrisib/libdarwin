/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class allows inverse kinematic control of the robot's left arm
 ************************************************************************/
#ifndef LEFT_ARM_H
#define LEFT_ARM_H

#include "darwin/framework/Arm.h"

namespace Robot
{

    class LeftArm : public Arm
    {
        public:
            static LeftArm* GetInstance(){return _uniqueInstance;}

            LeftArm();

            // get the 3D coordinates of the end-effector relative to the robot's shoulder (in mm)
            virtual Point3D GetPosition();
            virtual Point3D GetPosition(JointData &position);

            // get the arm's current angles
            virtual double GetShoulderRoll();
            virtual double GetShoulderPitch();
            virtual double GetElbowAngle();
            virtual double GetHandAngle();

			// Set the angles of the arm directly
            virtual void SetAngles(double shoulderPitch, double shoulderRoll, double elbow);
            //virtual void SetAngles(double shoulderPitch, double shoulderRoll, double elbow, MoveFunc function);
            virtual void SetAngles(JointData &joints);

            // pull the limb inwards such that the torque on all motors is not greater than the provided force
            virtual void Pull(int force);

            // push the limb outwards such that the torque on all motors is not greater than the provided force
            virtual void Push(int force);

            // required by MotionModule
            virtual void Process();

            // open & close the hand
            virtual bool IsHandOpen();
            virtual int ReadHandTorque();
            
            virtual void SetEnable(bool enable, bool exclusive);
            virtual void SetEnableHand(bool enable, bool exclusive);

        private:
            static LeftArm *_uniqueInstance;

            //virtual Point3D *GetPosition(double elbowAngle, double shoulderRollAngle, double shoulderPitchAngle);

        protected:
    };
}
#endif
