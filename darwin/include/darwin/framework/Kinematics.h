/*
 *   Kinematics.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _KINEMATICS_H_
#define _KINEMATICS_H_

#include "darwin/framework/Matrix.h"
#include "darwin/framework/JointData.h"
#include "darwin/framework/Math.h"

// SBD: degree-radian macros moved to Math.h

namespace Robot
{
    /*!
     * \brief Contains measurements for the robot's physical form
     *
     * All measurements are in mm or degrees
     *
     * \image html darwin-dimensions.jpg
     *
     * This is a singleton class, but currently does not contain any instance functionality.
     */
	class Kinematics
	{
	private:
		static Kinematics* m_UniqueInstance;
        Kinematics();

	protected:

	public:
        /*!
         * \brief The horizontal distance from the pitch motor axis to the camera lense
         */
        static const double CAMERA_DISTANCE = 33.2;         //mm

        /*!
         * \brief The downward offset angle of the camera relative to the head
         */
        static const double EYE_TILT_OFFSET_ANGLE = 40.0;   //degree

        /*!
         * \brief The horizontal distance from the robot's midline to the centre of the leg
         */
        static const double LEG_SIDE_OFFSET = 37.0;         //mm

        /*!
         * \brief The length of the robot's upper leg
         */
        static const double THIGH_LENGTH = 93.0;            //mm

        /*!
         * \brief The length of the robot's lower leg
         */
        static const double CALF_LENGTH = 93.0;             //mm

        /*!
         * \brief The distance from the end of the lower leg to the robot's foot
         */
        static const double ANKLE_LENGTH = 33.5;            //mm

        /*!
         * \brief The total length of the leg from the top of the hip to the bottom of the foot
         */
        static const double LEG_LENGTH = 219.5;             //mm (THIGH_LENGTH + CALF_LENGTH + ANKLE_LENGTH)

        // Below here added by UofM AA Lab for arm inverse kinematics

        /*!
         * \brief The length of the robot's foot
         */
        static const double FOOT_LENGTH = 104.0;                    //mm

        /*!
         * \brief The width of the robot's foot
         */
        static const double FOOT_WIDTH = 66.0;                      //mm

        /*!
         * \brief The distance from the centre of the ankle motor to the outside edge of the robot's foot
         */
        static const double ANKLE_TO_OUTSTEP = 43.5;                //mm

        /*!
         * \brief The horizontal length of the robot's shoulder bracket
         */
		static const double SHOULDER_SIDE_OFFSET = (82.0 - 57.5);   //mm

        /*!
         * \brief The vertical offset of the robot's shoulder bracket
         */
		static const double SHOULDER_BOTTOM_OFFSET = 16.0;          //mm

        /*!
         * \brief The forward offset of the robot's elbow motor
         */
        static const double ELBOW_OFFSET = 16.0;                    //mm

        /*!
         * \brief The length of the robot's upper arm
         */
		static const double UPPER_ARM_LENGTH = 60.0;                //mm

        /*!
         * \brief The length of the robot's lower arm
         *
         * Standard arm; the gripper arm is approximately the same length to the end of the fingers
         */
		static const double LOWER_ARM_LENGTH = 129.0;               //mm

        /*!
         * \brief Horizontal distance from the robot's midline to the shoulder bracket
         */
		static const double HEAD_TO_SHOULDER = 82.0;				//mm (y-axis)

        /*!
         * \brief Vertical distance from the camera to the shoulder bracket
         */
		static const double EYE_TO_SHOULDER = 85.0;					//mm (z-axis)

        /*!
         * \brief Distance from the shoulder to the hip
         */
        static const double TORSO_LENGTH = 122.2;                   //mm (hip to shoulder)

        /*!
         * \brief Length of the robot's neck
         */
        static const double NECK_LENGTH = 50.5;                     //mm (shoulder to neck)

        /*!
         * \brief Veritcal distance from the head pitch motor to the camera
         */
        static const double NECK_TO_FACE = 34.5;                    //mm (neck to camera)

        /*!
         * \brief Vertical distance from the bottom of the torso to the hip pitch motor
         */
        static const double HIP_DROP_LENGTH = 30.0;                 //mm (torso to hip pitch motor)

        /*!
         * \brief Horizontal distance between the robot's legs
         */
        static const double HIP_WIDTH = 74.0;                       //mm (separation between hip motors)

        /*!
         * \brief Distance from the centre of the leg to the front of the foot
         */
        static const double ANKLE_TO_TOE = 52.0;                    //mm (ankle bracket ctr to toe)

        /*!
         * \brief Horizontal distance from the line of the legs to the shoulder
         *
         * The shoulders' origin lies slighting in front of the legs
         */
        static const double SHOULDER_FRONT_OFFSET = 5.0;            //mm (shoulders are 5mm in front of the hips)

        /*!
         * \brief Horizontal distance between the shoulder pitch motors
         */
        static const double SHOULDER_WIDTH = 2*57.5;                //mm (separation between the shoulder pitch motors)

        /*!
         * \brief Offset angle for the elbows
         *
         * The elbow hardware is installed such that a zero h/w angle results in a 90-degree bend in the elbows
         */
        static const double ELBOW_ANGLE_OFFSET = 90.0;      // degrees

        /*!
         * \brief Offset angle for the shoulder roll
         *
         * The shoulder hardware is installed such that a zero h/w angle results in the arms sticking out 45-degrees from the sides
         */
		static const double SHOULDER_ROLL_OFFSET = 45.0;    //degrees

		~Kinematics();

		static Kinematics* GetInstance()			{ return m_UniqueInstance; }
	};
}

#endif
