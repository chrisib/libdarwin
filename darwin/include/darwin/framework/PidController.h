/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is a generic PID controller superclass.  It provides no
 * useful control mechanisms, but should be inherited to allow control
 * of a specific variable (e.g. DoublePidController, IntPidController)
 ************************************************************************/


#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

#include <pthread.h>

#define MAX_INTEGRAL_ERRORS     10

namespace Robot
{
    /*!
     * \brief A generic PID controller that adjusts one variable toward the value stored in another.
     *
     * PID controllers use 3 gains -- kP, kI, and kD -- to perform the following calculations:
     * - \f$ y(t) \f$ is the process variable being adjusted toward \f$ r(t) \f$.
     * - \f$ u(t )\f$ is the output value when adjusting \f$ y(t) \f$ toward \f$ r(t) \f$
     * - \f$ e(t) = r(t) - y(t) \f$ is the error in the system
     * \f[
     *  u(t) = k_P e(t) + k_I + \int_0^te(\tau)d\tau + k_D\frac{de(t)}{dt}
     * \f]
     *
     * This superclass is variable type agnostic and is inherited by IntPidController and DoublePidController
     * which manipulate integers and floating-point values respectively.
     *
     * The I parameter acts only on the previous 5 values, so this is not a true PID controller. However,
     * for most humanoid robotics applications the I parameter is frequently set to 0 anyway, so this is
     * not a huge loss.
     */
    class PidController
    {
        public:
            /*!
             * \brief Create a new PID controller with P, I, and D gains
             * \param kP The proportional gain
             * \param kI The integral gain
             * \param kD The derivative gain
             */
            PidController(double kP, double kI, double kD);

            /*!
             * \brief Destructor
             *
             * The PidController doesn't dynamically allocate any memory, so this does nothing
             */
            virtual ~PidController();

            /*!
             * \brief Apply the formula to adjust the process variable toward the goal value
             */
            virtual void Update() = 0;

            /*!
             * \brief Use the PID controller as a MoveFunc for any of the Limb subclasses
             *
             * This allows controlling the limbs directly via PID output.
             *
             * Note that the parameters to this function are ignored and used only for compatibility with MoveFunc.
             * The internal process and goal variables inside the PidController are used to calculate the current and goal
             * positions.
             * \param currentPosition Ignored
             * \param targetPosition Ignored
             * \return \f$ u(t) \f$
             */
            virtual double MoveFunction(double currentPosition, double targetPosition) = 0;

            /*!
             * \brief Change the proportional gain
             * \param kP
             */
            void SetKP(double kP){this->kP = kP;}

            /*!
             * \brief Change the integral gain
             * \param kP
             */
            void SetKI(double kI){this->kI = kI;}

            /*!
             * \brief Change the derivative gain
             * \param kP
             */
            void SetKD(double kD){this->kD = kD;}

            /*!
             * \brief Get the proportional gain
             * \return
             */
            double KP(){return kP;}

            /*!
             * \brief Get the integral gain
             * \return
             */
            double KI(){return kI;}

            /*!
             * \brief Get the derivative gain
             * \return
             */
            double KD(){return kD;}

            /*!
             * \brief If true, additional debugging information is printed to stdout every time Update() is called
             */
            bool DEBUG_PRINT;

        protected:
            /*!
             * \brief The proportional gain
             */
            double kP;

            /*!
             * \brief The integral gain
             */
            double kI;

            /*!
             * \brief The derivative gain
             */
            double kD;
    };
}
#endif // PIDCONTROLLER_H
