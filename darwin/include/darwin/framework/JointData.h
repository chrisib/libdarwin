/*
 *   JointData.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _JOINT_DATA_H_
#define _JOINT_DATA_H_

namespace Robot
{
    /*!
     * \brief Contains the current positions and enable/disable status of the robot's motors for use
     * by MotionModule classes
     */
	class JointData
	{
	public:
        /*!
         * \brief The hardware IDs of the motors for every joint
         */
		enum
		{
			ID_R_SHOULDER_PITCH     = 1,
			ID_L_SHOULDER_PITCH     = 2,
			ID_R_SHOULDER_ROLL      = 3,
			ID_L_SHOULDER_ROLL      = 4,
			ID_R_ELBOW              = 5,
			ID_L_ELBOW              = 6,
			ID_R_HIP_YAW            = 7,
			ID_L_HIP_YAW            = 8,
			ID_R_HIP_ROLL           = 9,
			ID_L_HIP_ROLL           = 10,
			ID_R_HIP_PITCH          = 11,
			ID_L_HIP_PITCH          = 12,
			ID_R_KNEE               = 13,
			ID_L_KNEE               = 14,
			ID_R_ANKLE_PITCH        = 15,
			ID_L_ANKLE_PITCH        = 16,
			ID_R_ANKLE_ROLL         = 17,
			ID_L_ANKLE_ROLL         = 18,
			ID_HEAD_PAN             = 19,
			ID_HEAD_TILT            = 20,
			// CIB Right/Left grippers
			ID_R_HAND               = 21,
            ID_L_HAND               = 22,
			NUMBER_OF_JOINTS
		};

        /*!
         * \brief No longer used (?)
         *
         * Older versions of the MX-28 and RX-28 firmware did not use PID control internally
         * and instead used a slope system to determine how quickly the motors moved
         */
		enum
		{
			SLOPE_HARD			= 16,
			SLOPE_DEFAULT		= 32,
			SLOPE_SOFT			= 64,
			SLOPE_EXTRASOFT		= 128
		};

        /*!
         * \brief Defaut P, I, and D gains for MX-28 series servos
         */
		enum
		{
		    P_GAIN_DEFAULT      = 32,
		    I_GAIN_DEFAULT      = 0,
		    D_GAIN_DEFAULT      = 0
		};


	private:

	protected:
		bool m_Enable[NUMBER_OF_JOINTS];
		int m_Value[NUMBER_OF_JOINTS];
		double m_Angle[NUMBER_OF_JOINTS];
		int m_CWSlope[NUMBER_OF_JOINTS];
		int m_CCWSlope[NUMBER_OF_JOINTS];
		int m_PGain[NUMBER_OF_JOINTS];
        int m_IGain[NUMBER_OF_JOINTS];
        int m_DGain[NUMBER_OF_JOINTS];

	public:
		JointData();
		~JointData();

        /*!
         * \brief Copy all values from one JointData object to another
         *
         * Copies the joint positions, enable flags, PID gains, etc...
         * \param that The object whose values we are copying
         */
        void operator =(JointData that); //copy all values from one JointData object to another

        /*!
         * \brief Set whether or not a motor is enabled
         * \param id The ID of the motor to toggle
         * \param enable If true, the MotionModule that owns this JointData can control the motor
         */
        void SetEnable(int id, bool enable);

        /*!
         * \brief Set whether or not a motor is enabled
         * \param id The ID of the motor to toggle
         * \param enable If true, the MotionModule that owns this JointData can control the motor
         * \param exclusive If true, all other MotionModules registered with the MotionManager will lose control over this motor
         */
        void SetEnable(int id, bool enable, bool exclusive);

        /*!
         * \brief Set control over the head's pan and tilt motors
         * \param enable
         */
		void SetEnableHeadOnly(bool enable);

        /*!
         * \brief Set control over the head's pan and tilt motors
         * \param enable
         * \param exclusive
         */
        void SetEnableHeadOnly(bool enable, bool exclusive);

        /*!
         * \brief Set control over the right shoulder pitch & roll, right elbow, and right hand motors
         * \param enable
         */
		void SetEnableRightArmOnly(bool enable);

        /*!
         * \brief Set control over the right shoulder pitch & roll, right elbow, and right hand motors
         * \param enable
         * \param exclusive
         */
        void SetEnableRightArmOnly(bool enable, bool exclusive);

        /*!
         * \brief Set control over the left hand
         * \param enable
         * \param exclusive
         */
        void SetEnableRightHandOnly(bool enable, bool exclusive = false);

        /*!
         * \brief Set control over the left shoulder pitch & roll, left elbow, and left hand motors
         * \param enable
         */
		void SetEnableLeftArmOnly(bool enable);

        /*!
         * \brief Set control over the left shoulder pitch & roll, left elbow, and left hand motors
         * \param enable
         * \param exclusive
         */
        void SetEnableLeftArmOnly(bool enable, bool exclusive);

        /*!
         * \brief Set control over the left hand
         * \param enable
         * \param exclusive
         */
        void SetEnableLeftHandOnly(bool enable, bool exclusive = false);

        /*!
         * \brief Set control over the right hip pitch, roll, and yaw; right knee; right ankle pitch & roll motors
         * \param enable
         */
		void SetEnableRightLegOnly(bool enable);

        /*!
         * \brief Set control over the right hip pitch, roll, and yaw; right knee; right ankle pitch & roll motors
         * \param enable
         * \param exclusive
         */
        void SetEnableRightLegOnly(bool enable, bool exclusive);

        /*!
         * \brief Set control over the left hip pitch, roll, and yaw; left knee; left ankle pitch & roll motors
         * \param enable
         */
		void SetEnableLeftLegOnly(bool enable);

        /*!
         * \brief Set control over the left hip pitch, roll, and yaw; left knee; left ankle pitch & roll motors
         * \param enable
         * \param exclusive
         */
        void SetEnableLeftLegOnly(bool enable, bool exclusive);

        /*!
         * \brief Set control over the arm and hand motors
         * \param enable
         */
		void SetEnableUpperBodyWithoutHead(bool enable);

        /*!
         * \brief Set control over the arm and hand motors
         * \param enable
         * \param exclusive
         */
        void SetEnableUpperBodyWithoutHead(bool enable, bool exclusive);

        /*!
         * \brief Set control over both legs' motors
         * \param enable
         */
		void SetEnableLowerBody(bool enable);

        /*!
         * \brief Set control over both legs' motors
         * \param enable
         * \param exclusive
         */
        void SetEnableLowerBody(bool enable, bool exclusive);

        /*!
         * \brief Set control over all motors except those in the neck
         * \param enable
         */
		void SetEnableBodyWithoutHead(bool enable);

        /*!
         * \brief Set control over all motors except those in the neck
         * \param enable
         * \param exclusive
         */
        void SetEnableBodyWithoutHead(bool enable, bool exclusive);

        /*!
         * \brief Set control over all motors
         * \param enable
         */
		void SetEnableBody(bool enable);

        /*!
         * \brief Set control over all motors
         * \param enable
         * \param exclusive
         */
        void SetEnableBody(bool enable, bool exclusive);

        /*!
         * \brief Get whether or not a given motor is enabled
         * \param id The ID of the motor
         * \return
         */
		bool GetEnable(int id);

        /*!
         * \brief Set the raw hardware value of a motor
         * \param id The ID of the motor
         * \param value A value in the range 0-4095 (assuming recent MX-28 firmware) to set the motor's angle
         */
		void SetValue(int id, int value);

        /*!
         * \brief Get the raw hardware value of the motor
         * \param id The ID of the motor
         * \return The motor's position encoder value (0-4095)
         */
		int GetValue(int id);

        /*!
         * \brief Set the raw angle of a motor in degrees
         * \param id The ID of the motor
         * \param angle The motor's raw angle in degrees
         */
		void SetAngle(int id, double angle);

        /*!
         * \brief Get the raw angle of the motor in degrees
         * \param id The ID of the motor
         * \return The motor's raw hardware angle in degrees
         */
		double GetAngle(int id);

        /*!
         * \brief Set the raw angle of the motor in radians
         * \param id The ID of the motor
         * \param radian The motor's raw hardware angle in radians
         */
		void SetRadian(int id, double radian);

        /*!
         * \brief Get the motor's raw hardware angle in radians
         * \param id The motor's ID
         * \return The motor's raw angle in radians
         */
		double GetRadian(int id);

        /* Get/Set joint angles (deg/rad) using knowledge of where the joints are on the body
        for all joints: zero = standard anatomical position (legs straight, head forward, arms straight at sides)
        non-zero varies by joint:
            head: + left, up; - right, down
            shoulder roll: + forward; - backward
            shoulder pitch: + away from body
            elbow: + normal bend
            hip yaw: + toe outward
            hip roll: + away from midline
            hip pitch: + forward
            knee: + normal bend
            ankle roll: + sole away from midline
            anlke pitch: + toe up
        */
        /*!
         * \brief Set the body angle of the motor in degrees
         *
         * Body angles use knowledge of where the joint is in the robot to use kinematically-meaningful angles that are symmetric on either side of the body
         *
         * For all joints and angle of 0 will put that joint in the standard anatomical position (legs straight, feet flat, arms straight at sides, hands loosely closed, head forward)
         *
         * For non-zero values, the meaning varies by joint:
         * - head: + left, up; - right, down
         * - shoulder pitch: + forward; - backward
         * - shoulder roll: + away from body; - toward body
         * - elbow: + normal bend; - reverse-bend (impossible unless the arms have been modified)
         * - hand: + open; - close
         * - hip yaw: + toe outward; - toe inward
         * - hip roll: + away from midline; - toward midline
         * - hip pitch: + forward; - backward
         * - knee: + normal bend; - reverse bend (impossible unless the legs have been modified)
         * - ankle roll: + sole away from midline; - sole toward midline
         * - ankle pitch: + toe up; - toe down
         * \param id
         * \param angle
         */
        void SetBodyAngle(int id, double angle);

        /*!
         * \brief Get the body angle of the motor in degrees
         *
         * \see SetBodyAngle
         * \param id The ID of the motor
         * \return The body angle of the motor in degrees
         */
        double GetBodyAngle(int id);

        /*!
         * \brief Set the body angle of the motor in radians
         *
         * \see SetBodyAngle
         * \param id The ID of the motor
         * \param radian The body angle of the motor in radians
         */
        void SetBodyRadians(int id, double radian);

        /*!
         * \brief Get the body angle of the motor in radians
         *
         * \see SetBodyAngle
         * \param id The ID of the motor
         * \return The body angle of the motor in radians
         */
        double GetBodyRadians(int id);

        /*!
         * \brief Set the clockwise and counterclockwise slopes of the motor
         *
         * No longer used (?)
         * \param id The ID of the motor
         * \param cwSlope The clockwise slope
         * \param ccwSlope The anti-clockwise slope
         */
		void SetSlope(int id, int cwSlope, int ccwSlope);

        /*!
         * \brief Set the clockwise slope
         *
         * No longer used (?)
         * \param id
         * \param cwSlope
         */
		void SetCWSlope(int id, int cwSlope);

        /*!
         * \brief Get the clockwise slope
         *
         * No longer used (?)
         * \param id
         * \return
         */
        int  GetCWSlope(int id);

        /*!
         * \brief Set the anti-clockwise slope
         *
         * No longer used (?)
         * \param id
         * \param ccwSlope
         */
		void SetCCWSlope(int id, int ccwSlope);

        /*!
         * \brief Get the anti-clockwise slope
         *
         * No longer used (?)
         * \param id
         * \return
         */
		int  GetCCWSlope(int id);

        /*!
         * \brief Set the P gain of the motor
         * \param id
         * \param pgain
         */
        void SetPGain(int id, int pgain) { m_PGain[id] = pgain; }

        /*!
         * \brief Get the P gain of the motor
         * \param id
         * \return
         */
        int  GetPGain(int id)            { return m_PGain[id]; }

        /*!
         * \brief Set the I gain of the motor
         * \param id
         * \param igain
         */
        void SetIGain(int id, int igain) { m_IGain[id] = igain; }

        /*!
         * \brief Get the I gain of the motor
         * \param id
         * \return
         */
        int  GetIGain(int id)            { return m_IGain[id]; }

        /*!
         * \brief Set the D gain of the motor
         * \param id
         * \param dgain
         */
        void SetDGain(int id, int dgain) { m_DGain[id] = dgain; }

        /*!
         * \brief Get the D gain of the motor
         * \param id
         * \return
         */
        int  GetDGain(int id)            { return m_DGain[id]; }
	};
}

#endif
