/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * Generic superclass to contain non-static Dynamixel-related functions
 * Inherited by AX12, MX28, RX28
 * Future possible inheritors: AXS1
 ************************************************************************/

#ifndef _DYNAMIXEL_H_
#define _DYNAMIXEL_H_

namespace Robot
{
    /*!
     * \brief Contains generic attributes common to any Dyanamixel peripheral that can be connected to the robot.
     *
     * Dynamixel peripherals include motors (e.g MX-28, AX-12) and sensor (e.g. AX-S1, FSR feet)
     */
    class Dynamixel
    {
        // TODO: any non-static members that need inheriting?
    };
}

#endif // _DYNAMIXEL_H_
