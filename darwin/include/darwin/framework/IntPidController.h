/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * This class is a PID controller used to control the value of an int
 * Note: setValue is the value to be adjusted by the controller
 * targetValue is the value setValue is being adjusted towards
 * kP, kI, kD are the standard PID controller coefficients.
 ************************************************************************/

#ifndef INTPIDCONTROLLER_H
#define INTPIDCONTROLLER_H

#include "darwin/framework/PidController.h"

namespace Robot
{
    /*!
     * \brief A PID controller that adjusts an integer variable from its current value toward some goal value
     */
    class IntPidController : public PidController
    {
        public:
            IntPidController(int *setValue, int *targetValue, double kP, double kI, double kD);
            virtual ~IntPidController();

            virtual void Update();
            virtual double MoveFunction(double currentPosition, double targetPosition);

        private:
            int *targetValue;
            int *setValue;

            int previousErrors[MAX_INTEGRAL_ERRORS];    // circular queue that overwrites the oldest data
            int lastErrorIndex;                         // circular queue index
            int lastError;                              // quick reference to the most recent proportional error
    };
}

#endif // INTPIDCONTROLLER_H
