#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include "darwin/framework/Point.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <darwin/framework/CameraPosition.h>

namespace Robot
{
    /*!
     * \brief A rectangular, orthogonally-aligned bounding box around an object in an image
     *
     * Stores the position and dimensions of the object as well as its compactness
     */
    class BoundingBox
    {
        public:
            BoundingBox();
            BoundingBox(const BoundingBox &src);

            /*!
             * \brief The center of the object in the frame in pixels
             *
             * X is measured horizontally from the left side, Y is measured vertically from the top
             */
            Point2D center;

            /*!
             * \brief The height of the object in pixels
             */
            int height;

            /*!
             * \brief The width of the object in pixels
             */
            int width;

            /*!
             * \brief The top of the bounding box
             * \return
             */
            int top(){return center.Y-height/2;}

            /*!
             * \brief The bottom of the bounding box
             * \return
             */
            int bottom(){return center.Y+height/2;}

            /*!
             * \brief The left of the bounding box
             * \return
             */
            int left(){return center.X-width/2;}

            /*!
             * \brief The right of the bounding box
             * \return
             */
            int right(){return center.X+width/2;}

            /*!
             * \brief The average Y-channel value of the object detected in the bounding box
             */
            int avgY;

            /*!
             * \brief The average U-channel value of the object detected in the bounding box
             */
            int avgU;

            /*!
             * \brief The average V-channel value of the object detected in the bounding box
             */
            int avgV;

            /*!
             * \brief A 0-1 value indicating how much of the bounding box the object fills
             *
             * 0.0 is completely empty, 1.0 is completely full.
             */
            double fill;

            /*!
             * \brief Draw the bounding box on an image
             * \param img The image to draw on
             * \param colour The colour to draw the box
             * \param thickness The thickness of the lines of the box to draw
             */
            void Draw(cv::Mat &img, cv::Scalar &colour, int thickness);

            /*!
             * \brief Print the bounding box's parameters to stdout
             */
            void Print();

            /*!
             * \brief Copy the parameters of one bounding box to this one
             * \param src The bounding box to copy
             * \return
             */
            BoundingBox& operator =(const BoundingBox &src);

            /*!
             * \brief Calculate the approximate range to this object in mm, assuming it is on the ground.
             *
             * NOTE: calculations are based on the MotionStatus::m_CurrentJoints, so of the robot is actively
             * moving there may be additional error due to the motors moving to their goal positions asynchronously.
             * \param heightOffGround The object's distance above ground level (currently unused)
             * \return The range to the object in mm (always positive) measured along the ground
             */
            double GetRange(double heightOffGround=0.0);

            /*!
             * \brief Calculat the approximage angle to this object in degrees, assuming it is on the ground.
             *
             * The angle is measured from a line drawn from the robot going directly forward.  A value of 0 degrees
             * indicates that the object is directly in front of the robot's line of travel.  Negative values indicate
             * that the object is to the robot's right, while positive angles indicate the object os to the robot's left.
             *
             * As with GetRange, the calculations of the robot's pose are based on MotionStatus::m_CurrentJoints, so if
             * the robot is moving there may be additional error as the motors move to their goal positions asynchronously.
             * \param heightOffGround The height of the object above the ground (currently unused)
             * \return The angle to the object in degrees, measured along the ground
             */
            double GetAngle(double heightOffGround=0.0);

            /*!
             * \brief Does this bounding box completely encompass another one?
             * \param other
             * \return
             */
            bool Contains(BoundingBox &other);

            /*!
             * \brief Does this bounding box contain the given point?
             * \param point
             * \return
             */
            bool ContainsPoint(Point2D &point);

            /*!
             * \brief Does this bounding box intersect with/overlap with another one?
             * \param other
             * \return
             */
            bool Intersects(BoundingBox &other);

            /*!
             * \brief Compare bounding boxes by size for sorting based on area
             * \param a
             * \param b
             * \return
             */
            static bool CompareBySize(void *a, void *b);

            /*!
             * \brief Comapre bounding boxes by position in left-to-right then top-to-bottom order for sorting
             * \param a
             * \param b
             * \return
             */
            static bool CompareByLocation(void *a, void *b);


        private:
            /*!
             * \brief The current position of the robot's camera in 3D space
             */
            static CameraPosition cameraPosition;
    };
}

#endif // BOUNDINGBOX_H
