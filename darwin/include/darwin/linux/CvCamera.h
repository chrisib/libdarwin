/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 *
 * Alternative to LinuxCamera and YuvCamera; this class uses V4L for the image acquisition
 * but stores recorded images in YUV, RGB, and HSV formats
 ************************************************************************/
#ifndef CVCAMERA_H
#define CVCAMERA_H

#include <linux/videodev2.h>
#include <sys/time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "darwin/framework/Image.h"
#include "darwin/framework/minIni.h"
#include "darwin/framework/CameraSettings.h"
#include "darwin/framework/CvPreprocessor.h"
#include <vector>

namespace Robot
{
    /*!
     * \brief OpenCV compatible interface for the robot's camera
     *
     * This is a static class because the DARwIn-OP only has a single camera.  Eventually
     * we should refactor this to support multiple instances as we could theoretically
     * support multiple cameras (e.g. stereo vision system)
     */
    class CvCamera{
    private:
        /*!
         * \brief The single instance of this class that can be instantiated
         */
        static CvCamera* uniqueInstance;

        /*!
         * \brief The file descriptor for the camera device
         */
        int camera_fd;

        /*!
         * \brief A buffer used to read raw data from the camera
         */
        struct buffer {
            /*!
             * \brief A pointer to the start of the buffer
             */
            void * start;

            /*!
             * \brief The length of the bufer
             */
            size_t length;
        };

        /*!
         * \brief Array of buffers used to read the raw camera data
         */
        struct buffer * buffers;

        /*!
         * \brief The number of buffers in the array
         */
        unsigned int n_buffers;

        /*!
         * \brief Exit the process if the camera encounters an error
         *
         * Terminates the process with code EXIT_FAILURE
         *
         * This has been a source of headaches since it means camera errors result in the robot
         * randomly stopping.  We should refactor to eliminate this, or at the very least
         * signals an abort so that the LinuxDARwIn's safe shutdown handlers will prevent
         * the robot falling on its face.
         * \param A string to display that explains the error
         */
        void ErrorExit(const char* s);

        /*!
         * \brief Wrapper for an ioctl call that will try repeatedly while ioctl returns -1
         * \param fh The file handler to read from
         * \param request The ioctl request parameter
         * \param arg The corresponding argument to go with the parameter
         * \return The result of the ioctl call
         */
        int xioctl(int fh, int request, void *arg);

        /*!
         * \brief Read a raw frame from the camera hardware and process it
         *
         * Will call ErrorExit if the camera encounters an unrecoverable error
         * \return Returns TRUE if everything was successful, otherwise returns FALSE.
         */
        int ReadFrame();

        /*!
         * \brief Process the raw frame from the camera into an OpenCV image
         *
         * The raw camera data is in YUYV format (assuming standard DARwIn-OP hardware), read into
         * an OpenCV matrix.
         *
         * After the frame is converted we apply the preprocessing filters specified by the camera
         *
         * \see ApplyPreprocessors
         *
         * \param p The raw image data
         * \param size The length of the image data
         */
        void ProcessImage(const void *p, int size);

        /*!
         * \brief Have we already printed out the warning that adjusting the exposure is not supported?
         */
        bool exposureNotSupportedWarningPrinted;

    protected:
        /*!
         * \brief Creates the RGB, YUV and HSV images, initializes default values for everything
         */
        CvCamera();

        /*!
         * \brief The camera's settings
         */
        CameraSettings settings;

        /*!
         * \brief A list of preprocessing filters that are applied in-order to each frame captured from the camera
         */
        std::vector<CvPreprocessor*> preprocessors;

        /*!
         * \brief Apply each of the preprocessing filters to the camera's current frame
         */
        void ApplyPreprocessors();  // apply flips & any preprocessor filters in the vector

        /*!
         * \brief Get a human-readible string indicating the name of a V4L control
         * \param control The V4L control number (e.g. V4L2_CID_EXPOSURE_ABSOLUTE)
         * \return A human-readible string indicating the name of the control (e.g. "Exposure (Absolute)")
         */
        std::string GetControlName(int control);

    public:
        /*!
         * \brief The raw YUV frame captured from the camera
         */
        cv::Mat yuvFrame;

        /*!
         * \brief The same image as yuvFrame, but converted to RGB
         *
         * Only used if RGB_ENABLE or HSV_ENABLE is true; otherwise this frame is ignored
         */
        cv::Mat rgbFrame;

        /*!
         * \brief The same image as yuvFrame, but converted to HSV
         *
         * Only used if HSV_ENABLE is true; implies RGB_ENABLE=true since to convert from YUV to HSV we go YUV --> RGB --> HSV
         */
        cv::Mat hsvFrame;

        /*!
         * \brief If true, we print additional debugging messages to stdout as the camera captures frames
         */
        bool DEBUG_PRINT;
        //FrameBuffer* fbuffer;

        /*!
         * \brief Destructor; cleans up all resources used by the camera
         *
         * Because the camera is a singleton class this ideally should never actually be called
         **/
        ~CvCamera();

        /*!
         * \brief Get the singleton instance of this class
         * \return
         */
        static CvCamera* GetInstance() { return uniqueInstance; }

        /*!
         * \brief Initialize the camera class for the given device
         * \param deviceIndex The index of the V4L device we are opening (e.g. 0 for /dev/video0)
         * \return True if the camera was successfully initialized, otherwise false
         */
        virtual int Initialize(int deviceIndex);

        /*!
         * \brief Shutdown the camera and release resources used by it
         */
        virtual void Release();

        /*!
         * \brief Get the value of a V4L control parameter
         * \param control The control whose value we want to get (e.g. V4L2_CID_EXPOSURE)
         * \return The value of the control or -1 if there was an error
         */
        virtual int v4l2GetControl(int control);

        /*!
         * \brief Set the value of a V4L control parameter
         * \param control The V4L control whose value we want to set (e.g. V4L2_CID_EXPOSURE)
         * \param value The value to set the control to
         * \return 0 if the control was set successfully, otherwise -1
         */
        virtual int v4l2SetControl(int control, int value);

        /*!
         * \brief Reset a V4L control to its default value
         * \param control The V4L control to reset (e.g. V4L2_CID_EXPOSURE)
         * \return 0 if the control was successfully reset, otherwise -1
         */
        virtual int v4l2ResetControl(int control);

        /*!
         * \brief Load the camera settings from a configuration file handler
         *
         * Data is read from a section called "Camera"
         * \param ini A pointer to a minIni instance that contains the data to read
         */
        virtual void LoadINISettings(minIni* ini);

        /*!
         * \brief Save the camera settings to a configuration file
         *
         * Data is written to a section called "Camera"
         * \param ini A pointer to a minIni instance that we will write to.
         */
        virtual void SaveINISettings(minIni* ini);

        /*!
         * \brief Set the camera's settings to the parameter's values
         * \param newset The new camera settings to apply
         */
        virtual void SetCameraSettings(const CameraSettings& newset);

        /*!
         * \brief Get the camera's current settings
         * \return The camera's current settings object
         */
        virtual const CameraSettings& GetCameraSettings();

        /*!
         * \brief Set the auto white balance parameter for the camera
         * \param isAuto A 0/1 flag indicating if auto white balance should be enabled
         */
        virtual void SetAutoWhiteBalance(int isAuto) { v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, isAuto); }

        /*!
         * \brief Get whether or not auto white balance is turned on for the camera
         * \return A true/false value indicating if auto white balance is enabled or not
         */
        virtual unsigned char GetAutoWhiteBalance() { return (unsigned char)(v4l2GetControl(V4L2_CID_AUTO_WHITE_BALANCE)); }

        /*!
         * \brief Capture and process a new frame from the camera
         * \return True if the frame was successfully captured, otherwise false
         */
        virtual bool CaptureFrame();

        /*!
         * \brief Remove all preprocessors from the camera
         */
        virtual void ClearPreprocessors() {preprocessors.clear();}

        /*!
         * \brief Add a preprocessor to the camera
         * \param processor The new preprocessor to add
         */
        virtual void AddPreprocessor(CvPreprocessor *processor){preprocessors.push_back(processor);}

        /*!
         * \brief Get the list of preprocessors added to the camera
         * \return The camera's preprocessors
         */
        virtual std::vector<CvPreprocessor*> *GetPreprocessors(){return &preprocessors;}

        /*!
         * \brief If true, all frames captured by the camera are converted to HSV
         */
        bool HSV_ENABLE;

        /*!
         * \brief If true, all frames captured by the camera are converted to RGB
         */
        bool RGB_ENABLE;
    };
}
#endif // CVCAMERA_H
