/************************************************************************
 * (c) University of Manitoba Autonomous Agents Laboratory
 * Chris Iverach-Brereton
 * Andres ??
 *
 * Alternative to LinuxCamera; this class uses OpenCV for the image acquisition
 * and uses the YUV colour space instead of RGB
 ************************************************************************/

#ifndef _YUV_CAMERA_H_
#define _YUV_CAMERA_H_

#include <stdlib.h>
#include <linux/videodev2.h>
#include <sys/time.h>
#include <opencv/cv.h>

#include "darwin/framework/Image.h"
#include "darwin/framework/minIni.h"
#include "darwin/framework/CameraSettings.h"
#include "darwin/framework/CvPreprocessor.h"
#include <vector>

namespace Robot
{
    /*!
     * \brief An old OpenCV-compatible camera class that should no longer be used.
     *
     * \see CvCamera
     */
    class YuvCamera
	{
	private:
        static YuvCamera* uniqueInstance;

        CameraSettings settings;

	    int camera_fd;
	    struct buffer {
	        void * start;
	        size_t length;
	    };
	    struct buffer * buffers;
	    unsigned int n_buffers;

        // CIB
        // optionally disable flipping the image along each axis
        bool flipV, flipH;

        YuvCamera();

        void ErrorExit(const char* s);
	    int ReadFrame();

        std::vector<CvPreprocessor*> preprocessors;

	protected:

	public:
		cv::Mat img;
		bool DEBUG_PRINT;
        FrameBuffer* fbuffer;

		~YuvCamera();

        static YuvCamera* GetInstance()  __attribute__((deprecated("Use CvCamera class instead"))) { return uniqueInstance; }

        int Initialize(int deviceIndex);

	    int v4l2GetControl(int control);
	    int v4l2SetControl(int control, int value);
	    int v4l2ResetControl(int control);

	    void LoadINISettings(minIni* ini);
	    void SaveINISettings(minIni* ini);

	    void SetCameraSettings(const CameraSettings& newset);
	    const CameraSettings& GetCameraSettings();

	    void SetAutoWhiteBalance(int isAuto) { v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, isAuto); }
	    unsigned char GetAutoWhiteBalance() { return (unsigned char)(v4l2GetControl(V4L2_CID_AUTO_WHITE_BALANCE)); }

        void SetFlipHorizontal(bool v){flipH = v;}
        void SetFlipVertical(bool v){flipV = v;}
        bool GetFlipHorizontal(){return flipH;}
        bool GetFlipVertical(){return flipV;}

	    void CaptureFrame();

        void ClearPreprocessors() {preprocessors.clear();}
        void AddPreprocessor(CvPreprocessor *processor){preprocessors.push_back(processor);}
	};
}

#endif
