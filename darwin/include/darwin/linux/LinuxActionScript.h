/*
 * LinuxActionScript.h
 *
 *  Created on: 2011. 1. 18.
 *      Author: zerom
 */

#ifndef LINUXACTIONSCRIPT_H_
#define LINUXACTIONSCRIPT_H_

#include <stdio.h>
#include <pthread.h>

namespace Robot
{
    /*!
     * \brief A class for playing coordinated actions and sound effects in a script
     *
     * The external application madplay is used to play the MP3s in a separate process; madplay must be installed,
     * otherwise "Fork Failed" messages will appear.
     *
     * This is part of Robotis' demo library and isn't commonly needed for any "real" robotics work
     */
    class LinuxActionScript
    {
    private:
        static const int LINE_BUFFERSIZE = 512;
        static pthread_t m_pthread_id;
        static pid_t mp3_pid;

        static char* SkipLeading(const char* str);
        static int ParseLine(const char* linebuffer, int* pagenumber, char* filepath);

        static void* ScriptThreadProc(void* data);

    public:
        static bool m_stop;
        static bool m_is_running;

        static int ScriptStart(const char* filename);
        static int PlayMP3Wait(const char* filename);
        static int PlayMP3(const char* filename);
    };
}

#endif /* LINUXACTIONSCRIPT_H_ */
