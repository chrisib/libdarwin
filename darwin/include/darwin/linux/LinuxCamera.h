/*
 *   LinuxCamera.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _LINUX_CAMERA_H_
#define _LINUX_CAMERA_H_

#include <stdlib.h>
#include <linux/videodev2.h>
#include <sys/time.h>

#include "darwin/framework/Image.h"
#include "darwin/framework/minIni.h"
#include "darwin/framework/CameraSettings.h"

namespace Robot
{
    /*!
     * \brief Robotis' default camera class
     *
     * This camera class uses the homebrew FrameBuffer object for storing image data
     * instead of OpenCV.  This can be good or bad.  Generally I prefer the CvCamera
     * class because of the compatibility with OpenCV.
     */
	class LinuxCamera
	{
	private:
        static LinuxCamera* uniqueInstance;

        CameraSettings settings;

	    int camera_fd;
	    struct buffer {
	        void * start;
	        size_t length;
	    };
	    struct buffer * buffers;
	    unsigned int n_buffers;

        LinuxCamera();

        void ErrorExit(const char* s);
	    int ReadFrame();

	protected:

	public:
		bool DEBUG_PRINT;
        FrameBuffer* fbuffer;

		~LinuxCamera();

        static LinuxCamera* GetInstance() { return uniqueInstance; }

        int Initialize(int deviceIndex);

	    int v4l2GetControl(int control);
	    int v4l2SetControl(int control, int value);
	    int v4l2ResetControl(int control);

	    void LoadINISettings(minIni* ini);
	    void SaveINISettings(minIni* ini);

	    void SetCameraSettings(const CameraSettings& newset);
	    const CameraSettings& GetCameraSettings();

	    void SetAutoWhiteBalance(int isAuto) { v4l2SetControl(V4L2_CID_AUTO_WHITE_BALANCE, isAuto); }
	    unsigned char GetAutoWhiteBalance() { return (unsigned char)(v4l2GetControl(V4L2_CID_AUTO_WHITE_BALANCE)); }

	    void CaptureFrame();
	};
}

#endif
