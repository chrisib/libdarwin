/*
 *   LinuxMotionTimer.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _LINUX_MOTION_MANAGER_H_
#define _LINUX_MOTION_MANAGER_H_

#include <pthread.h>
#include <signal.h>
#include "darwin/framework/MotionManager.h"

namespace Robot
{
    /*!
     * \brief Class responsible for timing the bursts of messages to/from the CM730.
     *
     * The DARwIn-OP architecture is designed to send/receive data from the CM730 at regular
     * 8ms intervals.  This class performs the underlying timing for that procedure
     */
	class LinuxMotionTimer
	{
	private:
		static timer_t m_TimerID;

		static MotionManager* m_Manager;
	    static bool m_TimerRunning;
	    static void TimerProc(int arg);

	protected:

	public:
		static void Initialize(MotionManager* manager);
		static void Start()			{ m_TimerRunning = true; }
		static void Stop()			{ m_TimerRunning = false; }
		static bool IsRunning()		{ return m_TimerRunning; }
	};
}

#endif
