/*
 *   LinuxDARwIn.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _LINUX_DARWIN_H_
#define _LINUX_DARWIN_H_

#include "DARwIn.h"
#include "darwin/linux/LinuxMotionTimer.h"

#ifndef DUMMY_CM730
	#include "darwin/linux/LinuxCM730.h"
#else
	#include "darwin/framework/DummyCM730.h"
#endif

#include "darwin/linux/LinuxCamera.h"
#include "darwin/linux/LinuxNetwork.h"
#include "darwin/linux/LinuxActionScript.h"

#ifndef DUMMY_CM730
#  define CM730DEVICE LinuxCM730
#else
#  define CM730DEVICE DummyCM730
#endif

/*!
 * \brief The Robot namespace contains all of the classes that make up the DARwIn-OP library
 * \authors Chris Iverach-Brereton, Joshua Jung, Simon Barber-Dueck, ROBOTIC Inc.
 */
namespace Robot
{
    /*!
     * \brief The underlying harware I/O interface for the robot when running on Linux
     *
     * This class provides basic initialization functions for the Darwin
     * most of this code is copied from demo applications that ship with the
     * Darwin.  Additional initialization may be required by your own application
     */
    class LinuxDARwIn
    {
        public:
            /*!
             * \brief The default TTY port used to communicate with the robot's CM730 subcontroller
             */
            static const char* DEFAULT_TTY;

            /*!
             * \brief The secondary/fallback TTY port used to communicate with the robot's CM730 subcontroller
             *
             * Only used if initialization fails when using DEFAULT_TTY
             */
            static const char* SECONDARY_TTY;

            /*!
             *\brief The OS-specific interface used to communicate with the robot's subcontroller
             */
            static CM730DEVICE linux_cm730;

            /*!
             * \brief The robot's subcontroller
             */
            static CM730 cm730;

            /*!
             * \brief Change the current process' working directory to /proc/self/exe
             *
             * Not actually used internally anywhere in the library, but left here has a holdover from
             * Robotis' original library for backwards-compatibility purposes.
             */
            static void ChangeCurrentDir();

            /*!
             * \brief Register a new signal handler function to handle a specific signal
             * \param signal The signal the handler will handle
             * \param handler The function to invoke when the signal triggers
             *
             * Effectively just a wrapper for the signal() system function.  Not 100% sure why this is even here,
             * but likely legacy nonsense that should be removed eventually.
             */
            static void AddSignalHandler(int signal, void (*handler)(int)) __attribute__((deprecated));

            /*!
             * \brief Initialize the CM730 subcontroller
             *
             * The CM730 is connected over a serial interface to the robot's main controller.  Initialization is
             * first attempted on DEFAULT_TTY, falling back to SECONDARY_TTY before giving up.
             *
             * \return True if the hardware was successfully initialized, otherwise false
             */
            static bool InitCM730();

            /*!
             * \brief Initialize the robot using default parameters
             *
             * This function will initialize the CM730 and the LinuxMotionTimer, but nothing else;
             * the Action module will not be loaded nor will the safePosition variable be set.
             *
             * \return True if the robot was successfully initialized, otherwise false
             */
            static bool Initialize();

            /*!
             * \brief Initialize the robot specifying a motion file and a default safe motion
             *
             * This function will, in order:
             * 1- set the safePosition variable
             * 2- initalize the CM730
             * 3- connect the SegfaultHandler function to the SIGSEGV (if the library is compiled with the STACK_TRACE definition)
             * 4- Add the Action module to the MotionManager
             * 5- Initialize the LinuxMotionTimer
             * 6- Load the motion file into the Action module
             * 7- Enter the robot's safe position
             * 8- return true
             *
             * \param motionFilePath The path to a motion file to load that contains keyframe poses for the robot
             * \param initialMotion The index of the safe/default pose of the robot (usually a stable kneeling or standing position)
             * \return True if initialization was successful, otherwise false
             */
            static bool Initialize(const char* motionFilePath, int initialMotion);

            /*!
             * \brief Connect the SignalHandler function to the SIGABRT, SIGTERM, SIGQUIT and SIGINT signals
             * to facilitate clean shutdowns
             */
            static void InitializeSignals();

            /*!
             * \brief Check the firmware version of every servo and make sure that the servo version matches
             * the resolution this library was compiled to support.
             *
             * Each motor is assumed to be an MX-28 (or RX-28M -- they're the same motors with different branding)
             * unless specifically set otherwise.  Other supported motors are the RX-28 and AX-12 servos.
             *
             * \see JointData
             * \see AX12::IsAX12
             * \see RX28::IsRX28
             *
             * \return True if all the firmware is OK, otherwise false
             */
            static bool CheckFirmware();

            /*!
             * \brief Use the Action module to have the robot enter a known stable, safe position.
             *
             * If the robot was initialized using the Initialize() function the safe position will not
             * be set and this function will do nothing.
             *
             * \see safePosition
             */
            static void EnterSafePosition();

        private:
            /*!
             * \brief The default position the robot considers "safe"
             *
             * The robot will assume this position upon initialization.
             * This position should normally be a kneeling position with the arms in a neutral position.
             *
             * \see Action
             * \see EnterSafePosition
             */
            static int safePosition;

            /*!
             * \brief Handler for the terminate/kill/abort signals
             *
             * Will force a clean shutdown of the system to cleanly shut down the peripheral hardware and re-assume the robot's safe position.
             * \param sig The signal that the handler is handling
             *
             * \see EnterSafePosition
             */
            static void SignalHandler(int sig);

            /*!
             * \brief Handler for segmentation faults.
             *
             * Does its best to shutdown cleanly (no guarantees any of that will work since it's a segfault) and optionally
             * prints out a stack trace if the library is compiled with the BACK_TRACE flag defined (i.e. -DBACK_TRACE)
             * \param sig The signal that the handler is handling
             */
            static void SegfaultHandler(int sig);
    };
}

#endif
